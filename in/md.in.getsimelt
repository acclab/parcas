Input file for PARCAS MD - Au overheated above melting point

Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

# For 254282 Cu atoms, 64 processors and few output steps, one can do about 
# 6.8 ps in one hour on the Cray T3D. In 40 hours, 270 ps should be just 
# achievable.

nsteps   = 99999999      Max number of time steps
tmax     = 150000.0      Maximum time in fs 
restartt = 100000.0       Restart time in fs

seed     = 291277        Seed for random number generator

Time step criteria and related parameters. Internal time unit
is 10.1805*sqrt(mass) = 81.15 fs for Cu
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
mass     = 63.546        Atom mass in u (needed in time step calculation)

Simulation cell
---------------

latflag  = 3             Lattice flag (0=gen fcc, 1=read from mdlat.in)
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.25          Neighbour list skin thickness ratio, use ~ 1.15 

natoms   = 4096          Number of atoms in simulation
box(1)   = 61.801        Box size in the X-dir (Angstroms, sigma for LJ)
box(2)   = 29.760        Box size in the Y-dir (Angstroms, sigma for LJ)
box(3)   = 29.888        Box size in the Z-dir (Angstroms, sigma for LJ)
ncell(1) = 16            Number of unit cells along X-direction
ncell(2) = 8             Number of unit cells along Y-direction
ncell(3) = 8             Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 1.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------
 
mtemp    = 1             Temp. control (0 none,1=linear,4=set,5=borders)
temp0    = 0.0           Initial T for mtemp=6
ntimeini = 300           Time steps at temp0 before quench
initemp  = 0.0           Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 1211.35       Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 100.0         Berendsen temperature control tau (fs)
trate    = 5.0           Quench rate for mtemp=6 (K/fs)
tmodtau  = 30.0          Factor for determining melting point, if 0 not used

1/B should be 7.3d-4 (Cu), 3.6d-4 (Pt), 5.8d-4 (Au), 1.3d-3 (Al), 1.0d-3 (Si)

bpcbeta  = 7.3d-4        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 100.0         Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)

tscaleth = 4.08          Min. thickness of border region at which T is scaled 

damp     = 0.00000       Damping factor
amp      = 0.0000        Amplitude of initial displacement (Angstroms)

ndump    = 5             Print data every ndump steps
nmovie   = 1400          Number of steps btwn writing to md.movie (0=no movie)
ndefmovie= 1000          Number of steps btwn writing to defects.out 
nrestart = 9805          Number of steps between restart output
nliqanal = 1000          Nsteps between liquid analysis; works in parallell !

nintanal = 1000          Number of steps between interst. analysis (not par.)

Ekdef    = 0.40           Ekin threshold for labeling an atom a defect/liquid


Recoil calculation definitions
------------------------------

irec     = 0 -1          Index of recoiling atom
xrec     = -5.0          Desired initial position
yrec     = -5.0    
zrec     = 5.0    

recen    = 0.001        Initial recoil energy in eV
rectheta = 78.0          Initial recoil direction in degrees
recphi   = 35.0          Initial recoil direction in degrees











