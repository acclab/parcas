Input file for PARCAS MD 
 
Free format input file - parameters can be in any order, and comments
can be placed freely among them, as long as the parameters hold the
9-characters + "="-sign format. Each number must also be followed
by at least one space (not a tab !).

debug    = 0             Debug bitwise: 1 general

nsteps   = 99999999      Max number of time steps
tmax     = 80000.0       Maximum time in fs 
restartt = 0.0
endtemp  = 0.0           Ending T: end if T < endtemp

seed     = 723762        Seed for random number generator

Atom type-dependent parameters. 
Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al
-------------------------------------------------------------------

delta    = 0.05          Max time step (in MD units of SQRT(M*L^2/E))
timeEt   = 300.0  
ntype    = 2             Number of atom types
mass(0)  = 131.29        Atom mass in u
name(0)  = Xe            Name of atom type
mass(1)  = 196.97        Atom mass in u
name(1)  = Au            Name of atom type
substrate= Au            Substrate name, for use in elstop file name

iac(0,0) = -1            Interaction types: -1 kill 0 none 1 potmode 2 pair
iac(0,1) = 2               3 create EAM cross potential
iac(1,1) = 1             - automatic symmetry used: iac(1,0) = iac(0,1) 
potmode  = 2             0 LJ; 1 EAM; 2 EAM, direct Epair; 3-4 SW
spline   = 0  

reppotcut= 10.0          Reppotcut for SW and Tersoff: use 0 for no reppot

Simulation cell
---------------

latflag  = 0             Lattice: 0 FCC 1 mdlat.in 2 DIA 3-4 Restart 5 Read bas
nprtbl   = 100000        Number of steps between pair table calculations
neiskinr = 1.20          Neighbour list skin thickness, Si ~1.15, EAM ~ 1.2

natoms   = 1327104       Number of atoms in simulation
box(1)   = 293.76       Box size in the X-dir (Angstroms)
box(2)   = 293.76        Box size in the Y-dir (Angstroms)
box(3)   = 261.12       Box size in the Z-dir (Angstroms)
ncell(1) = 72            Number of unit cells along X-direction
ncell(2) = 72            Number of unit cells along Y-direction
ncell(3) = 64            Number of unit cells along Z-direction
pb(1)    = 1.0           Periodicity in X-direction (1=per, 0=non)
pb(2)    = 1.0           Periodicity in Y-direction (1=per, 0=non)
pb(3)    = 0.0           Periodicity in Z-direction (1=per, 0=non)

Simulation
----------

mtemp    = 8             Temp. control (0 none,1=linear,4=set,5/7=borders)
temp0    = 0.0           Initial T for mtemp=6
timeini  = 75000.0       Time at temp0 before quench
ntimeini = 99991000      Time steps at temp0 before quench
initemp  = 0.0           Initial temp: > 0 Gaussian, < 0 same v for all
temp     = 0.0           Desired temperature (Kelvin)
toll     = 0.0           Tolerance for the temperature control
btctau   = 100.0         Berendsen temp. control tau (fs), if 0 not used
trate    = 0.01          Quench rate for mtemp=6/8 (K/fs)

Debye T is 394 Al, 375 Ni, 315 Cu, 230 Pt, 170 Au, 625 Si, 360 Ge

tdebye   = 315.0         Debye temperature for displacements, overrides amp
amp      = 0.000000      Amplitude of initial displacement (Angstroms)

1/B: 7.3d-4 Cu, 5.3d-4 Ni, 3.6d-4 Pt, 5.8d-4 Au, 1.3d-3 Al, 1.0d-3 Si, 
     1.2d-3 Ge

bpcbeta  = 1.0d-3        Berendsen pressure control beta, 1/B (1/kbar)
bpctau   = 0.0           Berendsen pressure control time const. tau (fs)
bpcP0    = 0.0           Berendsen pressure control desired P (kbar)
bpcP0z   = 0.0           Berendsen pres. control. desired P_z (kbar)

tscaleth = 6.12          Min. thickness of border region at which T is scaled 
tscalzmin= 123.32        Scale 6 bottom layers 
tscalzmax= 130.56  

Emaxbrdr = 20.0          Maximum allowed kinetic energy for border atoms

damp     = 0.00000       Damping factor

ndump    = 5             Print data every ndump steps
nmovie   = 99500         Number of steps btwn writing to md.movie (0=no movie)
dtmov(1) = 500.0         Time interval for movie output before tmov(1) (fs)
tmov(1)  = 1000.0        Time at which to change output interval (fs)
dtmov(2) = 2000.0        Time interval for movie output before tmov(1) (fs)
tmov(2)  = 5000.0        Time at which to change output interval (fs)
dtmov(3) = 5000.0        Time interval for movie output for rest of run (fs)
tmov(3)  = 30000.0       Time at which to change output interval (fs)
dtmov(4) = 10000.0       Time interval for movie output for rest of run (fs)

nliqanal = 200           Nsteps between liquid analysis; works in parallell !
ndefmovie= 200           Number of steps between writing pressures.out
nrestart = 1000          Number of steps between restart output

Ekdef    = 0.144         Ekin threshold for labeling an atom defect/liquid 


Recoil calculation definitions
------------------------------

irec     = -2             Index of recoiling atom (-1 closest, -2 create)
recatype = 0              Recoil atom type
xrec     = -5.6           Desired initial position
yrec     = -4.7    
zrec     = -134.0

recen    = 100000.0      Initial recoil energy in eV
rectheta = 25.0          Initial recoil direction in degrees
recphi   = 25.0          Initial recoil direction in degrees

melstop  = 4             Mode for elstop: 0 none, 1 recoil, 2 for all, 3 Ek>10
elstopmin= 5.0 

sputlim  = 0.7           Sputtering limit, in internal length units




Lattice structure for latflag=5
--------------------------------

# This one here is for 111 FCC
# if a0 is the conventional fcc lattice constant, 111 FCC has
# the unit cell a=a0/sqrt(2), b=a0*sqrt(3/2), c=a0*sqrt(3)
# If a0=4.08 a=2.884995667 b=4.9969590752 c=7.066767294


nbasis   = 6

offset(1)= 0.25
offset(2)= 0.0833333
offset(3)= 0.1666667

lx(1,1)  = 0.0  
lx(1,2)  = 0.0  
lx(1,3)  = 0.0  
ltype(1) = 1

lx(2,1)  = 0.5 
lx(2,2)  = 0.5 
lx(2,3)  = 0.0 
ltype(2) = 1

lx(3,1)  = 0.0 
lx(3,2)  = 0.3333333 
lx(3,3)  = 0.3333333 
ltype(3) = 1

lx(4,1)  = 0.5 
lx(4,2)  = 0.8333333 
lx(4,3)  = 0.3333333 
ltype(4) = 1

lx(5,1)  = 0.5 
lx(5,2)  = 0.1666667 
lx(5,3)  = 0.6666667 
ltype(5) = 1

lx(6,1)  = 0.0 
lx(6,2)  = 0.6666667 
lx(6,3)  = 0.6666667 
ltype(6) = 1
















