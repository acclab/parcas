/*
 * parcasbinout_to_vtk.c
 *
 *  Created on: 28.8.2012
 *      Author: dlandau
 */


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "input.h"
#include "types.h"
#include "helpers.h"
#include "output.h"
#include "filter.h"


static int pos2ind_1d(double pos, double size, int ncell) {
  return (int)((pos / size + .5) * ncell);
}


/* Suggested optimizations:
 * - Use ImageDate instead of RectilinearGrid
 * - Parallelize. This should be fairly trivial, since the parallel VTK format has a single
 *   "index" file and separate data files that are the same kind as the serial file that
 *   this program writes.
 * - Write the data in binary instead of ascii. Again this should be trivial, since you
 *   probably need to just change "ascii" to "binary" or something similar (didn't check
 *   what exactly) and change fprintf("%f") to fwrite(). 
 * Other improvements:
 * - Add output of other values, e.g. temperature (calculated from Ek) etc.
 * - Maybe make wanted_spacing a runtime parameter and it should probably be a multiple
 *   of the lattice constant to avoid the striped nature of the current output. 
 *
 *   -- Daniel Landau*/

int main(int argc, char **argv){
  struct fixed_header fhdr;
  struct variable_header vhdr;
  struct configuration conf;

  conf.printproc=-1; // which processor info to print, -1 means all
  parse_command_line(argc, argv, &conf);


  int k;
  for (k = optind; k < argc; k++){ // optind is from getopt, start after options
    conf.fname = argv[k];
    FILE * fp = try_fopen(conf.fname, "rb");
    FILE * out_fp;

    char * new_file_name = (char *) malloc(strlen(conf.fname) + 4 + 1);
    strcpy(new_file_name, conf.fname);

    out_fp = try_fopen(strcat(new_file_name,".vtr"), "w+");
    free(new_file_name);

    fprintf(stderr,
            "=========FILE %s=========\n", conf.fname);


    /* Read in the static part of the header. It is 108 bytes in length. */
    read_fixed_header(fp, &fhdr, conf.progname, conf.fname);

    /* allocate arrays for variable length header and calculated values */
    allocate_variable_header(&fhdr, &vhdr);

    /* Read in the variable length header */
    read_fields_info(&fhdr, &vhdr, fp);
    read_types_info(&fhdr, &vhdr, fp);
    read_procs_info(&fhdr, &vhdr, fp);

    /*
     * The actual header is now parsed. Check the offsets and continue
     */
    check_offsets(&fhdr, fp, argv, conf.fname);

    /* Calculate all offsets */
    calculate_offsets(&vhdr, &fhdr);


    /* VTK XML output */
    /* For now this uses the type RectilinearGrid.
     * Unless we start optimizing for empty space etc.
     * this is stupid and ImageData should be used instead.
     * This shouldn't be a huge modification to make. -- Daniel */
    fprintf(out_fp, "<?xml version=\"1.0\"?>\n"
            "<VTKFile type=\"RectilinearGrid\" byte_order=\"LittleEndian\">\n");


    /* The following (especially the 2 in the z direction) is specific to my itch,
     * since I want to animate an x-y periodic system with bombardment
     * from the upper z direction -- Daniel Landau */
    double wanted_spacing = 10.0;

    int cells[3] = { (int)(fhdr.box_x / wanted_spacing),
            (int)(fhdr.box_y / wanted_spacing) ,
            (int)(2*fhdr.box_z / wanted_spacing) };
    double cell_sizes[3] = { (fhdr.box_x / cells[0]),
            (fhdr.box_y / cells[1]),
            (2*fhdr.box_z / cells[2]) };
    fprintf(out_fp, "  <RectilinearGrid WholeExtent=\"%d %d %d %d %d %d\">\n",
            0, cells[0]-1,
            0, cells[1]-1,
            0, cells[2]-1);



    double * density = try_malloc(sizeof(double) * cells[0]* cells[1] * cells[2], conf.progname, "density array malloc");
    int i;
    for (i = 0; i < cells[0]*cells[1]*cells[2]; i++)
      density[i] = 0.0;

    int start, end;
    start = conf.printproc < 0 ? 0         : conf.printproc;
    end   = conf.printproc < 0 ? fhdr.cpus : conf.printproc + 1;

    for (i = start; i < end; i++) {
      int file_atomsize = (3 + fhdr.realsize / 4 * (3 + fhdr.n_fields)) * sizeof(uint32_t);
      int count = vhdr.atomsperproc[i];

      uint8_t *buffer = (uint8_t*)try_malloc(count * file_atomsize, conf.progname, "Couldn't allocate buffer for reading one processes atoms.");
      struct atom atom;
      atom.fields = (double *)try_malloc((3 + fhdr.n_fields) * sizeof(double), conf.progname,
              "Couldn't allocate single atom fields.");

      try_fseek(vhdr.file_offsets[i], fp, &conf);
      try_fread(buffer, file_atomsize, count, fp, &conf);

      uint8_t * ptr = buffer;

      int j;
      for (j = 0; j < count; j++) {
        parse_atom(&conf, &fhdr, &atom, &ptr, j);
        double positions[3] = { atom.fields[0], atom.fields[1],  atom.fields[2] };
        int x_ind = pos2ind_1d(positions[0],fhdr.box_x,cells[0]);
        int y_ind = pos2ind_1d(positions[1],fhdr.box_y,cells[1]);
        int z_ind = pos2ind_1d(positions[2],fhdr.box_z,cells[2] / 2);
        int density_index = z_ind * cells[1]*cells[0] + y_ind * cells[0] + x_ind;
        if (0 <= density_index &&  density_index < cells[2]*cells[1]*cells[0])
          density[z_ind * cells[1]*cells[0]
                + y_ind * cells[0]
                + x_ind] += 1 / (cell_sizes[0]*cell_sizes[1]*cell_sizes[2]);
        else
/*            fprintf(stderr,"Atom outside acceptable bounds:\n"
                    "x_ind: %d, y_ind: %d, z_ind: %d\n"
                    "Positions: x: %f y: %f z: %f\n"
                    "Cells[0]: %d [1]: %d [2]: %d\n",
                    x_ind, y_ind, z_ind,
                    positions[0], positions[1], positions[2],
                    cells[0], cells[1], cells[2])*/;
      }

      free(atom.fields);
      free(buffer);
    }
    fprintf(out_fp, "    <Piece Extent=\"%d %d %d %d %d %d\">\n",
            0, cells[0]-1,
            0, cells[1]-1,
            0, cells[2]-1);

    fprintf(out_fp, "     <Coordinates>\n");

    fprintf(out_fp, " <DataArray type=\"Float32\" Name=\"x_coordinate\" format=\"ascii\">\n");
    for (i = 0; i < cells[0]; i++) {
      fprintf(out_fp, "%f ", i* (fhdr.box_x / cells[0]) );
    }
    fprintf(out_fp, "\n</DataArray>\n");

    fprintf(out_fp, " <DataArray type=\"Float32\" Name=\"y_coordinate\" format=\"ascii\">\n");
    for (i = 0; i < cells[1]; i++) {
      fprintf(out_fp, "%f ", i* (fhdr.box_y / cells[1]) );
    }
    fprintf(out_fp, "\n</DataArray>\n");

    fprintf(out_fp, " <DataArray type=\"Float32\" Name=\"z_coordinate\" format=\"ascii\">\n");
    for (i = 0; i < cells[2]; i++) {
      fprintf(out_fp, "%f ", i* (fhdr.box_z / cells[2]) );
    }
    fprintf(out_fp, "\n</DataArray>\n");

    fprintf(out_fp, "     </Coordinates>\n");

    fprintf(out_fp, "      <PointData Scalars=\"Density\">\n");
    fprintf(out_fp, " <DataArray type=\"Float32\" Name=\"Density\" format=\"ascii\">\n");

    for (i = 0; i < cells[0] * cells[1] * cells[2]; i++)
      fprintf(out_fp, "%f ", density[i]);
    fprintf(out_fp, "\n");


    fprintf(out_fp, " </DataArray>\n"
            "      </PointData>\n"
            "    </Piece>\n"
            "  </RectilinearGrid>\n"
            "\n"
            "</VTKFile>\n");

    free_variable_header(&fhdr, &vhdr);
    fclose(fp);
    fclose(out_fp);

  }

  return 0;
}
