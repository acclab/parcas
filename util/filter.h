/*
 * filter.h
 *
 *  Created on: 10.7.2012
 *      Author: dlandau
 */

#ifndef FILTER_H_
#define FILTER_H_
#include <stdint.h>

void run_filters(struct atom * atoms, struct variable_header * vhdr,
        struct fixed_header * fhdr, struct configuration * conf,
        int cpu, int ** kept_indices, int *n_kept);
#endif /* FILTER_H_ */
