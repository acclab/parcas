/*
 * input.c
 *
 *  Created on: 3.7.2012
 *      Author: dlandau
 */

#include "input.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

int32_t get_int32(uint8_t** ptr, enum byte_order file_order) {
  int32_t i;
  uint8_t * tmp = *ptr;
  if (file_order == BO_LITTLE_ENDIAN)
    i = (int32_t)(tmp[0]<<0) | (tmp[1]<<8) | (tmp[2]<<16) | (tmp[3]<<24);
  else // (file_order == BO_BIG_ENDIAN)
    i = (int32_t)(tmp[3]<<0) | (tmp[2]<<8) | (tmp[1]<<16) | (tmp[0]<<24);
  *ptr += sizeof(i);
  return i;
}

int64_t get_int64(uint8_t** ptr, enum byte_order file_order) {
  int64_t i = 0x0000000000000000; // Start from zero.
  int j;
  int64_t tmp;
  uint8_t * bytes = *ptr;
  for (j = 0; j < 8; j++) {
    tmp &= 0x0000000000000000; // Zero it just in case.
    if (file_order == BO_LITTLE_ENDIAN)
      tmp = bytes[j];
    else // (file_order == BO_BIG_ENDIAN)
      tmp = bytes[8 - j - 1];
    i = i | (tmp << (j * 8));
  }
  *ptr += sizeof(i);
  return i;
}

float get_real32(uint8_t** ptr, enum byte_order file_order) {
  int32_t i;
  float r;
  i = get_int32(ptr, file_order);
  memcpy(&r, &i, sizeof(r));
  return r;
}

double get_real64(uint8_t** ptr, enum byte_order file_order) {
  int64_t i;
  double r;
  i = get_int64(ptr, file_order);
  memcpy(&r, &i, sizeof(r));
  return r;
}


enum byte_order get_endianness_from_prot_real(uint8_t * ptr) {
  if (ptr[0] == 0x3f && ptr[1] == 0x30 && ptr[2] == 0x20 && ptr[3] == 0x10)
    return BO_BIG_ENDIAN;
  else if (ptr[3] == 0x3f && ptr[2] == 0x30 && ptr[1] == 0x20 && ptr[0] == 0x10)
    return BO_LITTLE_ENDIAN;
  else
    return BO_UNKNOWN;
}

void my_read(void* buf, size_t size, FILE* fp) {
  if (fread(buf, size, 1, fp) != 1) {
    perror("fread()");
    exit(1);
  }
}

void read_fixed_header(FILE* fp, struct fixed_header* fhdr,
        char* progname, char* fname) {
  /* Read in the static part of the header. It is 108 bytes in length. */
  uint8_t* buffer = (uint8_t *)malloc(108);
  my_read(buffer, 108, fp);
  uint8_t* ptr = buffer;
  fhdr->file_order = get_endianness_from_prot_real(ptr);
  if (fhdr->file_order == BO_UNKNOWN) {
    fprintf(stderr, "%s: %s: File seems to be middle endian. Not supported.\n",
            progname, fname);
    exit(1);
  }
  fhdr->prot_real = get_int32(&ptr, fhdr->file_order);
  fhdr->prot_int = get_int32(&ptr, fhdr->file_order);
  fhdr->fileversion = get_int32(&ptr, fhdr->file_order);
  fhdr->realsize = get_int32(&ptr, fhdr->file_order);
  fhdr->desc_off = get_int64(&ptr, fhdr->file_order);
  fhdr->atom_off = get_int64(&ptr, fhdr->file_order);
  fhdr->frame_num = get_int32(&ptr, fhdr->file_order);
  fhdr->part_num = get_int32(&ptr, fhdr->file_order);
  fhdr->total_parts = get_int32(&ptr, fhdr->file_order);
  fhdr->n_fields = get_int32(&ptr, fhdr->file_order);
  fhdr->natoms = get_int64(&ptr, fhdr->file_order);
  fhdr->mintype = get_int32(&ptr, fhdr->file_order);
  fhdr->maxtype = get_int32(&ptr, fhdr->file_order);
  fhdr->cpus = get_int32(&ptr, fhdr->file_order);
  fhdr->simu_time = get_real64(&ptr, fhdr->file_order);
  fhdr->timescale = get_real64(&ptr, fhdr->file_order);
  fhdr->box_x = get_real64(&ptr, fhdr->file_order);
  fhdr->box_y = get_real64(&ptr, fhdr->file_order);
  fhdr->box_z = get_real64(&ptr, fhdr->file_order);
  assert(ptr - buffer == 108);
  /* Do some sanity checking for the fixed header before continuing. */
  if (fhdr->realsize != 4 && fhdr->realsize != 8) {
    fprintf(stderr, "%s: %s: bad realsize (%d) "
            "(should be either 4 or 8)", progname, fname,
            fhdr->realsize);
    exit(1);
  }
  free(buffer);
}

int read_fields_info(const struct fixed_header* fhdr,
        const struct variable_header* vhdr, FILE* fp) {
  /* Read in the variable length header */
  int i;
  for (i = 0; i < fhdr->n_fields; i++) {
    my_read(vhdr->field_names[i], 4, fp);
    vhdr->field_names[i][4] = '\0';
    my_read(vhdr->field_units[i], 4, fp);
    vhdr->field_units[i][4] = '\0';
  }
  return i;
}

int read_types_info(const struct fixed_header* fhdr,
        const struct variable_header* vhdr, FILE* fp) {
  int i;
  for (i = fhdr->mintype; i <= fhdr->maxtype; i++) {
    my_read(vhdr->types[i - fhdr->mintype], 4, fp);
    vhdr->types[i - fhdr->mintype][4] = '\0';
  }
  return i;
}

void read_procs_info(const struct fixed_header* fhdr,
        const struct variable_header* vhdr, FILE* fp) {
  int i;
  uint8_t *ptr, *buffer;
  buffer = (uint8_t*)malloc(52);
  for (i = 0; i < fhdr->cpus; i++) {
    int j;
    my_read(buffer, 52, fp);
    ptr = buffer;
    vhdr->atomsperproc[i] = get_int32(&ptr, fhdr->file_order);
    for (j = 0; j < 6; j++)
      vhdr->boxes[i][j] = get_real64(&ptr, fhdr->file_order);
  }
  free(buffer);
}

void parse_atom(const struct configuration* conf,
        const struct fixed_header* fhdr, struct atom* atom,
        uint8_t** ptr, int iteration) {
  int f;
  atom->ind = get_int64(ptr, fhdr->file_order);
  atom->type = get_int32(ptr, fhdr->file_order);
  for (f = 0; f < fhdr->n_fields + 3; f++) {
    atom->fields[f] = (
            fhdr->realsize == 4 ?
                    (double) get_real32(ptr, fhdr->file_order) :
                    get_real64(ptr, fhdr->file_order));
  }

  if (abs(atom->type) - fhdr->mintype < 0 || abs(atom->type) > fhdr->maxtype) {
    fprintf(stderr, "%s: %s: type index out of bounds: %d %d\n",
            conf->progname, conf->fname, iteration, atom->type);
    exit(1);
  }
}


void try_fseek(off_t file_offset, FILE* fp,
        struct configuration* conf) {
  if (fseek(fp, file_offset, SEEK_SET) != 0) {
    fprintf(stderr, "%s: %s: Couldn't seek to correct place.\n", conf->progname,
            conf->fname);
    exit(1);
  }
}

void try_fread(uint8_t* buffer, int file_atomsize, int count, FILE* fp,
        struct configuration* conf) {
  int read_elements = fread(buffer, file_atomsize, count, fp);
  if (read_elements != count) {
    fprintf(stderr, "%s: %s: couldn't read atoms according to header: %d %d\n",
            conf->progname, conf->fname, read_elements, count);
    exit(1);
  }
}

FILE* try_fopen(char * fname, const char * mode) {
  FILE * fp = fopen(fname, mode);
  if (fp == NULL ) {
    printf("failed to open file %s\n", fname);
    exit(1);
  }
  return fp;
}

/* Calculate all offsets */
void calculate_offsets(const struct variable_header* vhdr,
        const struct fixed_header* fhdr) {
  int i;
  vhdr->file_offsets[0] = (off_t) fhdr->atom_off;
  for (i = 1; i < fhdr->cpus; i++) {
    vhdr->file_offsets[i] = vhdr->file_offsets[i - 1]
            + (off_t) ((3 + fhdr->realsize / 4 * (3 + fhdr->n_fields))
                    * sizeof(uint32_t) * vhdr->atomsperproc[i - 1]);
  }
}
