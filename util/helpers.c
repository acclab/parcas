#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <unistd.h>
#include <getopt.h>

#include "helpers.h"
#include "types.h"


void allocate_variable_header(const struct fixed_header* fhdr,
        struct variable_header* vhdr) {
  /* allocate arrays */
  vhdr->field_names = (char**)malloc_2d(fhdr->n_fields, 5, sizeof(char));
  vhdr->field_units = (char**)malloc_2d(fhdr->n_fields, 5, sizeof(char));
  vhdr->types = (char **) malloc_2d(fhdr->maxtype - fhdr->mintype + 1, 5, sizeof(char));
  vhdr->boxes = (double **) malloc_2d(fhdr->cpus, 6, sizeof(double));
  vhdr->atomsperproc = (int *)malloc(sizeof(int) * fhdr->cpus);
  vhdr->file_offsets = (off_t*) malloc(sizeof(off_t)*fhdr->cpus);

  if (vhdr->atomsperproc == NULL || vhdr->field_names == NULL
          || vhdr->field_units == NULL || vhdr->types == NULL
          || vhdr->boxes == NULL ) {
    fprintf(stderr,
            "Couldn't allocate one of vhdr.atomsperproc, vhdr.field_names, vhdr.field_units, vhdr.types, or vhdr.boxes\n");
    exit(1);
  }
}
void free_variable_header(const struct fixed_header* fhdr,
        struct variable_header* vhdr) {
  free_2d((void**)vhdr->types);
  free_2d((void**)vhdr->field_names);
  free_2d((void**)vhdr->field_units);
  free_2d((void**)vhdr->boxes);
  free(vhdr->atomsperproc);
  free(vhdr->file_offsets);
}

void ** malloc_2d(int rows, int columns, int datasize) {
  int i;
  uint8_t ** array;
  array = (uint8_t **) malloc(rows * sizeof(void *));
  array[0] = (uint8_t *) malloc(datasize * rows * columns);
  for (i = 1; i < rows; i++) {
    array[i] = array[i-1] + columns*datasize;
  }
  return (void**)array;
}

void free_2d(void ** array) {
  free(*array);
  free(array);
}

void get_and_dump_description( const struct fixed_header * const fhdr_in,
        FILE * fp, FILE * out_fp) {
  off_t file_off;
  struct fixed_header fhdr = *fhdr_in;
  int c;

  fprintf(out_fp, "------description------\n");
  file_off = (off_t) fhdr.desc_off;
  fseek(fp, file_off, SEEK_SET);
  if (fseek(fp, file_off, SEEK_SET) != 0) {
    perror("fseek()");
    exit(1);
  }
  /*
   * Write out description bytes until null character or start
   * of atom data.
   */
  while (file_off++ < fhdr.atom_off) {
    c = fgetc(fp);
    if (c < 0) {
      perror("fgetc()");
      exit(1);
    }
    if (c == '\0')
      break;

    fprintf(stderr,"Description bytes in binary file are not supported properly.\n"
            "If you know what you are doing, remove this error message and exit(1).\n");
    exit(1);
    putc(c, out_fp);
  }
  fprintf(out_fp, "------atom records------\n");
}

void
usage(const char *progname)
{
  fprintf(stderr,
          "Usage: %s [options] binfiles ...\n"
          "\n"
          "Possible options are:\n"
          "  -h|--help              Show this help and exit.\n"
          "  -x|--xyz               Write output in XYZ format.\n"
          "  -p|--proc [procnum]    Only print out data from process procnum\n"
          "  -F|--filter [filter]   Filter the atoms, [filter] is one of {\"top [nlayers]\", \"recoil [rsnum]\", fixed, none}.\n"
          "  -f|--file              Write straight to file, instead of stdout.\n"
          "  --print_xyz_indices    Print also atom index to xyz file.\n", progname);
}

void parse_command_line(int argc, char** argv, struct configuration* conf) {

  conf->progname = argv[0];
  conf->print_xyz_indices = 0;

  struct option longopts[] = {
          {"help", no_argument, NULL, 'h'},
          {"xyz", no_argument, NULL, 'x' },
          {"proc", required_argument, NULL, 'p' },
          {"filter", required_argument, NULL, 'F'},
          {"file", no_argument, NULL, 'f'},
          {"print_xyz_indices", no_argument, NULL, 'i' },
          {0, 0, 0, 0 }};
  int indexptr;

  char filter_name[50], filter_arg[150];
  int sscanf_ret_val;

  int c;
  while ((c = getopt_long(argc, argv, "hxp:F:f", longopts, &indexptr)) != -1) {
    switch (c) {
    case 'h':
      usage(argv[0]);
      exit(0);
      break; /* Not reached. */
    case 'x':
      conf->format = FORMAT_XYZ;
      break;
    case 'p':
      conf->printproc = atoi(optarg);
      break;
    case 'F':
      sscanf_ret_val = sscanf(optarg, "%s %s",filter_name, filter_arg);
      if (strcmp(filter_name, "top") == 0) {
        conf->filter = FILTER_TOP;
        if (sscanf_ret_val == 2) conf->top_filter_n_layers = atoi(filter_arg);
        else conf->top_filter_n_layers = 10;
      }
      else if (strcmp(filter_name, "none") == 0)
        conf->filter = FILTER_NONE;
      else if (strcmp(filter_name, "fixed") == 0)
        conf->filter = FILTER_FIXED;
      else if (strcmp(filter_name, "by_id") == 0) {
        conf->filter = FILTER_BY_ID;
/*        if (sscanf_ret_val != 2) {
          fprintf(stderr, "%s: by_id filter requires a file name as argument.\n", conf->progname );
          exit(1);
        }
        strcpy(conf->filter_by_id_file_name, filter_arg);*/
        fprintf(stderr,"%s: filter by id not yet implemented.\n", conf->progname);
        exit(1);
      }
      else if (strcmp(filter_name, "recoil") == 0) {
        conf->filter = FILTER_RECOIL;
        if (sscanf_ret_val != 2) {
          fprintf(stderr, "%s: recoil filter requires n_recoils as argument.\n", conf->progname );
          exit(1);
        }
        conf->n_recoil = atoi(filter_arg);
      }
      else {
        fprintf(stderr, "%s: Unknown filter: %s.\n",
                conf->progname, optarg);
        exit(1);
      }
      break;
    case 'f':
      conf->write_to_file = 1;
      break;
    case 'i':
      conf->print_xyz_indices = 1;
      break;
    case '?':
      /* Fall through. */
    default:
      fprintf(stderr, "%s: unknown option\n", conf->progname);
      usage(conf->progname);
      exit(1);
    }
  }
  if (optind >= argc) { // Did the user give also files?
    usage(conf->progname);
    exit(1);
  }
}


void check_offsets(const struct fixed_header* fhdr, FILE* fp, char** argv,
        char* fname) {
  /*
   * The actual header is now parsed. Check the offsets and continue
   * dumping the description and atom records.
   */
  off_t file_off = (off_t) ftell(fp);
  if (file_off > (off_t) fhdr->desc_off || file_off > (off_t) fhdr->atom_off
          || fhdr->desc_off > fhdr->atom_off) {
    fprintf(stderr, "%s: %s: corrupt offsets\n", argv[0], fname);
    exit(1);
  }
}

void* try_malloc(int size, char* progname,
        const char* errmsg) {
  void* buffer;
  buffer = (void*) malloc(size);
  if (buffer == NULL ) {
    fprintf(stderr, "%s: %s\n", progname, errmsg);
    exit(1);
  }
  return buffer;
}

struct atom * try_malloc_atoms(int count, const struct configuration* conf,
        const struct fixed_header* fhdr) {
  struct atom* atoms;
  atoms = (struct atom*) try_malloc(count * sizeof(struct atom),
          conf->progname, "Couldn't allocate atoms.");
  atoms[0].fields = (double*) try_malloc(
          count * (3 + fhdr->n_fields) * sizeof(double), conf->progname,
          "Couldn't allocate atom fields.");
  int k;
  for (k = 1; k < count; k++)
    atoms[k].fields = atoms[k - 1].fields + 3 + fhdr->n_fields;
  return atoms;
}

void free_atoms(struct atom* atoms) {
  free(atoms[0].fields);
  free(atoms);
}

