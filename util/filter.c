/*
 * filter.c
 *
 *  Created on: 10.7.2012
 *      Author: dlandau
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "types.h"
#include "filter.h"
#include "helpers.h"

static int get_cell_ind_in_1d(double rel_pos_dir, double size_dir, int ncell) {
  return (int)(rel_pos_dir / size_dir * ncell);
}
static int next_free_index(int start_index, int atoms_to_keep_per_cell, int * atom_indices_to_keep) {
  int j;
  for (j = start_index; j < start_index + atoms_to_keep_per_cell; j++)
    if (atom_indices_to_keep[j] == -1) return j;
  return -1;
}

static int is_in_cell(double position_relative_to_proc_edge,
        double proc_cell_size) {
  return position_relative_to_proc_edge > 0
          && position_relative_to_proc_edge < proc_cell_size;
}

void calculate_cpu_box(double * cpu_box, struct variable_header * vhdr, struct fixed_header * fhdr, int cpu) {
  int i;
  for (i = 0; i < 6; i++) {
    double tmp = vhdr->boxes[cpu][i];
    if (tmp > 0.5)
      tmp = 0.5;
    else if (tmp < -0.5)
      tmp = -0.5;
    cpu_box[i] = tmp;
  }

  for (i = 0; i <= 1; i++) cpu_box[i] *= fhdr->box_x;
  for (i = 2; i <= 3; i++) cpu_box[i] *= fhdr->box_y;
  for (i = 4; i <= 5; i++) cpu_box[i] *= fhdr->box_z; // Not needed?
}


void filter_top(struct atom * atoms, struct variable_header * vhdr,
        struct fixed_header * fhdr, struct configuration * conf,
        int cpu, int ** kept_indices, int *n_kept) {
  double cpu_box[6];
  calculate_cpu_box(cpu_box,vhdr,fhdr,cpu);
  int i;

  int n_cells[2];
  double proc_cell_sizes[2];

  double unit_cell_size = 3.60697;
  int atoms_to_keep_per_cell = conf->top_filter_n_layers;
  proc_cell_sizes[0] = (cpu_box[1] - cpu_box[0]);
  proc_cell_sizes[1] = (cpu_box[3] - cpu_box[2]);

  n_cells[0] = (int) proc_cell_sizes[0] / unit_cell_size;
  n_cells[1] = (int) proc_cell_sizes[1] / unit_cell_size;


  int max_atoms_to_keep = n_cells[0] * n_cells[1] * atoms_to_keep_per_cell;
  int * array_indices_to_keep = (int*) try_malloc(max_atoms_to_keep * sizeof(int), conf->progname, "Malloc failed in filter.\n");

  for (i = 0; i < max_atoms_to_keep; i++)
    array_indices_to_keep[i] = -1; // Special value meaning array position not in use

  for (i = 0; i < vhdr->atomsperproc[cpu]; i++) {
    double positions_relative_to_proc_edge[2];

    positions_relative_to_proc_edge[0] = atoms[i].fields[0] - cpu_box[0];
    if (!is_in_cell(positions_relative_to_proc_edge[0], proc_cell_sizes[0])) continue;

    positions_relative_to_proc_edge[1] = atoms[i].fields[1] - cpu_box[2];
    if (!is_in_cell(positions_relative_to_proc_edge[1], proc_cell_sizes[1])) continue;

    double z_pos = atoms[i].fields[2];
    int cell_indices[2];
    cell_indices[0] = get_cell_ind_in_1d(positions_relative_to_proc_edge[0], proc_cell_sizes[0], n_cells[0]);
    cell_indices[1] = get_cell_ind_in_1d(positions_relative_to_proc_edge[1], proc_cell_sizes[1], n_cells[1]);

    int start_index = (cell_indices[1]*n_cells[0] + cell_indices[0]) * atoms_to_keep_per_cell;
    int j;

    /* First branch of if-statement means that the cell isn't yet full, so we just add the atom.
     * The second means, that we might replace something or not. */
    int where_to_add;
    if ((where_to_add = next_free_index(start_index, atoms_to_keep_per_cell, array_indices_to_keep)) >= 0)
      array_indices_to_keep[where_to_add] = i;
    else {
      int what_to_add = i;
      for (j = start_index; j < start_index + atoms_to_keep_per_cell; j++) {
        int tmp = array_indices_to_keep[j];
        if (z_pos > atoms[tmp].fields[2]) { // 2 == z_pos
          z_pos = atoms[tmp].fields[2];
          array_indices_to_keep[j] = what_to_add;
          what_to_add = tmp;
        }
      }
    }
  }
  *kept_indices = array_indices_to_keep;
  *n_kept = max_atoms_to_keep;
}

void filter_none(struct atom * atoms, struct variable_header * vhdr,
        struct fixed_header * fhdr, struct configuration * conf,
        int cpu, int ** kept_indices, int *n_kept) {
  int max_atoms_to_keep = vhdr->atomsperproc[cpu];
  int * array_indices_to_keep = (int*) try_malloc(max_atoms_to_keep * sizeof(int), conf->progname, "Malloc failed in filter.\n");

  int i;
  for (i = 0; i < max_atoms_to_keep; i++)
    array_indices_to_keep[i] = i;
  *kept_indices = array_indices_to_keep;
  *n_kept = max_atoms_to_keep;
}

void filter_fixed(struct atom * atoms, struct variable_header * vhdr,
        struct fixed_header * fhdr, struct configuration * conf,
        int cpu, int ** kept_indices, int *n_kept) {
  int max_atoms_to_keep = vhdr->atomsperproc[cpu];
  int * array_indices_to_keep = (int*) try_malloc(max_atoms_to_keep * sizeof(int), conf->progname, "Malloc failed in filter.\n");

  int i;
  for (i = 0; i < max_atoms_to_keep; i++)
    if (atoms[i].type >= 0) // negative value means a fixed atom
      array_indices_to_keep[i] = -1;
    else
      array_indices_to_keep[i] = i;
  *kept_indices = array_indices_to_keep;
  *n_kept = max_atoms_to_keep;
}

void filter_recoil(struct atom * atoms, struct variable_header * vhdr,
        struct fixed_header * fhdr, struct configuration * conf,
        int cpu, int ** kept_indices, int *n_kept) {
  int max_atoms_to_keep = vhdr->atomsperproc[cpu];
  int * array_indices_to_keep = (int*) try_malloc(max_atoms_to_keep * sizeof(int), conf->progname, "Malloc failed in filter.\n");

  int i;
  for (i = 0; i < max_atoms_to_keep; i++)
    if (atoms[i].ind > fhdr->natoms - (int64_t)conf->n_recoil) // Recoil atoms are at the end.
      array_indices_to_keep[i] = i;
    else
      array_indices_to_keep[i] = -1;
  *kept_indices = array_indices_to_keep;
  *n_kept = max_atoms_to_keep;
}

void run_filters(struct atom * atoms, struct variable_header * vhdr,
        struct fixed_header * fhdr, struct configuration * conf,
        int cpu, int ** kept_indices, int *n_kept) {
  if (conf->filter == FILTER_NONE) {
    filter_none(atoms, vhdr, fhdr, conf, cpu, kept_indices, n_kept);
  } else if (conf->filter == FILTER_TOP) {
    filter_top(atoms, vhdr, fhdr, conf, cpu, kept_indices, n_kept);
  } else if (conf->filter == FILTER_FIXED) {
    filter_fixed(atoms, vhdr, fhdr, conf, cpu, kept_indices, n_kept);
  } else if (conf->filter == FILTER_RECOIL) {
    filter_recoil(atoms, vhdr, fhdr, conf, cpu, kept_indices, n_kept);
  }
}
