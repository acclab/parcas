#!/bin/bash
set -e

for i in 0 1 2 3 4 5 6 7 8 15 16 17 18 ; do
	sed -e "s/MOVIEMODE/$i/" -e 's/STEPS/10/' in/md.in.in > in/md.in
	mpirun -n 4 ../../parcas
	rm -f out/time out/time-avg
	sed -i '/s\/step\/nat/d' out/md.out
	rm -rf out_${i}_correct
	mv out out_${i}_correct
done
