!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>, 
! University of Helsinki
! 
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! 
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module silica_wat_locals
    implicit none

    public
    private :: DP


    integer, parameter :: DP = kind(1.0d1)

    real(kind=DP), allocatable :: s2_dx(:), s2_dy(:), s2_dz(:), s2_ph(:), s2_dph(:), s2_r(:)
    real(kind=DP), allocatable :: s3_dx(:), s3_dy(:), s3_dz(:), s3_r(:)
    integer, allocatable :: num2_index(:), num2_natoms(:), num3_index(:), num3_natoms(:)
    real(kind=DP), allocatable :: sz_Z(:)
end module



module silica_wat_old
    implicit none

    public
    private :: DP


    integer, parameter :: DP = kind(1.0d1)

    integer, parameter :: max_wat_types = 3
    integer            :: Otype = -1
    integer, parameter :: max_neib = 64

    real(kind=DP), allocatable, dimension(:,:) :: A_arr, B_arr, p_arr, q_arr, r02_arr, sigma_arr
    real(kind=DP), allocatable, dimension(:,:) :: coordmax_arr,coordmin_arr, Dco_arr, p5_arr, p6_arr, op7_arr, p8_arr, p9_arr
    real(kind=DP), allocatable, dimension(:,:,:) :: &
        lambda_arr, gamma_arr, r03_arr, costheta0_arr
    !real(kind=DP), allocatable, dimension(:,:)  :: cutoffmax,cutoffmaxsq

    real(kind=DP), parameter :: swsigma_Si = 2.0951_DP  ! SW length unit (Angstroms)
    real(kind=DP), parameter :: epsilon_Si = 2.16817_DP ! = 50 kcal/mol 2.16722
    !real(kind=DP), parameter :: ucut = 1.9_DP        ! Si-Si-Si cutoff (swsigma)

    !Si-Si from   Jpn. J. Appl. Phys. Vol. 38 (1999)
    real(kind=DP), parameter :: A_SiSi  = 7.049556277_DP*epsilon_Si*0.5_DP !7.049556277
    real(kind=DP), parameter :: B_SiSi  = 0.6022245584_DP
    real(kind=DP), parameter :: p_SiSi  = 4.0_DP
    real(kind=DP), parameter :: q_SiSi  = 0.0_DP
    real(kind=DP), parameter :: r0_SiSi = 1.8_DP*swsigma_Si

    !Si-O from  ECS Transactions, 33 (6) 901-912 (2010)
    real(kind=DP), parameter :: A_SiO   = 45.5110912532_DP*0.5_DP ! 115.364065913_DP*0.5_DP*epsilon_Si
    real(kind=DP), parameter :: B_SiO   = 0.0379999789415_DP        !0.9094442793_DP
    real(kind=DP), parameter :: p_SiO   = 5.3_DP     !2.58759_DP
    real(kind=DP), parameter :: q_SiO   = -1.1_DP    !2.39370_DP
    real(kind=DP), parameter :: r0_SiO  = 2.7236_DP  !1.4_DP*swsigma_Si

    !O-O from   Jpn. J. Appl. Phys. Vol. 38 (1999)
    real(kind=DP), parameter :: A_OO    = -12.292427744_DP*0.5_DP*epsilon_Si !eV
    real(kind=DP), parameter :: B_OO    = 0.0_DP
    real(kind=DP), parameter :: p_OO    = 0.0_DP
    real(kind=DP), parameter :: q_OO    = 2.24432_DP
    real(kind=DP), parameter :: r0_OO   = 1.25_DP*swsigma_Si ! originally 1.25_DP

    !Si-Si-Si
    real(kind=DP), parameter :: lambda_SiSiSi     = 16.404_DP*epsilon_Si ! Original SW 21.0_DP*epsilon_Si
    real(kind=DP), parameter :: gamma_SiSiSi      = 1.0473_DP*swsigma_Si ! Original SW 1.2*swsigma_Si
    real(kind=DP), parameter :: r0_SiSiSi         = 1.8_DP*swsigma_Si
    real(kind=DP), parameter :: costheta0_SiSiSi  = -1.0_DP/3.0_DP

    !Si-Si-O
    real(kind=DP), parameter :: lambda_SiSiO      = 10.667_DP*epsilon_Si
    real(kind=DP), parameter :: gamma_SiSi_SiSiO  = 1.93973_DP*swsigma_Si
    real(kind=DP), parameter :: gamma_SiO_SiSiO   = 0.25_DP*swsigma_Si
    real(kind=DP), parameter :: r0_SiSi_SiSiO     = 1.9_DP*swsigma_Si
    real(kind=DP), parameter :: r0_SiO_SiSiO      = 1.4_DP*swsigma_Si
    real(kind=DP), parameter :: costheta0_SiSiO   = -1.0_DP/3.0_DP

    !Si-O-Si
    real(kind=DP), parameter :: lambda_SiOSi      = 2.9572_DP*epsilon_Si
    real(kind=DP), parameter :: gamma_SiOSi       = 0.71773_DP*swsigma_Si
    real(kind=DP), parameter :: r0_SiOSi          = 1.4_DP*swsigma_Si
    real(kind=DP), parameter :: costheta0_SiOSi   = -0.812_DP! -0.6155238_DP

    !O-Si-O
    real(kind=DP), parameter :: lambda_OSiO      = 3.1892_DP*epsilon_Si
    real(kind=DP), parameter :: gamma_OSiO       = 0.3220_DP*swsigma_Si
    real(kind=DP), parameter :: r0_OSiO          = 1.65_DP*swsigma_Si
    real(kind=DP), parameter :: costheta0_OSiO   = -1.0_DP/3.0_DP

    !oxygen coordination
    ! For Si-O
    real(kind=DP), parameter :: p5_Si               = 5.274 ! 0.097_DP  !m1
    real(kind=DP), parameter :: p6_Si               = 0.712 ! 1.6_DP    !m2
    real(kind=DP), parameter :: p7_Si               = 0.522 ! 0.3654_DP !m3
    real(kind=DP), parameter :: p8_Si               =-0.0372! 0.1344_DP !m4
    real(kind=DP), parameter :: p9_Si               =-4.52 ! 6.4176_DP !m5
    real(kind=DP), parameter :: op7_Si              = 1.0_DP/p7_Si

    !oxygen coordination
    ! For Ge-O
    real(kind=DP), parameter :: p5_Ge               = 0.01
    real(kind=DP), parameter :: p6_Ge               = 1.22
    real(kind=DP), parameter :: p7_Ge               = 0.34
    real(kind=DP), parameter :: p8_Ge               = 0.0315
    real(kind=DP), parameter :: p9_Ge               = 14.22
    real(kind=DP), parameter :: op7_Ge              = 1.0_DP/p7_Ge

    real(kind=DP), parameter :: Dco = 0.05_DP ! 0.1_DP  !*swsigma
    real(kind=DP), parameter :: Rco =  1.2_DP ! 1.3_DP  !*swsigma

    !Ge-Ge from  Posselt
    real(kind=DP), parameter :: swsigma_Ge    = 2.181_DP
    real(kind=DP), parameter :: epsilon_Ge    = 1.93_DP

    real(kind=DP), parameter :: A_GeGe  = 7.049556277_DP*epsilon_Ge*0.5_DP
    real(kind=DP), parameter :: B_GeGe  = 0.6022245584_DP
    real(kind=DP), parameter :: p_GeGe  = 4.0_DP             ! now hard-coded
    real(kind=DP), parameter :: q_GeGe  = 0.0_DP             ! now hard-coded
    real(kind=DP), parameter :: r0_GeGe = 1.8_DP*swsigma_Ge

    !Ge-O fitted to the Ge-O parameters in ECS Transactions, 33 (6) 901-912 (2010)
    real(kind=DP), parameter :: A_GeO   = 5479.887*epsilon_Ge*0.5_DP
    real(kind=DP), parameter :: B_GeO   = 0.99787
    real(kind=DP), parameter :: p_GeO   = 2.35927
    real(kind=DP), parameter :: q_GeO   = 2.35442
    real(kind=DP), parameter :: r0_GeO  = 1.4_DP*swsigma_Ge

    !Ge-Ge-Ge from Posselt
    real(kind=DP), parameter :: lambda_GeGeGe     = 19.5_DP*epsilon_Ge
    real(kind=DP), parameter :: gamma_GeGeGe      = 1.19_DP*swsigma_Ge
    real(kind=DP), parameter :: r0_GeGeGe         = 1.8_DP*swsigma_Ge
    real(kind=DP), parameter :: costheta0_GeGeGe  = -1.0_DP/3.0_DP


    !Ge-Si mixing rules see: http://journals.aps.org/prb/pdf/10.1103/PhysRevB.47.9931
    real(kind=DP), parameter :: swsigma_SiGe    = (swsigma_Si+swsigma_Ge)*0.5_DP
    real(kind=DP), parameter :: epsilon_SiGe    = sqrt(epsilon_Si*epsilon_Ge)


    !real(kind=DP), parameter :: A_GeSi  = 7.049556277_DP*epsilon_SiGe*0.5_DP
    real(kind=DP), parameter :: A_GeSi  = sqrt(A_SiSi*A_GeGe)
    real(kind=DP), parameter :: B_GeSi  = 0.6022245584_DP
    real(kind=DP), parameter :: p_GeSi  = 4.0_DP             ! now hard-coded
    real(kind=DP), parameter :: q_GeSi  = 0.0_DP             ! now hard-coded
    !real(kind=DP), parameter :: r0_GeSi = 1.8_DP*swsigma_SiGe
    real(kind=DP) :: r0_GeSi =  0.5_DP*(r0_SiSi+r0_GeGe)

    !real(kind=DP), parameter :: lambda_SiGe       = sqrt(lambda_SiSiSi/epsilon_Si*lambda_GeGeGe/epsilon_Ge)
    !real(kind=DP), parameter :: gamma_SiGe        = (gamma_SiSiSi/swsigma_Si+gamma_GeGeGe/swsigma_Ge)*0.5_DP

    real(kind=DP), parameter :: lambda_SiSi = lambda_SiSiSi
    real(kind=DP), parameter :: lambda_GeGe = lambda_GeGeGe
    real(kind=DP), parameter :: lambda_SiGe = sqrt(lambda_GeGeGe*lambda_SiSiSi)
    real(kind=DP), parameter :: lambda_GeSi = sqrt(lambda_GeGeGe*lambda_SiSiSi)



    !Si-Si-Ge
    !real(kind=DP), parameter :: lambda_SiSiGe     = sqrt(lambda_SiSiSi/epsilon_Si*lambda_SiGe)*epsilon_SiGe
    real(kind=DP), parameter :: lambda_SiSiGe     = sqrt(lambda_SiSi*lambda_SiGe)
    real(kind=DP), parameter :: gamma_SiSi_SiSiGe = gamma_SiSiSi
    real(kind=DP), parameter :: gamma_SiGe_SiSiGe = gamma_SiSiSi/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: r0_SiSi_SiSiGe    = r0_SiSiSi
    real(kind=DP), parameter :: r0_SiGe_SiSiGe    = r0_SiSiSi/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: costheta0_SiSiGe  = -1.0_DP/3.0_DP

    !Si-Ge-Si
    real(kind=DP), parameter :: lambda_SiGeSi     = sqrt(lambda_GeSi*lambda_GeSi)
    real(kind=DP), parameter :: gamma_SiGeSi      = gamma_GeGeGe/swsigma_Ge*swsigma_SiGe
    real(kind=DP), parameter :: r0_SiGeSi         = r0_GeGeGe/swsigma_Ge*swsigma_SiGe
    real(kind=DP), parameter :: costheta0_SiGeSi  = -1.0_DP/3.0_DP
    !Ge-Si-Ge
    real(kind=DP), parameter :: lambda_GeSiGe     = sqrt(lambda_SiGe*lambda_SiGe)
    real(kind=DP), parameter :: gamma_GeSiGe      = gamma_SiSiSi/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: r0_GeSiGe         = r0_SiSiSi/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: costheta0_GeSiGe  = -1.0_DP/3.0_DP

    !Ge-Ge-Si
    real(kind=DP), parameter :: lambda_GeGeSi     = sqrt(lambda_GeSi*lambda_GeGe)
    real(kind=DP), parameter :: gamma_GeGe_GeGeSi = gamma_GeGeGe
    real(kind=DP), parameter :: gamma_GeSi_GeGeSi = gamma_GeGeGe/swsigma_Ge*swsigma_SiGe
    real(kind=DP), parameter :: r0_GeGe_GeGeSi    = r0_GeGeGe
    real(kind=DP), parameter :: r0_GeSi_GeGeSi    = r0_GeGeGe/swsigma_Ge*swsigma_SiGe
    real(kind=DP), parameter :: costheta0_GeGeSi  = -1.0_DP/3.0_DP

    ! O-Si-Ge
    real(kind=DP), parameter :: lambda_OSiGe      = lambda_SiSiO
    real(kind=DP), parameter :: gamma_SiGe_OSiGe  = gamma_SiSi_SiSiO/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: gamma_SiO_OSiGe   = gamma_SiO_SiSiO !/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: r0_SiGe_OSiGe     = r0_SiSi_SiSiO/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: r0_SiO_OSiGe      = r0_SiO_SiSiO !/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: costheta0_OSiGe   = -1.0_DP/3.0_DP

    ! Si-O-Ge
    real(kind=DP), parameter :: lambda_SiOGe      = lambda_SiOSi
    real(kind=DP), parameter :: gamma_OGe_SiOGe   = gamma_SiOSi/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: gamma_OSi_SiOGe   = gamma_SiOSi
    real(kind=DP), parameter :: r0_OGe_SiOGe      = r0_SiOSi/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: r0_OSi_SiOGe      = r0_SiOSi
    real(kind=DP), parameter :: costheta0_SiOGe   = costheta0_SiOSi

    ! Ge-Ge-O
    real(kind=DP), parameter :: lambda_GeGeO     = lambda_SiSiO
    real(kind=DP), parameter :: gamma_GeGe_GeGeO = gamma_SiSi_SiSiO/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: gamma_GeO_GeGeO  = gamma_SiO_SiSiO/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: r0_GeGe_GeGeO    = r0_SiSi_SiSiO/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: r0_GeO_GeGeO     = r0_SiO_SiSiO/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: costheta0_GeGeO  = -1.0_DP/3.0_DP

    ! Si-Ge-O
    real(kind=DP), parameter :: lambda_SiGeO      = lambda_SiSiO
    real(kind=DP), parameter :: gamma_GeSi_SiGeO  = gamma_SiSi_SiSiO/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: gamma_GeO_SiGeO   = gamma_SiO_SiSiO/swsigma_Si*swsigma_Ge !/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: r0_GeSi_SiGeO     = r0_SiSi_SiSiO/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: r0_GeO_SiGeO      = r0_SiO_SiSiO/swsigma_Si*swsigma_Ge !/swsigma_Si*swsigma_SiGe
    real(kind=DP), parameter :: costheta0_SiGeO   = -1.0_DP/3.0_DP

    ! O-Ge-O
    real(kind=DP), parameter :: lambda_OGeO      = lambda_OSiO
    real(kind=DP), parameter :: gamma_OGeO       = gamma_OSiO/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: r0_OGeO          = r0_OSiO/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: costheta0_OGeO   = -1.0_DP/3.0_DP

    ! Ge-O-Ge
    real(kind=DP), parameter :: lambda_GeOGe      =  lambda_SiOSi
    real(kind=DP), parameter :: gamma_GeOGe       =  gamma_SiOSi/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: r0_GeOGe          =  r0_SiOSi/swsigma_Si*swsigma_Ge
    real(kind=DP), parameter :: costheta0_GeOGe   = -0.69_DP

contains

    function mix_energy(a,b)
        implicit none
        real(kind=DP), intent(in) :: a,b
        real(kind=DP) :: mix_energy
        mix_energy = sqrt(a*b)
        return
    end function

    function mix_dist(a,b)
        implicit none
        real(kind=DP), intent(in) :: a,b
        real(kind=DP) :: mix_dist
        mix_dist = 0.5_DP*(a+b)
        return
    end function


end module silica_wat_old


module silica_wat_params_new
    implicit none

    public
    private :: DP


    integer, parameter :: DP = kind(1.0d1)

    real(kind=DP), parameter :: swsigma = 2.0951_DP  ! SW length unit (Angstroms)
    real(kind=DP), parameter :: epsilon = 2.16722_DP ! = 50 kcal/mol
    real(kind=DP), parameter :: ucut = 1.9_DP        ! Si-Si-Si cutoff (swsigma)
    real(kind=DP), parameter :: opswsigma = 1.0_DP/swsigma
    real(kind=DP), parameter :: eosigma = epsilon/swsigma


    ! Scaling parameters for Ge, compared to Si
    real(kind=DP), parameter :: sw2mod2 = 0.82_DP     ! Modification of Ge-Ge S-W potential strength, x*-3.86
    real(kind=DP), parameter :: sw3mod2 = 0.67742_DP  ! Modification of S-W three-body strength, x*31
    real(kind=DP), parameter :: sw2mod3 = 0.90556_DP  ! Si-Ge
    real(kind=DP), parameter :: sw3mod3 = 0.82320_DP  ! Si-Ge

    real(kind=DP), parameter :: GeO_Lscale = 1.74_DP/1.61_DP ! Ge-O bond compared to Si-O, from S-Weber-pot.
    real(kind=DP), parameter :: GeGe_Lscale = 2.181_DP/2.0951_DP  ! Ge-Ge bond compared to Si-Si
    real(kind=DP), parameter :: GeSi_Lscale = 2.1376_DP/2.0951_DP ! Ge-Si bond compared to Si-Si

    real(kind=DP), parameter :: GeGe_Escale = 1.93_DP/2.16722_DP*sw2mod2  ! Ge-Ge compared to Si-Si
    real(kind=DP), parameter :: GeSi_Escale = 2.0451_DP/2.16722_DP*sw2mod3   !Ge-Si compared to Si-Si
    real(kind=DP), parameter :: GeO_Escale = 5.00_DP/6.42_DP            ! GeO2 Ecoh compared to SiO2



    !Si-O from   ECS Transactions, 33 (6) 901-912 (2010)
    real(kind=DP), parameter :: A_SiSi  = 15.2778_DP*0.5_DP
    real(kind=DP), parameter :: B_SiSi  = 11.6031_DP
    real(kind=DP), parameter :: p_SiSi  = 4.0_DP             ! now hard-coded
    real(kind=DP), parameter :: q_SiSi  = 0.0_DP             ! now hard-coded
    real(kind=DP), parameter :: r0_SiSi = 3.77118_DP

    real(kind=DP), parameter :: A_SiO   = 20.174_DP*0.5_DP !eV
    real(kind=DP), parameter :: B_SiO   = 4.3201_DP
    real(kind=DP), parameter :: p_SiO   = 5.3_DP
    real(kind=DP), parameter :: q_SiO   = -1.1_DP
    real(kind=DP), parameter :: r0_SiO  = 2.7236_DP

    real(kind=DP), parameter :: A_OO    = -139.621_DP*0.5_DP !eV
    real(kind=DP), parameter :: B_OO    = 0.0_DP            ! now hard-coded
    real(kind=DP), parameter :: p_OO    = 0.0_DP            ! now hard-coded
    real(kind=DP), parameter :: q_OO    = 2.24_DP
    real(kind=DP), parameter :: r0_OO   = 2.6189_DP           ! originally 1.25_DP

    real(kind=DP), parameter :: myy_1_SiSiSi        = 8.6688_DP
    real(kind=DP), parameter :: gamma_SiSi_1_SiSiSi = 0.1844_DP
    real(kind=DP), parameter :: r0_SiSi_1_SiSiSi    = 2.5141_DP ! originally 1.2
    real(kind=DP), parameter :: costheta0_1_SiSiSi  = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: alpha_1_SiSiSi      = -1.35_DP

    real(kind=DP), parameter :: myy_2_SiSiSi        = 12.7388_DP
    real(kind=DP), parameter :: nyy_2_SiSiSi        = 1.6_DP
    real(kind=DP), parameter :: ksi_2_SiSiSi        = 6.0_DP
    real(kind=DP), parameter :: z0_2_SiSiSi         = 0.0_DP
    real(kind=DP), parameter :: gamma_SiSi_2_SiSiSi = 2.1161_DP
    real(kind=DP), parameter :: r0_SiSi_2_SiSiSi    = 3.771  ! originally 1.8
    real(kind=DP), parameter :: costheta0_2_SiSiSi  = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: alpha_2_SiSiSi      = 0.3_DP

    real(kind=DP), parameter :: myy_1_SiSiO         = 6.5016_DP
    real(kind=DP), parameter :: nyy_1_SiSiO         = 3.6_DP
    real(kind=DP), parameter :: ksi_1_SiSiO         = 2.0_DP
    real(kind=DP), parameter :: z0_1_SiSiO          = 2.6_DP  ! Same used for Ge ?
    real(kind=DP), parameter :: gamma_SiSi_1_SiSiO  = 0.0670_DP
    real(kind=DP), parameter :: gamma_SiO_1_SiSiO   = 0.2598_DP
    real(kind=DP), parameter :: r0_SiSi_1_SiSiO     = 2.4094_DP
    real(kind=DP), parameter :: r0_SiO_1_SiSiO      = 2.3046_DP
    real(kind=DP), parameter :: costheta0_1_SiSiO   = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: alpha_1_SiSiO       = 0.3_DP

    real(kind=DP), parameter :: myy_2_SiSiO         = 12.7388_DP
    real(kind=DP), parameter :: gamma_SiSi_2_SiSiO  = 2.1161_DP
    real(kind=DP), parameter :: gamma_SiO_2_SiSiO   = 2.2671_DP
    real(kind=DP), parameter :: r0_SiSi_2_SiSiO     = 3.7712_DP
    real(kind=DP), parameter :: r0_SiO_2_SiSiO      = 3.1427_DP
    real(kind=DP), parameter :: costheta0_2_SiSiO   = -1.0_DP/3.0_DP
    real(kind=DP), parameter :: alpha_2_SiSiO       = 0.3_DP

    real(kind=DP), parameter :: myy_1_SiOSi         = 5.418_DP
    real(kind=DP), parameter :: gamma_OSi_1_SiOSi   = 0.314265_DP
    real(kind=DP), parameter :: r0_OSi_1_SiOSi      = 2.51412_DP
    real(kind=DP), parameter :: costheta0_1_SiOSi   = -0.812_DP
    real(kind=DP), parameter :: alpha_1_SiOSi       = 1.6_DP

    real(kind=DP), parameter :: myy_1_OSiO          = 22.756_DP
    real(kind=DP), parameter :: gamma_SiO_1_OSiO    = 0.6494_DP ! typo in paper
    real(kind=DP), parameter :: r0_SiO_1_OSiO       = 2.3046_DP ! typo in paper
    real(kind=DP), parameter :: costheta0_1_OSiO    = -1.0_DP/3.0_DP ! typo in paper

    ! Si-O interaction softening function parameters
    real(kind=DP), parameter :: p1_Si = 0.30367_DP
    real(kind=DP), parameter :: p2_Si = 3.93233_DP
    real(kind=DP), parameter :: p3_Si = 0.25345_DP
    real(kind=DP), parameter :: p4_Si = 3.93233_DP
    real(kind=DP), parameter :: p5_Si = 5.274_DP
    real(kind=DP), parameter :: p6_Si = 0.712_DP
    real(kind=DP), parameter :: p7_Si = 0.522_DP
    real(kind=DP), parameter :: p8_Si = -0.0372_DP
    real(kind=DP), parameter :: p9_Si = -4.52_DP
    real(kind=DP), parameter :: op7_Si= 1.0_DP/p7_Si
    real(kind=DP), parameter :: R_Si  = 2.5141_DP
    real(kind=DP), parameter :: D_Si  = 0.1048_DP


    !Ge-O NATURAL from   ECS Transactions, 33 (6) 901-912 (2010)
    real(kind=DP), parameter :: A_GeGe  = 13.6056_DP*0.5_DP !
    real(kind=DP), parameter :: B_GeGe  = 13.6264_DP ! Scaling needed in A only
    real(kind=DP), parameter :: p_GeGe  = 4.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: q_GeGe  = 0.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: r0_GeGe = 3.9258_DP !
    real(kind=DP), parameter :: A_GeO   = 45743.7_DP*0.5_DP !
    real(kind=DP), parameter :: B_GeO   = 1.0016_DP !
    real(kind=DP), parameter :: p_GeO   = 2.8435_DP !
    real(kind=DP), parameter :: q_GeO   = 2.8388_DP !
    real(kind=DP), parameter :: r0_GeO  = 2.75_DP !



    !!!!!TBD
    !!!!!TBD
    !!! Parameters below are not validated.....
    real(kind=DP), parameter :: p_GeSi  = 4.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: q_GeSi  = 0.0_DP !           ! now hard-coded
    real(kind=DP), parameter :: r0_GeSi = 1.8_DP !
    real(kind=DP), parameter :: myy_1_OGeO          = 10.5_DP*GeO_Escale ! These scale the 3-body potential energy
    real(kind=DP), parameter :: myy_1_GeOGe         = 2.5_DP*GeO_Escale !
    real(kind=DP), parameter :: myy_1_GeOSi         = 2.5_DP*(1+GeO_Escale)/2.0_DP ! Avg. of Si-O and Ge-O
    real(kind=DP), parameter :: myy_1_GeGeGe        = 4.0_DP*GeGe_Escale !
    real(kind=DP), parameter :: myy_1_SiGeSi        = 4.0_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_1_GeSiGe        = 4.0_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_1_GeSiSi        = 4.0_DP*(1+GeSi_Escale)/2.0_DP ! Avg. of Si-Si and Ge-Si
    real(kind=DP), parameter :: myy_1_GeGeSi        = 4.0_DP*(GeGe_Escale+GeSi_Escale)/2.0_DP ! Avg. of Ge-Ge and Ge-Si
    real(kind=DP), parameter :: myy_1_GeGeO         = 3.0_DP*(GeGe_Escale+GeO_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_1_GeSiO         = 3.0_DP*(1+GeSi_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_1_SiGeO         = 3.0_DP*(GeSi_Escale+GeO_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_2_GeGeGe        = 5.878_DP*GeGe_Escale !
    real(kind=DP), parameter :: myy_2_SiGeSi        = 5.878_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_2_GeSiGe        = 5.878_DP*GeSi_Escale !
    real(kind=DP), parameter :: myy_2_GeSiSi        = 5.878_DP*(1+GeSi_Escale)/2.0_DP ! Avg. of Si-Si and Ge-Si
    real(kind=DP), parameter :: myy_2_GeGeSi        = 5.878_DP*(GeGe_Escale+GeSi_Escale)/2.0_DP ! Avg. of Ge-Ge and Ge-Si
    real(kind=DP), parameter :: myy_2_GeGeO         = 5.878_DP*(GeGe_Escale+GeO_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_2_GeSiO         = 5.878_DP*(1+GeSi_Escale)/2.0_DP !
    real(kind=DP), parameter :: myy_2_SiGeO         = 5.878_DP*(GeSi_Escale+GeO_Escale)/2.0_DP !

    ! Ge r0 parameters
    real(kind=DP), parameter :: r0_OGe_1_GeOGe      = 1.2_DP*GeO_Lscale ! 0.2 in the paper a mistake
    real(kind=DP), parameter :: r0_GeO_1_OGeO       = 1.2_DP*GeO_Lscale ! originally 1.1
    real(kind=DP), parameter :: r0_GeO_1_GeGeO      = 1.10_DP*GeO_Lscale
    real(kind=DP), parameter :: r0_GeGe_1_GeGeGe    = 1.0_DP*GeGe_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeGe_1_GeGeO     = 1.15_DP*GeGe_Lscale
    real(kind=DP), parameter :: r0_GeO_2_GeGeO      = 1.5_DP*GeO_Lscale
    real(kind=DP), parameter :: r0_GeGe_2_GeGeO     = 1.8_DP*GeGe_Lscale
    real(kind=DP), parameter :: r0_GeGe_2_GeGeGe    = ucut*GeGe_Lscale ! originally 1.8
    ! Ge-Si r0 parameters HERE WE ASSUME THAT AT ANOTHER BOND Si/Ge CHANGE DOES NOT CHANGE THINGS
    ! i.e. for example r0_GeO_1_GeOSi=r0_GeO_1_GeOGe
    real(kind=DP), parameter :: r0_GeSi_1_GeSiGe    = 1.0_DP*GeSi_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeSi_1_SiGeSi    = 1.0_DP*GeSi_Lscale ! originally 1.2
    real(kind=DP), parameter :: r0_GeSi_1_GeSiO     = 1.15_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_GeSiO     = 1.8_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_1_SiGeO     = 1.15_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_SiGeO     = 1.8_DP*GeSi_Lscale
    real(kind=DP), parameter :: r0_GeSi_2_GeSiGe    = ucut*GeSi_Lscale ! originally 1.8
    real(kind=DP), parameter :: r0_GeSi_2_SiGeSi    = ucut*GeSi_Lscale ! originally 1.8

end module silica_wat_params_new
