!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module binout
    use my_mpi
    use datatypes
    use defs
    use para_common, only: myproc, nprocs, rmn, rmx

    use typeparam

    use random
    use output_logger, only: logger
    use file_units, only: TEMP_FILE


    implicit none

    private
    public :: initBinaryMode
    public :: initFilesequence
    public :: binmode_string_to_bitmask
    public :: binarysave
    public :: binaryread


    integer, parameter :: WRITE_FILE_VERSION = 4  ! What fileversion does this produce

    ! Integer and real types used.
    real(kind=real32b)   :: r4, r42(2)
    real(kind=real64b)   :: r8
    integer(kind=int32b) :: i4
    integer(kind=int64b) :: i8
    character(len=4)     :: c4

    ! Prototype values.
    integer(kind=int32b), parameter :: protoInt = int(z'11223344', kind=int32b)
    real(kind=real32b),   parameter :: protoReal = 721409.0 / 1048576.0

    ! Per-atom fields. Includes 3*coord, though they are not counted in filedescr%fields.
    integer, parameter :: maxFields = 14
    ! Atom and field caches (if fewer than maxField fields then actually more atoms can be handled)
    ! FIXME!!! The correct way is to read the fixed part of
    ! input header and then allocate a buffer big enough.
    integer, parameter :: maxAtom = 2000000
    ! FIXME!!! The correct way is to read the fixed part of
    ! input header and then allocate a buffer big enough.
    ! 125 000 is large enough for current generation
    integer, parameter :: maxCpus = 125000

    ! Buffer for atom data.
    real(kind=real32b), allocatable, save :: buffer(:)
    real(kind=real32b), allocatable, save :: header(:)

    ! Used to gather number of atoms on each proc and to compute offsets in file.
    integer, allocatable, save :: atomsperproc(:)
    integer(kind=int64b), allocatable, save :: offsets(:)

    real(kind=real64b), allocatable, save :: rmnperproc(:, :)
    real(kind=real64b), allocatable, save :: rmxperproc(:, :)

    ! Periodic boundary conditions?
    logical, save :: periodic(3)

    ! .TRUE. if byte order has to be swapped while reading.
    logical, save :: swap_needed = .false.

    ! Describes the fields to be saved and what the current frame number
    ! is for each group of files (e.g. restarts, movies,...).
    type :: filedescr
        character(len=22)    :: filename
        ! Size of a floating-point value in bytes: 4 for floats, 8 for doubles
        integer              :: realsize
        integer              :: frame
        integer              :: overwrite
        integer              :: fields
        ! Field names saved in the file header.
        character(len=4)     :: fieldName(maxFields)
        ! Field units saved in the file header.
        character(len=4)     :: fieldUnit(maxFields)
        ! One FIELD_* constant per field.
        integer              :: fieldType(maxFields)
        ! Offset of fields in data for each atom, taking into account realsize.
        integer              :: fieldOffset(maxFields)
    end type filedescr

    type(filedescr) :: fds(5) ! 5 simultaneous outputs supported

    ! Must be in order of the bitmask used for binmode,
    ! i.e. bitmask for Epot = 2**FIELD_EPOT.
    integer, parameter :: FIELD_EPOT = 0
    integer, parameter :: FIELD_EKIN = 1
    integer, parameter :: FIELD_VX   = 2
    integer, parameter :: FIELD_VY   = 3
    integer, parameter :: FIELD_VZ   = 4
    integer, parameter :: FIELD_WXX  = 5
    integer, parameter :: FIELD_WYY  = 6
    integer, parameter :: FIELD_WZZ  = 7
    integer, parameter :: FIELD_WXY  = 8
    integer, parameter :: FIELD_WXZ  = 9
    integer, parameter :: FIELD_WYZ  = 10

    type :: FieldDef
        integer :: type  ! One of the FIELD_* constants
        character(4) :: name
        character(4) :: unit
    end type

    type(FieldDef), parameter :: field_defs(0:10) = [ &
        FieldDef(FIELD_EPOT, "Epot", "eV  "), &
        FieldDef(FIELD_EKIN, "Ekin", "eV  "), &
        FieldDef(FIELD_VX  , "V.x ", "A/fs"), &
        FieldDef(FIELD_VY  , "V.y ", "A/fs"), &
        FieldDef(FIELD_VZ  , "V.z ", "A/fs"), &
        FieldDef(FIELD_WXX , "W.xx", "eV  "), &
        FieldDef(FIELD_WYY , "W.yy", "eV  "), &
        FieldDef(FIELD_WZZ , "W.zz", "eV  "), &
        FieldDef(FIELD_WXY , "W.xy", "eV  "), &
        FieldDef(FIELD_WXZ , "W.xz", "eV  "), &
        FieldDef(FIELD_WYZ , "W.yz", "eV  ") &
    ]

contains

    ! Functions swap32 and swap64 swap the byte order of a word, which
    ! is needed for endian conversions. The code might look like a bit
    ! clumsy but it compiles to rather nice machine code. ;)

    pure function swap32(x) result(y)
        integer(kind=int32b), intent(in) :: x
        integer(kind=int32b) :: y

        y = transfer(IOR(IOR(ISHFT(x, -24), &
            ISHFT(IAND(x, int(z'00ff0000', kind=int32b)), -8)), &
            IOR(ISHFT(IAND(x, int(z'0000ff00', kind=int32b)), 8), &
            ISHFT(x, 24))), y)
    end function swap32

    pure function swap64(x) result(y)
        integer(kind=int64b), intent(in) :: x
        integer(kind=int64b) :: y

        y = transfer(IOR(IOR(IOR(ISHFT(x, -56), &
            ISHFT(IAND(x, int(z'00ff000000000000', kind=int64b)), -40)), &
            IOR(ISHFT(IAND(x, int(z'0000ff0000000000', kind=int64b)), -24), &
            ISHFT(IAND(x, int(z'000000ff00000000', kind=int64b)), -8))), &
            IOR(IOR(ISHFT(IAND(x, int(z'00000000ff000000', kind=int64b)), 8), &
            ISHFT(IAND(x, int(z'0000000000ff0000', kind=int64b)), 24)), &
            IOR(ISHFT(IAND(x, int(z'000000000000ff00', kind=int64b)), 40), &
            ISHFT(x, 56)))), y)
    end function swap64

    pure function get_int32(x) result(y)
        real(kind=real32b), intent(in) :: x
        integer(kind=int32b) :: y

        y = TRANSFER(x, i4)
        if (swap_needed) then
            y = swap32(y)
        end if
    end function get_int32

    pure function get_int64(x) result(y)
        real(kind=real32b), intent(in) :: x(2)
        integer(kind=int64b) :: y

        y = TRANSFER(x, i8)
        if (swap_needed) then
            y = swap64(y)
        end if
    end function get_int64

    pure function get_real32(x) result(y)
        real(kind=real32b), intent(in) :: x
        real(kind=real32b) :: y

        y = TRANSFER(get_int32(x), r4)
    end function get_real32

    pure function get_real64(x) result(y)
        real(kind=real32b), intent(in) :: x(2)
        real(kind=real64b) :: y

        y = TRANSFER(get_int64(x), r8)
    end function get_real64


    ! Check record length (and do other sanity checks).
    ! Return .FALSE. if everything is OK.
    function invalidReclen()
        logical :: invalidReclen

        integer(kind=int32b) :: i32
        real(kind=real32b)   :: r32
        integer              :: intlen, reallen

        ! Initialize to keep picky compilers quiet.
        i32 = 0
        r32 = 0

        ! Assume failure, so RETURN is sufficient.
        invalidReclen = .true.

        ! Are i32 and r32 are the same size?
        inquire(iolength=intlen) i32
        inquire(iolength=reallen) r32
        if (intlen /= reallen) return

        ! Is i32 32-bit two's complement integer?
        i32 = -16909061
        if (BIT_SIZE(i32) /= 32) return
        if (IBITS(i32, 24, 8) /= 254) return
        if (IBITS(i32, 16, 8) /= 253) return
        if (IBITS(i32,  8, 8) /= 252) return
        if (IBITS(i32,  0, 8) /= 251) return

        ! Is IEEE 754 single precision r32?
        r32 = protoReal
        i32 = TRANSFER(r32, i32)
        if (i32 /= 1060118544 .and. &
            i32 /= 809439264  .and. &
            i32 /= 537935664  .and. &
            i32 /= 270544959) return

        ! Everything is all right.
        invalidReclen = .false.

    end function invalidReclen


    !
    ! Convert a string like "Epot;Ekin;V.x;V.y;V.z" to a bitmask usable
    ! by the initFilesequence subroutine.
    ! Accepts "all" as meaning all fields, or an integer as a bitmask.
    !
    function binmode_string_to_bitmask(str) result(bitmask)
        character(*), intent(in) :: str
        integer :: bitmask

        integer :: i, a, b, ierr

        if (len(str) == 0 .or. str == "") then
            bitmask = 0
            return
        end if

        if (str == "all") then
            bitmask = not(0)
            return
        end if

        ! The read below "succeeds" if str begins with ";", and reads garbage.
        if (str(1:1) == ";") then
            call my_mpi_abort("Bad binmode string: begins with semi-colon (;)", 0)
        end if

        ! Try to parse as integer bitmask, for backwards compatibility.
        read(str, *, iostat=ierr) bitmask
        if (ierr == 0) then
            if (bitmask < 0) bitmask = not(0)  ! all
            return
        end if

        ! Go through the string, checking that it is in the correct format,
        ! and identify the given fields, separated by ";".
        bitmask = 0
        b = 0
        do
            a = b + 1
            if (str(a:) == "") exit

            b = a - 1 + index(str(a:), ";")
            if (b < a) b = len(str) + 1  ! last one

            if (a == b) call my_mpi_abort("Bad binmode string: empty name", a)

            do i = 0, ubound(field_defs, dim=1)
                if (str(a:b-1) == field_defs(i)%name) then
                    if (iand(bitmask, 2**field_defs(i)%type) /= 0) then
                        call my_mpi_abort("Bad binmode string: " // &
                            trim(str(a:b-1)) // " given twice", 0)
                    end if

                    bitmask = ior(bitmask, 2**field_defs(i)%type)
                    exit
                end if
            end do
            if (i > ubound(field_defs, dim=1)) then
                print *, a, b, len(str)
                call my_mpi_abort("Bad binmode string: unknown field " // trim(str(a:b-1)), b-a)
            end if

            if (b > len(str)) exit
        end do

    end function


    subroutine initFilesequence(fileid, mode, name, overwrite, realsize)

        integer, intent(in) :: fileid, realsize, overwrite
        character(len=*), intent(in) :: mode, name

        integer :: i, ifield, bitmask

        if (fileid > SIZE(fds) .or. fileid < 1) then
            call my_mpi_abort('Invalid fileid for binary output.', fileid)
        end if

        if (realsize /= 4 .and. realsize /= 8) then
            call my_mpi_abort("Invalid realsize for binary input.", realsize)
        end if

        fds(fileid)%overwrite = overwrite
        fds(fileid)%filename = name
        fds(fileid)%frame = 0
        fds(fileid)%realsize = realsize

        ! Select which fields are to be saved (and where).
        bitmask = binmode_string_to_bitmask(mode)

        ifield = 0
        do i = 0, ubound(field_defs, dim=1)
            if (iand(bitmask, 2**field_defs(i)%type) /= 0) then
                ifield = ifield + 1
                fds(fileid)%fields = ifield
                fds(fileid)%fieldName(ifield) = field_defs(i)%name
                fds(fileid)%fieldUnit(ifield) = field_defs(i)%unit
                fds(fileid)%fieldType(ifield) = field_defs(i)%type
                if (realsize == 4) then
                    ! First one at offset 7
                    fds(fileid)%fieldOffset(ifield) = 6 + ifield
                else
                    ! First one at offset 10
                    fds(fileid)%fieldOffset(ifield) = 8 + 2 * ifield
                end if
            end if
        end do

    end subroutine initFilesequence


    !
    ! Initialize atom caches and do some sanity checking
    !
    subroutine initBinaryMode(per)

        real(kind=real64b) :: per(3)   ! 1.0d0 if periodic.

        ! Verify proper record lengths.
        if (invalidReclen()) then
            call my_mpi_abort('Invalid integer and real types for binary output.', myproc)
        end if

        ! This format can only support up to [+-]127 atom types.
        ! TODO why? This atom type is 4 bytes, not 1!
        if (itypehigh > 127 .or. itypelow < -127) then
            call my_mpi_abort('Too many atom types for binary output.', myproc)
        end if

        if (nprocs > maxCpus) then
            call my_mpi_abort('Too many cpus used, please increase maxCpus in binout.f90', nprocs)
        end if

        ! Allocate buffers
        ! TODO missing factor 2 from maxfields for double precision?
        allocate(buffer((5+maxfields) * maxAtom))
        allocate(header(28 + 2*maxfields + (itypehigh-itypelow+1) + 13*maxCpus))

        allocate(atomsperproc(nprocs))
        allocate(offsets(nprocs))
        allocate(rmnperproc(3, nprocs))
        allocate(rmxperproc(3, nprocs))

        ! Periodic boundary conditions?
        periodic(:) = (per(:) > 0.99 .and. per(:) < 1.01)

    end subroutine initBinaryMode


    ! The number of bytes required for one atom in the file.
    pure function bytes_per_atom(realsize, fields)
        integer, intent(in) :: realsize, fields
        integer :: bytes_per_atom

        ! atomindex + atype + x(3) + fields
        bytes_per_atom = 8 + 4 + realsize * (3 + fields)
    end function bytes_per_atom


    ! Read a frame of atoms from a restart.
    ! Number of CPUs can be the same or different from the restart.
    ! TODO: this silently assumes that the box size in the file is
    ! the same as the box size in md.in.
    subroutine binaryread(filename, append, natoms, myatoms, &
            atype, aindex, coord, velocity, dslice, ECM)

        character(len=*), intent(in) :: filename
        logical, intent(in)  :: append
        integer, intent(inout) :: natoms ! Total atoms on all procs
        integer, intent(inout) :: myatoms ! Local atoms for proc
        integer, contiguous, intent(inout) :: atype(:)
        integer, contiguous, intent(inout) :: aindex(:)
        real(kind=real64b), contiguous, intent(inout) :: coord(:), velocity(:)
        real(kind=real64b), intent(in), optional::  dslice(3), ECM(3)

        real(kind=real64b), dimension(3) :: scale_factor
        real(kind=real64b), dimension(3) :: tmprmn, tmprmx

        logical:: doslice
        integer :: u
        integer :: stat(MPI_STATUS_SIZE)
        integer :: atombytes, atomreal32s
        integer :: fileversion
        integer(kind=int64b) :: fileatoms
        integer ::filecpus
        integer :: fileitypehigh, fileitypelow
        integer :: fields
        integer :: vxi, vyi, vzi, ekini
        integer :: hi, i, j
        logical :: generateVelocities
        integer(kind=mpi_parameters) :: err
        integer :: readstrategy
        integer :: filerealsize
        integer(kind=int64b) :: offset
        real(kind=real64b) :: filebox(3)
        real(kind=real64b) :: vscale(3, -itypehigh : itypehigh)

        real(kind=real64b), allocatable :: filermn(:, :), filermx(:, :)
        integer, allocatable :: fileatomsperproc(:)

        integer :: nr_good
        integer, allocatable :: goodi(:)
        integer(kind=mpi_parameters), allocatable :: sizes(:)
        integer(kind=int64b), allocatable :: fileoffsets(:)
        integer(kind=mpi_parameters) :: fileview
        integer :: numReadAtoms
        integer :: numAtomsAdded, old_myatoms
        real(kind=real64b),parameter:: smallr8 = 1.0e-10
        real(kind=real64b) :: t1, t5, avetime, maxtime

        real(kind=real64b) :: ekin,vel,trialVel


        doslice = .false.
        if (present(dslice)) doslice = any(dslice(:) > 0)

        if (.not. append) then
            myatoms = 0
            natoms = 0
        end if

        old_myatoms = myatoms


        t1 = MPI_Wtime()

        if (myproc == 0) then
            write(*,*) "slice",doslice,dslice(:)
        end if

        call MPI_File_open(MPI_COMM_WORLD, filename,&
            IOR(MPI_MODE_RDONLY, MPI_MODE_UNIQUE_OPEN), MPI_INFO_NULL, u, err)
        if (err /= 0) then
            call my_mpi_abort('Error opening input file ' // trim(filename), err)
        end if


        ! Root process read collectively the header (+ a part of the beginning of the file)
        if (myproc == 0) then
            call MPI_File_Read(u, header, SIZE(header)*4, MPI_BYTE, stat, err)
            ! TODO: if the file is too short, this fails silently
        end if
        ! Broadcast header data to others
        call MPI_Bcast(header, SIZE(header)*4, MPI_BYTE, 0, MPI_COMM_WORLD, err)

        ! FIXME, here reallocate header if too small (maxcpus fix)

        ! Figure out the input's byte order.
        if (header(1) == protoReal) then
            swap_needed = .false.
        else if (swap32(TRANSFER(header(1), i4)) == TRANSFER(protoReal, i4)) then
            swap_needed = .true.
        else
            ! Either the current machine or the input is middle endian. Funny.
            call my_mpi_abort('Unsupported input byte order.', 0)
        end if

        ! header(2:2) = protoInt unused
        ! header(5:6) = descriptionOffset unused

        fileversion = get_int32(header(3))
        if (fileversion /= 4 .and. myproc == 0) then
            call logger("WARNING: binaryread: Found unexpected file format version", fileversion, 0)
            call logger("WARNING: Continuing, but expect problems!", 0)
        end if

        filerealsize = get_int32(header(4))
        fields = get_int32(header(12))
        fileatoms = get_int64(header(13:14))
        fileitypelow = get_int32(header(15))
        fileitypehigh = get_int32(header(16))
        filecpus = get_int32(header(17))
        filebox(1) = ABS(get_real64(header(22:23)))
        filebox(2) = ABS(get_real64(header(24:25)))
        filebox(3) = ABS(get_real64(header(26:27)))

        if (fileatoms > huge(natoms) - natoms) then
            call my_mpi_abort('binaryread: too many atoms. PARCAS only supports 2e9 atoms.', 0)
        end if

        if (filerealsize /= 4 .and. filerealsize /= 8) then
            call my_mpi_abort('binaryread: unexpected real number size', filerealsize)
        end if

        if (filecpus > maxCpus) then
            call my_mpi_abort('Too many cpus in input file, please increase maxCpus in binout.f90', err)
        end if

        if (fileitypelow /= itypelow .or. fileitypehigh /= itypehigh) then
            call my_mpi_abort('binaryread: types do not match', err)
        end if

        hi = 28 ! Number of bytes in header so far, hi == header index


        ! Find fields storing the velocity and ekin.
        vxi = -1
        vyi = -1
        vzi = -1
        ekini = -1
        do i = 1, fields
            if ("V.x " == TRANSFER(header(hi), c4)) then
                vxi = i
            end if
            if ("V.y " == TRANSFER(header(hi), c4)) then
                vyi = i
            end if
            if ("V.z " == TRANSFER(header(hi), c4)) then
                vzi = i
            end if
            if (vxi == i .or. vyi == i .or. vzi == i) then
                c4 = TRANSFER(header(hi+1), c4)
                if (c4 /= "A/fs") then
                    call my_mpi_abort("binaryread: Unknown unit '" // c4 // "' for velocity", i)
                end if
            end if

            if ("Ekin" == TRANSFER(header(hi), c4)) then
                ekini = i
                c4 = TRANSFER(header(hi+1), c4)
                if (c4 /= "eV  ") then
                    call my_mpi_abort("binaryread: Unknown unit '" // c4 // "' for Ekin", i)
                end if
            end if
            hi = hi + 2
        end do


        ! Check whether we need to generate random velocities based on
        ! read-in Ekin values.
        generateVelocities = .false.
        if (vxi < 0 .or. vyi < 0 .or. vzi < 0) then
            if (ekini < 0) then
                call my_mpi_abort('binaryread: No velocities or kinetic energies &
                    &in inputfile, cannot read or generate velocities', 1)
            else
                if (myproc == 0) then
                    write(*,*) "WARNING: generating random velocities based on kinetic energies"
                end if
                generateVelocities = .true.
            end if
        end if


        ! Check that the element names match (warn if not).
        do i = itypelow, itypehigh
            c4 = TRANSFER(header(hi), c4)
            if (c4 /= element(i)) then
                call logger("WARNING: binary read: unexpected element " // c4 // &
                    ", expected " // element(i) // " for type ", i, 0)
            end if
            hi = hi + 1
        end do


        allocate(fileatomsperproc(0 : filecpus-1))
        allocate(filermn(3, filecpus))
        allocate(filermx(3, filecpus))

        do i = 0, filecpus-1
            j = hi + i*13
            fileatomsperproc(i) = get_int32(header(j))

            filermn(1, i+1) = get_real64(header(j+ 1:j+ 2))
            filermx(1, i+1) = get_real64(header(j+ 3:j+ 4))
            filermn(2, i+1) = get_real64(header(j+ 5:j+ 6))
            filermx(2, i+1) = get_real64(header(j+ 7:j+ 8))
            filermn(3, i+1) = get_real64(header(j+ 9:j+10))
            filermx(3, i+1) = get_real64(header(j+11:j+12))
        end do


        ! readstrategy = 0 read in relevant areas of the file (some extra atoms need to be discarded)
        ! readstrategy = 1 all READ in parallel (num cpus same as in file)
        if(nprocs == filecpus) then
            readstrategy = 1
        else
            readstrategy = 0
        end if

        atombytes = bytes_per_atom(filerealsize, fields)
        atomreal32s = atombytes / 4

        if (readstrategy == 1) then
            ! Compute displacements and read in file for this process
            offset = get_int64(header(7:8))  ! Atom data offset
            do i = 1, myproc
                offset = offset + atombytes * fileatomsperproc(i-1)
            end do

            call MPI_File_read_at_all(u, offset, buffer, &
                atombytes * fileatomsperproc(myproc), &
                MPI_BYTE, MPI_STATUS_IGNORE, err)

            numReadAtoms = fileatomsperproc(myproc)
        else
            ! Determine read areas.
            ! Read in the data.
            ! Filter the data.

            allocate(fileoffsets(filecpus))
            allocate(sizes(filecpus))
            allocate(goodi(filecpus))

            ! Calculate every part's size in bytes.
            sizes(1:filecpus) = atombytes * fileatomsperproc(0:filecpus-1)

            ! Calculate every part's offset in bytes.
            fileoffsets(1) = get_int64(header(7:8))
            do i = 2, filecpus
                fileoffsets(i) = sizes(i-1) + fileoffsets(i-1)
            end do

            if (doslice) then
                ! scale_factor is used because when slicing, rmn and filermn etc. are
                ! not synced with each other. It used when determining which processors
                ! pieces to read in from file, when discarding individual atoms and
                ! finally it is used to scale the coordinates.
                scale_factor(:) = dslice(:) * 2d0 / filebox(:)
            end if

            ! Determine which input parts might contain particles for this process.
            nr_good = 0
            if (.not. doslice) then
                do i = 1, filecpus
                    if (all(filermx(:,i) >= rmn(:) - smallr8) .and. &
                        all(filermn(:,i) <= rmx(:) + smallr8)) then
                        nr_good = nr_good + 1
                        goodi(nr_good) = i
                    end if
                end do
            else ! slicing
                if (any(ECM(:) /= 0)) then
                    call my_mpi_abort('Input slicing not yet implemented for non-zero ECM.', 1)
                end if

                where (rmn < -0.5)
                    tmprmn = -0.5
                elsewhere
                    tmprmn = rmn
                end where

                where (rmx > 0.5)
                    tmprmx = 0.5
                elsewhere
                    tmprmx = rmx
                end where

                do i = 1, filecpus
                    if (all(filermx(:,i) >= scale_factor(:) * tmprmn(:)) .and. &
                        all(filermn(:,i) <= scale_factor(:) * tmprmx(:))) then
                        nr_good = nr_good + 1
                        goodi(nr_good) = i
                    end if
                end do
            end if

            call MPI_Type_create_hindexed(nr_good, sizes(goodi(1:nr_good)), &
                fileoffsets(goodi(1:nr_good)), MPI_BYTE, fileview, err)
            call MPI_Type_commit(fileview,err)
            call MPI_File_set_view(u, int(0, kind=MPI_OFFSET_KIND), MPI_BYTE, &
                fileview, 'native', MPI_INFO_NULL, err)

            call MPI_File_read_all(u, buffer, SUM(sizes(goodi(1:nr_good))), &
                MPI_BYTE, MPI_STATUS_IGNORE, err)

            numReadAtoms = SUM(fileatomsperproc(goodi(1:nr_good)-1))

            call MPI_Type_free(fileview,err)
            deallocate(fileoffsets)
            deallocate(sizes)
            deallocate(goodi)
        end if
        call MPI_File_close(u,err)

        ! Still need to fix delta(i), that is done later, elsewhere, in code.
        do i = fileitypelow, fileitypehigh
            vscale(:, i) = 1.0d0 / (vunit(i) * filebox(:))
            vscale(:,-i) = vscale(:, i)
        end do

        hi = 0 ! hi == buffer index, reuse header index var
        j = 1 + myatoms
        do i = 1, numReadAtoms
            ! First read the coordinates and check whether to use this atom.
            ! Scale coordinates to reduced units.
            if (filerealsize == 4) then
                coord(j*3-2) = get_real32(buffer(hi+4)) / filebox(1)
                coord(j*3-1) = get_real32(buffer(hi+5)) / filebox(2)
                coord(j*3-0) = get_real32(buffer(hi+6)) / filebox(3)
            else ! 8 byte real
                coord(j*3-2) = get_real64(buffer(hi+4:hi+5)) / filebox(1)
                coord(j*3-1) = get_real64(buffer(hi+6:hi+7)) / filebox(2)
                coord(j*3-0) = get_real64(buffer(hi+8:hi+9)) / filebox(3)
            end if

            if (.not. binaryread_use_atom(readstrategy, coord(3*j-2:3*j), rmn, rmx, &
                doslice, dslice, scale_factor, filebox, ECM)) then

                hi = hi + atomreal32s
                cycle
            end if

            if (doslice) then
                ! Center the coordinates to new center of mass.
                ! FIXME: what if filebox /= box
                coord(j*3-2 : j*3-0) = coord(j*3-2 : j*3-0) - ECM(:) / filebox(:)

                ! Scale so that coord in [-0.5, 0.5).
                ! FIXME: scale_factor not yet calculated if read_strategy == 1
                ! FIXME: scale_factor(i) not good when dslice(i)<0
                coord(j*3-2 : j*3-0) = coord(j*3-2 : j*3-0) / scale_factor(:)
            end if

            ! We want this atom. Read the rest of the fields.
            i8 = get_int64(buffer(hi+1:hi+2))
            if (i8 > huge(aindex(j))) call my_mpi_abort("Binary input: Atom index too large", 0)
            aindex(j) = int(i8)
            atype(j) = get_int32(buffer(hi+3))

            if (filerealsize == 4) then
                if (generateVelocities) then
                    ekin = get_real32(buffer(hi+6+ekini))
                else
                    velocity(j*3-2) = get_real32(buffer(hi+6+vxi))
                    velocity(j*3-1) = get_real32(buffer(hi+6+vyi))
                    velocity(j*3-0) = get_real32(buffer(hi+6+vzi))
                end if
            else ! 8 byte real
                if (generateVelocities) then
                    ekin =  get_real64(buffer(hi+8+2*ekini:hi+9+2*ekini))
                else
                    velocity(j*3-2) = get_real64(buffer(hi+8+2*vxi:hi+9+2*vxi))
                    velocity(j*3-1) = get_real64(buffer(hi+8+2*vyi:hi+9+2*vyi))
                    velocity(j*3-0) = get_real64(buffer(hi+8+2*vzi:hi+9+2*vzi))
                end if
            end if
            hi = hi + atomreal32s

            ! Generate velocities if they could not be read.
            if (generateVelocities) then
                ! Velocity of this kinetic energy in fs/A.
                vel = SQRT(2.0*ekin/mass(atype(j)))*9.822694727d-2

                ! Generate velocity components giving absolute velocity vel
                ! using a trial and error method.
                do
                    velocity(j*3-2) = 2.0 * MyRanf(0) - 1.0d0
                    velocity(j*3-1) = 2.0 * MyRanf(0) - 1.0d0
                    velocity(j*3-0) = 2.0 * MyRanf(0) - 1.0d0
                    trialVel = sqrt(sum(velocity(3*j-2 : 3*j-0)**2))
                    if (trialVel < 0.99) then
                        ! Scale velocities to the required velocity (to fs/A).
                        velocity(3*j-2:3*j-0) = velocity(3*j-2:3*j) * vel / trialVel
                        exit
                    end if
                end do
            end if

            velocity(3*j-2:3*j) = velocity(3*j-2:3*j) * vscale(:, atype(j))

            j = j + 1
        end do

        myatoms = j - 1


        deallocate(fileatomsperproc)
        deallocate(filermn)
        deallocate(filermx)
        t5 = MPI_Wtime()

        call MPI_Reduce(t5-t1, avetime,1 ,MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, err)
        call MPI_Reduce(t5-t1, maxtime,1 ,MPI_DOUBLE_PRECISION, MPI_MAX, 0, MPI_COMM_WORLD, err)
        avetime = avetime / nprocs
        if (myproc == 0) then
            write(*,*) "IO-read tot time", avetime, maxtime
        end if


        ! Compute number of atoms read in
        numAtomsAdded = myatoms - old_myatoms
        call MPI_Allreduce(MPI_IN_PLACE, numAtomsAdded, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, err)

        ! natoms now contains total number of atoms read in
        ! (and atoms read in earlier if appending)
        natoms = natoms + numAtomsAdded

        ! Check number of atoms read in
        if (fileatoms /= numAtomsAdded) then
            if (doslice) then
                if (myproc == 0) then
                    write(*,*) "Slicing restart file: fileatoms,readatoms:",fileatoms,numAtomsAdded
                end if
            else
                block
                    ! If no slicing, then all atoms in file should have been read in.
                    ! Save the read in data for debugging. This expects file number 3
                    ! to be the restart file.
                    real(real64b) :: delta(itypelow:itypehigh)
                    delta(:) = 1d0

                    call binarysave(3, myatoms, 0, atype, aindex, 0d0, 0d0, &
                        filebox, coord, velocity, &
                        velocity, velocity, velocity, & ! correspond to wxxi input, not used
                        velocity, velocity, velocity, & ! correspond to wxyi input, not used
                        velocity, velocity, velocity, & ! energy inputs, not used
                        delta, .false.)
                end block

                call my_mpi_abort('Did not read in all atoms in file &
                    &(new restart in out for debugging), fatal error', &
                    int(numAtomsAdded - fileatoms))
            end if
        end if

    end subroutine binaryread


    pure function binaryread_use_atom(readstrategy, x, rmn, rmx, &
            doslice, dslice, scale_factor, filebox, ECM) result(useatom)

        integer, intent(in) :: readstrategy
        real(real64b), intent(in) :: x(3), rmn(3), rmx(3)
        logical, intent(in) :: doslice
        real(real64b), intent(in) :: dslice(3), scale_factor(3)
        real(real64b), intent(in) :: filebox(3), ECM(3)
        logical :: useatom

        real(real64b) :: dx(3)

        if (readstrategy == 1) then
            useatom = .true.
        else
            useatom = .false.
            if (.not. doslice) then
                ! Check whether the particle is within this process' box. Accept
                ! the particle only if it is.
                ! Also accept if it seems to be outside of system
                ! (this should not happen though)
                if (all(x(:) >= rmn(:) .and. x(:) < rmx(:))) then
                    useatom = .true.
                else if (any(rmn(:) == -0.5d0 .and. x(:) <= -0.5d0)) then
                    useatom = .true.
                else if (any(rmx(:) == +0.5d0 .and. x(:) >= +0.5d0)) then
                    useatom = .true.
                end if
            else
                ! Reminder: scale_factor(:) == (dslice(:) * 2d0 / filebox(:))
                if (all(x(:) >= scale_factor(:)*rmn(:)) .and. &
                    all(x(:) <  scale_factor(:)*rmx(:))) then
                    useatom = .true.
                end if

                ! Accepted atoms seemingly inside the region. When using
                ! open boundaries, rmn is set to -999d30 for the
                ! processor in the edge. This is not what we want
                ! here. For boundary processes, reject more atoms.
                if (any(rmn(:) < -0.5d0 .and. x(:) <= -0.5d0 * scale_factor(:)) .or. &
                    any(rmx(:) > +0.5d0 .and. x(:) >= +0.5d0 * scale_factor(:))) then
                    useatom = .false.
                end if
            end if
        end if

        ! Is inside processors box. Now check if it is inside slice.
        if (useatom .and. doslice) then
            ! Use an atom region around ECM defined by dslice
            ! The atom selection criteria is as follows:
            ! For each dimension q=1,2,3
            !    - If dslice(q)<0 accept atom in this dimension
            !    - Else if ECM(q)-dslice(q) < x_q < ECM(q)+dslice(q) (with periodics)
            !      accept atom i in this dimension
            !    - If atom is accepted in each dimension, put it in buffer, send
            !      to processor one for printing
            ! TODO: what about periodics?

            dx(:) = abs(x(:) * filebox(:) - ECM(:))
            useatom = all(dslice(:) < 0 .or. dx(:) < dslice(:))
        end if

    end function binaryread_use_atom


    ! Save a frame of atoms.
    subroutine binarysave(fileid, myatoms, frameid, atype, aindex, &
            time_fs, duration_fs, box, &
            coord, velocity, &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
            Epair, Enonpair, Ekin, delta, &
            doslice, dslice, ECM)

        integer, intent(in) :: fileid, myatoms, frameid
        integer, contiguous, intent(in) :: atype(:), aindex(:)
        real(real64b), intent(in) :: time_fs, duration_fs, box(3)
        real(real64b), contiguous, intent(in) :: coord(:), velocity(:)
        real(real64b), contiguous, intent(in) :: wxxi(:), wyyi(:), wzzi(:)
        real(real64b), contiguous, intent(in) :: wxyi(:), wxzi(:), wyzi(:)
        real(real64b), contiguous, intent(in) :: Epair(:), Enonpair(:), Ekin(:)
        real(real64b), intent(in) :: delta(itypelow:itypehigh)
        logical, intent(in):: doslice
        real(real64b), intent(in), optional::  dslice(3), ECM(3)

        integer :: fields
        real(kind=real64b) :: vscale(3, -itypehigh : itypehigh)
        real(kind=real64b) :: dx(3)

        integer :: i, hi
        real(kind=real64b) :: t1,t2,t3,t4,t5,t6,t7,t8,avetime,maxtime
        integer(kind=int64b) :: offset
        integer(kind=int64b) :: totatoms
        integer(kind=mpi_parameters) :: err
        integer :: u
        integer :: atombytes
        integer :: pos
        integer :: outatoms
        character(len=22) :: realfilename
        logical :: useatom
        integer :: foffset, ifield

        integer(kind=mpi_parameters) :: ionprocs  ! number of io processes
        integer(kind=mpi_parameters) :: ioprocess  ! denotes if this is a process writing out data
        integer(kind=mpi_parameters) :: iocommunicator ! communicator that does the IO

        t1 = MPI_Wtime()

        fields = fds(fileid)%fields

        ! Velocity scale (for each axis, for each atom type).
        do i = itypelow, itypehigh
            vscale(1:3, i) = (vunit(i) / delta(i)) * box(1:3)
            vscale(1:3,-i) = vscale(1:3, i)
        end do


        t2 = MPI_Wtime()
        ! Stuff the buffer with data and compute how many atoms we will write to disk.
        ! Not the fastest way to do it, with all the if()s, but we would anyway need to
        ! check for each atoms if they are OK. This part should not be a bottleneck in the I/O
        pos = 0
        outatoms = 0
        do i = 1, myatoms
            useatom = .true.
            if (doslice) then
                ! Use an atom region around ECM defined by dslice
                ! The atom selection criteria is as follows:
                ! For each dimension q=1,2,3
                !    - If dslice(q)<0 accept atom in this dimension
                !    - Else if ECM(q)-dslice(q) < x_q < ECM(q)+dslice(q) (with periodics)
                !      accept atom i in this dimension
                !    - If atom is accepted in each dimension, put it in buffer, send
                !      to processor one for printing
                ! TODO what about periodics?
                dx(:) = abs(coord(3*i-2:3*i) * box(:) - ECM(:))

                useatom = all(dslice(:) < 0 .or. dx(:) < dslice(:))
            end if

            if (.not. useatom) cycle

            outatoms = outatoms + 1

            ! First write out aindex and atype.
            i8 = aindex(i)
            buffer(pos+1:pos+2) = TRANSFER(i8, r42)
            buffer(pos+3) = TRANSFER(atype(i), r4)

            ! Then scalar fields, two cases depending on realsize.
            if (fds(fileid)%realsize == 4) then
                buffer(pos+4) = real(coord(i*3-2)*box(1), real32b)
                buffer(pos+5) = real(coord(i*3-1)*box(2), real32b)
                buffer(pos+6) = real(coord(i*3-0)*box(3), real32b)

                do ifield = 1, fds(fileid)%fields
                    ! Includes space for aindex, atype, coord.
                    foffset = pos + fds(fileid)%fieldOffset(ifield)

                    select case (fds(fileid)%fieldType(ifield))
                    case (FIELD_EPOT)
                        buffer(foffset) = real(Epair(i) + Enonpair(i), real32b)
                    case (FIELD_EKIN)
                        buffer(foffset) = real(Ekin(i), real32b)
                    case (FIELD_VX)
                        buffer(foffset) = real(velocity(i*3-2) * vscale(1, atype(i)), real32b)
                    case (FIELD_VY)
                        buffer(foffset) = real(velocity(i*3-1) * vscale(2, atype(i)), real32b)
                    case (FIELD_VZ)
                        buffer(foffset) = real(velocity(i*3-0) * vscale(3, atype(i)), real32b)
                    case (FIELD_WXX)
                        buffer(foffset) = real(wxxi(i), real32b)
                    case (FIELD_WYY)
                        buffer(foffset) = real(wyyi(i), real32b)
                    case (FIELD_WZZ)
                        buffer(foffset) = real(wzzi(i), real32b)
                    case (FIELD_WXY)
                        buffer(foffset) = real(wxyi(i), real32b)
                    case (FIELD_WXZ)
                        buffer(foffset) = real(wxzi(i), real32b)
                    case (FIELD_WYZ)
                        buffer(foffset) = real(wyzi(i), real32b)
                    case default
                        call my_mpi_abort("BUG: Unknown field in binarysave", &
                            fds(fileid)%fieldType(ifield))
                    end select
                end do

                pos = pos + (6+fields)
                if (pos + (6+fields) > SIZE(buffer)) then
                    ! FIXME, fail more gracefully
                    call my_mpi_abort('buffer in binout is too small, sorry!.', 0)
                end if

            else ! 8 byte reals
                buffer(pos+4:pos+5) = TRANSFER(coord(i*3-2)*box(1), r42)
                buffer(pos+6:pos+7) = TRANSFER(coord(i*3-1)*box(2), r42)
                buffer(pos+8:pos+9) = TRANSFER(coord(i*3-0)*box(3), r42)

                do ifield = 1, fds(fileid)%fields
                    ! Includes space for aindex, atype, coord, and takes care of realsize.
                    foffset = pos + fds(fileid)%fieldOffset(ifield)

                    select case (fds(fileid)%fieldType(ifield))
                    case (FIELD_EPOT)
                        buffer(foffset:foffset+1) = TRANSFER(Epair(i) + Enonpair(i), r42)
                    case (FIELD_EKIN)
                        buffer(foffset:foffset+1) = TRANSFER(Ekin(i), r42)
                    case (FIELD_VX)
                        buffer(foffset:foffset+1) = TRANSFER(velocity(i*3-2) * vscale(1, atype(i)), r42)
                    case (FIELD_VY)
                        buffer(foffset:foffset+1) = TRANSFER(velocity(i*3-1) * vscale(2, atype(i)), r42)
                    case (FIELD_VZ)
                        buffer(foffset:foffset+1) = TRANSFER(velocity(i*3-0) * vscale(3, atype(i)), r42)
                    case (FIELD_WXX)
                        buffer(foffset:foffset+1) = TRANSFER(wxxi(i), r42)
                    case (FIELD_WYY)
                        buffer(foffset:foffset+1) = TRANSFER(wyyi(i), r42)
                    case (FIELD_WZZ)
                        buffer(foffset:foffset+1) = TRANSFER(wzzi(i), r42)
                    case (FIELD_WXY)
                        buffer(foffset:foffset+1) = TRANSFER(wxyi(i), r42)
                    case (FIELD_WXZ)
                        buffer(foffset:foffset+1) = TRANSFER(wxzi(i), r42)
                    case (FIELD_WYZ)
                        buffer(foffset:foffset+1) = TRANSFER(wyzi(i), r42)
                    case default
                        call my_mpi_abort("BUG: Unknown field in binarysave", &
                            fds(fileid)%fieldType(ifield))
                    end select
                end do

                pos = pos + (9 + 2*fields)
                if (pos + (9 + 2*fields) > SIZE(buffer)) then
                    !FIXME, fail more gracefully
                    call my_mpi_abort('buffer in binout is too small, sorry!.', 0)
                end if
            end if
        end do

        t3 = MPI_Wtime()
        ! Collect header data to myproc=0
        call MPI_Gather(outatoms, 1, MY_MPI_INTEGER, atomsperproc, &
            1, MY_MPI_INTEGER, 0, MPI_COMM_WORLD, err)
        call MPI_Gather(rmn, 3, MPI_DOUBLE_PRECISION, rmnperproc, &
            3, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, err)
        call MPI_Gather(rmx, 3, MPI_DOUBLE_PRECISION, rmxperproc, &
            3, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, err)


        atombytes = bytes_per_atom(fds(fileid)%realsize, fields)


        hi = 0 ! Silence erroneous compiler warning

        if (myproc == 0) then
            ! Cannot simply use sum(atomsperproc) since that might overflow
            ! a 32-bit value.
            totatoms = 0
            do i = 1, nprocs
                totatoms = totatoms + atomsperproc(i)
            end do

            ! Write header data.
            header(1) = protoReal
            header(2) = TRANSFER(protoInt, r4)
            header(3) = TRANSFER(WRITE_FILE_VERSION, r4) ! version for file
            header(4) = TRANSFER(fds(fileid)%realsize, r4) ! 4 for reals, 8 for doubles
            header(5:6) = 0  ! Description offset, in bytes
            header(7:8) = 0  ! Atom data offset, in bytes (filled later)
            header(9) = TRANSFER(frameid, r4) ! which frame in a series of frames
            header(10) = TRANSFER(0, r4) ! part number, fixed to 0 as we write one file
            header(11) = TRANSFER(1, r4) ! number of parts, fixed to 1 as we write one file
            header(12) = TRANSFER(fields, r4)
            header(13:14)= TRANSFER(totatoms, r42)
            header(15) = TRANSFER(itypelow, r4)
            header(16) = TRANSFER(itypehigh, r4)
            header(17) = TRANSFER(nprocs, r4)

            header(18:19) = TRANSFER(time_fs * 1d-15, r42)
            header(20:21) = TRANSFER(duration_fs * 1d-15, r42)
            header(22:23) = TRANSFER(ABS(box(1)), r42)
            header(24:25) = TRANSFER(ABS(box(2)), r42)
            header(26:27) = TRANSFER(ABS(box(3)), r42)
            if (periodic(1)) header(22:23) = TRANSFER(-ABS(box(1)), r42)
            if (periodic(2)) header(24:25) = TRANSFER(-ABS(box(2)), r42)
            if (periodic(3)) header(26:27) = TRANSFER(-ABS(box(3)), r42)

            hi = 28

            ! Note that fieldnames should be exactly 4 characters.
            do i = 1, fields
                header(hi)   = TRANSFER(fds(fileid)%fieldName(i), r4)
                header(hi+1) = TRANSFER(fds(fileid)%fieldUnit(i), r4)
                hi = hi + 2
            end do
            do i = itypelow, itypehigh
                header(hi) = TRANSFER(element(i), r4)
                hi=hi + 1
            end do
            do i = 1, nprocs
                header(hi) = TRANSFER(atomsperproc(i), r4)
                header(hi+ 1:hi+ 2) = TRANSFER(rmnperproc(1, i), r42)  ! X min
                header(hi+ 3:hi+ 4) = TRANSFER(rmxperproc(1, i), r42)  ! X max
                header(hi+ 5:hi+ 6) = TRANSFER(rmnperproc(2, i), r42)  ! Y min
                header(hi+ 7:hi+ 8) = TRANSFER(rmxperproc(2, i), r42)  ! Y max
                header(hi+ 9:hi+10) = TRANSFER(rmnperproc(3, i), r42)  ! Z max
                header(hi+11:hi+12) = TRANSFER(rmxperproc(3, i), r42)  ! Z max
                hi = hi + 13
            end do
            hi = hi - 1 ! Correct hi to compute offsets and size of header to write.

            ! The size of the header is known now. Set the description and atom
            ! data offsets. The offsets are the same since no description is
            ! written for now.
            header(7:8) = TRANSFER(hi * 4_8, r42)
            header(5:6) = header(7:8)  ! There is no description.

            ! Compute displacements in the file for all processes. Process 0
            ! writes straight after the header, process 1 writes straight
            ! after process 0's data and so on. All the offsets must be in
            ! 64-bit integers.
            offsets(1) = hi * 4_8 ! header size
            do i = 2, nprocs
                offsets(i) = atombytes * atomsperproc(i-1) + offsets(i-1)
            end do
        end if

        call MPI_Scatter(offsets, 1, MPI_INTEGER8, offset, &
            1, MPI_INTEGER8, 0, MPI_COMM_WORLD, err)

        t4 = MPI_Wtime()


        ! Create IO communicator.
        if (myproc == 0 .or. outatoms > 0) then
            ioprocess = 1
        else
            ! Do not create communicator for other processes.
            ioprocess = MPI_UNDEFINED
        end if

        call MPI_Comm_split(MPI_COMM_WORLD, ioprocess, myproc, iocommunicator, err)


        t5 = MPI_Wtime()
        ! Only processes doing IO are involved here.
        if (ioprocess == 1) then
            call MPI_Comm_size(iocommunicator, ionprocs, err)

            ! Create file name for this frame.
            if (fds(fileid)%overwrite > 0) then
                realfilename = fds(fileid)%filename
            else
                ! FIXME, name is cut off
                write (realfilename, '(A10,I7.7)') fds(fileid)%filename, fds(fileid)%frame
            end if

            if (myproc == 0) then
                call MPI_File_delete(realfilename, MPI_INFO_NULL, err)
                ! Only root creates the file, this reduces the number of metadataoperations at scale.
                ! TODO really? Shouldn't MPI_File_open be optimized for this?
                open(TEMP_FILE, file=realfilename)
                close(TEMP_FILE)
            end if

            ! Wait for file to be created.
            call MPI_Barrier(iocommunicator, err)

            ! Open file.
            call MPI_File_open(iocommunicator, realfilename, &
                ior(MPI_MODE_WRONLY, MPI_MODE_UNIQUE_OPEN), &
                MPI_INFO_NULL, u, err)
            if (err /= MPI_SUCCESS) then
                call my_mpi_abort('Error writing binary output file ' // trim(realfilename), err)
            end if
            t6 = MPI_Wtime()

            if (myproc == 0) then
                call MPI_File_write_at(u, 0_8, header, 4 * hi, MPI_BYTE, MPI_STATUS_IGNORE, err)
                if (err /= MPI_SUCCESS) then
                    call my_mpi_abort("Error writing binary header to output file", err)
                end if
            end if

            call MPI_File_write_at_all(u, offset, buffer, atombytes*outatoms, &
                MPI_BYTE, MPI_STATUS_IGNORE, err)

            if (err /= MPI_SUCCESS) then
                call my_mpi_abort('Error writing binary output file data with collective MPI-IO.', err)
            end if
            t7 = MPI_Wtime()
            call MPI_File_close(u, err)

            t8 = MPI_Wtime()


            call MPI_Reduce(t8-t1,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
            call MPI_Reduce(t8-t1,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
            avetime = avetime / nprocs
            if (myproc == 0) then
                write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO tot time ",avetime,maxtime,ionprocs
            end if

            call MPI_Reduce(t6-t5,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
            call MPI_Reduce(t6-t5,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
            avetime = avetime / nprocs
            if (myproc == 0) then
                write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO fopen time",avetime,maxtime,ionprocs
            end if

            call MPI_Reduce(t7-t6,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
            call MPI_Reduce(t7-t6,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
            avetime = avetime / nprocs
            if (myproc == 0) then
                write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO write time",avetime,maxtime,ionprocs
            end if

            call MPI_Reduce(t8-t7,avetime,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,iocommunicator,err)
            call MPI_Reduce(t8-t7,maxtime,1,MPI_DOUBLE_PRECISION,MPI_MAX,0,iocommunicator,err)
            avetime = avetime / nprocs
            if (myproc == 0) then
                write(*,'(I3," ",A,2E11.4,I7)') fileid,"IO fclose time",avetime,maxtime,ionprocs
            end if

            call MPI_Comm_free(iocommunicator, err)
        end if

        ! Frame saved successfully.
        fds(fileid)%frame = fds(fileid)%frame + 1

        ! Barrier not needed but lets keep it here to avoid any nasty bugs
        ! or apparent load imbalance elsewhere.
        call my_mpi_barrier()

    end subroutine binarysave


end module binout
