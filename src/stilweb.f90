!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

!
! Code originates from ancient HARWELL code, from which it was modified
! for PARCAS by Kai Nordlund in Jan 1997.
!

module stillinger_weber_mod

    use output_logger, only: log_buf, logger

    use datatypes, only: real64b
    use defs, only: NNMAX
    use typeparam, only: iac, rcut, itypelow, itypehigh, element
    use my_mpi
    use splinereppot_mod, only: reppot_only, reppot_fermi

    use timers, only: tmr, TMR_SEMICON_COMMS, TMR_SEMICON_REPPOT

    use para_common, only: &
        myatoms, np0pairtable, &
        buf

    use mdparsubs_mod, only: potential_pass_back_border_atoms


    implicit none
    save

    private
    public :: Init_SW_Pot
    public :: Stilweb_Force


    !
    ! Si & Ge potential parameters: Column 1 is for Si-Si
    !                               Column 2 is for Ge-Ge
    !                               Column 3 is for Si-Ge
    !
    ! Future extension: 4 Si-O, 5 O-O, 6 C, 7 C-Si, 8 C-Ge, 9 Sn etc.
    !
    ! Si parameters are from Stillinger and Weber PRB 31 (1985) 6987.
    ! Ge parameters are from Ding and Andersen PRB 34 (1986) 6987.
    ! Alternative Ge (lambda=21,eps=1.918): Wang and Stroud PRB 38 (1988) 1384
    !
    ! The strength of the entire potential epsilon can be modified with the
    ! sw2mod readin parameter, and the strength of the three-body part (ALAM)
    ! independently with the sw3mod readin parameter
    !
    ! The atom types in atype(:) and typeparam are used in the following way:
    !
    ! On init, all type combinations are looped through. Recognized
    ! iac(it,jt)==1 combinations are set to the correct IATAB value.
    ! For others, a warning is issued.


    ! Map from Parcas atom types (atype) to indexes (iaind) in param arrays.
    integer, allocatable :: iatab(:,:)

    ! Parameter arrays.
    real(real64b) :: ALAM(3), EPSA(3), EPS(3), SIGA(3), SIG(3), EPSLAM(3), GAMSIG(3), R0SQ(3)
    real(real64b) :: altALAM(1), altEPSA(1), altEPS(1), altSIGA(1), altSIG(1), altEPSLAM(1), altGAMSIG(1)
    ! epsilon * lambda for three-body term, as mix of the
    ! two epslam values. Indexes: (iaind, iaind2)
    real(real64b) :: EPSLAM_MNL(3,3)


    ! p = 4, q = 0 hard-coded for speed.
    ! B common for all parameterizations.
    real(real64b), parameter :: Bpar = 0.6022245584

    data ALAM  /21.0   , 31.0   , 25.51  /   ! lambda
    data EPS   /2.16722, 1.93   , 2.0451 /   ! epsilon (eV)
    data EPSA  /15.2764, 13.6056, 14.4170/   ! epsilon*A (eV)
    data SIG   /2.0951 , 2.181  , 2.1376 /   ! sigma (A)
    data SIGA  /3.77118, 3.9258 , 3.84768/   ! sigma*a (A)
    data EPSLAM/45.507 , 59.83  , 52.171 /   ! epsilon*lambda
    data GAMSIG/2.51412, 2.6172 , 2.56512/   ! gamma*sigma


    ! Alternative parametrizations
    ! 1: Alternative Ge used by Matthias Posselt, Phys. Rev. B 80, 045202

    data altALAM   /19.5d0         /   ! lambda
    data altEPS    /1.93d0         /   ! epsilon (eV)
    data altEPSA   /13.6056436146d0/   ! epsilon*A (eV)
    data altSIG    /2.181d0        /   ! sigma (A)
    data altSIGA   /3.9258d0       /   ! sigma*a (A)
    data altEPSLAM /37.635d0       /   ! epsilon*lambda
    data altGAMSIG /2.59539d0      /   ! gamma*sigma


    ! Fermi joining parameters.
    ! Si-Si 15,1.4 checked: gave threshold displ. energy 18 eV,
    ! looked good, energy conserved with both ZBL and DMol.
    real(real64b), parameter :: &
        bf(3) = [15.0, 15.0, 15.0], &
        rf(3) = [1.4, 1.5, 1.45]

    ! Ugly repulsive potential cutoff to speed things up a bit
    ! At 2.2, Si fermi function derivative is ~1e-4
    real(real64b) :: reppotcut(3) = [2.2, 2.3, 2.25]


contains


    subroutine Stilweb_Force(x0, atype, xnp, box, pbc, &
            nborlist, Epair, Ethree, &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, calc_vir)

        !
        ! Input:
        !   x0            contains positions in A scaled by 1/box,
        !   myatoms       Number of atoms in my node, in para_common.f90
        !   np0pairtable  myatoms + number of of atoms from neighbor nodes
        !   box(3)        Box size (box centered on 0)
        !   pbc(3)        Periodics: if = 1.0d0 periodic
        !
        ! Output:
        !   xnp           contains forces in eV/A scaled by 1/box
        !   Epair         V_2 per atom in eV
        !   Ethree        V_3 per atom in eV
        !   wxxi,...      virials per atom in eV
        !

        ! Variables passed in and out

        real(kind=real64b), contiguous, intent(out) :: xnp(:)
        real(kind=real64b), contiguous, intent(out) :: Epair(:), Ethree(:)
        real(kind=real64b), contiguous, intent(out) :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)

        real(kind=real64b), contiguous, intent(in) :: x0(:)
        real(kind=real64b), intent(in) :: box(3), pbc(3)
        integer, contiguous, intent(in) :: atype(:)
        integer, contiguous, intent(in) :: nborlist(:)

        logical, intent(in) :: calc_vir


        ! Local variables

        real(kind=real64b) :: AI,daix,daiy,daiz
        real(kind=real64b) :: BOX2X,BOX2Y,BOX2Z
        real(kind=real64b) :: PH,DPH,PH1,PH2,HALFPH
        real(kind=real64b) :: COSMNL,COSS,DPHN,DPHM,DPHL

        real(kind=real64b) :: DXNL,DXNM,DXNMS
        real(kind=real64b) :: DYNL,DYNM,DYNMS,DYNMSM
        real(kind=real64b) :: DZNL,DZNM,DZNMS,DZNMSM

        real(kind=real64b) :: HCOS,HMNL,HKMNL,HKMNLL,HKMNLM
        real(kind=real64b) :: RNL,RNLA,RNLEF,RNLSQ,RNM,RNMA,RNMEF,RNMSQ

        integer :: K,K1,N,M,L,IATN,IATM,IATL,IATYP1,IATYP2,IATYP3
        integer :: IAIND,IAIND2

        integer :: ingbr,jngbr,nngbr, nbor_start
        logical :: is_pbc(3)

        real(real64b), dimension(NNMAX) :: dx_arr, dy_arr, dz_arr, r_arr

        integer :: i
        real(kind=real64b) :: t1,dummy1,dummy2


        !-------------------------------------------------------------------------------!

        ! Convert from internal units to Angstrom.
        ! Use buf(i) instead of x0(i) in the computations.
        do i = 1, 3*np0pairtable, 3
            buf(i+0) = x0(i+0) * box(1)
            buf(i+1) = x0(i+1) * box(2)
            buf(i+2) = x0(i+2) * box(3)
        end do


        !-----------------------------------------------------------------------------
        ! Compute the accelerations into xnp in eV/A. Later converted to
        ! internal units (divided by box). Compute energies into Epair and Ethree.
        ! Compute virials into wxxi,... in eV.
        !
        ! Take care to calculate pressure, total pot. only for atoms in my node!
        !
        ! rnmef and rnlef are the differences between distance and the potential cutoff.
        !
        ! N.B. No particles are moved at this stage.
        !

        ! Initialize force calculation stuff

        is_pbc(:) = (pbc(:) == 1d0)

        box2x = box(1) / 2.0
        box2y = box(2) / 2.0
        box2z = box(3) / 2.0

        xnp(:3*np0pairtable) = 0.0
        Epair(:np0pairtable) = 0.0
        Ethree(1 : myatoms) = 0.0
        wxxi(1 : myatoms) = 0.0
        wyyi(1 : myatoms) = 0.0
        wzzi(1 : myatoms) = 0.0
        if (calc_vir) then
            wxyi(1 : myatoms) = 0.0
            wxzi(1 : myatoms) = 0.0
            wyzi(1 : myatoms) = 0.0
        end if


        !--------------------------------------------------------------
        !
        ! Calculate accelerations using neighbour list
        ! --------------------------------------------
        !
        ! Loop structure:
        !
        ! Loop over all of my atoms N:
        !     Loop over all neighbors M from first to last:
        !         Compute distance from N to M
        !         Save dx/dy/dz/r in arrays
        !         Compute pair potential
        !         Loop over neighbors L from first to M-1'th:
        !             Fetch distances from arrays
        !             Compute three-body potential
        !
        ! This way the distance to each neighbor has to be computed only once,
        ! and the second loop can still be in one piece.
        !

        K = 0  ! index into neighbor list

        do IATN=1,myatoms             !  <------- main loop over atoms N start
            N=IATN*3-2
            IATYP1=abs(atype(iatn))

            K = K + 1
            nngbr = nborlist(K)
            if (nngbr < 0 .or. nngbr > NNMAX) then
                print *,'stilweb nborlist HORROR ERROR !',nngbr
            endif

            nbor_start = K

            do ingbr=1,nngbr       !  <----- loop over neighbours  M start

                K = K + 1
                iatm = nborlist(K)
                M = iatm*3-2

                iatyp2 = abs(atype(iatm))

                if (iac(iatyp1,iatyp2) /= 1) then
                    ! Handle other interaction types
                    if (iac(iatyp1,iatyp2) == 0) cycle
                    if (iac(iatyp1,iatyp2) == -1) then
                        call logger("ERROR: IMPOSSIBLE INTERACTION")
                        call logger("myproc:", myproc, 4)
                        call logger("iatn:  ", iatn, 4)
                        call logger("iatm:  ", iatm, 4)
                        call logger("iatyp1:", iatyp1, 4)
                        call logger("iatyp2:", iatyp2, 4)
                        call my_mpi_abort('Impossible interaction iac = -1', myproc)
                    end if
                end if

                IAIND=IATAB(IATYP1,IATYP2)

                ! Skip round the calculation if separation exceeds r0.

                ! Mark that we should skip this neighbor in the 3-body loop.
                ! This is overwritten with the real value below unless the
                ! neighbor is beyond the cutoff.
                r_arr(ingbr) = -1d0

                DXNM = buf(N) - buf(M)
                if(is_pbc(1)) then
                    if (dxnm >= box2x) then
                        dxnm = dxnm - 2 * box2x
                    else if (dxnm < -box2x) then
                        dxnm = dxnm + 2 * box2x
                    end if
                end if
                dx_arr(ingbr) = dxnm
                DXNMS=DXNM*DXNM
                if(DXNMS.gt.R0SQ(IAIND)) cycle

                DYNMSM = R0SQ(IAIND) - DXNMS
                DYNM = buf(N+1) - buf(M+1)
                if(is_pbc(2)) then
                    if (dynm >= box2y) then
                        dynm = dynm - 2 * box2y
                    else if (dynm < -box2y) then
                        dynm = dynm + 2 * box2y
                    end if
                end if
                dy_arr(ingbr) = dynm
                DYNMS=DYNM*DYNM
                if(DYNMS.gt.DYNMSM) cycle

                DZNMSM = DYNMSM- DYNMS
                DZNM = buf(N+2) - buf(M+2)
                if(is_pbc(3)) then
                    if (dznm >= box2z) then
                        dznm = dznm - 2 * box2z
                    else if (dznm < -box2z) then
                        dznm = dznm + 2 * box2z
                    end if
                end if
                dz_arr(ingbr) = dznm
                DZNMS=DZNM*DZNM
                if(DZNMS.gt.DZNMSM) cycle

                RNMSQ = DXNMS + DYNMS + DZNMS
                RNM = sqrt(RNMSQ)
                RNMEF = RNM - SIGA(IAIND)

                ! Check for cutoff (skip over if exceeded).
                ! According to stilweb publication, also 3-body
                ! part should be skipped if this is less than cutoff.
                ! The checks against r0sq above actually do the same
                ! thing, but this is safer against numerical error.
                if (rnmef > -1d-8) cycle

                r_arr(ingbr) = rnm


                ! Two particle potential

                ! Use Newtons third law for 2-body part; take care
                ! to pick exactly half of all interactions.

                if (DXNM > 0 .or. (DXNM == 0 .and. DYNM > 0) .or. &
                    (DXNM == 0 .and. DYNM == 0 .and. DZNM > 0)) then

                    if (iac(iatyp1,iatyp2) == 2) then

                        call reppot_only(rnm, ph, dph, iatyp1, iatyp2)

                    else

                        ! Stillinger-Weber pair potential

                        PH1 = EPSA(IAIND) * exp(SIG(IAIND) / RNMEF)
                        PH2 = PH1 * Bpar /  (RNM / SIG(IAIND))**4

                        PH = PH2 - PH1
                        DPH = -4 * PH2 / RNM - SIG(IAIND) * PH / RNMEF**2

                        ! Add repulsive pair potential times Fermi function

                        if (rnm < reppotcut(iaind)) then
                            t1 = mpi_wtime()
                            call reppot_fermi(rnm, ph, dph, bf(iaind), rf(iaind), &
                                iatyp1, iatyp2, dummy1, dummy2)
                            tmr(TMR_SEMICON_REPPOT) = tmr(TMR_SEMICON_REPPOT) + (mpi_wtime()-t1)
                        end if

                    end if

                    ! If no Newton III:
                    !PH = PH * 0.5d0
                    !DPH = DPH * 0.5d0

                    ! Calculate physical properties
                    ! daix = (x/r) (d POT/dr) = vector component of force/accelerations

                    ! The energy ph computed above is for the atom pair, so divide it
                    ! equally between the atoms.

                    HALFPH=PH/2.0
                    Epair(IATN)=Epair(IATN)+HALFPH
                    Epair(IATM)=Epair(IATM)+HALFPH

                    ai = -dph / rnm

                    daix = dxnm * ai
                    xnp(N) = xnp(N) + daix
                    xnp(M) = xnp(M) - daix

                    daiy = dynm * ai
                    xnp(N+1) = xnp(N+1) + daiy
                    xnp(M+1) = xnp(M+1) - daiy

                    daiz = dznm * ai
                    xnp(N+2) = xnp(N+2) + daiz
                    xnp(M+2) = xnp(M+2) - daiz

                    wxxi(iatn) = wxxi(iatn) + daix * dxnm
                    wyyi(iatn) = wyyi(iatn) + daiy * dynm
                    wzzi(iatn) = wzzi(iatn) + daiz * dznm

                    if (calc_vir) then
                        wxyi(iatn) = wxyi(iatn) + daix * dynm
                        wxzi(iatn) = wxzi(iatn) + daix * dznm
                        wyzi(iatn) = wyzi(iatn) + daiy * dznm
                    endif

                end if   ! <--- End of if Newton III

                if (iac(IATYP1,IATYP2) /= 1) cycle

                K1 = nbor_start
                do jngbr = 1, ingbr-1 ! <---- three body loop over atoms L

                    K1=K1+1
                    IATL=nborlist(K1)
                    L=IATL*3-2

                    iatyp3 = abs(atype(iatl))

                    ! Handle other interaction types
                    if (iac(IATYP1,IATYP3) /= 1) cycle


                    IAIND2=IATAB(IATYP1,IATYP3)

                    ! Three particle potential

                    rnl = r_arr(jngbr)
                    if (rnl < 0) cycle
                    rnlef = rnl - SIGA(iaind2)
                    rnlsq = rnl**2
                    dxnl = dx_arr(jngbr)
                    dynl = dy_arr(jngbr)
                    dznl = dz_arr(jngbr)


                    COSMNL = (DXNM*DXNL + DYNM*DYNL + DZNM*DZNL) / (RNM*RNL)
                    coss = cosmnl + 1.0d0/3.0

                    ! Check for bond angle of ideal diamond lattice (skip if eq.)
                    if (ABS(COSS) < 1d-10) cycle

                    HMNL = EPSLAM_MNL(IAIND,IAIND2) * COSS**2 * &
                        exp(GAMSIG(IAIND)/RNMEF + GAMSIG(IAIND2)/RNLEF)
                    RNMA = GAMSIG(IAIND)  * HMNL / (RNMEF**2 * RNM)
                    RNLA = GAMSIG(IAIND2) * HMNL / (RNLEF**2 * RNL)
                    HCOS = 2 * HMNL / COSS
                    HKMNL = HCOS / (RNM * RNL)
                    HKMNLM = HCOS * COSMNL / RNMSQ
                    HKMNLL = HCOS * COSMNL / RNLSQ

                    ! Physical properties

                    Ethree(iatn) = Ethree(iatn) + hmnl

                    DPHM = -RNMA*DXNM + HKMNL*DXNL - HKMNLM*DXNM
                    DPHL = -RNLA*DXNL + HKMNL*DXNM - HKMNLL*DXNL
                    DPHN = -DPHM - DPHL
                    xnp(N) = xnp(N) + DPHN
                    xnp(M) = xnp(M) + DPHM
                    xnp(L) = xnp(L) + DPHL
                    wxxi(iatn) = wxxi(iatn) - DPHM*DXNM - DPHL*DXNL

                    if (calc_vir) then
                        wxyi(iatn) = wxyi(iatn) - DPHM*DYNM - DPHL*DYNL
                        wxzi(iatn) = wxzi(iatn) - DPHM*DZNM - DPHL*DZNL
                    endif

                    DPHM = -RNMA*DYNM + HKMNL*DYNL - HKMNLM*DYNM
                    DPHL = -RNLA*DYNL + HKMNL*DYNM - HKMNLL*DYNL
                    DPHN = -DPHM - DPHL
                    xnp(N+1) = xnp(N+1) + DPHN
                    xnp(M+1) = xnp(M+1) + DPHM
                    xnp(L+1) = xnp(L+1) + DPHL
                    wyyi(iatn) = wyyi(iatn) - DPHM*DYNM - DPHL*DYNL

                    if (calc_vir) then
                        wyzi(iatn) = wyzi(iatn) - DPHM*DZNM - DPHL*DZNL
                    endif

                    DPHM = - RNMA*DZNM+HKMNL*DZNL-HKMNLM*DZNM
                    DPHL = - RNLA*DZNL+HKMNL*DZNM-HKMNLL*DZNL
                    DPHN = - DPHM-DPHL
                    xnp(N+2) = xnp(N+2) + DPHN
                    xnp(M+2) = xnp(M+2) + DPHM
                    xnp(L+2) = xnp(L+2) + DPHL
                    wzzi(iatn) = wzzi(iatn) - DPHM*DZNM - DPHL*DZNL

                end do !  <---- end of three body loop over atoms L

            end do !  <---- end of 2-body loop over atoms M

        end do !  <-------- end of main loop over atoms N


        ! Send back forces and Epair to the neighboring nodes.
        ! Ethree does not need to be sent back, since only atoms in this
        ! node get any from the computation.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            call potential_pass_back_border_atoms(xnp, Epair)
            tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
        end if


        ! Convert forces from eV/A to internal units.
        do i = 1, 3*myatoms, 3
            xnp(i+0) = xnp(i+0) / box(1)
            xnp(i+1) = xnp(i+1) / box(2)
            xnp(i+2) = xnp(i+2) / box(3)
        end do

    end subroutine Stilweb_Force



    subroutine Init_SW_Pot(potmode, reppotcutin, sw2mod, sw3mod)

        integer, intent(in) :: potmode
        real(real64b), intent(in) :: reppotcutin
        real(real64b), intent(in) :: sw2mod(3), sw3mod(3)

        integer :: i, j, ialt, jalt

        ! Handle alternative parametrizations
        if (potmode >= 300) then
            if (potmode == 303) then
                ialt = 2
                jalt = 1
            else
                print *,'Unknown SW potmode',potmode
                call my_mpi_abort('Unknown potmode', potmode)
            endif

            ALAM(ialt) = altALAM(jalt)
            EPS(ialt) = altEPS(jalt)
            EPSA(ialt) = altEPSA(jalt)
            SIG(ialt) = altSIG(jalt)
            SIGA(ialt) = altSIGA(jalt)
            EPSLAM(ialt) = altEPSLAM(jalt)
            GAMSIG(ialt) = altGAMSIG(jalt)
            if (iprint) then
                call logger("Using alternative SW pot:")
                call logger("potmode:", potmode, 4)
                call logger("ialt:   ", ialt, 4)
                call logger("jalt:   ", jalt, 4)
                call logger("ALAM:   ", ALAM(:), 0)
                call logger("EPS:    ", EPS(:), 0)
                call logger("EPSA:   ", EPSA(:), 0)
                call logger("SIG:    ", SIG(:), 0)
                call logger("SIGA:   ", SIGA(:), 0)
                call logger("EPSLAM: ", EPSLAM(:), 0)
                call logger("GAMSIG: ", GAMSIG(:), 0)
            end if
        end if


        ! Handle strength modifications
        do i = 1, 3
            EPS(i) = sw2mod(i) * EPS(i)
            EPSA(i) = sw2mod(i) * EPSA(i)
            ALAM(i) = sw3mod(i) * ALAM(i)
            EPSLAM(i) = EPS(i) * ALAM(i)

            if (iprint .and. (sw2mod(i)/=1.0d0 .or. sw3mod(i) /=1.0d0)) then
                write(log_buf,'(A,I3,F13.8)')'SW modified EPS   ', i, EPS(i)
                call logger(log_buf)
                write(log_buf,'(A,I3,F13.8)')'SW modified EPSA  ', i, EPSA(i)
                call logger(log_buf)
                write(log_buf,'(A,I3,F13.8)')'SW modified ALAM  ', i, ALAM(i)
                call logger(log_buf)
                write(log_buf,'(A,I3,F13.8)')'SW modified EPSLAM', i, EPSLAM(i)
                call logger(log_buf)
            end if
        end do

        ! Compute 3-body EPS*ALAM from mixing rule.
        do i = 1, 3
            EPSLAM_MNL(i,:) = sqrt(EPSLAM(i) * EPSLAM(:))
        end do


        ! Handle modified reppot cutoff.
        ! A value of exactly 10 means use the default.
        if (reppotcutin /= 10.0d0) then
            reppotcut(:) = reppotcutin
        end if


        ! Map PARCAS types to SW indexes and find cutoffs.
        allocate(iatab(itypelow:itypehigh, itypelow:itypehigh))

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                ! For impurity elements, use Si cutoff for speed.
                iatab(i,j) = 1

                if (iac(i,j) == 1) then
                    if (element(i) == 'Si' .and. element(j) == 'Si') then
                        iatab(i,j) = 1
                    else if (element(i) == 'Ge' .and. element(j) == 'Ge') then
                        iatab(i,j) = 2
                    else if (element(i) == 'Si' .and. element(j) == 'Ge') then
                        iatab(i,j) = 3
                    else if (element(i) == 'Ge' .and. element(j) == 'Si') then
                        iatab(i,j) = 3
                    else
                        call logger("Stilweb error: can't handle pair")
                        call logger("element(i):", element(i), 4)
                        call logger("element(j):", element(j), 4)
                        call logger("Please extend stilweb.f arrays")
                        call my_mpi_abort('stilwebpot', myproc)
                    end if
                end if

                rcut(i,j) = SIGA(iatab(i,j))
            end do
        end do


        R0SQ(:) = SIGA(:)**2

    end subroutine Init_SW_Pot

end module stillinger_weber_mod
