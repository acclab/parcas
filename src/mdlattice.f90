!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module mdlattice_mod

    use iso_fortran_env, only: IOSTAT_END, IOSTAT_EOR
    use defs, only: NPMAX
    use datatypes, only: real64b, int32b
    use PhysConsts, only: invkBeV
    use my_mpi

    use typeparam
    use basis
    use lat_flags

    use binout, only: binaryread
    use random, only: MyRanf, gasdev

    use para_common, only: &
        rmn, rmx, &
        myatoms, &
        myproc, &
        iprint, debug

    use output_logger, only: &
        log_buf, &
        logger, &
        logger_write, &
        logger_clear_buffer, &
        logger_append_buffer


    implicit none

    private
    public :: Get_Atoms
    public :: Cryst_Gen


    ! TODO: move this to another module, managing all file handlers
    integer, parameter :: LATTICE_FILE = 5


  contains

    !***********************************************************************
    ! Support subroutines for lattice readin or creation:
    ! Get_Atoms(), FCC_Gen(), DIA_Gen(), Add2list()
    !***********************************************************************

    !***********************************************************************
    ! Read in the initial lattice
    !***********************************************************************
    subroutine Get_Atoms(x0, x1, atomindex, atype, natoms, nfixed, &
            amp, initemp, tdebye, box, pbc, latflag, restartmode, mdlatxyz, &
            dsliceIn, ECMIn)

        ! Variables passed in and out
        real(real64b), contiguous, intent(out) :: x0(:)
        real(real64b), contiguous, intent(out) :: x1(:)
        real(real64b), intent(in)  :: box(3)
        real(real64b), intent(in)  :: pbc(3)
        real(real64b), intent(in)  :: amp,initemp,tdebye
        real(real64b), intent(in) :: ECMIn(3), dsliceIn(3) ! slicing on binary restart reading

        integer(int32b), contiguous, intent(out) :: atype(:)
        integer(int32b), contiguous, intent(out) :: atomindex(:)
        integer, intent(out) :: nfixed
        integer, intent(inout) :: natoms
        integer, intent(in)  :: latflag
        integer, intent(in)  :: mdlatxyz
        integer, intent(in)  :: restartmode

        ! Local variables and constants
        integer :: nxoutside, nyoutside, nzoutside, noutside
        logical :: noutsideFound

        integer :: i, m, i3, nread


        if (iprint) then
            call logger("Simulation System:", 2)
            call logger("------------------", 2)
        end if

        !
        ! restart mode determined by latflag:
        !     0 No restart, create FCC
        !     1 Read in only coordinates and atom type
        !     2 No restart, create DIA
        !     3 Read in velocities and atom indices as well, guess recoil
        !     4 Read in velocities and atom indices as well, don't guess recoil
        !

        ! restartmode 9 is for reading the binary restart file
        if(restartmode == 9) then

            if (.not. (latflag == LATFLAG_FILE_RESTART_GUESS_ION .or. &
                    latflag == LATFLAG_FILE_RESTART_NEW_ION)) then
                call my_mpi_abort("only latflag=3 or 4 supported by " // &
                    "binary restarts", latflag)
            end if

            call binaryread("in/restart", .false., nread, myatoms, atype, atomindex, &
                x0, x1, dsliceIn, ECMIn)

        else

            call read_lattice_file(latflag, mdlatxyz, x0, x1, atype, &
                atomindex, natoms, nread, box, pbc, amp, initemp, tdebye)

        end if

        if (natoms < 0) then
            if (iprint) call logger("Number of read in atoms:", nread, 0)
            natoms = nread
        end if


        nfixed = count(atype(:myatoms) < 0)
        call my_mpi_sum(nfixed, 1)

        if (iprint) then
            call logger("Number of read in fixed atoms:", nfixed, 0)
        end if


        noutside = 0
        nxoutside = 0
        nyoutside = 0
        nzoutside = 0
        do i = 1, myatoms
            i3=i*3-3
            noutsideFound = .false.
            if (check_outside_bounds(x0(i3+1))) then
                nxoutside = nxoutside + 1
                noutsideFound = .true.
            end if
            if (check_outside_bounds(x0(i3+2))) then
                nyoutside = nyoutside + 1
                noutsideFound = .true.
            end if
            if (check_outside_bounds(x0(i3+3))) then
                nzoutside = nzoutside + 1
                noutsideFound = .true.
            end if
            if (noutsideFound) then
                noutside = noutside + 1
            end if
        enddo

        call my_mpi_sum(noutside, 1)
        call my_mpi_sum(nxoutside, 1)
        call my_mpi_sum(nyoutside, 1)
        call my_mpi_sum(nzoutside, 1)

        if (iprint) then
            call logger("Number of read in atoms outside the basic cell borders:")
            call logger("Total:", noutside, 4)
            call logger("X:    ", nxoutside, 4)
            call logger("Y:    ", nyoutside, 4)
            call logger("Z:    ", nzoutside, 4)
        endif


        if (nread /= natoms) then
            if (iprint) then
                call logger()
                call logger("WARNING: Total number of atoms read in does not")
                call logger("WARNING: equal the number of atoms specified.")
                call logger("WARNInG: Can be ok if input slicing is used.")
                call logger("natoms:", natoms, 4)
                call logger("nread: ", nread, 4)
            end if
            natoms = nread
        endif


        m = myatoms
        call my_mpi_sum(m, 1)

        if (m /= natoms) then
            if (iprint) then
                call logger()
                call logger("Total number of atoms distributed does not")
                call logger("equal the number of atoms specified.")
                call logger("natoms:     ", natoms, 4)
                call logger("distributed:", m, 4)
            endif
            call logger("myproc myatoms", myproc, myatoms, 0)
            call my_mpi_barrier()
            call my_mpi_abort("Natoms != Nat distributed", myatoms)
        endif

    end subroutine Get_Atoms


    logical function check_outside_bounds(x) result(ret)
        real(real64b), intent(in) :: x
        ret = x < -0.5d0 .or. x >= 0.5d0
    end function


    subroutine read_lattice_file(latflag, mdlatxyz, x0, x1, atype, atomindex, &
        natoms, nread, box, pbc, amp, initemp, tdebye)

        integer, intent(in) :: latflag
        integer, intent(in) :: mdlatxyz

        real(real64b), contiguous, intent(out) :: x0(:)
        real(real64b), contiguous, intent(out) :: x1(:)
        integer, contiguous, intent(out) :: atype(:)
        integer, contiguous, intent(out) :: atomindex(:)

        integer, intent(in) :: natoms
        integer, intent(out) :: nread

        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: pbc(3)

        real(real64b), intent(in) :: amp, initemp, tdebye

        integer, parameter :: nsize = 100000

        real(real64b) :: x0d(3, nsize)
        real(real64b) :: x1d(3, nsize)
        integer       :: atypes(nsize)
        integer       :: idxd(nsize)

        character(len=:), allocatable :: filename
        character(len=160) :: xyzcomment

        integer :: i
        integer :: idx
        integer :: chunkSize
        integer :: natoms_max
        integer :: xyz_header_num

        integer :: iostatus



        if (debug) print *, "Syncing Get_Atoms() between processors"
        call my_mpi_barrier()
        if (debug) print *, "Sync complete"
        if (debug) print *, "Opening mdlat.in", myproc


        ! Open the lattice file
        filename = "in/mdlat.in"
        if (mdlatxyz == 1) then
            filename = "in/mdlat.in.xyz"
        endif

        if (iprint) then
            call logger("Opening " // filename)
            open(LATTICE_FILE, file=filename, status="old", iostat=iostatus)

            if (iostatus /= 0) then
                call logger("ERROR opening " // filename)
                call logger("latflag:", latflag, 4)
                call my_mpi_abort("mdlat open", myproc)
            end if
        end if

        if (debug) print *, "mdlat opened", myproc


        ! If the file is in XYZ format, read the header data
        if (iprint .and. mdlatxyz == 1) then
            read(LATTICE_FILE, *) xyz_header_num
            if (natoms >= 0 .and. xyz_header_num /= natoms) then
                call logger("Wrong number of atoms in XYZ file!", 0)
                call logger("XYZ header:", xyz_header_num, 0)
                call logger("natoms:    ", natoms, 0)
                call my_mpi_abort("natoms mismatch", xyz_header_num)
            endif
            read(LATTICE_FILE, "(A160)") xyzcomment
            call logger("mdlat.in.xyz comment:", trim(xyzcomment), 0)
        endif

        ! Loop over the input file in chunks and read the lines. This way core 0
        ! can handle all the IO without allocating too much memory.
        ! Send out the correct data to the other cores, when reading atom data
        ! belonging to them.

        nread   = 0
        myatoms = 0
        idx     = 0

        if (natoms >= 0) then
            natoms_max = natoms
        else
            natoms_max = huge(natoms)
        end if

        do i = 1, natoms_max, nsize

            ! Make sure the last iteration is not going beyond the range of natoms
            chunkSize = min(nsize, natoms_max - i + 1)

            ! Read the chunk data on the I/O process
            if (iprint) then
                call read_chunk_of_data(latflag, mdlatxyz, chunkSize, nread, &
                    x0d, x1d, atypes, idxd, box, pbc, amp, initemp, tdebye)
            end if

            ! Chunk size might change if the file ended
            call my_mpi_bcast(chunkSize, 1, 0)
            if (.not. iprint) nread = nread + chunkSize

            ! Broadcast all the information to every process
            call broadcast_chunk_of_data(i, latflag, chunkSize, x0d, x1d, &
                atypes, idxd)

            ! Figure out what to do with the received chunk data
            call parse_chunk_of_data(latflag, chunkSize, idx, myatoms, &
                x0, x1, atype, atomindex, &
                x0d, x1d, atypes, idxd)

            if (chunkSize < nsize) exit  ! end of file
        end do

        if (iprint .and. mdlatxyz == 1 .and. xyz_header_num /= nread) then
            call logger("Failed to read all atoms from XYZ file", 0)
            call logger("XYZ header:", xyz_header_num, 0)
            call logger("Num read:  ", nread, 0)
            call my_mpi_abort("Failed to read all atoms from XYZ file", nread)
        end if

        ! Close the file handle
        if (iprint) then
            call logger("Closing " // filename)
            close(LATTICE_FILE)
        end if

    end subroutine read_lattice_file


    subroutine read_chunk_of_data(latflag, mdlatxyz, chunkSize, nread, x0d, x1d, &
            atypes, idxd, box, pbc, amp, initemp, tdebye)

        integer, intent(in) :: latflag
        integer, intent(in) :: mdlatxyz

        integer, intent(inout) :: chunkSize
        integer, intent(inout) :: nread
        real(real64b), contiguous, intent(out) :: x0d(:,:)
        real(real64b), contiguous, intent(out) :: x1d(:,:)
        integer, contiguous, intent(out) :: atypes(:)
        integer, contiguous, intent(out) :: idxd(:)

        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: pbc(3)

        real(real64b), intent(in) :: amp, initemp, tdebye


        character(len=8) :: xyzatom
        integer :: dummy1, dummy2
        integer :: j, k, jtype, ios

        ! This is only ever called by the IO process (myproc == 0).

        ! Read in the chunk of particle information
        do j = 1, chunkSize

            nread = nread + 1

            if (latflag == LATFLAG_FILE_COORDINATES_ONLY) then
                if (mdlatxyz==1) then
                    read(LATTICE_FILE,*, iostat=ios) xyzatom, x0d(:,j), atypes(j)
                else
                    read(LATTICE_FILE,*, iostat=ios) x0d(:,j), atypes(j), dummy1, dummy2
                end if
            else if (latflag == LATFLAG_FILE_RESTART_GUESS_ION .or. &
                     latflag == LATFLAG_FILE_RESTART_NEW_ION) then
                if (mdlatxyz==1) then
                    read(LATTICE_FILE,*, iostat=ios) xyzatom, x0d(:,j), atypes(j), &
                        idxd(j), x1d(:,j)
                else
                    read(LATTICE_FILE,*, iostat=ios) x0d(:,j), atypes(j), dummy1, dummy2, &
                        x1d(:,j), idxd(j)
                end if
            else
                call my_mpi_abort("No valid latflag", latflag)
            end if

            if (ios > 0) then ! error
                call my_mpi_abort("Error reading lattice file", nread)
            else if (ios == IOSTAT_EOR) then ! too short line
                call my_mpi_abort("Error reading lattice file: line too short", nread)
            else if (ios == IOSTAT_END) then ! end of file
                chunkSize = j - 1
                nread = nread - 1
                exit
            end if

            jtype = abs(atypes(j))

            ! Since the read atom type is used for indexing into arrays,
            ! validate it immediately.
            if (jtype < itypelow .or. jtype > itypehigh) then
                call logger("Atom type invalid!")
                call logger("Type limits:", itypelow, itypehigh, 0)
                call logger("Given type:", atypes(j), 0)
                if (latflag /= LATFLAG_FILE_COORDINATES_ONLY) then
                    call logger("Atom index:", idxd(j), 0)
                else
                    call logger("Atom index:", nread, 0)
                end if
                call my_mpi_abort("read bad atom type", atypes(j))
            end if

            if (mdlatxyz == 1) then
                ! Unit transformation into mdlat.in units
                x0d(1,j) = x0d(1,j)/box(1)
                x0d(2,j) = x0d(2,j)/box(2)
                x0d(3,j) = x0d(3,j)/box(3)

                ! Check atom name
                if (TRIM(xyzatom) /= TRIM(element(jtype))) then
                    call logger("mdlatxyz atom name mismatch")
                    call logger("line number:", nread, 4)
                    call logger("read name:  ", xyzatom, 4)
                    call logger("type name:  ", element(jtype), 4)
                    call my_mpi_abort("atom name mismatch", -1)
                endif

                if (latflag == LATFLAG_FILE_RESTART_GUESS_ION .or. &
                        latflag == LATFLAG_FILE_RESTART_NEW_ION) then
                    ! Unit transformation into mdlat.in units
                    ! (note: division by delta comes later)
                    x1d(1,j) = x1d(1,j)/(vunit(jtype)*box(1))
                    x1d(2,j) = x1d(2,j)/(vunit(jtype)*box(2))
                    x1d(3,j) = x1d(3,j)/(vunit(jtype)*box(3))
                endif
            endif

            ! Random movement from ideal positions
            if (latflag /= LATFLAG_FILE_RESTART_GUESS_ION .and. &
                    latflag /= LATFLAG_FILE_RESTART_NEW_ION) then
                call displaceatom(x0d(:,j),box,atypes(j),amp,initemp,tdebye)
            endif

            do k = 1, 3
                ! If the atom is exactly on the border, it can be moved slightly
                ! towards the center of the cell. This helps with the periodic
                ! boundary check. ! TODO is this strictly necessary?
                if (x0d(k,j) ==  0.5d0) x0d(k,j) = x0d(k,j) - 1e-10
                if (x0d(k,j) == -0.5d0) x0d(k,j) = x0d(k,j) + 1e-10
                call fix_periodic(pbc(k), x0d(k, j))
            end do
        end do
    end subroutine read_chunk_of_data


    subroutine broadcast_chunk_of_data(atomi, latflag, chunkSize, x0d, x1d, &
            atypes, idxd)

        integer, intent(in) :: atomi
        integer, intent(in) :: latflag

        integer, intent(in) :: chunkSize

        real(real64b), intent(inout) :: x0d(3, chunkSize)
        real(real64b), intent(inout) :: x1d(3, chunkSize)

        integer, intent(inout) :: atypes(chunkSize)
        integer, intent(inout) :: idxd(chunkSize)


        integer :: ierror


        if (debug) print *, "Broadcasting", atomi, latflag, myproc


        call mpi_bcast(x0d, size(x0d), mpi_double_precision, 0, &
            mpi_comm_world, ierror)
        call mpi_bcast(atypes, size(atypes), my_mpi_integer, 0, &
            mpi_comm_world, ierror)
        if (latflag == LATFLAG_FILE_RESTART_GUESS_ION .or. &
                latflag == LATFLAG_FILE_RESTART_NEW_ION) then
            call mpi_bcast(x1d, size(x1d), mpi_double_precision, 0, &
                mpi_comm_world, ierror)
            call mpi_bcast(idxd, size(idxd), my_mpi_integer, 0, &
                mpi_comm_world, ierror)
        endif

        if (debug) print *, "Broadcast complete", atomi, myproc

    end subroutine broadcast_chunk_of_data


    subroutine parse_chunk_of_data(latflag, chunkSize, idx, myatoms, x0, x1, atype, &
            atomindex, x0d, x1d, atyped, idxd)

        integer, intent(in) :: latflag
        integer, intent(in) :: chunkSize

        integer, intent(inout) :: idx
        integer, intent(inout) :: myatoms

        real(real64b), intent(out) :: x0(:)
        real(real64b), intent(out) :: x1(:)
        integer, intent(out) :: atype(:)
        integer, intent(out) :: atomindex(:)

        real(real64b), contiguous, intent(in) :: x0d(:,:)
        real(real64b), contiguous, intent(in) :: x1d(:,:)
        integer, contiguous, intent(in) :: atyped(:)
        integer, contiguous, intent(in) :: idxd(:)

        integer :: m3
        integer :: j


        do j = 1, chunkSize
            idx = idx + 1
            if (position_inside_processor(x0d(:,j))) then

                myatoms = myatoms + 1

                ! Check to insure myatoms < NPMAX
                if (myatoms > NPMAX) then
                    call logger("myatoms > NPMAX in Get_Atoms")
                    call my_mpi_abort('natoms in Get_Atoms', myproc)
                endif

                ! Assign atom position
                m3 = 3*myatoms - 3
                x0(m3+1 : m3+3) = x0d(:,j)

                atype(myatoms) = atyped(j)

                if (latflag == LATFLAG_FILE_RESTART_GUESS_ION .or. &
                        latflag == LATFLAG_FILE_RESTART_NEW_ION) then
                    ! Assign atom velocity
                    x1(m3+1 : m3+3) = x1d(:,j)
                    ! Assign atom index
                    if (idxd(j) < 0) then
                        print *, "Get_atoms warning: weird atom index"
                        print *, myproc, myatoms, idxd(j)
                    endif
                    atomindex(myatoms) = idxd(j)
                else
                    atomindex(myatoms) = idx
                endif
            endif
        enddo

    end subroutine parse_chunk_of_data



    !***********************************************************************
    ! Generate arbitrary lattice from scratch
    !***********************************************************************
    subroutine  Cryst_Gen(x0, atomindex, atype, natoms, amp, initemp, &
            tdebye, box, pbc, ncells, latflag)

        ! Variables passed in and out
        real(real64b), intent(out) :: x0(:)
        real(real64b), intent(in) :: pbc(3)
        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: amp, initemp, tdebye

        integer, intent(inout) :: natoms
        integer, intent(in) :: ncells(:)
        integer, intent(in) :: latflag
        integer(int32b), intent(out) :: atype(:)
        integer(int32b), intent(out) :: atomindex(:)

        ! Local variables and constants
        real(real64b) :: u(3), lat(3), x(3)
        integer :: i, j, k, l, m, idx

        ! Unit cell offset from unit cell origin in x y z
        real(real64b) :: xoffset(3)

        ! Basis of atoms in unit cell, set below in if clause
        real(real64b), allocatable :: xbasis(:,:)

        ! Number of atoms in basis, set in if below
        integer :: nbasis


        if (iprint) then
            call logger("Generate Crystal Lattice:", 2)
            call logger("-------------------------", 2)
        end if

        allocate(xbasis(size(readbasis, dim=1), 3))

        select case (latflag)
        case (LATFLAG_CREATE_FCC)
            if (iprint) call logger("FCC", 6)
            nbasis = 4
            xoffset(:)  = 0.25
            xbasis(1,:) = [0.0, 0.0, 0.0]
            xbasis(2,:) = [0.0, 0.5, 0.5]
            xbasis(3,:) = [0.5, 0.0, 0.5]
            xbasis(4,:) = [0.5, 0.5, 0.0]
            readtype(:) = 1
            changeprob(:) = 0
            changeto(:) = 1

        case (LATFLAG_CREATE_DIA)
            if (iprint) call logger("DIA", 6)
            nbasis = 8
            xoffset(:)  = 0.125
            xbasis(1,:) = [0.0,  0.0,  0.0]
            xbasis(2,:) = [0.0,  0.5,  0.5]
            xbasis(3,:) = [0.5,  0.0,  0.5]
            xbasis(4,:) = [0.5,  0.5,  0.0]
            xbasis(5,:) = [0.25, 0.25, 0.25]
            xbasis(6,:) = [0.25, 0.75, 0.75]
            xbasis(7,:) = [0.75, 0.25, 0.75]
            xbasis(8,:) = [0.75, 0.75, 0.25]
            readtype(:) = 1
            changeprob(:) = 0
            changeto(:) = 1

        case (LATFLAG_CREATE_LATTICE)
            if (iprint) call logger("Crystal structure defined in md.in", 6)
            ! Use read in lattice structure
            nbasis = nreadbasis
            xoffset(:) = readoffset(:)
            xbasis(:,:) = readbasis(:,:)

            if (iprint) then
                do l = 1, nbasis
                    if (abs(readtype(l)) < itypelow .or. abs(readtype(l)) > itypehigh) then
                        call logger("Invalid ltype for latflag 5!")
                        call logger("Lattice basis index:", l, 0)
                        call logger("Read in type:", readtype(l), 0)
                        call logger("Type limits:", itypelow, itypehigh, 0)
                        call my_mpi_abort("invalid ltype", readtype(l))
                    end if
                    if (changeprob(l) > 0d0 .and. &
                        (abs(changeto(l)) < itypelow .or. abs(changeto(l)) > itypehigh)) then
                        call logger("Invalid changt for latflag 5!")
                        call logger("Lattice basis index:", l, 0)
                        call logger("Read in type:", changeto(l), 0)
                        call logger("Type limits:", itypelow, itypehigh, 0)
                        call my_mpi_abort("invalid changt", changeto(l))
                    end if
                end do
            end if

        case default
            call logger("Cryst_Gen ERROR: unknown lattice type", latflag, 0)
            call my_mpi_abort("Invalid latflag", latflag)
        end select

        if (iprint) then
            lat(:) = box(:) / ncells(:)
            call logger("Number of atoms in basis:", nbasis, 6)
            call logger("Unit cell:", 6)
            call logger("ax, ay, az:", lat, 10)
            call logger("offset:    ", xoffset, 10)
            call logger()
            do i = 1, nbasis
                if (changeprob(i) > 0) then
                    write (log_buf,"(A,I0,A,I0,A,3F8.4,A,I0,A,F8.5)") &
                        "Atom ", i, " type ", readtype(i), &
                        " pos", xbasis(i,:), &
                        " changed to ", changeto(i), " with prob.", changeprob(i)
                else
                    write (log_buf,"(A,I0,A,I0,A,3F8.4)") &
                        "Atom ", i, " type ", readtype(i), &
                        " pos", xbasis(i,:)
                end if
                call logger(log_buf)
            end do
            call logger()
        end if

        myatoms = 0

        ! The lattice constant in PARCAS's internal units
        lat(:) = 1d0 / ncells(:)
        idx = 0

        do i = 1, ncells(1)
            u(1) = lat(1)*(i-1) + lat(1)*xoffset(1) - 0.5d0
            ! cycle if we are far away from the process domain,
            ! conservative safe assumptions
            if (u(1) < rmn(1) - lat(1) .or. u(1) > rmx(1) + lat(1)) then
                idx = idx + ncells(2)*ncells(3)*nbasis
                cycle
            end if
            do j = 1, ncells(2)
                u(2) = lat(2)*(j-1) + lat(2)*xoffset(2) - 0.5d0
                if (u(2) < rmn(2) - lat(2) .or. u(2) > rmx(2) + lat(2)) then
                    idx = idx + ncells(3)*nbasis
                    cycle
                end if
                do k = 1, ncells(3)
                    u(3) = lat(3)*(k-1) + lat(3)*xoffset(3) - 0.5d0
                    if (u(3) < rmn(3) - lat(3) .or. u(3) > rmx(3) + lat(3)) then
                        idx = idx + nbasis
                        cycle
                    end if
                    do l = 1, nbasis
                        idx = idx + 1
                        x(:) = u(:) + xbasis(l,:) * lat(:)
                        call Add2List(x, readtype(l), idx, box, pbc, x0, atype, &
                            atomindex, initemp, tdebye, amp, changeprob(l), changeto(l))
                    end do
                end do
            end do
        end do

        m = myatoms
        call my_mpi_sum(m, 1)

        if (natoms < 0) natoms = m

        if (m /= natoms) then
            call logger_clear_buffer()
            call logger_append_buffer()
            call logger_append_buffer("Total number of atoms generated does")
            call logger_append_buffer("not equal the number of atoms specified")
            call logger_append_buffer("myproc:   ", myproc, 4)
            call logger_append_buffer("natoms:   ", natoms, 4)
            call logger_append_buffer("generated:", m, 4)
            call logger_write(trim(log_buf))
            call my_mpi_abort("natoms wrong in Cryst_Gen", myproc)
        endif


        if (iprint) then
            m = product(ncells) * nbasis
            if (natoms /= m) then
                call logger("Generated unexpected number of atoms")
                call logger("Generated:", natoms, 0)
                call logger("Expected: ", m, 0)
                call my_mpi_abort("Generated unexpected number of atoms", m)
            end if

            call logger("Created Lattice with:", 6)
            call logger("nx:    ", ncells(1), 10)
            call logger("ny:    ", ncells(2), 10)
            call logger("nz:    ", ncells(3), 10)
            call logger("natoms:", natoms, 10)
            call logger()
            call logger()
        end if

    end subroutine Cryst_Gen


    !***********************************************************************
    ! Add an atom to the list, if appropriate
    !***********************************************************************
    subroutine Add2List(xin, typein, idx, box, pbc, x0, atype, atomindex, &
            initemp, tdebye, amp, changeprob, changeto)

        real(real64b), intent(in)  :: xin(3)
        integer, intent(in) :: typein, idx
        real(real64b), intent(in)  :: box(3), pbc(3)
        real(real64b), intent(inout) :: x0(:)
        integer, intent(inout) :: atype(:), atomindex(:)
        real(real64b), intent(in) :: initemp, tdebye, amp
        real(real64b), intent(in) :: changeprob
        integer, intent(in) :: changeto

        real(real64b) :: x(3)
        integer :: m3


        x(:) = xin(:)
        call fix_periodic(pbc, x)

        if (.not. position_inside_processor(x)) return

        myatoms = myatoms + 1

        if (myatoms > NPMAX) then
            call logger("myatoms > NPMAX in Add2List", myatoms, 0)
            call my_mpi_abort('NPMAX in Add2List', NPMAX)
        endif

        atomindex(myatoms) = idx
        atype(myatoms) = typein

        ! Replace the currently added atom with another type, based on the
        ! change probability, 'changeprob'.
        if (changeprob > 0d0) then
            if (MyRanf(-1) < changeprob) then
                atype(myatoms) = changeto
            end if
        end if

        ! Random movement from ideal positions
        call displaceatom(x, box, atype(myatoms), amp, initemp, tdebye)

        m3 = 3*myatoms - 3
        x0(m3+1 : m3+3) = x(:)

    end subroutine Add2List


    logical function position_inside_processor(x) result(ret)
        real(real64b), intent(in) :: x(3)

        ret = all(x >= rmn .and. x < rmx)
    end function position_inside_processor


    ! Make sure the atoms are inside the periodic boundaries of the system.
    ! Upper limit is exclusive, lower limit inclusive.
    elemental subroutine fix_periodic(pbc, x1)
        real(kind=real64b), intent(in)    :: pbc
        real(kind=real64b), intent(inout) :: x1

        if (x1 >= 0.5d0 .and. pbc == 1.0) then
            x1 = x1 - 1.0d0
        elseif (x1 < -0.5d0 .and. pbc == 1.0) then
            x1 = x1 + 1.0d0
        endif
    end subroutine fix_periodic



    !***********************************************************************
    ! Create an initial atom displacement according to tdebye or amp
    !***********************************************************************
    subroutine displaceatom(x,box,atype,amp,initemp,tdebye)

        real(real64b), intent(inout) :: x(3)
        real(real64b), intent(in) :: box(3)
        integer, intent(in) :: atype
        real(real64b), intent(in) :: amp,initemp,tdebye

        real(real64b) :: sd
        integer :: i

        logical, save              :: firsttime = .true.
        logical, allocatable, save :: firstthistype(:)

        if (.not. allocated(firstthistype)) then
            allocate(firstthistype(itypelow:itypehigh))
            firstthistype(:) = .true.
        end if

        ! Do not displace any of the fixed atoms!
        if (atype < 0) return

        ! Do not apply any displacement if there is no thermal movement
        if (amp == 0.0d0 .and. tdebye == 0.0d0) return

        ! Give all atoms a Gaussian displacement based on the initial
        ! temperature
        if (tdebye /= 0.0d0 .and. initemp > 0.0d0) then
            ! 20.89 is sqrt(9 hbar^2/k_B u)
            ! The expression comes from the second order approximation of a
            ! nasty elliptical integral
            sd = 20.89*SQRT((initemp*invkBeV)/mass(atype))/ &
                (SQRT(3.0)*(tdebye*invkBeV))

            if (firstthistype(atype) .and. iprint) then
                write (log_buf,"(A,F13.6,A,I3)") &
                    "Giving Gaussian Debye displacements with sigma", &
                    sd," for atype",atype
                call logger(log_buf)
            end if
            do i = 1, 3
                x(i) = x(i) + sd * gasdev() / box(i)
            end do
        else
            if (firsttime .and. iprint) then
                call logger("Giving displacements with maximum " // &
                            "displacement amplitude:", amp, 0)
            end if
            do i = 1, 3
                x(i) = x(i) + amp * (MyRanf(-1) - 0.5d0) / box(i)
            end do
        end if

        firsttime = .false.
        firstthistype(atype) = .false.

    end subroutine displaceatom


end module mdlattice_mod
