!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module tersoff_compound_mod

    use readsubs_mod

    use datatypes, only: real64b

    use typeparam
    use my_mpi

    use timers, only: tmr, TMR_SEMICON_COMMS

    use PhysConsts, only: pi
    use defs
    use splinereppot_mod, only: reppot_only, reppot_fermi

    use para_common, only: myatoms, np0pairtable

    use mdparsubs_mod, only: potential_pass_back_border_atoms

    use output_logger, only: log_buf, logger
    use file_units, only: TEMP_FILE


    implicit none
    save

    private
    public :: Init_Tersoff_Compound
    public :: Tersoff_Compound_Force


    ! All the arrays are zero-based, so the real maximum is one larger than
    ! the one given here.
    integer, parameter :: MXTT = 4

    real(real64b), dimension(0:MXTT, 0:MXTT) :: &
        tersA, tersB, beta, rlambda, rmu, gc, gd, gh, ters_n, &
        rlambda3_cube, gd2, gd2i, gc2, g2c2, ters2i, xi, &
        trcut, dcut, rmd, rpd, a, halfa, r_ter_max, &
        reppotcut, bf, rf

    real(real64b) :: tersoffomega(0:MXTT, 0:MXTT, 0:MXTT)

    logical :: albepot
    logical :: exponentone
    real(real64b), dimension(0:MXTT, 0:MXTT) :: albegamma, albecossign, albe2mu
    real(real64b) :: albe2mu3(0:MXTT, 0:MXTT, 0:MXTT)


    character(len=*), parameter :: tercversion = "<08/06/26 18:40 knordlun>"
    character(len=*), parameter :: jksym = "Uses jk symmetry. Supports Albe Pt-C potential  "


contains


    subroutine Tersoff_Compound_Force(x0,atype,xnp,box,pbc,  &
            nborlist,Epair,Ethree, &
            wxxi,wyyi,wzzi,wxyi,wxzi,wyzi,calc_vir)

        !***********************************************************************
        ! Routines to calculate Tersoff potential see PRB 39, 5566 (1989).
        ! Jamie Morris 6 June 1995, parallel version by Dave Turner - Nov of 1995
        ! Taken into PARCAS from alcmd V3.0 starting Jan 28 1998 - Kai Nordlund
        ! Extended to many-atom interactions and into Albe versio by Kai Nordlund.
        !
        !***********************************************************************
        !
        ! potmode determines mode
        !
        ! This version is a compound version. potmode selection is actually done
        ! in Init_pot below
        !

        ! x0 is atom units in x/boxsizex, y/boxsizey and z/boxsizez
        ! atype is the atom type
        ! xnp is output forces in units eV/A/boxsize
        ! atomindex is a unique index of each atom, needed on parallel computers
        ! natoms is the total number of atoms
        ! myatoms (from para_common.f90) is the number of atoms in this processor
        !   - on single processors natoms=myatoms
        ! np0pairtable is myatoms plus the atoms received from neighbor processors
        ! box(i) is the box size in Angstroms, 1=x, 2=y 3=z
        ! pbc(i) tells whether  periodics are on (1.0d0 means on, 0.0d0 off)

        ! nborlist(i) is a standard Verlet neighbour list input, organized as follows:
        !
        !  nborlist(1)    Number of neighbours for atom 1 (eg. N1)
        !  nborlist(2)    Index of first neighbour of atom 1
        !  nborlist(3)    Index of second neighbour of atom 1
        !   ...
        !  nborlist(N1+1)  Index of last neighbour of atom 1
        !  nborlist(N1+2)  Number of neighbours for atom 2
        !  nborlist(N1+3)  Index of first neighbour of atom 2
        !   ...
        !   ...           And so on for all N atoms
        !
        !
        ! - Epair,Ethree are output energies in eV
        ! - wxxi,wyyi,wzzi are virial outputs
        !

        ! ------------------------------------------------------------------
        ! Variables passed in and out

        real(real64b), contiguous, intent(in) :: x0(:)
        real(real64b), intent(in) :: box(3), pbc(3)
        real(real64b), contiguous, intent(out) :: xnp(:)
        integer, contiguous, intent(in) :: atype(:)

        real(real64b), contiguous, intent(out) :: Epair(:), Ethree(:)
        real(real64b), intent(out) :: wxxi(:), wyyi(:), wzzi(:)
        real(real64b), intent(out) :: wxyi(:), wxzi(:), wyzi(:)
        logical, intent(in) :: calc_vir

        integer, intent(in) :: nborlist(:)

        ! ------------------------------------------------------------------
        ! Local variables and constants

        real(real64b), dimension(3, NNMAX) :: xa, xab, xai
        real(real64b), dimension(3, NNMAX) :: dfcxr, xr2
        real(real64b), dimension(NNMAX) :: ra, rai, ra2i
        real(real64b), dimension(NNMAX) :: fc, dfc, z, db
        real(real64b) :: cth(NNMAX*NNMAX)

        !A.Kuronen
        real(real64b), dimension(NNMAX**2) :: gtij, gtik, dGij, dGik

        ! Previously implicit variables
        integer :: i, j, k, i3, j3, k3, ij, ik, mjk
        integer :: nnbors


        real(real64b) :: dr, ra2, dfcr, fatt, frep
        real(real64b) :: dfatt, dfrep, bzp, bzp1, bij, cpair, cmany, df, c1
        real(real64b) :: cj, ck, swap

        real(real64b) :: dbfcdgcth, dbfcdgrr, dbjgt, dbkgt, dbfcdg, dbfcjk, dbfckj

        ! ------------------------------------------------------------------
        ! New variables by Kai Nordlund
        integer :: in, in0

        real(real64b) :: help1, help2, help3, V
        real(real64b), dimension(NNMAX**2) :: expij, expik
        real(real64b), dimension(NNMAX**2) :: dexpij, dexpik
        real(real64b) :: dbfcgde

        !A.Kuronen
        real(real64b) :: lij, lik

        real(real64b) :: ghmctheta, denomi
        real(real64b) :: theta, s, fermi, dfermi
        real(real64b) :: tersoffomegaij, tersoffomegaik

        ! type variables
        integer :: typei, typeij(NNMAX), typej, typek
        integer :: iact(NNMAX)

        real(real64b) :: t1


        xnp(:3*np0pairtable) = 0.0
        Epair(:np0pairtable) = 0.0
        Ethree(1 : myatoms) = 0.0
        wxxi(1 : myatoms) = 0.0
        wyyi(1 : myatoms) = 0.0
        wzzi(1 : myatoms) = 0.0

        if (calc_vir) then
            wxyi(1 : myatoms) = 0.0
            wxzi(1 : myatoms) = 0.0
            wyzi(1 : myatoms) = 0.0
        endif


        ! Neighbour list counter
        in=0
        do i = 1,myatoms   ! <-------------------- Loop over atoms i
            i3 = i*3-3

            typei=abs(atype(i))

            in=in+1; in0=in
            nnbors = nborlist(in)
            in=in+nnbors

            if (nnbors < 0 .or. nnbors > NNMAX) then
                print *,'Tersoff_compound nnbors HORROR ERROR',nnbors,NNMAX
                print *,i,i3
                call my_mpi_abort('Tersoff_compound nnbors', int(nnbors))
            endif


            !
            ! Calculate f_C and df_C
            !
            do ij = 1,nnbors          ! <-------- First loop over neighbours j
                j = nborlist(in0+ij)
                j3 = j*3-3

                typeij(ij)=abs(atype(j))
                typej=typeij(ij)
                iact(ij)=iac(typei,typej)

                if (iact(ij) /= 1) then
                    ! Handle other interaction types
                    ! Set fc to zero - all other steps will thus be ignored
                    fc(ij)=0d0

                    if (iact(ij) == 0) then
                        cycle
                    else if (iact(ij) == 2) then
                        continue  ! pairpot later
                    else
                        call logger("TERSOFF_COMPOUND ERROR: IMPOSSIBLE INTERACTION")
                        call logger("myproc:", myproc, 4)
                        call logger("i:     ", i, 4)
                        call logger("j:     ", j, 4)
                        call logger("typei: ", typei, 4)
                        call logger("typej: ", typej, 4)
                        call my_mpi_abort('INTERACTION -1', int(myproc))
                    endif

                endif

                xa(1,ij) = x0(j3+1)-x0(i3+1)
                if (xa(1,ij) >=  0.5d0) xa(1,ij) = xa(1,ij)-pbc(1)
                if (xa(1,ij) < -0.5d0) xa(1,ij) = xa(1,ij)+pbc(1)
                xab(1,ij) = box(1)*xa(1,ij)

                xa(2,ij) = x0(j3+2)-x0(i3+2)
                if (xa(2,ij) >=  0.5d0) xa(2,ij) = xa(2,ij)-pbc(2)
                if (xa(2,ij) < -0.5d0) xa(2,ij) = xa(2,ij)+pbc(2)
                xab(2,ij) = box(2)*xa(2,ij)

                xa(3,ij) = x0(j3+3)-x0(i3+3)
                if (xa(3,ij) >=  0.5d0) xa(3,ij) = xa(3,ij)-pbc(3)
                if (xa(3,ij) < -0.5d0) xa(3,ij) = xa(3,ij)+pbc(3)
                xab(3,ij) = box(3)*xa(3,ij)

                ra2 = xab(1,ij)*xab(1,ij)+xab(2,ij)*xab(2,ij)+xab(3,ij)*xab(3,ij)

                ra(ij) = SQRT(ra2)

                if (iact(ij)==2) then
                    if (ra(ij) > r_ter_max(typei,typej)) cycle

                    ! Here must multiply by box for virial, since xab only contains
                    ! one box at this point.

                    call reppot_only(ra(ij),V,df,typei,typej)
                    !print *,'reppot',i,j,ra(ij),df
                    V=V*0.5d0
                    df=df*0.5d0/ra(ij)
                    Epair(i)=Epair(i)+V; Epair(j)=Epair(j)+V
                    c1 = df*xa(1,ij)
                    xnp(i3+1) = xnp(i3+1)+c1; xnp(j3+1) = xnp(j3+1)-c1
                    wxxi(i) = wxxi(i) - c1*xab(1,ij)*box(1)

                    if (calc_vir) then
                        wxyi(i) = wxyi(i) - c1*xab(2,ij)*box(2)
                        wxzi(i) = wxzi(i) - c1*xab(3,ij)*box(3)
                    endif

                    c1 = df*xa(2,ij)
                    xnp(i3+2) = xnp(i3+2)+c1; xnp(j3+2) = xnp(j3+2)-c1
                    wyyi(i) = wyyi(i) - c1*xab(2,ij)*box(2)

                    if (calc_vir) then
                        wyzi(i) = wyzi(i) - c1*xab(3,ij)*box(3)
                    endif

                    c1 = df*xa(3,ij)
                    xnp(i3+3) = xnp(i3+3)+c1; xnp(j3+3) = xnp(j3+3)-c1
                    wzzi(i) = wzzi(i) - c1*xab(3,ij)*box(3)

                    cycle
                endif
                if (iact(ij)/=1) cycle

                fc(ij) = 0d0
                dfc(ij) = 0d0
                if (ra(ij) <= rpd(typei,typej)) then
                    if (ra(ij) < rmd(typei,typej)) then
                        fc(ij)=1d0
                    else
                        theta=a(typei,typej)*(ra(ij)-trcut(typei,typej))
                        s = SIN(theta)
                        fc(ij)= 0.5d0*(1d0-s)
                        ! replaced dFcut= -0.5d0*a*COS(theta)
                        ! using cos theta =sqrt(1-sin^2 theta)
                        ! Note that halfa contains a minus sign.
                        dfc(ij)= halfa(typei,typej)*SQRT(1d0-s*s)
                    endif
                endif

                if (fc(ij) == 0d0) cycle

                rai(ij) = 1d0/ra(ij)
                dfcr = dfc(ij)*rai(ij)
                ra2i(ij) = 1d0/ra2

                xai(1,ij) = rai(ij)*xa(1,ij)
                dfcxr(1,ij) = dfcr*xa(1,ij)
                xr2(1,ij) = xa(1,ij)*ra2i(ij)

                xai(2,ij) = rai(ij)*xa(2,ij)
                dfcxr(2,ij) = dfcr*xa(2,ij)
                xr2(2,ij) = xa(2,ij)*ra2i(ij)

                xai(3,ij) = rai(ij)*xa(3,ij)
                dfcxr(3,ij) = dfcr*xa(3,ij)
                xr2(3,ij) = xa(3,ij)*ra2i(ij)

                z(ij) = 0d0
            enddo                     ! <-------- End of first loop over neighbours j

            !
            ! Calculate g(theta) and dg(theta) and hence zeta_ij and zeta_ik
            !
            mjk = 0
            do ij = 1,nnbors-1        ! <-------- Second loop over neighbours j

                if (fc(ij) == 0d0) cycle

                typej=typeij(ij)
                j = nborlist(in0+ij)
                do ik = ij+1,nnbors            ! <------- Loop over neighbours k
                    !if (ij==ik) cycle
                    if (fc(ik) == 0d0) cycle

                    typek=typeij(ik)
                    mjk = mjk + 1
                    cth(mjk) = (xab(1,ij)*xab(1,ik)+xab(2,ij)*xab(2,ik)+xab(3,ij)*xab(3,ik)) &
                        * rai(ij)*rai(ik)

                    ! Inlined function G_theta
                    ! A.Kuronen
                    ghmctheta = gh(typei,typej) + albecossign(typei,typej)*cth(mjk)
                    denomi = 1d0/(gd2(typei,typej)+ghmctheta*ghmctheta)
                    gtij(mjk) =  albegamma(typei,typej)*(1d0+(gd2i(typei,typej) - &
                        denomi)*gc2(typei,typej))
                    dGij(mjk) =  albegamma(typei,typej)*(-albecossign(typei,typej))* &
                        (g2c2(typei,typej)*denomi*denomi*ghmctheta)

                    !print *,'gtij',gtij(mjk),albegamma(typei,typej),gd2i(typei,typej),denomi,gc2(typei,typej)

                    ! The indexes to albecossign(typei,typej) below are correct,
                    ! confirmed by Kai. albecossign must anyway be the same for
                    ! both types for this to make sense.
                    ghmctheta = gh(typei,typek) + albecossign(typei,typej)*cth(mjk)
                    denomi = 1d0/(gd2(typei,typek)+ghmctheta*ghmctheta)
                    gtik(mjk) = albegamma(typei,typek)*(1d0+(gd2i(typei,typek) - &
                        denomi)*gc2(typei,typek))
                    dGik(mjk) = albegamma(typei,typek)*(-albecossign(typei,typej))* &
                        (g2c2(typei,typek)*denomi*denomi*ghmctheta)

                    ! exp(lambda3...) term

                    lij=rlambda3_cube(typei,typej)
                    lik=rlambda3_cube(typei,typek)
                    if (exponentone) then
                        ! Support for albe2mu3, i.e. override rlambda3
                        if (albe2mu3(typei,typej,typek) /= 0.0d0 .or. &
                            albe2mu3(typei,typek,typej) /= 0.0d0) then
                            lij=albe2mu3(typei,typej,typek)
                            lik=albe2mu3(typei,typek,typej)
                            !print *,'lij',typei,typej,typek,lij,lik
                        endif
                    endif

                    if (albepot) then
                        ! Albe pot: lambda3 linearly rather than lambda3^3
                        ! and reverse ij and ik parameters

                        ! This handling of albe2mu3 only works for CH systems, may
                        ! need to be changed for others
                        if (albe2mu3(typei,typej,typek) /= 0.0d0 .or. &
                            albe2mu3(typei,typek,typej) /= 0.0d0) then
                            lij=albe2mu3(typei,typek,typej)
                            lik=albe2mu3(typei,typej,typek)
                        else
                            lij=albe2mu(typei,typek)
                            lik=albe2mu(typei,typej)
                        endif

                        ! Trick to implement Albe potential: swap gij and gik
                        swap=gtij(mjk); gtij(mjk)=gtik(mjk); gtik(mjk)=swap;
                        swap=dGij(mjk); dGij(mjk)=dGik(mjk); dGik(mjk)=swap;

                        ! Also swap tersoffomega to enable handling of Brenner-like
                        ! alpha-ij-term exponential
                        tersoffomegaij = tersoffomega(typei,typek,typej)
                        tersoffomegaik = tersoffomega(typei,typej,typek)
                    else
                        tersoffomegaij = tersoffomega(typei,typej,typek)
                        tersoffomegaik = tersoffomega(typei,typek,typej)
                    end if

                    if (lij/=0d0 .or. lik/=0d0) then

                        dr = ra(ij) - ra(ik)

                        if (.not. albepot .and..not. exponentone) then
                            help1 = dr * dr
                            help2 = help1 * dr
                            help3 = 3d0*help1
                        else
                            help2 = dr
                            help3 = 1d0;
                        endif
                        expij(mjk) = exp(lij*help2)
                        ! expik(mjk) = exp(-lik*help2)
                        ! Optimization trick by Jan Westerholm
                        if ( lij .eq. lik ) then
                            expik(mjk) = 1.0d0/expij(mjk)
                        else
                            expik(mjk) = exp(-lik*help2)
                        endif

                        dexpij(mjk)= help3*lij*expij(mjk)
                        dexpik(mjk)= help3*lik*expik(mjk)
                        !print *,'lambda3',i,j,k,dr,expij(mjk),expik(mjk),dexpij(mjk),dexpik(mjk)
                    else
                        expij(mjk) = 1d0
                        expik(mjk) = 1d0
                        dexpij(mjk) = 0d0
                        dexpik(mjk) = 0d0
                    end if

                    z(ij) = z(ij) + fc(ik)*tersoffomegaik*gtij(mjk)*expij(mjk)
                    z(ik) = z(ik) + fc(ij)*tersoffomegaij*gtik(mjk)*expik(mjk)

                enddo ! <------ End of loop over k
            enddo                     ! <-------- End of second loop over neighbours j

            !
            !  Calculate f_A, f_R and b_ij
            !
            do ij = 1,nnbors          ! <-------- Third loop over neighbours j
                if (fc(ij) == 0d0) cycle

                ! Now xab is only used for virials, so add another box
                ! for scaling directly to the correct units.
                xab(:,ij) = box(:) * xab(:,ij)

                j = nborlist(in0+ij)
                j3=j*3-3

                typej=typeij(ij)
                Fatt = -tersB(typei,typej)*exp(-rmu(typei,typej)*ra(ij))
                Frep = tersA(typei,typej)*exp(-rlambda(typei,typej)*ra(ij))
                dFatt = -Fatt*rmu(typei,typej)
                dFrep = -Frep*rlambda(typei,typej)

                if (ters_n(typei,typej) /= 1d0) then
                    bzp = (beta(typei,typej)*z(ij))**ters_n(typei,typej)

                    ! This hack may be necessary for the Tersoff II Si potential
                    ! to avoid floating overflows.
                    ! if (beta(typei,typej)*z(ij) == 0) then
                    !    x=0
                    ! else
                    !    x=ters_n(typei,typej)*log(beta(typei,typej)*z(ij))
                    ! endif
                    !if (x > 690) then
                    !        print *,'Warning: power function overflow',x
                    !        x=690.01
                    !endif
                    !bzp=exp(x)

                    bzp1 = 1d0+bzp
                    ! K. Nordlund added xi here
                    bij = xi(typei,typej)*bzp1**ters2i(typei,typej)
                else
                    bzp = (beta(typei,typej)*z(ij))
                    bzp1 = 1d0+bzp
                    bij = xi(typei,typej)/sqrt(bzp1)
                endif
                !print *,'bij',bij,typei,typej,beta(typei,typej),z(ij),bzp1,ra(ij)

                ! Energies and 'two-body' forces done

                cpair=fc(ij)*Frep
                cmany=fc(ij)*bij*Fatt
                !print *,typei,typej,fc(ij),bij,Fatt
                ! Send in V=0 here because we split up cpair and cmany
                !V=cpair+cmany
                V=0;
                df=(fc(ij)*(dFrep+bij*dFatt)+dfc(ij)*(Frep+bij*Fatt))

                fermi=1d0
                dfermi=1d0
                if (ra(ij) < reppotcut(typei,typej)) then
                    !print '(A,4F10.3)','rep0',ra(ij),V,df

                    call reppot_fermi(ra(ij),V,df,bf(typei,typej),&
                        rf(typei,typej),typei,typej,fermi,dfermi)
                    !if (ra(ij) < 2.2) print '(A,4F10.3)','rep2',ra(ij),V,df,fermi

                    df=df+dfermi*(cpair+cmany)

                endif
                ! reppot routine has output (1.0d0-fermi)*Vrepulsive
                ! so it can be added to Epair without additional
                ! multiplication with the fermi term.
                cpair=cpair*0.5d0*fermi+0.5d0*V
                cmany=cmany*fermi
                df=df*0.5d0*rai(ij)

                Epair(i)=Epair(i)+cpair
                Epair(j)=Epair(j)+cpair
                Ethree(i)=Ethree(i)+cmany

                ! Use this to get consistency with Albe/Brenner formalism
                ! Does not work in parallel!
                !Ethree(i)=Ethree(i)+cmany/2.0
                !Ethree(j)=Ethree(j)+cmany/2.0

                if (z(ij) /= 0d0) then
                    db(ij) = -0.25d0*cmany*bzp/(bzp1*z(ij))
                else
                    db(ij) = 0d0
                endif

                ! Get ij derivative, i.e. everything except db terms

                c1 = df*xa(1,ij)
                xnp(i3+1) = xnp(i3+1)+c1
                xnp(j3+1) = xnp(j3+1)-c1
                wxxi(i) = wxxi(i) - c1 * xab(1,ij)
                ! if (x0(i3+1)*box(1)>10.0) then
                !     print *,'wxxi 1',wxxi(i),c1*xa(1,ij),c1,xa(1,ij),box(1),i,j,&
                !              x0(i3+1)*box(1),x0(j3+1)*box(1),pbc(1)
                ! end if

                if (calc_vir) then
                    wxyi(i) = wxyi(i) - c1 * xab(2,ij)
                    wxzi(i) = wxzi(i) - c1 * xab(3,ij)
                endif

                c1 = df*xa(2,ij)
                xnp(i3+2) = xnp(i3+2)+c1
                xnp(j3+2) = xnp(j3+2)-c1
                wyyi(i) = wyyi(i) - c1 * xab(2,ij)

                if (calc_vir) then
                    wyzi(i) = wyzi(i) - c1 * xab(3,ij)
                endif

                c1 = df*xa(3,ij)
                xnp(i3+3) = xnp(i3+3)+c1
                xnp(j3+3) = xnp(j3+3)-c1
                wzzi(i) = wzzi(i) - c1 * xab(3,ij)

            enddo                     ! <-------- End of third loop over neighbours j

            !
            !  Get three-body db derivatives
            !
            mjk = 0
            do ij = 1,nnbors-1        ! <-------- Fourth loop over neighbours j
                if (fc(ij) == 0d0) cycle

                j = nborlist(in0+ij)
                j3=j*3-3
                typej=typeij(ij)

                !!$
                do ik = ij+1,nnbors        ! <---- Loop over neighbours k
                    !if (ij==ik) cycle
                    if (fc(ik) == 0d0) cycle

                    typek=typeij(ik)
                    k = nborlist(in0+ik)
                    k3=k*3-3
                    mjk = mjk + 1

                    tersoffomegaij=tersoffomega(typei,typej,typek)
                    tersoffomegaik=tersoffomega(typei,typek,typej)
                    if (albepot) then
                        swap=tersoffomegaij; tersoffomegaij=tersoffomegaik; tersoffomegaik=swap;
                    endif


                    dbfcjk=db(ij)*fc(ik)*tersoffomegaik
                    dbfckj=db(ik)*fc(ij)*tersoffomegaij
                    dbjgt = db(ij)*gtij(mjk)*expij(mjk)
                    dbkgt = db(ik)*gtik(mjk)*expik(mjk)
                    dbfcdG = dGij(mjk)*dbfcjk*expij(mjk)+dGik(mjk)*dbfckj*expik(mjk)

                    dbfcdGcth = dbfcdG*cth(mjk)
                    dbfcdGrr = dbfcdG*rai(ik)*rai(ij)

                    dbfcgde=gtij(mjk)*dbfcjk*dexpij(mjk)-gtik(mjk)*dbfckj*dexpik(mjk)

                    cj=dbkgt*dfcxr(1,ij)*tersoffomegaij+dbfcdGrr*xa(1,ik)-dbfcdGcth*xr2(1,ij)
                    !cj=dbfcdGrr*xa(1,ik)-dbfcdGcth*xr2(1,ij)
                    cj=cj+dbfcgde*xai(1,ij)
                    ck=dbjgt*dfcxr(1,ik)*tersoffomegaik+dbfcdGrr*xa(1,ij)-dbfcdGcth*xr2(1,ik)
                    ck=ck-dbfcgde*xai(1,ik)

                    xnp(i3+1) = xnp(i3+1)+cj+ck
                    xnp(j3+1) = xnp(j3+1)-cj
                    xnp(k3+1) = xnp(k3+1)-ck
                    wxxi(i) = wxxi(i) - cj*xab(1,ij) - ck*xab(1,ik)
                    !if (x0(i3+1)*box(1)>10.0) print *,'wxxi 2',wxxi(i),cj*xa(1,ij),ck*xa(1,ik)

                    if (calc_vir) then
                        wxyi(i) = wxyi(i) - cj*xab(2,ij) - ck*xab(2,ik)
                        wxzi(i) = wxzi(i) - cj*xab(3,ij) - ck*xab(3,ik)
                    endif

                    cj=dbkgt*dfcxr(2,ij)*tersoffomegaij+dbfcdGrr*xa(2,ik)-dbfcdGcth*xr2(2,ij)
                    !cj=dbfcdGrr*xa(2,ik)-dbfcdGcth*xr2(2,ij)
                    cj=cj+dbfcgde*xai(2,ij)
                    ck=dbjgt*dfcxr(2,ik)*tersoffomegaik+dbfcdGrr*xa(2,ij)-dbfcdGcth*xr2(2,ik)
                    ck=ck-dbfcgde*xai(2,ik)

                    xnp(i3+2) = xnp(i3+2)+cj+ck
                    xnp(j3+2) = xnp(j3+2)-cj
                    xnp(k3+2) = xnp(k3+2)-ck
                    wyyi(i) = wyyi(i) - cj*xab(2,ij) - ck*xab(2,ik)

                    if (calc_vir) then
                        wyzi(i) = wyzi(i) - cj*xab(3,ij) - ck*xab(3,ik)
                    endif

                    cj=dbkgt*dfcxr(3,ij)*tersoffomegaij+dbfcdGrr*xa(3,ik)-dbfcdGcth*xr2(3,ij)
                    !cj=dbfcdGrr*xa(3,ik)-dbfcdGcth*xr2(3,ij)
                    cj=cj+dbfcgde*xai(3,ij)
                    ck=dbjgt*dfcxr(3,ik)*tersoffomegaik+dbfcdGrr*xa(3,ij)-dbfcdGcth*xr2(3,ik)
                    ck=ck-dbfcgde*xai(3,ik)

                    xnp(i3+3) = xnp(i3+3)+cj+ck
                    xnp(j3+3) = xnp(j3+3)-cj
                    xnp(k3+3) = xnp(k3+3)-ck
                    wzzi(i) = wzzi(i) - cj*xab(3,ij) - ck*xab(3,ik)

                enddo! <---- End of loop over neighbours k
            enddo                     ! <-------- Fourth loop over neighbours j
        enddo             ! <-------------------- End of loop over atoms i


        ! pass back forces and energies of atoms from neighboring processors.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            call potential_pass_back_border_atoms(xnp, Epair)
            tmr(TMR_SEMICON_COMMS) = tmr(TMR_SEMICON_COMMS) + (mpi_wtime() - t1)
        end if

        Epair(:myatoms) = 0.5d0 * Epair(:myatoms)
        Ethree(:myatoms) = 0.5d0 * Ethree(:myatoms)

    end subroutine Tersoff_Compound_Force



    ! ------------------------------------------------------------------------

! This subroutine exceptionally not indented due to very long lines.

subroutine Init_Ter_Compound_Pot(rc, i, j, potmode, reppotcutin)
!
! Parameters are taken from R.Smith, NIM B67 (1992) 335. and
! M.Sayed et al., NIM B 102 (1995) 218.
! They are changed into Tersoff notation [PRB 39 (1988) 5566.]
!
! This routine is called only for iac(i,j) == 1
!
! Potential is selected according to potmode and atom names.
!
!  - Even-numbered potentials (10,12,14,...) don't use lambda3
!    (lambda3 reset to 0 at end of subroutine)
!  - Odd-numbered potentials (11,13,15,...) use lambda3.
!
! Otherwise the numbers have pair-specific meanings:
!
! Si-Si       10,11  Tersoff III (= C)
!             12,13  Tersoff II  (= B)
! Ge-Ge       10,11  Plain tersoff for Ge
! C-C         10,11  C tersoff potential, original
!             12,13  C Tersoff potential with Nordlund PRL 77, 699 modif.
!
! B-B         10,11  B potential from Jpn. J. Appl. Phys. 39 (2000) L49
! N-N         10,11  N potential from Jpn. J. Appl. Phys. 39 (2000) L49
!
! Si-Ge       10,11   Tersoff parametrization for Si-Ge, PRB 39 (1989) 5566
! Si-C        10,11   Tersoff parametrization for Si-C,  PRB 39 (1989) 5566
! Ge-C        10,11   Tersoff parametrization for Ge-C,  PRB 39 (1989) 5566
!
! Ga-Ga       10,11   Sayed parametrization for GaAs
! Ga-As       10,11   Sayed parametrization for GaAs
! As-As       10,11   Sayed parametrization for GaAs
!
! Ga-Ga       12,13   Smith parametrization for GaAs
! Ga-As       12,13   Smith parametrization for GaAs
! As-As       12,13   Smith parametrization for GaAs
!
! Ga-Ga       14,15   Smith 'homogenic' parametrization for GaAs
! Ga-As       14,15   Smith 'homogenic' parametrization for GaAs
! As-As       14,15   Smith 'homogenic' parametrization for GaAs

! Ga-Ga       14,15   Smith parametrization for GaAs
! Ga-As       14,15   Smith parametrization for GaAs with Sayed g(theta)
! As-As       14,15   Smith parametrization for GaAs
!
! In-As       10,11   Ashu
! In-In       10,11   Ashu
! In-Ga       10,11   Ashu
! Al-Al       10,11   Sayed
! Al-As       10,11   Sayed
!
! Ga-Ga       90      Albe Ga parameters without 2mu rewritten into Smith form
! Ga-Sb       90      Parameters for Ga-Sb from PRB 75, 115202 (2007) without lambda3
! Sb-Sb       90      Sb rescaled from Albe As parameters, without 2mu
!
! Ga-Ga       20,21   Albe
! Ga-Ga       22,23   Albe (old)
! Ga-As       20,21   Albe
! As-As       20,21   Albe
!
! Ga-As       28,29   Albe best set for Ga-Ga, As-As and Ga-As (so far)
! Ga-Ga       28,29   Albe best set for GaAsN potential, shorter cutoff
! As-As       28,29   Albe best set for GaAsN potential
! As-N        28,29   Albe best set for GaAsN potential, fake potential!
!
!
! N-N         20,21   Albe V for N-N
! N-N         22,23   Albe III for N-N (ground state polymeric N)
! N-N         24,25   Albe IV for N-N
! N-N         26,27   Albe-Nord I for N-N
! N-N         28,29   Kotakoski new N-N potential
! B-N         50,51   Albe original B-N pot, Comp. Mat. Sci. 10, 111
!
! Ga-N        20,21   Albe-Nord I for Ga-N
! Ga-N        40,41   Albe-Nord best for Ga-N
!
! Pt-Pt       14,15   Albe
! Pt-C        14,15   Albe
! C-C         14,15   Brenner II params
!
! Pt-Pt       18,19   Albe
! Pt-C        18,19   Albe
! C-C         18,19   Brenner II params
!
! Zn-Zn       42,43   Erhart Zn I
! Zn-O        42,43   Erhart Zn-O I
! O-O         42,43   Juslin-Nordlund O
!
! Zn-Zn       44,45   Erhart Zn I longer cutoff
! Zn-O        44,45   Erhart Zn-O I longer cutoff
! O-O         44,45   Juslin-Nordlund O longer cutoff
!
! Si-Si       48,49   Munetoh-Motooka Si-O parametrization, Comp. Mat. Sci 39 (2007), 334
! Si-O        48,49   Munetoh-Motooka Si-O parametrization, Comp. Mat. Sci 39 (2007), 334
! O-O         48,49   Munetoh-Motooka Si-O parametrization, Comp. Mat. Sci 39 (2007), 334
!
! W-W         60,61   Albe-Salonen W 2nd-nearest neighbour model II
! W-W         62,63   Albe-Salonen W 2nd-nearest neighbour model I
! W-W         64,65   Juslin W 2nd-nearest neighbour model test
! W-W         66,67   Juslin best non-angular model
! W-W         85,85   Tommy Ahlgrens W-W potential with better defect props.
!
! W-C         68,69   Jannes version
!
! W-C         72,73   Pauls version "W-C II", better than 68,69
! W-C-H       74,75   WCH version published in 2005, with "W-C III"
! W-H         95      Potential by Xiao-Chun Li, JNM 408 (2011) 12. Note: albepot=.false.
! W-N         87,88   Jussi Polvi W-N potential (beta stage, 25.1.2016)
!
! Be-Be       74,75   Carolina Be-Be I and Niklas Be-H I
! Be-Be       82,83   Carolina Be-Be II and Be-C Niklas Be-H II
! Be-O        82,83   Be-O pot by Jesper Byggmastar, using Be-Be II and O-O short cutoff
! O-O         82,83   Same as potmode 42,43, but using short cutoff
! O-H         82,83   O-H pot for Be-O-H applications (See Hodille, J. Phys.: Condens: Matter 31 (2019) 185001)
!
! O-O         60,61   Juslin-Nordlund O model I
!
! Pt-Pt       60,61   The Fe-Pt potential of Michael M\"uller et al. (60 and 61 are identical.)
! Fe-Fe       60,61    See J. Phys.: Condens. Matter 19, 326220 (2007), Acta Mater. 55, 6617 (2007),
! Fe-Pt       60,61    and Michael's PhD thesis and refs. therein.
!                     Note that in some of the publications Fe-Fe and Pt-Pt parameters are the
!                       other way around in the tables.
! Fe-C        60,61   FeC potential by Krister Henriksson
! Fe-C        62,63   FeC potential by Krister Henriksson
! Fe-Cr       60-63   First FeCr potential by Krister Henriksson
! Fe-Cr       64,65   Second FeCr potential by Krister Henriksson
! Fe-O        64,65   Fe-O pot by Jesper Byggmastar, using O-O short cutoff
!
! Fe-Fe       70,71   Fe potential by Jesper Byggmastar
!
! Y-Y         64,65   Y potential by Morten Nagel
! Y-Y         66,67   Y potential by K. Fan et al, Chin. J. Chem. Phys. 26 (2013) 526
!
! Au-Au       60,61   Au potential by Marie Backman
!
! -           16,17   Read in Tersoff params
!
! InAlGaAs    18,19   'Best set' of parameters with respect to elastic and melt prop.
!
! Si-N        91,92   Mota Si3N4 potential, PRB 58, 8323 (1998)
!
!  Logic in subroutine:
!  1. Recognize interaction pair we want, set ii accordingly if Si, Ge C, otherwise set by hand
!  2. Get the appropriate parameters from a*(ii) arrays into 2D arrays
!     or for Si-Ge-C cross potentials construct them by hand
!
! reppotcutin determines whether the read-in high-energy
! repulsive potential is used or not (with the Fermi function), it is a
! simple switchoff value. I usually set it either to 10.0 (reppot used)
! or 0.0 (reppot not used).
!
! rc(typei,typej) is an array variable telling what the
! cutoff is per atom type pair typei,typej, given from this init routine
! to the main code.
! ------------------------------------------------------------------
! Variables passed in and out
real(real64b), intent(out) :: rc
real(real64b), intent(in) :: reppotcutin
integer, intent(in) :: potmode
integer, intent(in) :: i, j   ! Note that i,j here are atom types, not indices!

! Local help variables
real(real64b) :: R1, S1, R2, S2, xihelp
integer ii, ii1, ii2
integer :: ios, nl

real(real64b), dimension(0:MXTT, 0:MXTT) :: &
    brennerS, brennerDe, brennerre, brennerbeta, brennergamma, terslambda3

character(len=120) :: fname, terf(50)
character(len=8) :: name1, name2

! -------------------------------------------------------------------------------
! Si-Ge-C-O parameters

real(real64b), dimension(11) :: atersA, atersB, abeta, arlambda, armu, agc, agd, agh
real(real64b), dimension(11) :: aters_n, arlambda3, atrcut, adcut
real(real64b), dimension(11) :: abf, arf

!               Si III      Si II      Ge         Ge         C plain    C mod.     B          B res  N          N res  O
! ii value:     1           2          3          4          5          6          7          8      9          10     11
data atersA    /1.8308d3  , 3.2647d3 , 1.769d3  , 1.769d3  , 1.3936d3 , 1.3936d3 , 2.7702d2 , 0.0d0, 1.1000d4 , 0.0d0, 1.88255d3 /
data atersB    /4.7118d2  , 9.5373d1 , 4.1923d2 , 4.1923d2 , 3.4674d2 , 3.4674d2 , 1.8349d2 , 0.0d0, 2.1945d2 , 0.0d0, 2.18787d2 /
data abeta     /1.1000d-6 , 3.3675d-1, 9.0166d-7, 9.0166d-7, 1.5724d-7, 1.5724d-7, 1.6000d-6, 0.0d0, 1.0562d-1, 0.0d0, 1.1632d-7 /
data arlambda  /2.4799d0  , 3.2394d0 , 2.4451d0 , 2.4451d0 , 3.4879d0 , 3.4879d0 , 1.9922d0 , 0.0d0, 5.7708d0 , 0.0d0, 4.17108d0 /
data armu      /1.7322d0  , 1.3258d0 , 1.7047d0 , 1.7047d0 , 2.2119d0 , 2.2119d0 , 1.5856d0 , 0.0d0, 2.5115d0 , 0.0d0, 2.35692d0 /
data agc       /1.0039d5  , 4.8381d0 , 1.0643d5 , 1.0643d5 , 3.8049d4 , 3.8049d4 , 5.2629d-1, 0.0d0, 7.9934d4 , 0.0d0, 6.46921d4 /
data agd       /1.6217d1  , 2.0417d0 , 1.5625d1 , 1.5625d1 , 4.3484d0 , 4.3484d0 , 1.5870d-3, 0.0d0, 1.3432d2 , 0.0d0, 4.11127d0 /
data agh       /-5.9825d-1, 0.0d0    ,-4.3884d-1,-4.3884d-1,-5.7058d-1,-5.7058d-1, 0.5000d0 , 0.0d0,-0.9973d0 , 0.0d0,-8.45922d-1/
data aters_n   /7.8734d-1 , 2.2956d1 , 7.5627d-1, 7.5627d-1, 7.2751d-1, 7.2751d-1, 3.9929d0 , 0.0d0, 12.4498d0, 0.0d0, 1.04968d0 /
data arlambda3 /1.7322d0  , 1.3258d0 , 1.7047d0 , 1.7047d0 , 2.2119d0 , 2.2119d0 , 1.9922d0 , 0.0d0, 5.7708d0 , 0.0d0, 2.35692d0 /
data atrcut    /2.85d0    , 3.0d0    , 2.95d0   , 2.95d0   , 1.95d0   , 2.13d0   , 1.95d0   , 0.0d0, 2.15d0   , 0.0d0, 1.85d0    /
data adcut     /0.15d0    , 0.2d0    , 0.15d0   , 0.15d0   , 0.15d0   , 0.33d0   , 0.15d0   , 0.0d0, 0.15d0   , 0.0d0, 0.15d0    /
data abf       /12.0d0    , 12.0d0   , 12.0d0   , 12.0d0   , 14.0d0   , 14.0d0   , 10.0d0   , 0.0d0, 10.0d0   , 0.0d0, 12.0d0    /
data arf       /1.60d0    , 1.60d0   , 1.66d0   , 1.66d0   , 0.95d0   , 0.95d0   , 0.75d0   , 0.0d0, 0.75d0   , 0.0d0, 0.4d0     /

! --------------------------------------------------------------------------------

if (i > MXTT .or. j > MXTT) then
    call logger("MXTT too low in tersoff_compound")
    call logger("MXTT:", MXTT, 4)
    call logger("i:   ", i, 4)
    call logger("j:   ", j, 4)
    call my_mpi_abort('MXTT too low', int(MXTT))
endif

! Initialize some variables to 'zero' values just to be on safe side
xihelp=1.0d0
albepot=.false.
exponentone=.false.
albegamma(i,j)=1.0d0
albecossign(i,j)=-1.0d0
albe2mu(i,j)=0.0d0
brennergamma(i,j)=1.0d0
brennerre(i,j)=0.0d0
brennerDe(i,j)=0.0d0
brennerS(i,j)=0.0d0
brennerbeta(i,j)=1.0d0
terslambda3(i,j)=0.0d0

name1=element(i)
name2=element(j)

! Find out if we are dealing with Si-Ge-C-B-N-O systems
ii=-1; ii1=-1; ii2=-1;
if (potmode>=10 .and. potmode <=13 .or. potmode==48 .or. potmode==49) then
    if (name1=='Si' .and. name2=='Si') then
        if (potmode==10 .or. potmode==11) ii=1;
        if (potmode==12 .or. potmode==13) ii=2;
        if (potmode==48 .or. potmode==49) then ! ilinov; addition for Si-O
            ii=1; atrcut(1)=2.65d0;
        endif
    else if (name1=='Ge' .and. name2=='Ge') then
        if (potmode==10 .or. potmode==11) ii=3;
        if (potmode==12 .or. potmode==13) ii=4;
    else if (name1=='C' .and. name2=='C') then
        if (potmode==10 .or. potmode==11) ii=5;
        if (potmode==12 .or. potmode==13) ii=6;
    else if (name1=='Si' .and. name2=='Ge') then
        ii1=1; ii2=3; xihelp=1.00061d0;
    else if (name1=='Ge' .and. name2=='Si') then
        ii1=3; ii2=1; xihelp=1.00061d0;
    else if (name1=='Si' .and. name2=='C') then
        ii1=1; ii2=5; xihelp=0.9776d0;
    else if (name1=='C' .and. name2=='Si') then
        ii1=5; ii2=1; xihelp=0.9776d0;
    else if (name1=='Ge' .and. name2=='C') then
        ii1=3; ii2=5; ! xi not given by Tersoff
    else if (name1=='C' .and. name2=='Ge') then
        ii1=5; ii2=3; ! xi not given by Tersoff
    else if (name1=='B' .and. name2=='B') then
        ii=7
    else if (name1=='N' .and. name2=='N') then
        ii=9
    else if (name1=='B' .and. name2=='N') then
        ii1=7; ii2=9; xihelp=1.1593d0;
    else if (name1=='N' .and. name2=='B') then
        ii1=9; ii2=7; xihelp=1.1593d0;
    else if (name1=='B' .and. name2=='C') then
        ii1=7; ii2=5; xihelp=1.0025d0;
    else if (name1=='C' .and. name2=='B') then
        ii1=5; ii2=7; xihelp=1.0025d0;
    else if (name1=='N' .and. name2=='C') then
        ii1=9; ii2=5; xihelp=0.9685d0; ! tersoffomega set in Init3_
    else if (name1=='C' .and. name2=='N') then
        ii1=5; ii2=9; xihelp=0.9685d0; ! tersoffomega set in Init3_
        !++ ilinov; Si-O stuff
    else if (name1=='Si' .and. name2=='O') then
        ii1=1; ii2=11; xihelp=1.17945d0;
        atrcut(1)=2.65d0;  ! override rcut for Si
    else if (name1=='O' .and. name2=='Si') then
        ii1=11; ii2=1; xihelp=1.17945d0;
        atrcut(1)=2.65d0;  ! override rcut for Si
    else if (name1=='O' .and. name2=='O' .and. (potmode==48 .or. potmode==49)) then
        ii=11
        !-- ilinov; Si-O stuff
    endif
endif

! ------------------------------------------------------------------------- !
! ----------------------- Tersoff - format parameters --------------------- !
! ------------------------------------------------------------------------- !
if (ii>=0) then                           ! pure Si or Ge or C

    if (iprint) print *, 'Using elemental SiGeC Tersoff pot for pair ',name1,name2;

    tersA(i,j)=atersA(ii)
    tersB(i,j)=atersB(ii)
    beta(i,j)=abeta(ii)
    rlambda(i,j)=arlambda(ii)
    rmu(i,j)=armu(ii)
    gc(i,j)=agc(ii)
    gd(i,j)=agd(ii)
    gh(i,j)=agh(ii)
    ters_n(i,j)=aters_n(ii)
    trcut(i,j)=atrcut(ii)
    dcut(i,j)=adcut(ii)
    terslambda3(i,j)=arlambda3(ii)

    bf(i,j)=abf(ii)
    rf(i,j)=arf(ii)

else if (ii1>=0 .and. ii2>=0) then       ! Si-Ge-C-N-B-O mixture

    if (iprint) print *, 'Using mixture SiGeC Tersoff pot for pair ', &
        name1,' ',name2,' with xi=',xihelp

    gc(i,j)=agc(ii1);
    gd(i,j)=agd(ii1);
    gh(i,j)=agh(ii1);
    ters_n(i,j)=aters_n(ii1);

    rlambda(i,j)=(arlambda(ii1)+arlambda(ii2))/2.0;
    tersA(i,j)=sqrt(atersA(ii1)*atersA(ii2));
    tersB(i,j)=sqrt(atersB(ii1)*atersB(ii2));
    rmu(i,j)=(armu(ii1)+armu(ii2))/2.0;
    beta(i,j)=abeta(ii1);

    ! The cutoffs are defined from S and R, not R and D
    ! Hence need to first calc R and S, then new R and D
    R1=atrcut(ii1)-adcut(ii1); S1=atrcut(ii1)+adcut(ii1)
    R2=atrcut(ii2)-adcut(ii2); S2=atrcut(ii2)+adcut(ii2)
    trcut(i,j)=(sqrt(R1*R2)+sqrt(S1*S2))/2.0
    dcut(i,j)=sqrt(S1*S2)-trcut(i,j);

    bf(i,j)=sqrt(abf(ii1)*abf(ii2));
    rf(i,j)=(arf(ii1)+arf(ii2))/2.0
    terslambda3(i,j)=(arlambda3(ii1)+arlambda3(ii2))/2.0

else if (potmode==16 .or. potmode==17) then

    fname = "in/tersoff." // trim(name1) // "." // trim(name2) // ".in"
    open(unit=TEMP_FILE, file=fname, status="old", iostat=ios)
    if (ios /= 0) then
        fname = "in/tersoff." // trim(name2) // "." // trim(name1) // ".in"
        open(unit=TEMP_FILE, file=fname, status="old", iostat=ios)
    end if
    if (ios /= 0) then
        call logger("Error during opening " // trim(fname))
        call my_mpi_abort('Tersoff param error', myproc)
    end if

    if (iprint) then
        call logger("Reading in Tersoff params from", fname, 0)
    end if
    do nl = 1, size(terf)
        read(TEMP_FILE, '(A80)', iostat=ios) terf(nl)
        if (ios < 0) exit
        if (ios > 0) then
            call my_mpi_abort("Error reading file " // trim(fname), ios)
        end if
    end do
    if (nl > size(terf)) then
        call my_mpi_abort("Too long tersoff file " // trim(fname), myproc)
    end if
    nl = nl - 1

    close(TEMP_FILE)


    call readcreal('bf ',bf(i,j),terf,nl,12.0d0)
    call readcreal('rf ',rf(i,j),terf,nl,1.0d0)
    call readcreal('c ',gc(i,j),terf,nl,0.0d0)
    call readcreal('d ',gd(i,j),terf,nl,0.0d0)
    call readcreal('h ',gh(i,j),terf,nl,0.0d0)
    call readcreal('n ',ters_n(i,j),terf,nl,0.0d0)
    call readcreal('beta ',beta(i,j),terf,nl,0.0d0)
    call readcreal('mu ',rmu(i,j),terf,nl,0.0d0)
    call readcreal('lambda ',rlambda(i,j),terf,nl,0.0d0)
    call readcreal('A ',tersA(i,j),terf,nl,0.0d0)
    call readcreal('B ',tersB(i,j),terf,nl,0.0d0)
    call readcreal('trcut ',trcut(i,j),terf,nl,3.6d0)
    call readcreal('dcut ',dcut(i,j),terf,nl,1.0d0)
    call readcreal('lambda3 ',terslambda3(i,j),terf,nl,0.0d0)


! Ashu In-In
else if (name1=='In' .and. name2=='In' .and. &
    ((potmode>=10.and.potmode<=11).or.(potmode>=18 .and. potmode<=19))) then

    bf(i,j)=12.0d0
    rf(i,j)=1.0d0
    gc(i,j)=0.084215d0
    gd(i,j)=19.2626d0
    gh(i,j)=7.39228d0
    ters_n(i,j)=3.40223d0
    beta(i,j)=2.10871d0
    rmu(i,j)=1.68117d0
    rlambda(i,j)=2.6159d0
    tersA(i,j)=2975.54d0
    tersB(i,j)=360.61d0
    trcut(i,j)=3.6d0
    dcut(i,j)=0.10d0

! Ashu In-Ga
else if (((name1=='In' .and. name2=='Ga') .or. (name1=='Ga' .and. name2=='In')) .and. &
    ((potmode>=10.and.potmode<=11).or. (potmode>=18 .and. potmode<=19))) then

    bf(i,j)=12.0d0
    rf(i,j)=1.0d0
    gc(i,j)=0.080256d0
    gd(i,j)=195.2950d0
    gh(i,j)=7.26910d0
    ters_n(i,j)=3.43739d0
    beta(i,j)=0.70524d0
    rmu(i,j)=1.58600d0
    rlambda(i,j)=2.5621d0
    tersA(i,j)=1214.917d0
    tersB(i,j)=177.22d0
    trcut(i,j)=3.6d0
    dcut(i,j)=0.10d0

! Janne Nord averaged Al-Ga term
else if (((name1=='Ga' .and. name2=='Al') .or. &
    (name1=='Al' .and. name2=='Ga')) .and. (potmode>=18 .and. potmode<=19)) then

    bf(i,j)=12.0d0
    rf(i,j)=1.0d0
    gc(i,j)=0.0755633d0
    gd(i,j)=19.6825d0
    gh(i,j)=3.2433d0
    ters_n(i,j)=4.59759d0
    beta(i,j)=0.273372d0
    rmu(i,j)=1.20913d0
    rlambda(i,j)=2.54688d0
    tersA(i,j)=699.824d0
    tersB(i,j)=55.9917d0
    trcut(i,j)=3.5d0
    dcut(i,j)=0.10d0

! Ashu In-As
else if (((name1=='In' .and. name2=='As') .or. &
    (name1=='As' .and. name2=='In')) .and. (potmode>=10 .and. potmode<=11)) then

    bf(i,j)=12.0d0
    rf(i,j)=1.0d0
    gc(i,j)=1.30678d0
    gd(i,j)=91.4432d0
    gh(i,j)=-0.56983d0
    ters_n(i,j)=6.33190d0
    beta(i,j)=0.38712d0
    rmu(i,j)=1.67123d0
    rlambda(i,j)=2.53034d0
    tersA(i,j)=2246.55d0
    tersB(i,j)=417.665d0
    trcut(i,j)=3.6d0
    dcut(i,j)=0.10d0

! Janne Nord's best In-As term
else if (((name1=='In' .and. name2=='As') .or. &
    (name1=='As' .and. name2=='In')) .and. (potmode>=18 .and. potmode<=19)) then

    bf(i,j)=12.0d0
    rf(i,j)=1.0d0
    gc(i,j)     = 5.172421d0
    gd(i,j)     = 1.665967d0
    gh(i,j)     = -5.413316d-1
    ters_n(i,j) = 7.561694d-1
    beta(i,j)   = 3.186402d-1
    rmu(i,j)    = 1.422429d0
    rlambda(i,j)= 2.597556d0
    tersA(i,j)  = 1968.295443
    tersB(i,j)  = 266.571631
    trcut(i,j)  = 3.6d0
    dcut(i,j)   = 0.10d0


! Janne Nord's best In-Al term
else if (((name1=='In' .and. name2=='Al') .or. &
    (name1=='Al' .and. name2=='In')) .and. potmode>=18 .and.potmode<=19) then

    bf(i,j)=12.0d0
    rf(i,j)=1.0d0
    gc(i,j)=0.0793871d0
    gd(i,j)=19.4153d0
    gh(i,j)=2.2076d0
    ters_n(i,j)=4.55057d0
    beta(i,j)=0.817396d0
    rmu(i,j)=1.30431d0
    rlambda(i,j)=2.60062d0
    tersA(i,j)=1210.89d0
    tersB(i,j)=91.1332d0
    trcut(i,j)=3.5d0
    dcut(i,j)=0.10d0

! Mota 1998 Si3N4 potential Si-N term
else if (((name1=='Si' .and. name2=='N') .or. &
    (name1=='N' .and. name2=='Si')) .and. potmode>=91 .and. potmode<=92) then

    bf(i,j)=12.0d0  ! Check, these are just defaults
    rf(i,j)=1.0d0   ! Check, these are just defaults
    rmu(i,j)=2.2161d0
    rlambda(i,j)=3.958315d0
    tersA(i,j)=3414.525919d0
    tersB(i,j)=319.1842545d0
    trcut(i,j)=2.35726d0
    dcut(i,j)=0.15272d0
    if (name1=='Si') then
        gc(i,j)=1.00393d5 ! Explained in Si3N4_structure_unitcell.xyz, using Mota paper definitions
        gd(i,j)=1.62173d1
        gh(i,j)=-5.98253d-1
        ters_n(i,j)=7.87343d-1
        beta(i,j)=1.10003d-6
    else     ! for N
        gc(i,j)=2.031203d4
        gd(i,j)=2.551033d1
        gh(i,j)=-5.623903d-1
        ters_n(i,j)=1.33041
        beta(i,j)=5.293803d-3
    endif

! Mota 1998 Si3N4 potential Si-Si term (original Tersoff III)
else if ((name1=='Si' .and. name2=='Si') .and. &
    potmode>=91 .and. potmode<=92) then

    bf(i,j)=12.0d0
    rf(i,j)=1.6d0
    rmu(i,j)=1.7322d0
    rlambda(i,j)=2.4799d0
    tersA(i,j)=1.83083d3
    tersB(i,j)=4.71183d2
    trcut(i,j)=2.85d0
    dcut(i,j)=0.15d0
    gc(i,j)=1.00393d5
    gd(i,j)=1.62173d1
    gh(i,j)=-5.98253d-1
    ters_n(i,j)=7.87343d-1
    beta(i,j)=1.10003d-6

! Mota 1998 Si3N4 potential N-N term (only repulsive part for Si3N4)
else if ((name1=='N' .and. name2=='N') .and. &
    potmode>=91 .and. potmode<=92) then

    bf(i,j)=10.0d0
    rf(i,j)=0.75d0
    rmu(i,j)=2.7d0
    rlambda(i,j)=5.43673d0
    tersA(i,j)=6.368143d3
    tersB(i,j)=0d0
    trcut(i,j)=1.95d0
    dcut(i,j)=0.15d0
    gc(i,j)=2.031203d4
    gd(i,j)=2.551033d1
    gh(i,j)=-5.623903d-1
    ters_n(i,j)=1.33041
    beta(i,j)=5.293803d-3


else ! Not Tersoff Si-Ge-C or In
    ! ------------------------------------------------------------------------- !
    ! -------------------- Brenner/Smith - format parameters ------------------ !
    ! ------------ Smith, Sayed, Powell and hybrid III-V params --------------- !
    ! ------------------------------------------------------------------------- !

    if (iprint) print *, 'Using customized Tersoff pot for pair ',name1,name2

    ! Set default Fermi parameters
    ! 12 and 1.0d0 obtained by Kai Nordlund 8.1 1999 for Sayed through checking through all
    ! possible bij values for GaGa, GaAs and AsAs. This value gives smooth fits
    ! in all possible cases. The reppot interaction commences above ~ 20 eV.
    ! For Albe pot this gives decent fits, but better values are given below
    bf(i,j)=12.0d0
    rf(i,j)=1.0d0

    ! Sayed Al-Al
    if ((name1=='Al'.and.name2=='Al').and. &
        ((potmode>=10.and.potmode<=13).or. (potmode>=18 .and. potmode<=19))) then

        gc(i,j)=0.074836d0
        gd(i,j)=19.569127d0
        gh(i,j)=-0.659266d0
        ters_n(i,j)=6.086505d0
        brennerbeta(i,j)=1.094932d0
        brennergamma(i,j)=0.316846d0
        brennerDe(i,j)=1.50d0
        brennerre(i,j)=2.466d0 !2.37 Contradiction between values in tables 1 and 2 in Smith's paper!
        brennerS(i,j)=2.787598d0
        trcut(i,j)=3.6d0
        dcut(i,j)=0.10d0
        terslambda3(i,j)=1.5d0

        !if (potmode>=24.and.potmode<=25) then
        !    dcut(i,j)=0.3d0
        !endif


    ! Sayed Al-As
    else if (((name1=='Al'.and.name2=='As').or.(name1=='As' .and. name2=='Al')) .and. &
        ((potmode>=10.and.potmode<=13).or. (potmode>=18 .and. potmode<=19))) then

        gc(i,j)=1.449752d0
        gd(i,j)=0.828713d0
        gh(i,j)=-0.520946d0
        ters_n(i,j)=4.047579d0
        brennerbeta(i,j)=1.479377d0
        brennergamma(i,j)=0.330946d0
        brennerDe(i,j)=2.492976d0
        brennerre(i,j)=2.353425d0 !2.37 Contradiction between values in tables 1 and 2 in Smith's paper!
        brennerS(i,j)=1.802706d0
        trcut(i,j)=3.6d0
        dcut(i,j)=0.10d0
        terslambda3(i,j)=1.5d0

    else if (name1=='Ga' .and. name2=='Ga' .and. &
        ((potmode>=10.and.potmode<=13).or. (potmode>=18 .and. potmode<=19))) then

        ! Both Smith and Sayed Ga
        gc(i,j)=0.07629773d0
        gd(i,j)=19.796474d0
        gh(i,j)=7.1459174d0
        ters_n(i,j)=3.4729041d0
        brennerbeta(i,j)=1.36741067d0
        brennergamma(i,j)=0.23586237d0
        brennerDe(i,j)=1.40d0
        brennerre(i,j)=2.465d0 !2.37 Contradiction between values in tables 1 and 2 in Smith's paper!
        brennerS(i,j)=1.68257787d0
        trcut(i,j)=3.5d0
        dcut(i,j)=0.10d0
        terslambda3(i,j)=1.491d0

    else if ((name1=='Ga' .and. name2=='Ga') .and. (potmode>=14.and.potmode<=15)) then

        ! Smith 'homogenic' Ga
        gc(i,j)=0.07837998d0
        gd(i,j)=4.5049412d0
        gh(i,j)=-3.4109679d0
        ters_n(i,j)=5.5039372d0
        brennerbeta(i,j)=1.6137669d0
        brennergamma(i,j)=0.38090204d0
        brennerDe(i,j)=2.18d0
        brennerre(i,j)=2.345d0
        brennerS(i,j)=1.53511190d0
        trcut(i,j)=3.5d0
        dcut(i,j)=0.10d0
        ! Smith doesn't give lambda3

    else if (((name1=='Ga' .and. name2=='As') .or. (name1=='As' .and. name2=='Ga')) .and. &
        ((potmode>=10.and.potmode<=11).or. (potmode>=18 .and. potmode<=19))) then

        ! Sayed Ga-As terms
        gc(i,j)=1.226302d0
        gd(i,j)=0.790396d0
        gh(i,j)=-0.518489d0
        ters_n(i,j)=6.317410d0
        brennerbeta(i,j)=1.560903d0
        brennergamma(i,j)=0.357192d0
        brennerDe(i,j)=2.18d0
        brennerre(i,j)=2.34d0
        brennerS(i,j)=1.641366d0
        trcut(i,j)=3.5d0
        dcut(i,j)=0.1d0
        terslambda3(i,j)=1.723d0

    else if (((name1=='Ga' .and. name2=='As') .or. (name1=='As' .and. name2=='Ga')) &
        .and. (potmode>=12.and.potmode<=15)) then

        ! Smith Ga-As terms
        gc(i,j)=0.07837998d0
        gd(i,j)=4.5049412d0
        gh(i,j)=-3.4109679d0
        ters_n(i,j)=5.5039372d0
        brennerbeta(i,j)=1.6137669d0
        brennergamma(i,j)=0.38090204d0
        brennerDe(i,j)=2.18d0
        brennerre(i,j)=2.345d0
        brennerS(i,j)=1.53511190d0
        trcut(i,j)=3.5d0
        dcut(i,j)=0.10d0
        ! Smith doesn't give lambda3

    else if (name1=='As' .and. name2=='As' .and. &
        ((potmode>=10.and.potmode<=13).or. (potmode>=18 .and. potmode<=19))) then

        ! Smith/Sayed As-As terms
        gc(i,j)=5.2731318d0
        gd(i,j)=0.75102662d0
        gh(i,j)=0.15292354d0
        ters_n(i,j)=0.60879133d0
        brennerbeta(i,j)=1.43553337d0
        brennergamma(i,j)=0.00748809d0
        brennerDe(i,j)=3.964d0
        brennerre(i,j)=2.1026d0
        brennerS(i,j)=1.37912649d0
        trcut(i,j)=3.5d0
        dcut(i,j)=0.1d0
        terslambda3(i,j)=1.729d0 !1.500 Sayed et al. give TWO values in their table 2 !!

    else if (name1=='As' .and. name2=='As' .and. potmode>=14.and.potmode<=15) then

        ! Smith 'homogenic' As-As
        gc(i,j)=0.07837998d0
        gd(i,j)=4.5049412d0
        gh(i,j)=-3.4109679d0
        ters_n(i,j)=5.5039372d0
        brennerbeta(i,j)=1.6137669d0
        brennergamma(i,j)=0.38090204d0
        brennerDe(i,j)=2.18d0
        brennerre(i,j)=2.345d0
        brennerS(i,j)=1.53511190d0
        trcut(i,j)=3.5d0
        dcut(i,j)=0.10d0
        ! Smith doesn't give lambda3

    ! ---- Hybrid III-V params for Ga-Sb from Albe and Powell ----

    else if ( (name1=='Ga' .and. name2=='Ga') .and. potmode==90 ) then

        ! Albe, PRB 66, 035202 Ga params rewritten to Sayed/Powell form

        albepot=.false.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =-1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.007874000000000d0
        brennerS(i,j)      = 1.110000000000000d0
        brennerbeta(i,j)   = 1.080000000000000d0
        brennerDe(i,j)     = 1.400000000000000d0
        brennerre(i,j)     = 2.323500000000000d0
        gc(i,j)            = 1.918000000000000d0
        gd(i,j)            = 0.750000000000000d0
        gh(i,j)            = -0.301300000000000d0 ! Sign change because of albecossign

        trcut(i,j)         = 2.950000000000000d0
        dcut(i,j)          = 0.150000000000000d0

        bf(i,j)=12.0d0
        rf(i,j)=1.2d0

    else if ( ((name1=='Ga' .and. name2=='Sb') .or. &
        (name1=='Sb' .and. name2=='Ga')) .and.  &
        potmode==90 ) then

        albepot=.false.
        albecossign(i,j)   =-1.0d0

        ! Ga-Sb from Powell et al, PRB 75, 115202 2007
        !
        gc(i,j)            = 1.20875d0
        gd(i,j)            = 0.839761d0
        gh(i,j)            = -0.427706d0
        ters_n(i,j)        = 4.60221d0
        brennerbeta(i,j)   = 1.4777d0
        brennergamma(i,j)  = 0.363018d0
        brennerDe(i,j)     = 2.10427d0
        brennerre(i,j)     = 2.4991d0
        brennerS(i,j)      = 1.43393d0

        trcut(i,j)         = 3.500000000000000d0
        dcut(i,j)          = 0.100000000000000d0

        bf(i,j)=12.0d0
        rf(i,j)=1.0d0

    else if (name1=='Sb' .and. name2=='Sb' .and. potmode==90 ) then

        ! Albe As-As rescaled into Sb with lattice constant and Ecohesive
        ! Scaling factors obtained from As potential, Webminerals for a and CRC for Ecoh
        ! Scaling factor of 1.19d0 obtained to get correct density for system

        albepot=.false.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =-1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.455000000000000d0
        brennerS(i,j)      = 1.860000000000000d0
        brennerbeta(i,j)   = 1.435000000000000d0
        brennerDe(i,j)     = 3.960000000000000d0*2.718d0/2.8874d0  ! Scale Ecoh
        brennerre(i,j)     = 2.100000000000000d0*1.190d0   ! Scale a
        gc(i,j)            = 0.118600000000000d0
        gd(i,j)            = 0.161200000000000d0
        gh(i,j)            = -0.077480000000000D0 ! Sign change because of albecossign

        trcut(i,j)         = 3.400000000000000d0*1.190d0  ! Scale a
        dcut(i,j)          = 0.200000000000000d0*1.190d0  ! Scale a

        bf(i,j)=12.0d0
        rf(i,j)=1.0d0*1.190d0



    ! ------------------------------------------------------------------- !
    ! ----------------- Karsten Albe's Ga-As stuff etc. ----------------- !
    ! ------------------------------------------------------------------- !

    else if (name1=='Ga' .and. name2=='Ga' .and. &
        ((potmode>=20.and.potmode<=21).or.(potmode>=28.and.potmode<=29))) then

        ! Albe Ga-Ga

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.007874000000000d0
        brennerS(i,j)      = 1.110000000000000d0
        brennerbeta(i,j)   = 1.080000000000000d0
        brennerDe(i,j)     = 1.400000000000000d0
        brennerre(i,j)     = 2.323500000000000d0
        gc(i,j)            = 1.918000000000000d0
        gd(i,j)            = 0.750000000000000d0
        gh(i,j)            = 0.301300000000000d0
        albe2mu(i,j)       = 1.846000000000000d0

        trcut(i,j)         = 2.950000000000000d0
        dcut(i,j)          = 0.150000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

        bf(i,j)=12.0d0
        rf(i,j)=1.2d0

    else if (name1=='Ga' .and. name2=='Ga' .and. &
        (potmode>=22.and. potmode<=23)) then

        ! Albe Ga-Ga
        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000024000000000d0
        brennerS(i,j)      = 1.118000000000000d0
        brennerbeta(i,j)   = 1.080000000000000d0
        brennerDe(i,j)     = 1.400000000000000d0
        brennerre(i,j)     = 2.357000000000000d0
        gc(i,j)            = 40.000000000000000d0
        gd(i,j)            = 0.700000000000000d0
        gh(i,j)            = 0.334400000000000d0
        albe2mu(i,j)       = 1.829000000000000d0

        trcut(i,j)         = 3.200000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

    else if (name1=='Ga' .and. name2=='Ga' .and. &
        (potmode>=24.and. potmode<=25)) then

        ! Albe Ga-Ga
        ! As potmode 22-23, but h modified to give right Epot.
        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000024000000000d0
        brennerS(i,j)      = 1.118000000000000d0
        brennerbeta(i,j)   = 1.080000000000000d0
        brennerDe(i,j)     = 1.400000000000000d0
        brennerre(i,j)     = 2.357000000000000d0
        gc(i,j)            = 40.00000000000000d0
        gd(i,j)            = 0.700000000000000d0
        gh(i,j)            = 0.334400000000000d0+0.112d0
        albe2mu(i,j)       = 1.829000000000000d0

        trcut(i,j)         = 3.200000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif


    else if (name1=='As' .and. name2=='As' .and. &
        (potmode>=20.and. potmode<=21)) then

        ! Albe As-As III

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000200000000000d0
        brennerS(i,j)      = 1.740000000000000d0
        brennerbeta(i,j)   = 1.435000000000000d0
        brennerDe(i,j)     = 3.960000000000000d0
        brennerre(i,j)     = 2.100000000000000d0
        gc(i,j)            = 1.321000000000000d0
        gd(i,j)            = 0.025000000000000d0
        gh(i,j)            = 0.050000000000000d0
        albe2mu(i,j)       = 3.790000000000000d0

        trcut(i,j)         = 3.400000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

    else if (name1=='As' .and. name2=='As' .and. &
        ((potmode>=22.and. potmode<=23))) then

        ! Albe As-As II

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.488000000000000d0
        brennerS(i,j)      = 1.940000000000000d0
        brennerbeta(i,j)   = 1.307800000000000d0
        brennerDe(i,j)     = 3.960000000000000d0
        brennerre(i,j)     = 2.100000000000000d0
        gc(i,j)            = 0.148000000000000d0
        gd(i,j)            = 0.188000000000000d0
        gh(i,j)            = 0.085000000000000d0
        albe2mu(i,j)       = 2.860000000000000d0

        trcut(i,j)         = 3.400000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

    else if (name1=='As' .and. name2=='As' .and. &
        ((potmode>=26.and. potmode<=27) &
        .or.(potmode>=28.and.potmode<=29))) then

        ! Albe As-As IV

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0


        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.455000000000000d0
        brennerS(i,j)      = 1.860000000000000d0
        brennerbeta(i,j)   = 1.435000000000000d0
        brennerDe(i,j)     = 3.960000000000000d0
        brennerre(i,j)     = 2.100000000000000d0
        gc(i,j)            = 0.118600000000000d0
        gd(i,j)            = 0.161200000000000d0
        gh(i,j)            = 0.077480000000000D0
        albe2mu(i,j)       = 3.161000000000000d0

        trcut(i,j)         = 3.400000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

        bf(i,j)=12.0d0
        rf(i,j)=1.0d0

    else if (name1=='As' .and. name2=='As' .and. &
        (potmode>=22.and. potmode<=23)) then

        ! Albe As-As II

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.488000000000000d0
        brennerS(i,j)      = 1.940000000000000d0
        brennerbeta(i,j)   = 1.307800000000000d0
        brennerDe(i,j)     = 3.960000000000000d0
        brennerre(i,j)     = 2.100000000000000d0
        gc(i,j)            = 0.148000000000000d0
        gd(i,j)            = 0.188000000000000d0
        gh(i,j)            = 0.085000000000000d0
        albe2mu(i,j)       = 2.860000000000000d0

        trcut(i,j)         = 3.400000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

    else if (name1=='As' .and. name2=='As' .and. &
        (potmode>=24.and. potmode<=25)) then

        ! Albe As-As
        !
        ! As parameters which give stable alpha-As
        ! but make a Ga-As melt phase separate
        !
        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.385000000000000d0
        ! These values prevent Ga-As separation in the melt
        !brennerS(i,j)      = 1.1d0    !1.700000000000000d0
        !brennerbeta(i,j)   = 1.748d0  !1.190000000000000d0
        brennerS(i,j)      = 1.700000000000000d0
        brennerbeta(i,j)   = 1.190000000000000d0
        brennerDe(i,j)     = 3.960000000000000d0
        brennerre(i,j)     = 2.100000000000000d0
        gc(i,j)            = 0.090000000000000d0
        gd(i,j)            = 0.150000000000000d0
        gh(i,j)            = 0.080000000000000d0
        albe2mu(i,j)       = 1.910000000000000d0

        trcut(i,j)         = 3.400000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

    else if ((name1=='Ga' .and. name2=='As') .or. &
        (name1=='As' .and. name2=='Ga') .and.  &
        ((potmode>=20.and.potmode<=21).or.(potmode>=28.and.potmode<=29))) then

        !
        ! Albe Ga-As, cutoff modified to 3.1 from Jannes simulations.
        !
        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0

        albegamma(i,j)     = 0.016600000000000d0
        brennerS(i,j)      = 1.141700000000000d0
        brennerbeta(i,j)   = 1.522800000000000d0
        brennerDe(i,j)     = 2.100000000000000d0
        brennerre(i,j)     = 2.350000000000000d0
        gc(i,j)            = 1.290000000000000d0
        gd(i,j)            = 0.560000000000000d0
        gh(i,j)            = 0.237000000000000d0
        ! 2mu -7.00000000000000d0 would give right C44
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 3.100000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

        bf(i,j)=12.0d0
        rf(i,j)=1.0d0

    else if ((name1=='As' .and. name2=='N') .or. &
        (name1=='N' .and. name2=='As') .and.  &
        ((potmode>=28.and.potmode<=29))) then

        !
        ! Fake As-N parameters: just the As-As parameters for As-N!!
        !
        print *,'Warning: using fake As-N parameters based on pure As'

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0


        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.455000000000000d0
        brennerS(i,j)      = 1.860000000000000d0
        brennerbeta(i,j)   = 1.435000000000000d0
        brennerDe(i,j)     = 3.960000000000000d0
        brennerre(i,j)     = 2.100000000000000d0
        gc(i,j)            = 0.118600000000000d0
        gd(i,j)            = 0.161200000000000d0
        gh(i,j)            = 0.077480000000000D0
        albe2mu(i,j)       = 3.161000000000000d0

        trcut(i,j)         = 3.400000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

        bf(i,j)=12.0d0
        rf(i,j)=1.0d0


    ! ------------------------------------------------------------------- !
    ! ----------------- Karsten Albe's Ga-N stuff ---------------------- !
    ! ------------------------------------------------------------------- !

    ! Here the potmode numbering is a mess. But one should use modes>=40,
    ! the smaller numbers are deprecated.

    else if (name1=='Ga' .and. name2=='Ga' .and. &
        ((potmode>=40.and.potmode<=41)) ) then

        ! Albe Ga-Ga

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.007874000000000d0
        brennerS(i,j)      = 1.110000000000000d0
        brennerbeta(i,j)   = 1.080000000000000d0
        brennerDe(i,j)     = 1.400000000000000d0
        brennerre(i,j)     = 2.323500000000000d0
        gc(i,j)            = 1.918000000000000d0
        gd(i,j)            = 0.750000000000000d0
        gh(i,j)            = 0.301300000000000d0
        albe2mu(i,j)       = 1.846000000000000d0

        trcut(i,j)         = 2.870000000000000d0
        dcut(i,j)          = 0.150000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

        bf(i,j)=12.0d0
        rf(i,j)=1.2d0

    else if (name1=='N' .and. name2=='N' .and. &
        ((potmode>=40.and. potmode<=41) .or.  &
        (potmode>=28.and. potmode<=29))  ) then

        !
        ! Albe-Nord I for N-N
        !

        albepot=.true.

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.766120000000000d0
        brennerS(i,j)      = 1.492200000000000d0
        brennerbeta(i,j)   = 2.059450000000000d0
        brennerDe(i,j)     = 9.910000000000000d0
        brennerre(i,j)     = 1.110000000000000d0
        gc(i,j)            = 0.178493000000000d0
        gd(i,j)            = 0.201720000000000d0
        gh(i,j)            = 0.045238000000000d0
        albe2mu(i,j)       = 0.000000000000000d0

        trcut(i,j)         = 2.200000000000000d0
        dcut(i,j)          = 0.200000000000000d0

    else if ((name1=='Ga' .and. name2=='N') .or. &
        (name1=='N' .and. name2=='Ga') .and.  &
        ((potmode>=40.and.potmode<=41) .or. &
        (potmode>=28.and.potmode<=29) )) then

        !
        ! Albe/Nord Ga-N
        !
        bf(i,j)=12.0d0
        rf(i,j)=0.6d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        albepot=.true.

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.001632000000000d0
        brennerS(i,j)      = 1.112200000000000d0
        brennerbeta(i,j)   = 1.968000000000000d0
        brennerDe(i,j)     = 2.450000000000000d0
        brennerre(i,j)     = 1.921000000000000d0
        gc(i,j)            = 65.20700000000000d0
        gd(i,j)            = 2.821000000000000d0
        gh(i,j)            = 0.518000000000000d0
        albe2mu(i,j)       = 0.000000000000000d0

        trcut(i,j)         = 2.900000000000000d0
        dcut(i,j)          = 0.200000000000000d0


    else if (name1=='N' .and. name2=='N' .and. &
        (potmode>=20.and. potmode<=21)) then

        !
        ! Albe V for N-N (new formalism, not that in Albe's thesis).
        !

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000800000000000d0
        brennerS(i,j)      = 1.400000000000000d0
        brennerbeta(i,j)   = 2.564000000000000d0
        brennerDe(i,j)     = 9.910000000000000d0
        brennerre(i,j)     = 1.110000000000000d0
        gc(i,j)            = 3.750000000000000d0
        gd(i,j)            = 0.100000000000000d0
        gh(i,j)            = 0.110000000000000d0
        albe2mu(i,j)       = 15.000000000000000d0

        trcut(i,j)         = 2.300000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 0) then
            albe2mu(i,j)=0.0d0
        endif

    else if (name1=='N' .and. name2=='N' .and. &
        ( (potmode>=26.and. potmode<=27) .or. &
        (potmode>=87.and. potmode<=88) ) ) then

        !
        ! Albe-Nord I for N-N
        ! Used for W-N also, Jussi Polvi
        !
        albepot=.true.

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.766120000000000d0
        brennerS(i,j)      = 1.492200000000000d0
        brennerbeta(i,j)   = 2.059450000000000d0
        brennerDe(i,j)     = 9.910000000000000d0
        brennerre(i,j)     = 1.110000000000000d0
        gc(i,j)            = 0.178493000000000d0
        gd(i,j)            = 0.201720000000000d0
        gh(i,j)            = 0.045238000000000d0
        albe2mu(i,j)       = 0.000000000000000d0

        trcut(i,j)         = 2.300000000000000d0
        dcut(i,j)          = 0.200000000000000d0

    else if (name1=='N' .and. name2=='N' .and. &
        (potmode>=24.and. potmode<=25)) then

        !
        ! Albe IV for N-N (new formalism, not that in Albe's thesis).
        !

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 1.200000000000000d0
        brennerS(i,j)      = 2.110000000000000d0
        brennerbeta(i,j)   = 1.400000000000000d0
        brennerDe(i,j)     = 9.910000000000000d0
        brennerre(i,j)     = 1.110000000000000d0
        gc(i,j)            = 24.60000000000000d0
        gd(i,j)            = 3.900000000000000d0
        gh(i,j)            = 0.100000000000000d0
        albe2mu(i,j)       = 0.000000000000000d0

        trcut(i,j)         = 2.400000000000000d0
        dcut(i,j)          = 0.20000000000000d0

    else if (name1=='N' .and. name2=='N' .and. &
        (potmode>=22.and. potmode<=23)) then

        !
        ! Albe III for N-N (new formalism, not that in Albe's thesis).
        !

        albepot=.true.

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.120000000000000d0
        brennerS(i,j)      = 1.140000000000000d0
        brennerbeta(i,j)   = 1.750000000000000d0
        brennerDe(i,j)     = 9.910000000000000d0
        brennerre(i,j)     = 1.110000000000000d0
        gc(i,j)            = 13.000000000000000d0
        gd(i,j)            = 3.400000000000000d0
        gh(i,j)            = 0.000000000000000d0
        albe2mu(i,j)       = 0.000000000000000d0

        trcut(i,j)         = 2.300000000000000d0
        dcut(i,j)          = 0.20000000000000d0

    else if ((name1=='Ga' .and. name2=='N') .or. &
        (name1=='N' .and. name2=='Ga') .and.  &
        ((potmode>=20.and.potmode<=21))) then

        !
        ! Albe/Nord Ga-N
        !
        bf(i,j)=12.0d0
        rf(i,j)=0.6d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        albepot=.true.

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.001632000000000d0
        brennerS(i,j)      = 1.112200000000000d0
        brennerbeta(i,j)   = 1.968000000000000d0
        brennerDe(i,j)     = 2.450000000000000d0
        brennerre(i,j)     = 1.921000000000000d0
        gc(i,j)            = 65.20700000000000d0
        gd(i,j)            = 2.821000000000000d0
        gh(i,j)            = 0.518000000000000d0
        albe2mu(i,j)       = 0.000000000000000d0

        trcut(i,j)         = 2.900000000000000d0
        dcut(i,j)          = 0.200000000000000d0

    else if (name1=='N' .and. name2=='N' .and. &
        (potmode>=28.and. potmode<=29)  ) then

        ! Jani Kotakoskis new N-N for polymeric nitrogen

        albepot=.true.

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = 1.0d0

        ters_n(i,j)        = 0.9997824d0
        albegamma(i,j)     = 0.9112087d0
        brennerS(i,j)      = 1.5542735d0
        brennerbeta(i,j)   = 2.9207197d0
        brennerDe(i,j)     = 9.9100000d0
        brennerre(i,j)     = 1.1280817d0
        gc(i,j)            = -3.0095282d0
        gd(i,j)            = 1.1401040
        gh(i,j)            = 0.4189421
        albe2mu(i,j)       = -0.2496920

        trcut(i,j)         = 2.1d0
        dcut(i,j)          = 0.2d0

        if (mod(potmode,2) == 0) then
            ! This will worsen the elastic constants for polymeric N, but
            ! gets the sc r1 closer to DFT value
            albe2mu(i,j)=0.0d0
        endif

    else if (name1=='B' .and. name2=='B' .and. &
        (potmode>=50 .and. potmode<=51)  ) then

        ! Albe original B-B potential, see Comp. Mat. Sci. 10, 111

        albepot=.false.

        ! Not set here
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = -1.0d0

        ters_n(i,j)        = 3.9929061
        albegamma(i,j)     = 1.0d0
        brennergamma(i,j)   = 0.0000016 ! Corresponds to gamma in front chi_ij in publication
        brennerS(i,j)      = 1.0769
        brennerbeta(i,j)   = 1.5244506
        brennerDe(i,j)     = 3.08
        brennerre(i,j)     = 1.59
        gc(i,j)            = 0.52629
        gd(i,j)            = 0.001587
        gh(i,j)            = 0.5
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 2.0
        dcut(i,j)          = 0.1

    else if ( ( (name1=='B' .and. name2=='N') .or. (name1=='N' .and. name2=='B')) .and. &
        (potmode>=50 .and. potmode<=51)  ) then

        ! Albe original B-N potential, see Comp. Mat. Sci. 10, 111

        albepot=.false.

        ! Not set here
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = -1.0d0

        ters_n(i,j)        = 0.364153367
        albegamma(i,j)     = 1.0d0
        brennergamma(i,j)  = 0.000011134 ! Corresponds to gamma in front chi_ij in publication
        brennerS(i,j)      = 1.0769
        brennerbeta(i,j)   = 2.04357
        brennerDe(i,j)     = 6.36
        brennerre(i,j)     = 1.33
        gc(i,j)            = 1092.9287
        gd(i,j)            = 12.38
        gh(i,j)            = -0.5413
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 2.0
        dcut(i,j)          = 0.1

    else if (name1=='N' .and. name2=='N' .and. &
        (potmode>=50 .and. potmode<=51)  ) then

        ! Albe original N-N potential, see Comp. Mat. Sci. 10, 111

        albepot=.false.

        ! Not set here
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = -1.0d0

        ters_n(i,j)        = 0.6184432
        albegamma(i,j)     = 1.0d0
        brennergamma(i,j)   = 0.019251 ! Corresponds to gamma in front chi_ij in publication
        brennerS(i,j)      = 1.0769
        brennerbeta(i,j)   = 1.92787
        brennerDe(i,j)     = 9.91
        brennerre(i,j)     = 1.11
        gc(i,j)            = 17.7959
        gd(i,j)            = 5.9484
        gh(i,j)            = 0
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 1.0d0
        dcut(i,j)          = 0.1



    ! ------------------------------------------------------------------- !
    ! ---------------- Karsten Albe's Pt-C-stuff ------------------------ !
    ! ------------------------------------------------------------------- !
    else if ((name1=='Pt' .and. name2=='Pt') .and. (potmode>=14.and.potmode<=15)) then

        albepot=.true.
        ! This here for DMol potential
        bf(i,j)=10.0d0
        rf(i,j)=1.5d0

        gc(i,j)=0.000000000000000d0
        gd(i,j)=1.000000000000000d0
        gh(i,j)=1.000000000000000d0
        albecossign(i,j)=+1.0d0
        ters_n(i,j)=1.0d0
        brennerbeta(i,j)=1.65921d0
        brennergamma(i,j)=1.0d0
        albegamma(i,j)=0.20967626783d0
        brennerDe(i,j)=3.71d0
        brennerre(i,j)=2.34d0
        brennerS(i,j)=1.79493d0
        trcut(i,j)=3.400000000000000d0
        dcut(i,j)=0.200000000000000d0

        if (mod(potmode,2) == 1) then
            albe2mu(i,j) = 2.03801d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if ((name1=='Pt' .and. name2=='Pt') .and. (potmode>=18.and.potmode<=19)) then

        !
        ! Albe's second Pt-Pt parametrization with angular terms
        !
        albepot=.true.
        ! This here for DMol potential
        bf(i,j)=10.0d0
        rf(i,j)=1.5d0

        gc(i,j)=34.00000000000000d0
        gd(i,j)=1.100000000000000d0
        gh(i,j)=1.000000000000000d0
        albecossign(i,j)=+1.0d0
        ters_n(i,j)=1.0d0
        brennerbeta(i,j)=1.64249d0
        brennergamma(i,j)=1.0d0
        albegamma(i,j)=8.542d-4
        brennerDe(i,j)=3.683d0
        brennerre(i,j)=2.384d0
        brennerS(i,j)=2.24297d0
        trcut(i,j)=3.100000000000000d0
        dcut(i,j)=0.200000000000000d0

        if (mod(potmode,2) == 1) then
            albe2mu(i,j) = 2.67d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if (((name1=='Pt' .and. name2=='C') .or. (name1=='C' .and. name2=='Pt')) .and. &
        (potmode>=14.and.potmode<=15)) then

        albepot=.true.
        ! This here for DMol potential
        bf(i,j)=9.0d0
        rf(i,j)=0.4d0

        gc(i,j)            = 2.63584d0
        gd(i,j)            = 1.73596d0
        gh(i,j)            = 1.0d0
        albecossign(i,j)   =+1.0d0
        ters_n(i,j)        = 1.0d0
        brennerbeta(i,j)   = 1.80472d0
        brennergamma(i,j)  = 1.0d0
        albegamma(i,j)     = 3.129814d-2
        brennerDe(i,j)     = 5.3d0
        brennerre(i,j)     = 1.813d0
        brennerS(i,j)      = 1.1d0
        trcut(i,j)         = 2.65d0
        dcut(i,j)          = 0.15d0

        if (mod(potmode,2) == 1) then
            albe2mu(i,j) = 0.0d0  ! brennerbeta(i,j)*sqrt(8.0d0/brennerS(i,j))
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if (((name1=='Pt' .and. name2=='C') .or. (name1=='C' .and. name2=='Pt')) .and. &
        (potmode>=18.and.potmode<=19)) then

        !
        ! Albe's second Pt-C parametrization
        !

        albepot=.true.
        ! This here for DMol potential
        bf(i,j)=9.0d0
        rf(i,j)=0.4d0

        gc(i,j)            = 1.23d0
        gd(i,j)            = 0.36d0
        gh(i,j)            = 1.0d0
        albecossign(i,j)   =+1.0d0
        ters_n(i,j)        = 1.0d0
        brennerbeta(i,j)   = 1.836d0
        brennergamma(i,j)  = 1.0d0
        albegamma(i,j)     = 9.7d-3
        brennerDe(i,j)     = 5.3d0
        brennerre(i,j)     = 1.84d0
        brennerS(i,j)      = 1.1965d0
        trcut(i,j)         = 2.65d0
        dcut(i,j)          = 0.15d0

        if (mod(potmode,2) == 1) then
            albe2mu(i,j) = 0.0d0  !brennerbeta(i,j)*sqrt(8.0d0/brennerS(i,j))
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if ((name1=='C' .and. name2=='C') .and. &
        ((potmode>=14.and.potmode<=15) .or. (potmode>=18.and.potmode<=19))) then

        !
        ! Note that this is both for 14,15 and 18,19
        !

        albepot=.true.
        ! This here for DMol potential
        bf(i,j)=8.0d0
        rf(i,j)=0.6d0

        gc(i,j)            = 330d0
        gd(i,j)            = 3.5d0
        gh(i,j)            = 1.0d0
        albecossign(i,j)   = +1.0d0
        ters_n(i,j)        = 1.0d0
        brennerbeta(i,j)   = 2.1d0
        brennergamma(i,j)  = 1.0d0
        albegamma(i,j)     = 2.0813d-4
        brennerDe(i,j)     = 6.0d0
        brennerre(i,j)     = 1.39d0
        brennerS(i,j)      = 1.22d0
        trcut(i,j)         = 1.85d0
        dcut(i,j)          = 0.15d0

    ! -------------------------------------------------------------------- !
    ! --------- Fe-Pt potential by Michael M\"uller et al. --------------- !
    ! -------------------------------------------------------------------- !

    ! The parameters are as in Michael's PhD Thesis, see above.
    ! Tommi J{\"a}rvi, 9.5.2008

    ! --- Fe-Pt ---
    else if ( ( (name1=='Fe' .and. name2=='Pt') .or. (name1=='Pt' .and. name2=='Fe') ) &
        .and. (potmode>=60.and.potmode<=61)) then
        albepot=.true.

        ! Reppot params for zbl (Tommi)
        bf(i,j)=11.0d0
        rf(i,j)=1.4d0

        brennerDe(i,j)= 2.64759104d0
        brennerre(i,j)= 2.36130052d0
        brennerbeta(i,j)= 1.45616698d0
        brennerS(i,j)= 2.26243642d0
        albegamma(i,j)= 0.05633499d0

        gc(i,j)= 0.35073555d0
        gd(i,j)= 0.16902364d0
        gh(i,j)= 0.45035775d0

        trcut(i,j)= 4.20d0
        dcut(i,j)= 0.20d0

        albe2mu(i,j)= 0.95780361d0

        ! Michael told me he had just taken the following two parameters
        ! from other potentials without modifying.
        ! However, the defaults (in the beginning of
        ! this subroutine) are
        !
        ! albecossign(i,j)=-1.0d0
        ! brennergamma(i,j)=1.0d0
        !
        ! and at least albecossign is significant since it really has to
        ! be +1.
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0
        albecossign(i,j)=+1.0d0


    ! --- Pt-Pt ---
    else if ((name1=='Pt' .and. name2=='Pt') .and. (potmode>=60.and.potmode<=61)) then
        albepot=.true.

        ! Reppot params for Dmol-potential (Tommi)
        bf(i,j)=10.0d0
        rf(i,j)=1.5d0

        brennerDe(i,j)= 3.2d0
        brennerre(i,j)= 2.42d0
        brennerbeta(i,j)= 1.61d0
        brennerS(i,j)= 2.2955906d0
        albegamma(i,j)= 0.1854649d0

        gc(i,j)= 0.0609071d0
        gd(i,j)= 0.08d0
        gh(i,j)= 0.455d0

        trcut(i,j)= 3.75d0
        dcut(i,j)= 0.2d0

        albe2mu(i,j)= 1.5856477d0

        ! Michael told me he had just taken the following two parameters
        ! from other potentials without modifying.
        ! However, the defaults (in the beginning of
        ! this subroutine) are
        !
        ! albecossign(i,j)=-1.0d0
        ! brennergamma(i,j)=1.0d0
        !
        ! and at least albecossign is significant since it really has to
        ! be +1.
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

        ! --- Fe-Fe ---
    else if ((name1=='Fe' .and. name2=='Fe') .and. (potmode>=60.and.potmode<=65)) then
        albepot=.true.

        ! Reppot params for zbl (Kai)
        bf(i,j)=2.90d0
        rf(i,j)=0.95d0

        brennerDe(i,j)= 1.5d0
        brennerre(i,j)= 2.29d0
        brennerbeta(i,j)= 1.4d0
        brennerS(i,j)= 2.0693109d0
        albegamma(i,j)= 0.0115751d0

        gc(i,j)= 1.2898716d0
        gd(i,j)= 0.3413219d0
        gh(i,j)= -0.26d0

        trcut(i,j)= 3.15d0
        dcut(i,j)= 0.2d0

        albe2mu(i,j)= 0.0d0

        ! Michael told me he had just taken the following two parameters
        ! from other potentials without modifying.
        ! However, the defaults (in the beginning of
        ! this subroutine) are
        !
        ! albecossign(i,j)=-1.0d0
        ! brennergamma(i,j)=1.0d0
        !
        ! and at least albecossign is significant since it really has to
        ! be +1.
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0



    ! ------------------------------------------------------------------- !
    ! -------- FeCCr etc  potentials, by Krister Henriksson ------------- !
    ! ------------------------------------------------------------------- !

    ! Old pot.: potmode 63 for Fe-C system, Phys. Rev. B 79, 144107
    ! New pot.: potmode 61


    ! --- Cr-Cr ---
    else if ((name1=='Cr' .and. name2=='Cr') .and. (potmode>=60.and.potmode<=65)) then
        albepot=.true.

        ! OK, potmode 61-63


        ! Reppot params for ZBL
        bf(i,j)=12.0d0
        rf(i,j)=1.7d0

        brennerDe(i,j)= 4.04222081d0
        brennerre(i,j)= 2.13018547d0
        brennerbeta(i,j)= 1.62158721d0
        brennerS(i,j)= 3.36793914d0
        albegamma(i,j)= 0.02388562d0

        gc(i,j)= 1.03288255d0
        gd(i,j)= 0.13813230d0
        gh(i,j)= -0.28569237d0

        trcut(i,j)= 3.20d0
        dcut(i,j)= 0.2d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !

        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

        ! --- Fe-Cr ---

    else if ( ((name1=='Fe' .and. name2=='Cr').or. (name1=='Cr' .and. name2=='Fe')) .and. (potmode>=60.and.potmode<=63)) then
        albepot=.true.

        ! Reppot params for ZBL, Same as for Cr-Cr
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        albegamma(i,j)     = 0.15741992d0
        brennerS(i,j)      = 2.27680906d0
        brennerbeta(i,j)   = 1.74769847d0
        brennerDe(i,j)     = 3.46809779d0
        brennerre(i,j)     = 2.17806868d0
        gc(i,j)            = 0.48446210d0
        gd(i,j)            = 0.31407288d0
        gh(i,j)            = -0.61019120d0

        trcut(i,j)         = 3.15d0
        dcut(i,j)          = 0.2d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !

        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    else if ( ((name1=='Fe' .and. name2=='Cr').or. (name1=='Cr' .and. name2=='Fe')) .and. (potmode>=64.and.potmode<=65)) then
        albepot=.true.

        ! Second (30.12.2011) parametrization for Fe-Cr
        ! Reppot params for ZBL
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        brennerDe(i,j)     = 3.48049488d0
        brennerre(i,j)     = 2.16998952d0
        brennerbeta(i,j)   = 1.75467567d0
        brennerS(i,j)      = 2.28661503d0

        albegamma(i,j)     = 0.15766130d0
        gc(i,j)            = 0.48531613d0
        gd(i,j)            = 0.31427413d0
        gh(i,j)            = -0.69d0

        trcut(i,j)         = 3.10d0
        dcut(i,j)          = 0.15d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !

        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    else if ( ( (name1=='Cr' .and. name2=='C').or. (name1=='C' .and. name2=='Cr')) &
        .and. (potmode>=60.and.potmode<=65)) then

        ! Cr-C
        albepot=.true.

        ! Reppot join params by Krister Henriksson 22.9.2008
        bf(i,j)=8.0d0
        rf(i,j)=1.2d0

        brennerDe(i,j)= 2.77620074d0
        brennerre(i,j)= 1.81289285d0
        brennerbeta(i,j)=  2.00816371d0
        brennerS(i,j)=     2.04637644d0
        albegamma(i,j)=   0.00068830d0

        gc(i,j)= 3.93353757d0
        gd(i,j)= 0.17497204d0
        gh(i,j)= -0.17850001d0

        trcut(i,j)= 2.95d0
        dcut(i,j)=  0.1d0

        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0


    else if ( ( (name1=='Fe' .and. name2=='C').or. (name1=='C' .and. name2=='Fe')) &
        .and. ((potmode>=60.and.potmode<=61) .or. (potmode>=64.and.potmode<=65))) then

        ! Fe-C
        albepot=.true.

        ! Reppot join params, see ~knordlun/md/potential/fe/fec
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        albegamma(i,j)     = 0.07485571d0
        brennerS(i,j)      = 1.43035110d0
        brennerbeta(i,j)   = 1.82109816d0
        brennerDe(i,j)     = 3.95000634d0
        brennerre(i,j)     = 1.53426579d0
        gc(i,j)            = 1.11674155d0
        gd(i,j)            = 0.94663188d0
        gh(i,j)            = -0.18665305d0

        trcut(i,j)         = 2.6d0
        dcut(i,j)          = 0.20d0

        albe2mu(i,j)= 0.0d0

        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    else if ( ( (name1=='Fe' .and. name2=='C').or. (name1=='C' .and. name2=='Fe')) &
        .and. (potmode>=62.and.potmode<=63)) then

        ! Fe-C old (PRB 79 (2009) 144107)
        albepot=.true.

        ! Reppot join params, see ~knordlun/md/potential/fe/fec
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        brennerDe(i,j)= 4.82645134d0
        brennerre(i,j)= 1.47736510d0
        brennerbeta(i,j)= 1.63208170d0
        brennerS(i,j)= 1.43134755d0
        albegamma(i,j)=  0.00205862d0

        gc(i,j)= 8.95583221d0
        gd(i,j)= 0.72062047d0
        gh(i,j)= 0.87099874

        trcut(i,j)= 2.5d0
        dcut(i,j)= 0.2d0

        albe2mu(i,j)= 0.0d0

        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0


    ! --- Cr-Cr ---
    else if ((name1=='Cr' .and. name2=='Cr') .and. (potmode>=60.and.potmode<=65)) then
        albepot=.true.

        ! OK, potmode 61-63


        ! Reppot params for ZBL
        bf(i,j)=12.0d0
        rf(i,j)=1.7d0

        brennerDe(i,j)= 4.04222081d0
        brennerre(i,j)= 2.13018547d0
        brennerbeta(i,j)= 1.62158721d0
        brennerS(i,j)= 3.36793914d0
        albegamma(i,j)= 0.02388562d0

        gc(i,j)= 1.03288255d0
        gd(i,j)= 0.13813230d0
        gh(i,j)= -0.28569237d0

        trcut(i,j)= 3.20d0
        dcut(i,j)= 0.2d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !

        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    ! --- Fe-Cr ---


    ! ######################################################################
    ! ######################################################################
    ! Y-Fe-O

    else if ( (name1=='Y' .and. name2=='Y') .and. &
        (potmode>=64 .and. potmode<=65) ) then

        ! Y-Y (KH) : November 10, 2014
        ! Version 2 ("tulip_out_124")
        albepot=.true.

        ! Reppot guess
        bf(i,j) = 10.0d0
        rf(i,j) = 1.0d0

        brennerDe(i,j)     = 1.7732388191d0
        brennerre(i,j)     = 2.8471290553d0
        brennerbeta(i,j)   = 0.83695869675d0
        brennerS(i,j)      = 1.0959868841d0

        albegamma(i,j)     = 2.9143890635d-06
        gc(i,j)            = 51.369295711d0
        gd(i,j)            = 0.4985125766d0
        gh(i,j)            = 0.72217934844d0

        trcut(i,j)         = 4.3d0
        dcut(i,j)          = 0.2d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !
        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0


    else if ( (name1=='Y' .and. name2=='Y') .and. &
        (potmode>=66 .and. potmode<=67) ) then

        ! Y-Y (KH) : December 1, 2014
        ! K. Fan et al, Chin. J. Chem. Phys. 26 (2013) 526
        albepot=.true.

        ! Reppot guess
        bf(i,j) = 10.0d0
        rf(i,j) = 1.0d0

        brennerDe(i,j)     = 2.64686d0
        brennerre(i,j)     = 2.99839d0
        brennerbeta(i,j)   = 0.79884d0
        brennerS(i,j)      = 3.34600d0

        albegamma(i,j)     = 0.05577d0
        gc(i,j)            = 1.34719d0
        gd(i,j)            = 0.34880d0
        gh(i,j)            = -0.49445d0

        trcut(i,j)         = 5.74046d0
        dcut(i,j)          = 0.22582d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !
        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    else if ( ((name1=='Y' .and. name2=='O') .or. (name1=='O' .and. name2=='Y')) .and. &
        (potmode>=64 .and. potmode<=65) ) then

        ! Copy of Y-Y to avoid exceptional behavior (November 10, 2014):
        ! Y-O, O-Y (KH)
        albepot=.true.

        ! Reppot guess
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        brennerDe(i,j)     = 1.83407413130d0
        brennerre(i,j)     = 2.84796364040d0
        brennerbeta(i,j)   = 0.83999137908d0
        brennerS(i,j)      = 1.09524652630d0

        albegamma(i,j)     = 2.9534124120d-06
        gc(i,j)            = 50.26932659900d0
        gd(i,j)            = 0.50431544302d0
        gh(i,j)            = 0.83768423204d0

        trcut(i,j)         = 3.78d0
        dcut(i,j)          = 0.08d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !
        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    else if ( ((name1=='Y' .and. name2=='Fe') .or. (name1=='Fe' .and. name2=='Y')) .and. &
        (potmode>=64 .and. potmode<=65) ) then

        ! Copy of Y-Y to avoid exceptional behavior (November 10, 2014):
        ! Fe-Y, Y-Fe (KH)
        albepot=.true.

        ! Reppot guess
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        brennerDe(i,j)     = 1.83407413130d0
        brennerre(i,j)     = 2.84796364040d0
        brennerbeta(i,j)   = 0.83999137908d0
        brennerS(i,j)      = 1.09524652630d0

        albegamma(i,j)     = 2.9534124120d-06
        gc(i,j)            = 50.26932659900d0
        gd(i,j)            = 0.50431544302d0
        gh(i,j)            = 0.83768423204d0

        trcut(i,j)         = 3.78d0
        dcut(i,j)          = 0.08d0

        ! But see also albe2mu3 in Init3_Ter_Compound_Pot !
        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    else if ( ((name1=='Fe' .and. name2=='O') .or. (name1=='O' .and. name2=='Fe')) .and. &
        (potmode>=64 .and. potmode<=65) ) then

        ! Fe-O potential by Jesper Byggmastar (2017), see also three-body
        ! parameters in Init3_Ter_Compound_Pot !!!
        albepot=.true.

        ! Fermi reppot parameters
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        brennerDe(i,j)     = 7.9851367774d0
        brennerre(i,j)     = 1.5555688567d0
        brennerbeta(i,j)   = 1.2135035992d0
        brennerS(i,j)      = 2.8735224898d0

        albegamma(i,j)     = 1.1742630926d0
        gc(i,j)            = 19.8656293772d0
        gd(i,j)            = 4.5750149838d0
        gh(i,j)            = 0.0904310711d0

        trcut(i,j)         = 3.15d0
        dcut(i,j)          = 0.15d0

        ! But see also albe2mu3 and tersoffomega in Init3_Ter_Compound_Pot !
        albe2mu(i,j)= 0.0d0
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0

    ! ######################################################################
    ! ######################################################################


    ! ------------------------------------------------------------------- !
    ! ------------------ Fe potential by Jesper Byggmastar -------------- !
    ! --- revised Muller, improves defects, thermal properties, etc. ---- !
    ! ------------------------------------------------------------------- !
    else if ((name1=='Fe' .and. name2=='Fe') .and. (potmode>=70.and.potmode<=71)) then

        albepot=.true.

        ! Fermi reppot parameters to join with ZBL
        bf(i,j)            = 10.0d0
        rf(i,j)            = 1.2d0

        brennerDe(i,j)     = 1.465d0
        brennerre(i,j)     = 2.3d0
        brennerbeta(i,j)   = 1.4d0
        brennerS(i,j)      = 2.0693109000d0

        albegamma(i,j)     = 0.0115751000d0
        gc(i,j)            = 1.2898716000d0
        gd(i,j)            = 0.3413219000d0
        gh(i,j)            = -0.38d0

        trcut(i,j)         = 3.26d0
        dcut(i,j)          = 0.2d0

        albe2mu(i,j)       = 0.8d0

        albecossign(i,j)   = 1.0d0
        brennergamma(i,j)  = 1.0d0
        ters_n(i,j)        = 1.0d0


    ! ------------------------------------------------------------------- !
    ! ----------------.- Au potential by Marie Backman ------------------ !
    ! ------------------------------------------------------------------- !

    ! --- Au-Au ---
    else if ((name1=='Au' .and. name2=='Au') .and. (potmode>=60.and.potmode<=61)) then

        albepot=.true.

        ! Reppot params, this is a guess by Kai
        bf(i,j)=10.0d0
        rf(i,j)=1.0d0

        brennerDe(i,j)= 2.302000000000000d0
        brennerre(i,j)= 2.463316000000000d0
        brennerbeta(i,j)= 1.586426000000000d0
        brennerS(i,j)= 1.950000000000000d0
        albegamma(i,j)= 0.000637449400000d0

        gc(i,j)= 3.351525000000000d0
        gd(i,j)= 0.164926200000000d0
        gh(i,j)= 0.994188400000000d0

        trcut(i,j)= 3.20d0
        dcut(i,j)= 0.2d0

        albe2mu(i,j)= 2.3d0

        ! Michael told me he had just taken the following two parameters
        ! from other potentials without modifying.
        ! However, the defaults (in the beginning of
        ! this subroutine) are
        !
        ! albecossign(i,j)=-1.0d0
        ! brennergamma(i,j)=1.0d0
        !
        ! and at least albecossign is significant since it really has to
        ! be +1.
        albecossign(i,j)=+1.0d0
        brennergamma(i,j)=1.0d0
        ters_n(i,j)=1.0d0



    ! ------------------------------------------------------------------- !
    ! ----------------- SiC potential, Albe formalism ------------------- !
    ! ------------------------------------------------------------------- !

    else if (name1=='Si' .and. name2=='Si' .and. &
        ((potmode>=42.and.potmode<=45)) ) then

        ! Si-Si I, better for use with Si-C potential
        !
        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        brennerDe(i,j)     = 3.24d0
        brennerre(i,j)     = 2.232d0
        brennerS(i,j)      = 1.842d0
        brennerbeta(i,j)   = 1.4761d0
        albegamma(i,j)     = 0.114354d0
        gc(i,j)            = 2.00494d0
        gd(i,j)            = 0.814719d0
        gh(i,j)            = 0.259d0
        ters_n(i,j)        = 1.0d0
        trcut(i,j)         = 2.82d0
        dcut(i,j)          = 0.15d0

        if (mod(potmode,2) == 1) then
            albe2mu(i,j)=4.67d0
        else
            albe2mu(i,j)=0.0d0
        endif

        bf(i,j)=12.0d0
        rf(i,j)=1.6d0

    else if (name1=='Si' .and. name2=='Si' .and. &
        ((potmode>=46.and.potmode<=47)) ) then

        ! Si-Si

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        brennerDe(i,j)     = 3.24d0
        brennerre(i,j)     = 2.222d0
        brennerS(i,j)      = 1.57d0
        brennerbeta(i,j)   = 1.4760d0
        albegamma(i,j)     = 0.09253d0
        gc(i,j)            = 1.136881d0
        gd(i,j)            = 0.63397d0
        gh(i,j)            = 0.335d0
        albe2mu(i,j)       = 0.0d0
        ters_n(i,j)        = 1.0d0
        trcut(i,j)         = 2.90d0
        dcut(i,j)          = 0.15d0
        bf(i,j)            = 12.0d0
        rf(i,j)            = 1.6d0

    else if (name1=='C' .and. name2=='C' .and. &
        (potmode>=42.and. potmode<=43)) then

        ! C-C

        albepot=.true.

        bf(i,j)=8.0d0
        rf(i,j)=0.6d0

        ! CPE: original Brenner
        brennerDe(i,j)     = 6.00d0
        brennerre(i,j)     = 1.39d0
        brennerbeta(i,j)   = 2.1d0
        brennerS(i,j)      = 1.22d0
        albegamma(i,j)     = 0.00020813d0
        gc(i,j)            = 330d0
        gd(i,j)            = 3.5d0
        gh(i,j)            = 1.0d0
        trcut(i,j)         = 1.85d0
        dcut(i,j)          = 0.15d0

        albecossign(i,j)   = +1.0d0
        ters_n(i,j)        = 1.0d0
        brennergamma(i,j)  = 1.0d0
        albe2mu(i,j)=0.0d0

    else if (name1=='C' .and. name2=='C' .and. &
        (potmode>=44.and. potmode<=47)) then

        ! C-C

        albepot=.true.

        bf(i,j)=8.0d0
        rf(i,j)=0.6d0

        ! CPE: PAR15b
        brennerDe(i,j)     = 6.0000000
        brennerre(i,j)     = 1.4276442
        brennerbeta(i,j)   = 2.0099457
        brennerS(i,j)      = 2.1671419
        albegamma(i,j)     = 0.1123327
        gc(i,j)            = 181.9100526
        gd(i,j)            = 6.2843249
        gh(i,j)            = 0.5556181
        trcut(i,j)         = 2.00
        dcut(i,j)          = 0.15

        albecossign(i,j)   = +1.0d0
        ters_n(i,j)        = 1.0d0
        brennergamma(i,j)  = 1.0d0
        albe2mu(i,j)=0.0d0

    else if ((name1=='Si' .and. name2=='C') .or. &
        (name1=='C' .and. name2=='Si') .and.  &
        ((potmode>=42.and.potmode<=47))) then

        ! Si-C

        bf(i,j)=10.0d0
        rf(i,j)=1.2d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        albepot=.true.

        ters_n(i,j)        = 1.0000000d0
        albegamma(i,j)     = 0.0118769d0
        brennerS(i,j)      = 1.8474739d0
        brennerbeta(i,j)   = 1.6990751d0
        brennerDe(i,j)     = 4.36d0
        brennerre(i,j)     = 1.790d0
        gc(i,j)            = 273986.61d0
        gd(i,j)            = 180.31411d0
        gh(i,j)            = 0.680d0
        albe2mu(i,j)       = 0.00d0

        trcut(i,j)         = 2.400000000000000d0
        dcut(i,j)          = 0.200000000000000d0


    ! ------------------------------------------------------------------- !
    ! ---------------- Erhart-Albe Zn-O stuff                   --------- !
    ! ------------------------------------------------------------------- !

    else if (name1=='Zn' .and. name2=='Zn' .and. &
        ((potmode>=42.and.potmode<=43)) ) then

        ! Zn-Zn

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.00000000000000d0
        brennerDe(i,j)     = 0.647019188808523d0
        brennerre(i,j)     = 2.438778327317569d0
        brennerbeta(i,j)   = 1.711590761444178d0
        brennerS(i,j)      = 1.815417479396682d0
        albegamma(i,j)     = 0.000043909423344d0
        gc(i,j)            = 77.916385525705749d0
        gd(i,j)            = 0.913439040684873d0
        gh(i,j)            = 1.000000000000000d0
        albe2mu(i,j)       = 0.0d0

        ! cutoffs for elemental interactions
        !trcut(i,j)         = 3.00d0
        !dcut(i,j)          = 0.20d0
        trcut(i,j)         = 2.50d0
        dcut(i,j)          = 0.20d0

        ! Fermi parameters added 17.8.2012 by Kai Nordlund, see znocasc/reppot/
        bf(i,j)=12.0d0
        rf(i,j)=1.2d0

    else if (name1=='O' .and. name2=='O' .and. &
        ( (potmode>=42.and. potmode<=43)) ) then

        ! O-O

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = +1.0d0

        ters_n(i,j)        = 1.0d0
        brennerDe(i,j)     = 5.166d0
        brennerre(i,j)     = 1.20752d0
        brennerbeta(i,j)   = 2.309d0
        brennerS(i,j)      = 1.38635d0
        albegamma(i,j)     = 0.825945d0
        gc(i,j)            = 0.356076d-1
        gd(i,j)            = 0.464955d-1
        gh(i,j)            = 0.450558d0
        albe2mu(i,j)       = 0.0d0

        ! cutoffs for elemental interactions
        !trcut(i,j)         = 2.10d0
        !dcut(i,j)          = 0.20d0
        trcut(i,j)         = 2.45d0
        dcut(i,j)          = 0.20d0

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

    else if ((name1=='Zn' .and. name2=='O') .or. &
        (name1=='O' .and. name2=='Zn') .and.  &
        ((potmode>=42.and.potmode<=43))) then

        ! Zn-O

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.0000000d0
        brennerS(i,j)      = 1.045496d0
        brennerbeta(i,j)   = 1.817435d0
        brennerDe(i,j)     = 3.60d0
        brennerre(i,j)     = 1.724032d0
        albegamma(i,j)     = 0.019335d0
        gc(i,j)            = 0.014098d0
        gd(i,j)            = 0.084028d0
        gh(i,j)            = 0.305451d0
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 2.60d0
        dcut(i,j)          = 0.20d0

        bf(i,j)=8.0d0
        rf(i,j)=0.4d0

    else if (name1=='Zn' .and. name2=='Zn' .and. &
        ((potmode>=44.and.potmode<=45)) ) then

        ! Zn-Zn

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.00000000000000d0
        brennerDe(i,j)     = 0.647019188808523d0
        brennerre(i,j)     = 2.438778327317569d0
        brennerbeta(i,j)   = 1.711590761444178d0
        brennerS(i,j)      = 1.815417479396682d0
        albegamma(i,j)     = 0.000043909423344d0
        gc(i,j)            = 77.916385525705749d0
        gd(i,j)            = 0.913439040684873d0
        gh(i,j)            = 1.000000000000000d0
        albe2mu(i,j)       = 0.0d0

        ! cutoffs for elemental interactions
        !trcut(i,j)         = 3.00d0
        !dcut(i,j)          = 0.20d0
        trcut(i,j)         = 2.85d0
        dcut(i,j)          = 0.20d0

        bf(i,j)=12.0d0
        rf(i,j)=1.2d0

    else if (name1=='O' .and. name2=='O' .and. &
        (potmode>=44.and. potmode<=45)) then

        ! O-O

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = +1.0d0

        ters_n(i,j)        = 1.0d0
        brennerDe(i,j)     = 5.166d0
        brennerre(i,j)     = 1.20752d0
        brennerbeta(i,j)   = 2.309d0
        brennerS(i,j)      = 1.38635d0
        albegamma(i,j)     = 0.825945d0
        gc(i,j)            = 0.356076d-1
        gd(i,j)            = 0.464955d-1
        gh(i,j)            = 0.450558d0
        albe2mu(i,j)       = 0.0d0

        ! cutoffs for elemental interactions
        !trcut(i,j)         = 2.10d0
        !dcut(i,j)          = 0.20d0
        trcut(i,j)         = 2.45d0
        dcut(i,j)          = 0.20d0

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0

    else if ((name1=='Zn' .and. name2=='O') .or. &
        (name1=='O' .and. name2=='Zn') .and.  &
        ((potmode>=44.and.potmode<=45))) then

        ! Zn-O

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.0000000d0
        brennerS(i,j)      = 1.045496d0
        brennerbeta(i,j)   = 1.817435d0
        brennerDe(i,j)     = 3.60d0
        brennerre(i,j)     = 1.724032d0
        albegamma(i,j)     = 0.019335d0
        gc(i,j)            = 0.014098d0
        gd(i,j)            = 0.084028d0
        gh(i,j)            = 0.305451d0
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 2.60d0
        dcut(i,j)          = 0.20d0

        bf(i,j)=8.0d0
        rf(i,j)=0.4d0


    ! ------------------------------------------------------------------- !
    ! --------------- Nord-Juslin-Erhart-Nordlund W-X stuff ------------- !
    ! ------------------------------------------------------------------- !

    else if ((name1=='W' .and. name2=='W') .and. &
        ( (potmode>=68.and.potmode<=69) .or.  (potmode>=72.and.potmode<=83) )) then

        ! Nord W-W test 1

        albepot=.true.
        ! Fermi parameters
        bf(i,j)=12.0d0
        rf(i,j)=1.3d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.001882272875231d0
        brennerS(i,j)      = 1.927082352878010d0
        brennerbeta(i,j)   = 1.385276317494857d0
        brennerDe(i,j)     = 5.418607940736659d0
        brennerre(i,j)     = 2.340953090561524d0
        gc(i,j)            = 2.149689176792354d0
        gd(i,j)            = 0.171256218947936d0
        gh(i,j)            =-0.277801444332501d0

        ! Originals, but these cause trouble for NaCl WC

        !trcut(i,j)         = 3.100000000000000d0
        !dcut(i,j)          = 0.100000000000000d0
        trcut(i,j)         = 3.500000000000000d0
        dcut(i,j)          = 0.300000000000000d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 0.458764875755342d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if ((name1=='W' .and. name2=='W') .and. (potmode>=70.and.potmode<=71)) then

        ! Nord W-W II

        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000122315898948d0
        brennerS(i,j)      = 1.921411212416015d0
        brennerbeta(i,j)   = 1.395663224591676d0
        brennerDe(i,j)     = 4.997926946790341d0
        brennerre(i,j)     = 2.371494406728725d0
        gc(i,j)            = 4.953869833078551d0
        gd(i,j)            = 0.109885284479351d0
        gh(i,j)            = -0.284488957675170d0

        trcut(i,j)         = 3.500000000000000d0
        dcut(i,j)          = 0.300000000000000d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 0.700917466481994d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if (((name1=='W' .and. name2=='C').or.(name1=='C' .and. name2=='W')) .and. (potmode>=68.and.potmode<=69)) then

        !  Nord W-C I, "h=-0.9" parameter set.

        albepot=.true.

        bf(i,j)=7.0d0
        rf(i,j)=1.2d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.004306273728286d0
        brennerS(i,j)      = 1.834589998456363d0
        brennerbeta(i,j)   = 1.811036335400849d0
        brennerDe(i,j)     = 7.481749427087867d0
        brennerre(i,j)     = 1.884410801553985d0
        gc(i,j)            = 0.185871991082193d0
        gd(i,j)            = 0.021443239017133d0
        gh(i,j)            =-0.900000000000000d0

        trcut(i,j)         = 3.200000000000000d0
        dcut(i,j)          = 0.200000000000000d0

    else if (((name1=='W' .and. name2=='C').or.(name1=='C' .and. name2=='W')) .and. (potmode>=70.and.potmode<=71)) then

        !  Nord W-C II, h=1.0d0 parameter set

        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.116495007507129d0
        brennerS(i,j)      = 1.823770845944165d0
        brennerbeta(i,j)   = 1.973870405011794d0
        brennerDe(i,j)     = 6.900000000000002d0
        brennerre(i,j)     = 1.918214354233695d0
        gc(i,j)            = 3.465130998703054d0
        gd(i,j)            = 1.425571282313113d0
        gh(i,j)            = 1.000000000000000d0

        trcut(i,j)         = 3.300000000000000d0
        dcut(i,j)          = 0.100000000000000d0


    else if (((name1=='W' .and. name2=='C')  &
        .or.(name1=='C'  .and. name2=='W')) &
        .and. (potmode>=72.and.potmode<=73)) then

        !  Erhart W-C II

        albepot=.true.
        ! Fermi parameters
        bf(i,j)=7.0d0
        rf(i,j)=1.2d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        brennerDe(i,j)     = 6.64d0
        brennerre(i,j)     = 2.00005560d0
        brennerbeta(i,j)   = 1.82440769d0
        brennerS(i,j)      = 3.36827374d0
        albegamma(i,j)     = 0.08035775d0
        gc(i,j)            = 1.08345281d0
        gd(i,j)            = 0.32046297d0
        gh(i,j)            = 0.750584d0
        ters_n(i,j)        = 1.0d0
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 3.20d0
        dcut(i,j)          = 0.20d0

    else if (((name1=='W' .and. name2=='C').or.(name1=='C' .and. name2=='W')) .and. (potmode>=74.and.potmode<=83)) then

        !  Erhart W-C III

        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=7.0d0
        rf(i,j)=1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        brennerDe(i,j)     = 6.64d0
        brennerre(i,j)     = 1.90547d0
        brennerbeta(i,j)   = 1.803696d0
        brennerS(i,j)      = 2.961485d0
        albegamma(i,j)     = 0.072885d0
        gc(i,j)            = 1.103037d0
        gd(i,j)            = 0.330176d0
        gh(i,j)            = 0.751067d0
        ters_n(i,j)        = 1.0d0

        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 2.800000000000000d0
        dcut(i,j)          = 0.200000000000000d0

    else if (((name1=='W' .and. name2=='C').or.(name1=='C' .and. name2=='W')) .and. (potmode>=60.and.potmode<=61)) then

        !  Nord W-C test 2

        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 4.135494952540000d-3
        brennerS(i,j)      = 1.80738720686100d0
        brennerbeta(i,j)   = 1.8251642956440d0
        brennerDe(i,j)     = 6.85156283995964d0
        brennerre(i,j)     = 1.85912319854053d0
        gc(i,j)            = 0.103326709166798d0
        gd(i,j)            = 1.117874454366700d-02
        gh(i,j)            = 1.00000000000000d0

        trcut(i,j)         = 3.300000000000000d0
        dcut(i,j)          = 0.200000000000000d0

    else if (((name1=='W' .and. name2=='H') .or. (name1=='H' .and. name2=='W')) .and. (potmode>=60.and.potmode<=83)) then

        !  Juslin W-H

        albepot=.true.
        ! Fermi parameters
        bf(i,j)=7.0d0
        rf(i,j)=0.5d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.005000000000000d0
        brennerS(i,j)      = 1.248000000000000d0
        brennerbeta(i,j)   = 1.523280000000000d0
        brennerDe(i,j)     = 2.748000000000000d0
        brennerre(i,j)     = 1.727000000000000d0
        gc(i,j)            = 1.788000000000000d0
        gd(i,j)            = 0.825500000000000d0
        gh(i,j)            = 0.389123950000000d0

        trcut(i,j)         = 2.150000000000000d0
        dcut(i,j)          = 0.200000000000000d0


    else if (((name1=='C' .and. name2=='C')) .and. (potmode>=60.and.potmode<=83)) then

        !  Brenner C-C

        albepot=.true.
        ! Fermi parameters
        bf(i,j)=8.0d0
        rf(i,j)=0.6d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000208130000000d0
        brennerS(i,j)      = 1.220000000000000d0
        brennerbeta(i,j)   = 2.100000000000000d0
        brennerDe(i,j)     = 6.000000000000000d0
        brennerre(i,j)     = 1.390000000000000d0
        gc(i,j)            = 330.0000000000000d0
        gd(i,j)            = 3.500000000000000d0
        gh(i,j)            = 1.000000000000000d0

        trcut(i,j)         = 1.850000000000000d0
        dcut(i,j)          = 0.150000000000000d0

        ! albe2mu handled as albe2mu3 in Init3_

    else if (((name1=='H' .and. name2=='H')) .and. (potmode>=60.and.potmode<=95)) then

        !  Brenner H-H

        albepot=.true.
        if (potmode==95) then
            albepot=.false.
            exponentone=.true.
        endif
        ! Fermi parameters
        bf(i,j)=15.0d0
        rf(i,j)=0.35d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 12.33000000000000d0
        brennerS(i,j)      = 2.343200000000000d0
        brennerbeta(i,j)   = 1.943600000000000d0
        brennerDe(i,j)     = 4.750900000000000d0
        brennerre(i,j)     = 0.741440000000000d0
        gc(i,j)            = 0.000000000000000d0
        gd(i,j)            = 1.000000000000000d0
        gh(i,j)            = 1.000000000000000d0

        trcut(i,j)         = 1.400000000000000d0
        dcut(i,j)          = 0.300000000000000d0

        ! albe2mu handled as albe2mu3 in Init3_

    else if (((name1=='H' .and. name2=='C')   ) .and. (potmode>=60.and.potmode<=83)) then

        !  Brenner H-C ; note that this is actually unsymmetric to C-H!

        albepot=.true.
        ! Fermi parameters
        bf(i,j)=10.0d0
        rf(i,j)=0.50d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 12.33000000000000d0
        brennerS(i,j)      = 1.690770000000000d0
        brennerbeta(i,j)   = 1.958300000000000d0
        brennerDe(i,j)     = 3.642200000000000d0
        brennerre(i,j)     = 1.119900000000000d0
        gc(i,j)            = 0.000000000000000d0
        gd(i,j)            = 1.000000000000000d0
        gh(i,j)            = 1.000000000000000d0

        trcut(i,j)         = 1.550000000000000d0
        dcut(i,j)          = 0.250000000000000d0

        ! albe2mu handled as albe2mu3 in Init3_

    else if (((name1=='C' .and. name2=='H')) .and. (potmode>=60.and.potmode<=83)) then

        !  Brenner C-H ; note that this is actually unsymmetric to H-C!

        albepot=.true.
        ! Fermi parameters
        bf(i,j)=10.0d0
        rf(i,j)=0.5d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000208130000000d0
        brennerS(i,j)      = 1.690770000000000d0
        brennerbeta(i,j)   = 1.958300000000000d0
        brennerDe(i,j)     = 3.642200000000000d0
        brennerre(i,j)     = 1.119900000000000d0
        gc(i,j)            = 330.0000000000000d0
        gd(i,j)            = 3.500000000000000d0
        gh(i,j)            = 1.000000000000000d0

        trcut(i,j)         = 1.550000000000000d0
        dcut(i,j)          = 0.250000000000000d0

        ! albe2mu handled as albe2mu3 in Init3_

    else if ((name1=='W' .and. name2=='W') .and. (potmode>=60.and.potmode<=61)) then


        ! Albe-Salonen W-W 2nd-nearest neighbour model II

        albepot=.true.
        ! Fermi parameters tested for this particular W pot on
        ! 1.11 2003. These seemed quite OK for bij=0.2 - 1.0d0
        bf(i,j)=12d0
        rf(i,j)=1.3d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.002637586783957d0
        brennerS(i,j)      = 1.992658891250826d0
        brennerbeta(i,j)   = 1.403627779065969d0
        brennerDe(i,j)     = 5.000000000000000d0
        brennerre(i,j)     = 2.378615290372260d0
        gc(i,j)            = 2.483715520260442d0
        gd(i,j)            = 0.223403111725576d0
        gh(i,j)            = -0.267809844965802d0

        trcut(i,j)         = 3.500000000000000d0
        dcut(i,j)          = 0.300000000000000d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 2.0*1.402346707087374d0
        else
            albe2mu(i,j) = 0.0d0
        endif


    else if ((name1=='W' .and. name2=='W') .and. (potmode>=62.and.potmode<=63)) then


        ! Albe-Salonen W-W 2nd-nearest neighbour model I

        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.002322799595561d0
        brennerS(i,j)      = 1.948674092120349d0
        brennerbeta(i,j)   = 1.330978332149339d0
        brennerDe(i,j)     = 5.000000000000000d0
        brennerre(i,j)     = 2.349219833065938d0
        gc(i,j)            = 3.124538663159744d0
        gd(i,j)            = 0.259732062360911d0
        gh(i,j)            = -0.288880261868054d0

        trcut(i,j)         = 3.500000000000000d0
        dcut(i,j)          = 0.300000000000000d0

        if (mod(potmode,2) == 1) then
            albe2mu(i,j) = 2.0*1.295875477644483d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if ((name1=='W' .and. name2=='W') .and. (potmode>=64.and.potmode<=65)) then

        ! Juslin W-W 2nd-nearest neighbour model test
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000526427528228d0
        brennerS(i,j)      = 1.165029016085129d0
        brennerbeta(i,j)   = 1.400000000000000d0
        brennerDe(i,j)     = 6.000000000000000d0
        brennerre(i,j)     = 2.150000000000000d0
        gc(i,j)            = 1.247481538042029d0
        gd(i,j)            = 0.121937058265176d0
        gh(i,j)            = -0.309422807610641d0

        trcut(i,j)         = 3.40000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 2.0*0.468339298803282d0
        else
            albe2mu(i,j) = 0.0d0
        endif


    else if ((name1=='W' .and. name2=='W') .and. (potmode>=66.and.potmode<=67)) then

        ! Juslin W-W 2nd neareast neighbour best non-angular model

        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.538499570745349d0
        brennerS(i,j)      = 3.718782124409342d0
        brennerbeta(i,j)   = 0.600000000000000d0
        brennerDe(i,j)     = 5.427439892071402d0
        brennerre(i,j)     = 2.000000000000000d0
        gc(i,j)            = 0.000000000000000d0
        gd(i,j)            = 1.000000000000000d0
        gh(i,j)            = 0.000000000000000d0

        trcut(i,j)         = 3.420000000000000d0
        dcut(i,j)          = 0.220000000000000d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 2.0*1.267726201230088d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if ((name1=='W' .and. name2=='W') .and. &
        ( (potmode>=85) .and. (potmode<=88) ) ) then

        ! Tommy Ahlgrens W-W potential
        ! Used for W-N also, Jussi Polvi

        albepot=.true.
        ! Fermi parameters
        bf(i,j)=12.0d0
        rf(i,j)=1.3d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j) =+1.0d0
        ters_n(i,j) = 1.000000000000000d0
        brennerDe(i,j) = 3.282547
        brennerre(i,j) = 2.460687
        brennerbeta(i,j) = 1.373146
        brennerS(i,j) = 2.215376
        albegamma(i,j) = 0.001293884
        gc(i,j) = 1.327324
        gd(i,j) = 0.135096
        gh(i,j) = -0.352
        if (potmode==87 .or. potmode==88) then
            trcut(i,j) = 4.4
        else
            trcut(i,j) = 4.4
        end if
        dcut(i,j) = 0.840189
        albe2mu(i,j)=0.0d0

    else if (((name1=='N' .and. name2=='W') .or. (name1=='W' .and. name2=='N')) &
        .and. (potmode>=87 .and. potmode<=88) ) then

        ! W-N potential by Jussi Polvi, 87 and 88 are the same now
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=12.0d0
        rf(i,j)=0.4d0

        brennergamma(i,j) =  1.0d0
        albecossign(i,j)  = +1.0d0
        ters_n(i,j)       =  1.0d0

        albegamma(i,j)     = 0.000091d0
        brennerS(i,j)      = 2.280000d0
        brennerbeta(i,j)   = 1.130000d0
        brennerDe(i,j)     = 7.830000d0
        brennerre(i,j)     = 1.780000d0
        gc(i,j)            = 72.000000d0
        gd(i,j)            = 0.710885d0
        gh(i,j)            = 1.000000d0
        trcut(i,j)         = 2.830000d0
        dcut(i,j)          = 0.820000d0
        ! albe2mu handled as albe2mu3 in Init3_

    else if (((name1=='W' .and. name2=='W')) .and. (potmode==95)) then

        !  W-W potential by Xiao-Chun Li, JNM 408 (2011) 12

        albepot=.false.
        exponentone=.true.
        ! Fermi parameters
        bf(i,j)=12.0d0
        rf(i,j)=1.3d0

        brennergamma(i,j) = 1.0d0
        albecossign(i,j) =+1.0d0

        ters_n(i,j) = 1.000000000000000d0
        brennerDe(i,j) = 2.87454d0
        brennerre(i,j) = 2.38631d0
        brennerbeta(i,j) = 1.33682d0
        brennerS(i,j) = 1.25348d0
        albegamma(i,j) = 0.00083879d0
        gc(i,j) = 0.850284d0
        gd(i,j) = 0.144317d0
        gh(i,j) = -0.36846d0
        trcut(i,j) = 4.131580d0
        dcut(i,j) = 0.930180d0
        ! albe2mu handled as albe2mu3 in Init3_


    else if (((name1=='W' .and. name2=='H') .or. (name1=='H' .and. name2=='W')) .and. (potmode==95)) then

        ! W-H         95      Potential by Xiao-Chun Li, JNM 408 (2011) 12
        ! Note that this is an ij and not ik potential, i.e. to be handled by albepot=.false.!!
        ! but should use exponent=1

        albepot=.false.
        exponentone=.true.
        ! Fermi parameters
        bf(i,j)=7.0d0
        rf(i,j)=0.5d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.008594666d0
        brennerS(i,j)      = 1.031565d0
        brennerbeta(i,j)   = 1.354368d0
        brennerDe(i,j)     = 3.035928d0
        brennerre(i,j)     = 1.76306d0
        gc(i,j)            = 0.146902d0
        gd(i,j)            = 0.393100d0
        gh(i,j)            = 0.558936d0

        trcut(i,j)         = 2.568113d0
        dcut(i,j)          = 0.133729d0

    else if ((name1=='O' .and. name2=='O') .and. (potmode>=60.and.potmode<=61)) then

        ! Juslin-Nordlund O-O model I, documented in ~njuslin/oxygenpub

        albepot=.true.
        ! Fermi parameters not set
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.825945417646114d0
        brennerS(i,j)      = 1.386347987838083d0
        brennerbeta(i,j)   = 2.309000000000000d0
        brennerDe(i,j)     = 5.166000000000000d0
        brennerre(i,j)     = 1.207520000000000d0
        gc(i,j)            = 0.035607614671197d0
        gd(i,j)            = 0.046495454866151d0
        gh(i,j)            = 0.450558170468507d0

        trcut(i,j)         = 2.200000000000000d0
        dcut(i,j)          = 0.100000000000000d0

    ! ------------------------------------------------------------------- !
    ! --------------- Caro's Be and Be-C and Be-W potential ------------- !
    ! ------------------------------------------------------------------- !
    ! - - - - - - - -     ...and Jesper's Be-O (2017)       - - - - - - - !
    ! ------------------------------------------------------------------- !

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=60.and.potmode<=61)) then

        ! Carolina's Be parameter set no. 1 (April 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 1.170d0
        brennerre(i,j)    = 2.035d0
        brennerS(i,j)     = 2.87d0
        brennerbeta(i,j)  = 1.23d0
        albegamma(i,j)    = 0.000002171035320d0
        gc(i,j)           = 30.91040883826698d0
        gd(i,j)           = 0.111062315462816d0
        gh(i,j)           = 0.920931089406969d0
        trcut(i,j)        = 2.685d0
        dcut(i,j)         = 0.300d0
        albe2mu(i,j)      = 0.0d0
        if (mod(potmode,2) == 1) albe2mu(i,j) = 3.5d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=62.and.potmode<=63)) then

        ! Carolina's Be parameter set - version 3 (April 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 1.15d0
        brennerre(i,j)    = 2.036d0
        brennerbeta(i,j)  = 1.221d0
        brennerS(i,j)     = 2.87d0
        albegamma(i,j)    = 0.000006212550349d0
        gc(i,j)           = 51.721907873261990d0
        gd(i,j)           = 0.298259299004561d0
        gh(i,j)           = 1.0d0
        trcut(i,j)        = 2.70d0
        dcut(i,j)         = 0.25d0
        albe2mu(i,j)      = 0.0d0
        if (mod(potmode,2) == 1) albe2mu(i,j) = 2.443636347450227d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=64.and.potmode<=65)) then

        ! Carolina's Be parameter set - version 5 (July 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 1.17d0
        brennerre(i,j)    = 2.035d0
        brennerbeta(i,j)  = 1.23d0
        brennerS(i,j)     = 2.87d0
        albegamma(i,j)    = 0.000003164130528d0
        gc(i,j)           = 31.979506339899400d0
        gd(i,j)           = 0.136447665922599d0
        gh(i,j)           = 0.920931089406969d0
        trcut(i,j)        = 2.685d0
        dcut(i,j)         = 0.223413451248490d0
        albe2mu(i,j)      = 0.0d0
        if (mod(potmode,2) == 1) albe2mu(i,j) = 2.422042024170299d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=66.and.potmode<=67)) then

        ! Carolina's Be parameter set - version 6 (July 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 1.17d0
        brennerre(i,j)    = 2.035d0
        brennerbeta(i,j)  = 1.23d0
        brennerS(i,j)     = 2.87d0
        albegamma(i,j)    = 0.000000999684623d0
        gc(i,j)           = 31.98225351327674d0
        gd(i,j)           = 0.077448440952681d0
        gh(i,j)           = 0.846725345091045d0
        trcut(i,j)        = 2.685d0
        dcut(i,j)         = 0.223413451248490d0
        albe2mu(i,j)      = 0.0d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=68.and.potmode<=69)) then

        ! Carolina's Be parameter set - version 7 (July 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 1.17d0
        brennerre(i,j)    = 2.035d0
        brennerbeta(i,j)  = 1.23d0
        brennerS(i,j)     = 2.87d0
        albegamma(i,j)    = 0.000000438703718d0
        gc(i,j)           = 33.03819549748577d0
        gd(i,j)           = 0.053441355882769d0
        gh(i,j)           = 0.844056252228855d0
        trcut(i,j)        = 2.685d0
        dcut(i,j)         = 0.223413451248490d0
        albe2mu(i,j)      = 0.0d0
        if (mod(potmode,2) == 1) albe2mu(i,j) = 0.975473979149553d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=70.and.potmode<=71)) then

        ! Carolina's Be parameter set - version 8 (Ferbruary 2008)
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000000478700966d0
        brennerS(i,j)      = 3.097660418194096d0
        brennerbeta(i,j)   = 1.250000000000000d0
        brennerDe(i,j)     = 1.170000000000000d0
        brennerre(i,j)     = 2.035000000000000d0
        gc(i,j)            = 32.341563260308420d0
        gd(i,j)            = 0.052527629106874d0
        gh(i,j)            = 0.829506468708494d0

        trcut(i,j)         = 2.685000000000000d0
        dcut(i,j)          = 0.223413451248490d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 0.975473979149553d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=72.and.potmode<=73)) then

        ! Carolina's Be parameter set - version 9 (February 2008)
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000000478700966d0
        brennerS(i,j)      = 3.097660418194096d0
        brennerbeta(i,j)   = 1.250000000000000d0
        brennerDe(i,j)     = 1.170000000000000d0
        brennerre(i,j)     = 2.035000000000000d0
        gc(i,j)            = 32.341563260308420d0
        gd(i,j)            = 0.052527629106874d0
        gh(i,j)            = 0.826237691754206d0

        trcut(i,j)         = 2.685000000000000d0
        dcut(i,j)          = 0.223413451248490d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 0.5d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    else if ((name1=='Be' .and. name2=='Be') &
        .and. (potmode>=74.and.potmode<=75)) then

        ! Carolina's Be parameter set - version 10 (April 2008)

        albepot=.true.
        bf(i,j)=7.0d0
        rf(i,j)=0.8d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000000478700966d0
        brennerS(i,j)      = 3.111673520671458d0
        brennerbeta(i,j)   = 1.280000000000000d0
        brennerDe(i,j)     = 1.170000000000000d0
        brennerre(i,j)     = 2.035000000000000d0
        gc(i,j)            = 32.327967760203990d0
        gd(i,j)            = 0.052653215949216d0
        gh(i,j)            = 0.826579993616552d0

        trcut(i,j)         = 2.685000000000000d0
        dcut(i,j)          = 0.223000000000000d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 0.85d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    ! Be-C Be-versions

    else if ( ( name1=='Be' .and. name2=='Be') &
        .and. (potmode>=76 .and. potmode<=77) ) then

        ! Carolina's Be parameter set - from Be-C version 1 (November 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 1.0d0
        brennerre(i,j)    = 2.090138603238d0
        brennerbeta(i,j)  = 1.22081184787339d0
        brennerS(i,j)     = 2.49d0
        albegamma(i,j)    = 0.000000984028015d0
        gc(i,j)           = 31.74441874925736d0
        gd(i,j)           = 0.097144404783703d0
        gh(i,j)           = 0.860046099665660d0
        albe2mu(i,j)      = 0.0d0
        if (mod(potmode,2) == 1) albe2mu(i,j) = 2.16673693329372d0
        trcut(i,j)        = 2.60d0
        dcut(i,j)         = 0.15d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0


    else if ( ( name1=='Be' .and. name2=='Be') &
        .and. (potmode>=78 .and. potmode<=79) ) then

        ! Carolina's Be parameter set - from Be-C version 2 (November 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 1.015d0
        brennerre(i,j)    = 2.070138603238000d0
        brennerbeta(i,j)  = 1.22081184787339d0
        brennerS(i,j)     = 2.49d0
        albegamma(i,j)    = 0.000000984028015d0
        gc(i,j)           = 31.74441874925736d0
        gd(i,j)           = 0.097144404783703d0
        gh(i,j)           = 0.860046099665660d0
        albe2mu(i,j)      = 0.0d0
        if (mod(potmode,2) == 1) albe2mu(i,j) = 2.16673693329372d0
        trcut(i,j)        = 2.60d0
        dcut(i,j)         = 0.15d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0


    else if ( ( name1=='Be' .and. name2=='Be') &
        .and. (potmode>=80 .and. potmode<=81) ) then

        ! Carolina's Be parameter set - from Be-C version 3 (February 2008)
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000000918136970d0
        brennerS(i,j)      = 2.365121291821326d0
        brennerbeta(i,j)   = 1.300000000000000d0
        brennerDe(i,j)     = 1.046690245432333d0
        brennerre(i,j)     = 2.091129855187804d0
        gc(i,j)            = 30.678879975146290d0
        gd(i,j)            = 0.099534709288600d0
        gh(i,j)            = 0.841044295641499d0

        trcut(i,j)         = 2.600000000000000d0
        dcut(i,j)          = 0.150000000000000d0

    else if ( ( name1=='Be' .and. name2=='Be') &
        .and. (potmode>=82 .and. potmode<=83) ) then

        ! Carolina's Be parameter set - from Be-C version 4 (August 2008)
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=15.0d0
        rf(i,j)=0.8d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000000819587260d0
        brennerS(i,j)      = 1.889782892624960d0
        brennerbeta(i,j)   = 1.300000000000000d0
        brennerDe(i,j)     = 1.035711767714829d0
        brennerre(i,j)     = 2.078799227759839d0
        gc(i,j)            = 89.389384247175660d0
        gd(i,j)            = 0.274428956657126d0
        gh(i,j)            = 0.760693434073536d0

        trcut(i,j)         = 2.535000000000000d0
        dcut(i,j)          = 0.150000000000000d0


    else if ((name1=='Be' .and. name2=='O' .or.(name1=='O' .and. name2=='Be')) &
        .and. ( (potmode>=82.and.potmode<=83) )) then

        ! Be-O potential by Jesper Byggmastar (2017)
        albepot=.true.
        bf(i,j)=15.0d0
        rf(i,j)=0.8d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.0d0
        albegamma(i,j)     = 0.3940568d0
        brennerS(i,j)      = 1.8754d0
        brennerbeta(i,j)   = 1.6525d0
        brennerDe(i,j)     = 6.783d0
        brennerre(i,j)     = 1.41d0
        gc(i,j)            = 1.4d0
        gd(i,j)            = 0.821d0
        gh(i,j)            = 0.488d0
        albe2mu(i,j)       = 0.d0

        trcut(i,j)         = 2.5d0
        dcut(i,j)          = 0.2d0


    else if ((name1=='O' .and. name2=='O' ) .and. ( (potmode>=82.and.potmode<=83) &
        .or. (potmode>=64 .and. potmode<=65))) then

        ! O-O by Erhart/Juslin/Nordlund et al. with short cutoff ('elemental version')
        ! used by Be-O and Fe-O potentials by Jesper Byggmastar
        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = +1.0d0

        ters_n(i,j)        = 1.0d0
        brennerDe(i,j)     = 5.166d0
        brennerre(i,j)     = 1.20752d0
        brennerbeta(i,j)   = 2.309d0
        brennerS(i,j)      = 1.38635d0
        albegamma(i,j)     = 0.825945d0
        gc(i,j)            = 0.356076d-1
        gd(i,j)            = 0.464955d-1
        gh(i,j)            = 0.450558d0
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 2.10d0
        dcut(i,j)          = 0.20d0

        bf(i,j)=12.0d0
        rf(i,j)=0.5d0


        ! H-O for Be-O-H application: here part for summation around H atoms
    else if ((name1=='H' .and. name2=='O') .and. ( (potmode>=82.and.potmode<=83) )) then

        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = +1.0d0

        ters_n(i,j)        = 1.0d0
        brennerDe(i,j)     = 4.544d0
        brennerre(i,j)     = 0.97520d0
        brennerbeta(i,j)   = 2.29198d0
        brennerS(i,j)      = 3.248455d0
        albegamma(i,j)     = 12.3300d0
        gc(i,j)            = 0.000000d0
        gd(i,j)            = 1.000000d0
        gh(i,j)            = 1.000000d0
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 1.6d0
        dcut(i,j)          = 0.30d0

        bf(i,j) = 15.0d0
        rf(i,j) = 0.4d0

        ! O-H for Be-O-H application: here part for summation around O atoms
    else if ((name1=='O' .and. name2=='H') .and. ( (potmode>=82.and.potmode<=83) )) then
        albepot=.true.

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   = +1.0d0

        ters_n(i,j)        = 1.0d0
        brennerDe(i,j)     = 4.544d0
        brennerre(i,j)     = 0.97520d0
        brennerbeta(i,j)   = 2.29198d0
        brennerS(i,j)      = 3.248455d0
        albegamma(i,j)     = 0.0010d0
        gc(i,j)            = 1.0000d0
        gd(i,j)            = 0.02800d0
        gh(i,j)            = 1.00000d0
        albe2mu(i,j)       = 0.0d0

        trcut(i,j)         = 1.6d0
        dcut(i,j)          = 0.30d0

        bf(i,j) = 15.0d0
        rf(i,j) = 0.4d0


    ! ------------------------------------------------------------------- !


    else if (  potmode>=60 .and. potmode<=83 &
        .and. name1=='C'  .and. name2=='C' ) then

        ! Brenner's parameter set II (PRB 1990)
        albepot=.true.
        bf(i,j)=8.0d0 ; rf(i,j)=0.6d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 6.0d0
        brennerre(i,j)    = 1.39d0
        brennerbeta(i,j)  = 2.1d0
        brennerS(i,j)     = 1.22d0
        albegamma(i,j)    = 0.00020813d0
        gc(i,j)           = 330.0d0
        gd(i,j)           = 3.5d0
        gh(i,j)           = 1.0d0
        trcut(i,j)        = 1.85d0
        dcut(i,j)         = 0.15d0
        albe2mu(i,j)      = 0.0d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0

    ! ------------------------------------------------------------------- !

    ! ------------------------------------------------------------------- !
    ! --------------- Juslin Be-H  ------------------- !
    ! ------------------------------------------------------------------- !

    ! Be-H I

    else if ((name1=='Be' .and. name2=='H' .or.(name1=='H' .and. name2=='Be')) .and. ( (potmode>=74.and.potmode<=75) )) then

        albepot=.true.

        ! Fermi parameters not set yet
        bf(i,j)=15.0d0
        rf(i,j)=0.8d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.14000000000d0
        brennerS(i,j)      = 3.00000000d0
        brennerbeta(i,j)   = 2.30000000000000d0
        brennerDe(i,j)     = 2.60000000000000d0
        brennerre(i,j)     = 1.338000000000000d0
        gc(i,j)            = 0.005700000000000d0
        gd(i,j)            = 0.00200000000000000d0
        gh(i,j)            = 1.00000000000000d0

        trcut(i,j)         = 1.75000000000000d0
        dcut(i,j)          = 0.15000000000000d0


    ! Be-H II

    else if ((name1=='Be' .and. name2=='H' .or.(name1=='H' .and. name2=='Be')) .and. ( (potmode>=82.and.potmode<=83) )) then

        albepot=.true.

        bf(i,j)=15.0d0
        rf(i,j)=0.8d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.19000000000d0
        brennerS(i,j)      = 2.50000000d0
        brennerbeta(i,j)   = 2.20000000000000d0
        brennerDe(i,j)     = 2.60000000000000d0
        brennerre(i,j)     = 1.338000000000000d0
        gc(i,j)            = 0.005700000000000d0
        gd(i,j)            = 0.00400000000000000d0
        gh(i,j)            = 1.00000000000000d0

        trcut(i,j)         = 1.80000000000000d0
        dcut(i,j)          = 0.15000000000000d0

    ! ------------------------------------------------------------------- !

    else if ( potmode>=76 .and. potmode<=77 &
        .and. ( (name1=='Be' .and. name2=='C') &
        .or.    (name2=='Be' .and. name1=='C') ) ) then

        ! Carolina's Be--C parameter set - version 1 (November 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 3.329621776707896d0
        brennerre(i,j)    = 1.72d0
        brennerbeta(i,j)  = 1.480606741096130d0
        brennerS(i,j)     = 2.010909777525212d0
        albegamma(i,j)    = 0.000245538464786d0
        gc(i,j)           = 18647.12398779292d0
        gd(i,j)           = 23.52455318007865d0
        gh(i,j)           = 1.0d0
        trcut(i,j)        = 2.30d0
        dcut(i,j)         = 0.15d0
        albe2mu(i,j)      = 0.0d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0

    else if ( potmode>=76 .and. potmode<=77 &
        .and. ( (name1=='Be' .and. name2=='C') &
        .or.    (name2=='Be' .and. name1=='C') ) ) then

        ! Carolina's Be--C parameter set - version 2 (November 2007)
        albepot=.true.
        bf(i,j)=-1.0d0 ; rf(i,j)=-1.0d0 ! Fermi parameters not set yet

        brennerDe(i,j)    = 2.860755671379450d0
        brennerre(i,j)    = 1.75d0
        brennerbeta(i,j)  = 1.43d0
        brennerS(i,j)     = 2.087855220781391d0
        albegamma(i,j)    = 0.000234338588381d0
        gc(i,j)           = 19877.30436546595d0
        gd(i,j)           = 21.85340618912338d0
        gh(i,j)           = 0.702719558878301d0
        trcut(i,j)        = 2.30d0
        dcut(i,j)         = 0.15d0
        albe2mu(i,j)      = 0.0d0
        brennergamma(i,j) = 1.0d0
        albecossign(i,j)  =+1.0d0
        ters_n(i,j)       = 1.0d0


    else if ( potmode>=80 .and. potmode<=81 &
        .and. ( (name1=='Be' .and. name2=='C') &
        .or.    (name2=='Be' .and. name1=='C') ) ) then

        ! Carolina's Be--C parameter set - version 3 (February 2008)
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=-1.0d0
        rf(i,j)=-1.0d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000032455587167d0
        brennerS(i,j)      = 2.661703130897084d0
        brennerbeta(i,j)   = 1.350000000000000d0
        brennerDe(i,j)     = 3.354356807591553d0
        brennerre(i,j)     = 1.712393019791131d0
        gc(i,j)            = 50.975653878236900d0
        gd(i,j)            = 0.351305958877629d0
        gh(i,j)            = 0.516675163742689d0

        trcut(i,j)         = 2.300000000000000d0
        dcut(i,j)          = 0.15d0


    else if ( potmode>=82 .and. potmode<=83 &
        .and. ( (name1=='Be' .and. name2=='C') &
        .or.    (name2=='Be' .and. name1=='C') ) ) then

        ! Carolina's Be--C parameter set - version 4 (August 2008)
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=16.0d0
        rf(i,j)=0.7d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.000030018449777d0
        brennerS(i,j)      = 2.766724347332735d0
        brennerbeta(i,j)   = 1.586760903647416d0
        brennerDe(i,j)     = 3.909329821145575d0
        brennerre(i,j)     = 1.724298966530114d0
        gc(i,j)            = 57.004093738903910d0
        gd(i,j)            = 0.358303811134503d0
        gh(i,j)            = 0.559996014890212d0

        trcut(i,j)         = 2.600000000000000d0
        dcut(i,j)          = 0.20d0

    !---------------- Be-W versions --------------------------------!

    else if ( potmode>=74 .and. potmode<=75 &
        .and. ( (name1=='Be' .and. name2=='W') &
        .or.    (name2=='Be' .and. name1=='W') ) ) then

        ! Carolina's Be--W parameter set (September 2009)
        ! (use with Be v. 10 latflag 75, W latflag 75)
        albepot=.true.
        ! Fermi parameters not set yet
        bf(i,j)=7.0d0
        rf(i,j)=1.7d0

        brennergamma(i,j)  = 1.0d0
        albecossign(i,j)   =+1.0d0

        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.073499549613506d0
        brennerS(i,j)      = 1.950000000000000d0
        brennerbeta(i,j)   = 0.800000000000000d0
        brennerDe(i,j)     = 3.809496508652753d0
        brennerre(i,j)     = 2.039489462352737d0
        gc(i,j)            = 5.899484660455570d0
        gd(i,j)            = 1.936384133400191d0
        gh(i,j)            = 0.369531869268705d0

        trcut(i,j)         = 3.000000000000000d0
        dcut(i,j)          = 0.200000000000000d0


    else if ( potmode>=82 .and. potmode<=83 &
        .and. ( (name1=='Be' .and. name2=='W') &
        .or.    (name2=='Be' .and. name1=='W') ) ) then

        ! Carolina's Be--W II parameter set
        ! (use with Be-Be II latflag 83, W latflag 83)
        albepot=.true.

        bf(i,j)=13d0
        rf(i,j)=1.3d0


        ters_n(i,j)        = 1.000000000000000d0
        albegamma(i,j)     = 0.217149597699585d0
        brennerS(i,j)      = 2.292852479620590d0
        brennerbeta(i,j)   = 0.912090704846423d0
        brennerDe(i,j)     = 3.790978789927637d0
        brennerre(i,j)     = 2.063300810496200d0
        gc(i,j)            = 34.395371588710910d0
        gd(i,j)            = 554.148353798116700d0
        gh(i,j)            = -0.866000000000000d0

        trcut(i,j)         = 3.300000000000000d0
        dcut(i,j)          = 0.200000000000000d0

        if (mod(potmode,2) == 1) then
            !Sometimes there should be 2 here, sometimes not
            !Thank Emppu Salonen for this feature
            albe2mu(i,j) = 1.2d0
        else
            albe2mu(i,j) = 0.0d0
        endif

    !-----------------------------------------------------------------!


    else
        if (iprint) then
            write(log_buf, '(A,A,A,I4)') &
                'tersoff_compound.f90: Unknown potmode combination ', &
                name1, name2, potmode
            call logger(log_buf)
        end if
        call my_mpi_abort('Unknown Tersoff potmode ', potmode)
    end if

    ! Brenner-style parameters set, now handle required transformations
    ! into Tersoff notation.

    rlambda(i,j)=brennerbeta(i,j)*sqrt(2.0d0*brennerS(i,j))
    tersA(i,j)=brennerDe(i,j)/(brennerS(i,j)-1.0d0)*exp(rlambda(i,j)*brennerre(i,j))
    rmu(i,j)=brennerbeta(i,j)*sqrt(2.0d0/brennerS(i,j))
    tersB(i,j)=brennerS(i,j)*brennerDe(i,j)/(brennerS(i,j)-1.0d0)*exp(rmu(i,j)*brennerre(i,j))
    beta(i,j)=brennergamma(i,j)

    if (iprint) then
        write (log_buf,'(A,5F12.5)') 'A lambda B mu beta ', &
            tersA(i,j),rlambda(i,j),tersB(i,j),rmu(i,j),beta(i,j)
        call logger(log_buf)
    endif

endif ! End of selecting brenner-style parameters.

! Tersoff compound term for Si-C and Si-Ge, 1.0d0 for all others.
xi(i,j)=xihelp;

! Use terslambda3 for odd potmodes, reset to zero for even ones.
if (terslambda3(i,j) /= 0d0) then
    if (mod(potmode,2) == 1) then
        print *,'Tersoff using lambda3', terslambda3(i,j)
    else
        terslambda3(i,j) = 0d0
        print *,'Tersoff NOT using lambda3'
    end if
end if

! Handle reppotcut: use potential properties unless reppotcutin overrides this
reppotcut(i,j)=trcut(i,j)+dcut(i,j);
if (reppotcutin < reppotcut(i,j)) reppotcut(i,j)=reppotcutin

! Initialize a number of help variables
ters2i(i,j) = -0.5d0/ters_n(i,j)
rlambda3_cube(i,j)=terslambda3(i,j)**3
rpd(i,j) = trcut(i,j)+dcut(i,j)
r_ter_max(i,j) = rpd(i,j)
rc = trcut(i,j)+dcut(i,j)
rmd(i,j) = trcut(i,j)-dcut(i,j)

a(i,j) = 0.5d0*pi/dcut(i,j)
halfa(i,j) = -0.5d0*a(i,j)

gd2(i,j) = gd(i,j)*gd(i,j)
gd2i(i,j) = 1d0/gd2(i,j)
gc2(i,j) = gc(i,j)*gc(i,j)
g2c2(i,j) = -2d0*gc2(i,j)


if (iprint) then
    print *,'Tersoff/Brenner/Albe params. for  ',name1,' - ',name2
    print *,'c           :',gc(i,j)
    print *,'d           :',gd(i,j)
    print *,'h           :',gh(i,j)
    print *,'n           :',ters_n(i,j)
    print *,'trcut       :',trcut(i,j)
    print *,'dcut        :',dcut(i,j)
    print *,'lambda3     :',terslambda3(i,j)
    print *,'rlambda     :',rlambda(i,j)
    print *,'xi          :',xi(i,j)
    print *,'A           :',tersA(i,j)
    print *,'B           :',tersB(i,j)
    print *,'rmu         :',rmu(i,j)
    print *,'rbeta       :',beta(i,j)
    print *,'brennergamma:',brennergamma(i,j)
    print *,'brennerDe   :',brennerDe(i,j)
    print *,'brennerre   :',brennerre(i,j)
    print *,'brennerS    :',brennerS(i,j)
    print *,'brennerbeta :',brennerbeta(i,j)
    print *,'albepot     :',albepot
    print *,'exponentone :',exponentone
    print *,'albecossign :',albecossign(i,j)
    print *,'albegamma   :',albegamma(i,j)
    print *,'albe2mu     :',albe2mu(i,j)
    print *,'fermi bf    :',bf(i,j)
    print *,'fermi rf    :',rf(i,j)
    print *,'trcut       :',trcut(i,j)
    print *,'dcut        :',dcut(i,j)
endif

end subroutine Init_Ter_Compound_Pot

    ! ------------------------------------------------------------------------

    subroutine Init_Ter_Compound_Pot3(i, j, k, potmode)
        ! Handle parameters that (may) depend on 3 atom types.
        ! Take care to keep them backward compatible so that they also
        ! work in 2-atom type mode
        !
        ! Subroutine needs to be called for all possible i,j,k combinations
        ! when iac==1 !
        !

        integer, intent(in) :: i, j, k
        integer, intent(in) :: potmode

        character(len=8) :: name1, name2, name3

        name1 = element(i)
        name2 = element(j)
        name3 = element(k)

        ! Set default values to be on safe side
        tersoffomega(i,j,k) = 1.0d0
        albe2mu3(i,j,k) = 0.0d0


        ! Go through possible i,j,potmode combinations which lead to
        ! 2-body atom types for the 3-body interactions

        ! BCN potential tersoffomega parameters
        if (potmode>=10 .and. potmode<=13) then
            if (name1=='N' .and. name2=='C') then
                tersoffomega(i,j,k)=0.6381d0
            else if (name1=='C' .and. name2=='N') then
                tersoffomega(i,j,k)=0.6381d0;
            endif
        endif

        ! Go through possible i,j,k,potmode combinations which lead to
        ! true 3-body parameters

        ! WCH potential CH omega and 2 mu terms, calculated from original Brenner parameters
        if (potmode>=60.and.potmode<=95) then

            ! omega
            if (name1=='C' .and. name2=='C' .and. name3=='H') then
                tersoffomega(i,j,k) = exp(-4.0*(1.39-1.1199))
            else if (name1=='C' .and. name2=='H' .and. name3=='C') then
                tersoffomega(i,j,k) = exp(-4.0*(1.1199-1.39))
            else if (name1=='H' .and. name2=='H' .and. name3=='C') then
                tersoffomega(i,j,k) = exp(-4.0*(0.74144-1.1199))
            else if (name1=='H' .and. name2=='C' .and. name3=='H') then
                tersoffomega(i,j,k) = exp(-4.0*(1.1199-0.74144))
            endif

            ! albe2mu
            if ( (name1=='C'.or.name1=='H').and.(name2=='C'.or.name2=='H').and.(name3=='C'.or.name3=='H') ) then
                if (.not. (name1=='C' .and. name2=='C' .and. name3=='C') ) then
                    albe2mu3(i,j,k)=4.0d0
                endif
            endif

            if ( name1=='Cr' .and. name2=='Cr' .and. name3=='Cr' ) then
                albe2mu3(i,j,k)=1.39662066d0
            else if (name1=='Cr' .and. name2=='Cr' .and. name3=='C') then
                tersoffomega(i,j,k) = 1.64028776d0
                albe2mu3(i,j,k) = 0.86406436d0
            else if (name1=='Cr' .and. name2=='C' .and. name3=='Cr') then
                tersoffomega(i,j,k) = 0.29399963d0
                albe2mu3(i,j,k) = -1.75204483d0
            else if (name1=='C' .and. name2=='Cr' .and. name3=='Cr') then
                tersoffomega(i,j,k) = 0.41905079d0
                albe2mu3(i,j,k) =  0.61221589d0
            endif

            if (potmode==95) then
                if (name1=='W' .and. name2=='H' .and. name3=='W') then
                    ! alpha_WHW from JNM 408 (2011) 12
                    ! But indices are reversed as LAMMPS has gik but Li use gij.
                    ! Note also unit problem in Table I: 1/nm vs. 1/A.
                    ! confirmed by comparison with LAMMPS inputs.
                    albe2mu3(i,j,k) = 0.451823d0
                endif
            endif

            if (potmode==65) then
                if      (name1=='Fe' .and. name2=='Fe' .and. name3=='Cr') then
                    albe2mu3(i,j,k)=1.0d0
                else if (name1=='Fe' .and. name2=='Cr' .and. name3=='Fe') then
                    albe2mu3(i,j,k)=1.0d0
                else if (name1=='Cr' .and. name2=='Fe' .and. name3=='Fe') then
                    albe2mu3(i,j,k)=1.0d0
                else if (name1=='Fe' .and. name2=='Cr' .and. name3=='Cr') then
                    albe2mu3(i,j,k)=1.0d0
                else if (name1=='Cr' .and. name2=='Fe' .and. name3=='Cr') then
                    albe2mu3(i,j,k)=1.0d0
                else if (name1=='Cr' .and. name2=='Cr' .and. name3=='Fe') then
                    albe2mu3(i,j,k)=1.0d0
                endif

                if      (name1=='Y' .and. name2=='Y' .and. name3=='Y') then
                    albe2mu3(i,j,k)     = 0.011313096134d0
                    tersoffomega(i,j,k) = 1.0135770386d0
                endif

            endif


            if (potmode==67) then
                if (name1=='Y' .and. name2=='Y' .and. name3=='Y') then
                    albe2mu3(i,j,k)     = 1.37930d0
                    tersoffomega(i,j,k) = 1.0d0
                endif
            endif

            ! Fe-O Byggmastar (2017) alpha and omegas
            ! NOTE: alphas (albe2mu3) not in same order as in publication,
            !       because lammps vs. parcas ijk order different:
            !        ijk same for omega, but lammps ijk = parcas ikj for alpha (albe2mu3)!!
            !         -- WHY???
            if (potmode>=64 .and. potmode<=65) then
                if (name1=='Fe' .and. name2=='Fe' .and. name3=='O') then
                    albe2mu3(i,j,k)=0.3316469057d0
                    tersoffomega(i,j,k)=0.9201732967d0
                else if (name1=='Fe' .and. name2=='O' .and. name3=='Fe') then
                    albe2mu3(i,j,k)=1.0854004606d0
                    tersoffomega(i,j,k)=0.6842003046d0
                else if (name1=='Fe' .and. name2=='O' .and. name3=='O') then
                    albe2mu3(i,j,k)=3.4803491740d0
                    tersoffomega(i,j,k)=1.0988780255d0
                else if (name1=='O' .and. name2=='Fe' .and. name3=='Fe') then
                    albe2mu3(i,j,k)=1.0361635661d0
                    tersoffomega(i,j,k)=0.9823723952d0
                else if (name1=='O' .and. name2=='Fe' .and. name3=='O') then
                    albe2mu3(i,j,k)=0.001d0
                    tersoffomega(i,j,k)=1.0d0
                else if (name1=='O' .and. name2=='O' .and. name3=='Fe') then
                    albe2mu3(i,j,k)=0.7546312148d0
                    tersoffomega(i,j,k)=1.0d0
                endif
            endif

            if (potmode>=82 .and. potmode<=83) then
                if (name1=='Be' .and. name2=='O' .and. name3=='H') then
                    albe2mu3(i,j,k) = 05.0000000d0
                else if (name1=='Be' .and. name2=='H' .and. name3=='O') then
                    albe2mu3(i,j,k) = 05.0000000d0
                endif
            endif

        endif

        if (iprint) then
            print *,'Tersoff/Brenner/Albe 3-atom type params. for  ',name1,' - ',name2,' - ',name3

            print *,'tersoffomega',tersoffomega(i,j,k)
            print *,'albe2mu3',albe2mu3(i,j,k)
        endif


    end subroutine Init_Ter_Compound_Pot3


    !
    ! Initialize parameters for all atom types.
    !
    subroutine Init_Tersoff_Compound(potmode, reppotcutin)

        integer, intent(in) :: potmode
        real(real64b), intent(in) :: reppotcutin

        integer :: i, j, k
        real(real64b) :: rcutmax

        rcut(:,:) = 0

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                if (iac(i,j) == 1) then
                    call Init_Ter_Compound_Pot(rcut(i,j), i, j, potmode, reppotcutin)
                end if
            end do
        end do

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                do k = itypelow, itypehigh
                    if (iac(i,j) == 1 .and. iac(i,k) == 1 .and. iac(j,k) == 1) then
                        call Init_Ter_Compound_Pot3(i, j, k, potmode)
                    end if
                end do
            end do
        end do

        rcutmax = max(maxval(rcut), maxval(rcutin))

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                if (iac(i,j) /= 1) then
                    if (rcutin(i,j) >= 0) then
                        rcut(i,j) = rcutin(i,j)
                    else
                        rcut(i,j) = rcutmax
                    end if
                    r_ter_max(i,j) = rcut(i,j)
                end if
            end do
        end do

        if (iprint) then
            call logger()
            call logger("Compound Tersoff version", tercversion, 0)
            call logger()
            call logger(jksym, 10)
            call logger()
            call logger("Tersoff parameters:")
            call logger()

            call print_tersoff_param("tersA:", tersA)
            call print_tersoff_param("tersB:", tersB)
            call print_tersoff_param("beta:", beta)
            call print_tersoff_param("rlambda:", rlambda)
            call print_tersoff_param("rmu:", rmu)
            call print_tersoff_param("gc:", gc)
            call print_tersoff_param("gd:", gd)
            call print_tersoff_param("gh:", gh)
            call print_tersoff_param("ters_n:", ters_n)
            call print_tersoff_param("rmu:", rmu)
            call print_tersoff_param("rlambda3:", rlambda3_cube**(1.0d0/3.0))

            call logger()
            call logger()

            call logger("albepot:", albepot, 0)
            call logger("exponentone:", exponentone, 0)
            call print_tersoff_param("albecossign:", albecossign)
            call print_tersoff_param("albegamma:", albegamma)
            call print_tersoff_param("albe2mu:", albe2mu)

            call print_tersoff_param3("albe2mu3:", albe2mu3)
            call print_tersoff_param3("tersoffomega:", tersoffomega)

            call logger()
            call logger()
        endif

    end subroutine Init_Tersoff_Compound

    ! ------------------------------------------------------------------------

    subroutine print_tersoff_param(vName, v)
        character(len=*), intent(in) :: vName
        real(real64b), intent(in) :: v(:, :)

        integer :: i

        call logger(vName)
        do i = 1, size(v, dim=1)
            write(log_buf, "(5F15.6)") v(i, :)
            call logger(log_buf, 4)
        end do

    end subroutine print_tersoff_param

    subroutine print_tersoff_param3(vName, v)
        character(len=*), intent(in) :: vName
        real(real64b), intent(in) :: v(:, :, :)

        integer :: i, j

        call logger(vName)
        do i = 1, size(v, dim=1)
            do j = 1, size(v, dim=2)
                write(log_buf, "(5F15.6)") v(i, j, :)
                call logger(log_buf, 4)
            end do
            call logger()
        end do

    end subroutine print_tersoff_param3

end module tersoff_compound_mod
