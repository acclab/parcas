module output_logger

    use datatypes, only: real64b

    use para_common, only: iprint, debug, myproc, nprocs

    use utils, only: to_str

    use my_mpi

    use file_units, only: TIME_LOG_FILE


    implicit none


    private

    public :: log_buf

    public :: logger_setup
    public :: logger_finalize
    public :: logger_write
    public :: logger


    public :: debugger

    public :: time_logger_init
    public :: time_logger
    public :: time_logger_finalize

    public :: logger_clear_buffer
    public :: logger_append_buffer


    !===========================================================================
    ! Helper field for writing custom print lines with the logger.
    !
    ! Can be used like this:
    !
    !   write (log_buf, *) "hello", 123, "world", "again"
    !   call logger(log_buf)
    !
    !===========================================================================

    character(len=1000) :: log_buf = ""


    ! Populated by MPI for writing collectively to the same file from all
    ! processes
    integer :: LOGGER_FILE_HANDLE


    interface logger

        module procedure :: log_newline

        module procedure :: log_a

        module procedure :: log_a_a
        module procedure :: log_a_i
        module procedure :: log_a_f
        module procedure :: log_a_l

        module procedure :: log_a_a10_a
        module procedure :: log_a_a10_i
        module procedure :: log_a_a10_f
        module procedure :: log_a_a10_l

        module procedure :: log_a_a_i_i
        module procedure :: log_a_i_i_i
        module procedure :: log_a_r_i_i

        module procedure :: log_a_i_i
        module procedure :: log_a_r_r
        module procedure :: log_a_r_r_r
        module procedure :: log_a_r3
        module procedure :: log_a_i3

        module procedure :: log_forces

    end interface logger


    interface logger_append_buffer
        module procedure :: log_append_newline
        module procedure :: log_append_a

        module procedure :: log_append_a_a
        module procedure :: log_append_a_i
        module procedure :: log_append_a_i_i
        module procedure :: log_append_a_i_i_i
        module procedure :: log_append_a_i3
        module procedure :: log_append_a_r
        module procedure :: log_append_a_l_l
        module procedure :: log_append_a_r_r
        module procedure :: log_append_a_r_r_r
        module procedure :: log_append_a_r3
    end interface


    interface debugger
        module procedure :: debug_a
!~         module procedure :: debug_a_a
!~         module procedure :: debug_a_i
!~         module procedure :: debug_a_r
    end interface debugger


    interface time_logger
        module procedure :: time_logger_newline

        module procedure :: time_logger_line
        module procedure :: time_logger_time
    end interface time_logger

    real(real64b) :: time_total
    real(real64b) :: time_steps


  contains


    subroutine logger_setup(filename)
        character(*), intent(in) :: filename

        call my_mpi_open_output_file(filename, LOGGER_FILE_HANDLE)
    end subroutine


    subroutine logger_finalize()
        integer :: ierror

        call mpi_file_close(LOGGER_FILE_HANDLE, ierror)
    end subroutine


    !
    ! Writes the given buffer to both output stream and the shared file handle.
    !
    subroutine logger_write(buffer, ordered)
        character(*), intent(in) :: buffer
        logical, intent(in), optional :: ordered

        integer :: i
        integer(mpi_parameters) :: ierror

        if (len(buffer) == 0) return

        if (present(ordered) .and. ordered) then
            ! Write to the standard output in an ordered manner
            do i = 0, nprocs-1
                call my_mpi_barrier()
                if (myproc == i) call write_stdout(buffer)
            end do

            ! Write to the shared file handle.
            call mpi_file_write_ordered( &
                LOGGER_FILE_HANDLE, &
                buffer, len(buffer), mpi_character, &
                MPI_STATUS_IGNORE, ierror)
        else
            ! Write to the standard output stream.
            call write_stdout(buffer)

            ! Write to the shared file handle.
            call mpi_file_write_shared( &
                LOGGER_FILE_HANDLE, &
                buffer, len(buffer), mpi_character, &
                MPI_STATUS_IGNORE, ierror)
        end if

        if (ierror /= MPI_SUCCESS) then
            call my_mpi_abort("FAILED TO WRITE TO SHARED FILE", ierror)
        end if
    end subroutine


    !
    ! Helper routine for logger_write.
    ! Writes the given buffer to the standard output stream.
    !
    subroutine write_stdout(buffer)
        character(*), intent(in) :: buffer
        character :: last

        ! Just writing with advance='no' all the time will at some
        ! point hit the maximum record length of the Fortran runtime.
        ! For GNU and Intel, this is not noticable since the max length
        ! is 2^31 - 1, but for Cray it is only 2^15 - 1, which comes up
        ! even for short simulations.
        ! To work around this, check if the last character in the buffer
        ! is the new line character. Then it makes no difference for the
        ! output whether we write the whole buffer with advance='no' or
        ! all but the last character with advance='yes'. So each time
        ! the buffer ends with a new line character, a new record is
        ! begun, and the record length reset.

        last = buffer(len(buffer):len(buffer))

        if (last == new_line('')) then
            write(6, '(A)') buffer(: len(buffer) - 1)
        else
            write(6, '(A)', advance='no') buffer
        end if

    end subroutine write_stdout


    subroutine logger_clear_buffer()
        log_buf = ""
    end subroutine


    subroutine log_newline()
        call logger("", 0)
    end subroutine log_newline


    !===========================================================================
    ! Generic function handling all the printing
    !===========================================================================
    subroutine log_a(a1,indent)
        character(*), intent(in)  :: a1
        integer, intent(in), optional :: indent

        ! Must write everything in one go to avoid interleaved output
        ! from many processes.

        if (present(indent)) then
            call logger_write(repeat(" ", indent) // trim(a1) // new_line(a1))
        else
            call logger_write(trim(a1) // new_line(a1))
        end if
    end subroutine log_a


    !===========================================================================
    ! Useful functions for logging text followed by a value
    !===========================================================================

    subroutine log_a_a(a1,a2,indent)
        character(*), intent(in) :: a1, a2
        integer, intent(in) :: indent

        call logger(a1 // " " // adjustl(a2), indent)
    end subroutine log_a_a


    subroutine log_a_i(a1,i1,indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i1
        integer, intent(in) :: indent

        call logger(a1, to_str(i1), indent)
    end subroutine log_a_i


    subroutine log_a_f(a1,f1,indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: f1
        integer, intent(in) :: indent

        call logger(a1, to_str(f1), indent)
    end subroutine log_a_f


    subroutine log_a_l(a1,l1,indent)
        character(*), intent(in) :: a1
        logical, intent(in) :: l1
        integer, intent(in) :: indent

        call logger(a1, to_str(l1), indent)
    end subroutine log_a_l


    !===========================================================================
    ! Useful functions for logging text prepending by another text, and
    ! followed by a value
    !===========================================================================

    subroutine log_a_a10_a(a1,a2,a3,indent)
        character(*), intent(in) :: a1, a2, a3
        integer, intent(in) :: indent

        character(10) :: tmp
        !character(len(a1) + len(tmp) + len_trim(a3) + 2) :: line
        character(120) :: line

        tmp = a2

        write(line, "(A,1X,A10,1X,A)") a1, tmp, trim(adjustl(a3))
        call logger(line, indent)
    end subroutine log_a_a10_a


    subroutine log_a_a10_i(a1,a2,i1,indent)
        character(*), intent(in) :: a1, a2
        integer, intent(in) :: i1
        integer, intent(in) :: indent

        call logger(a1, a2, to_str(i1), indent)
    end subroutine log_a_a10_i


    subroutine log_a_a10_f(a1,a2,f1,indent)
        character(*), intent(in) :: a1, a2
        real(real64b), intent(in) :: f1
        integer, intent(in) :: indent

        call logger(a1, a2, to_str(f1), indent)
    end subroutine log_a_a10_f


    subroutine log_a_a10_l(a1,a2,l1,indent)
        character(*), intent(in) :: a1, a2
        logical, intent(in) :: l1
        integer, intent(in) :: indent

        call logger(a1, a2, to_str(l1), indent)
    end subroutine log_a_a10_l


    !===========================================================================
    ! Useful functions for logging 2D arrays, showing the value alongside the
    ! indices.
    !===========================================================================

    subroutine log_a_a_i_i(a1,a2,i1,i2,indent)
        character(*), intent(in) :: a1, a2
        integer, intent(in) :: i1, i2
        integer, intent(in) :: indent

        character(len(a1) + len_trim(a2) + 10) :: line
        write(line, "(A,A,2I5)") a1, trim(a2), i1, i2

        call logger(line, indent)
    end subroutine log_a_a_i_i


    subroutine log_a_i_i_i(a1,i1,i2,i3,indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i1, i2, i3
        integer, intent(in) :: indent

        call logger(a1, to_str(i1), i2, i3, indent)
    end subroutine log_a_i_i_i


    subroutine log_a_r_i_i(a1,r1,i1,i2,indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: r1
        integer, intent(in) :: i1, i2
        integer, intent(in) :: indent

        call logger(a1, to_str(r1), i1, i2, indent)
    end subroutine log_a_r_i_i



    !===========================================================================
    ! MISC logging functions
    !===========================================================================

    subroutine log_a_i_i(a1, i1, i2, indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i1, i2
        integer, intent(in) :: indent

        call logger(a1, to_str(i1) // " " // to_str(i2), indent)
    end subroutine log_a_i_i


    subroutine log_a_r_r(a1, r1, r2, indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: r1, r2
        integer, intent(in) :: indent

        call logger(a1, to_str(r1) // " " // to_str(r2), indent)
    end subroutine log_a_r_r


    subroutine log_a_r_r_r(a1, r1, r2, r3, indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: r1, r2, r3
        integer, intent(in) :: indent

        character(80) :: tmp
        write(tmp, "(3G17.6)") r1, r2, r3

        call logger(a1, to_str(r1) // " " // to_str(r2) // " " // to_str(r3), indent)
    end subroutine log_a_r_r_r


    subroutine log_a_r3(a1, r, indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: r(3)
        integer, intent(in) :: indent

        call logger(a1, r(1), r(2), r(3), indent)
    end subroutine log_a_r3


    subroutine log_a_i3(a1, i, indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i(3)
        integer, intent(in) :: indent

        call logger(a1, i(1), i(2), i(3), indent)
    end subroutine


    subroutine log_forces(istep,time_fs,natoms,sumfx,sumfy,sumfz,nsummed,indent)
        integer, intent(in) :: istep
        real(real64b), intent(in) :: time_fs
        integer, intent(in) :: natoms
        real(real64b), intent(in) :: sumfx, sumfy, sumfz
        integer, intent(in) :: nsummed
        integer, intent(in) :: indent

        character(len=80) :: tmp
        write(tmp, "(I11,F15.2,3G20.10,I10)") istep, time_fs, natoms, &
            sumfx, sumfy, sumfz, nsummed

        call logger(tmp, indent)
    end subroutine



    !***************************************************************************
    !===========================================================================
    !
    ! LOG TO THE LOG_BUF INTERFACE
    !
    !    Useful when printing multi-line messages when using MPI
    !
    !===========================================================================
    !***************************************************************************

    subroutine log_append_newline()
        log_buf = trim(log_buf) // new_line("")
    end subroutine


    subroutine log_append_a(a1,indent)
        character(*), intent(in)  :: a1
        integer, intent(in), optional :: indent

        if (present(indent)) then
            log_buf = trim(log_buf) // repeat(" ", indent) // trim(a1) // &
                new_line("")
        else
            log_buf = trim(log_buf) // trim(a1) // new_line("")
        end if

    end subroutine


    subroutine log_append_a_a(a1,a2,indent)
        character(*), intent(in) :: a1, a2
        integer, intent(in) :: indent

        call logger_append_buffer(a1 // " " // a2, indent)
    end subroutine


    subroutine log_append_a_i(a1,i1,indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i1
        integer, intent(in) :: indent

        character(30) :: tmp
        write(tmp, "(I13)") i1

        call logger_append_buffer(a1, adjustl(tmp), indent)
    end subroutine


    subroutine log_append_a_i_i(a1, i1, i2, indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i1, i2
        integer, intent(in) :: indent

        character(30) :: tmp
        write(tmp, "(2I9)") i1, i2

        call logger_append_buffer(a1, tmp, indent)
    end subroutine


    subroutine log_append_a_i_i_i(a1,i1,i2,i3,indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i1, i2, i3
        integer, intent(in) :: indent

        character(50) :: tmp
        write(tmp, "(3I5)") i1, i2, i3

        call logger_append_buffer(a1, adjustl(tmp), indent)
    end subroutine


    subroutine log_append_a_i3(a1, i, indent)
        character(*), intent(in) :: a1
        integer, intent(in) :: i(3)
        integer, intent(in) :: indent

        character(50) :: tmp
        write(tmp, "(3I5)") i(:)

        call logger_append_buffer(a1, trim(tmp), indent)
    end subroutine


    subroutine log_append_a_r(a1,r1,indent)
        character(*), intent(in) :: a1
        real(kind=real64b), intent(in) :: r1
        integer, intent(in) :: indent

        character(len=30) :: tmp
        write(tmp, "(G17.6)") r1

        call logger_append_buffer(a1, adjustl(tmp), indent)
    end subroutine


    subroutine log_append_a_r_r(a1, r1, r2, indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: r1, r2
        integer, intent(in) :: indent

        character(len=80) :: tmp
        write(tmp, "(2G17.6)") r1, r2

        call logger_append_buffer(a1, tmp, indent)
    end subroutine


    subroutine log_append_a_r_r_r(a1, r1, r2, r3, indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: r1, r2, r3
        integer, intent(in)            :: indent

        character(len=80) :: tmp
        write(tmp, "(3G17.6)") r1, r2, r3

        call logger_append_buffer(a1, tmp, indent)
    end subroutine


    subroutine log_append_a_r3(a1, r, indent)
        character(*), intent(in) :: a1
        real(real64b), intent(in) :: r(3)
        integer, intent(in)            :: indent

        character(len=80) :: tmp
        write(tmp, "(3G17.6)") r(:)

        call logger_append_buffer(a1, tmp, indent)
    end subroutine


    subroutine log_append_a_l_l(a1, l1, l2, indent)
        character(*), intent(in) :: a1
        logical, intent(in) :: l1, l2
        integer, intent(in) :: indent

        character(len=30) :: tmp
        write(tmp, *) l1, l2

        call logger_append_buffer(a1, tmp, indent)
    end subroutine


    !***************************************************************************
    !===========================================================================
    !
    ! DEBUGGER INTERFACE
    !
    !===========================================================================
    !***************************************************************************

    subroutine debug_a(a1)
        character(*), intent(in) :: a1

        character(30) :: tmp1

        if (debug) then
            write(tmp1, "(I10)") myproc
            print "(4A)", "[DEBUG,", trim(adjustl(tmp1)), "] ", a1
        end if

    end subroutine debug_a



    !***************************************************************************
    !===========================================================================
    !
    ! TIMER INTERFACE
    !
    !===========================================================================
    !***************************************************************************

    subroutine time_logger_init(filename, time, steps)
        character(*), intent(in) :: filename
        real(real64b), intent(in) :: time
        integer, intent(in) :: steps

        open(TIME_LOG_FILE, file=filename, status="replace")
        time_total = time
        time_steps = steps
    end subroutine


    subroutine time_logger_newline()
        call time_logger("")
    end subroutine


    subroutine time_logger_line(line)
        character(*), intent(in) :: line
        write(TIME_LOG_FILE, "(A)") trim(line)
    end subroutine


    subroutine time_logger_time(time, task, total)
        real(real64b), intent(in) :: time
        character(*), intent(in) :: task
        real(real64b), intent(in), optional :: total

        integer :: indent
        real(real64b) :: percent, millis

        if (present(total)) then
            if (total == 0) then
                percent = 0
            else
                percent = 100d0 * time / total
            end if
            indent = 8
        else
            percent = 100d0 * time / time_total
            indent = 4
        end if
        millis = time * 1000d0 / time_steps

        write(TIME_LOG_FILE, "(A,F7.2,' % ',F10.3,' ms    ',A)") &
            repeat(" ", indent), percent, millis, task

    end subroutine time_logger_time


    subroutine time_logger_finalize()
        close(TIME_LOG_FILE)
    end subroutine


end module output_logger
