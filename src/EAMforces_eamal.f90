!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module EAMAL_params_mod
    use datatypes, only: real64b

    implicit none
    save

    public
    private :: real64b

    ! Since in Fortran the innermost index runs fastest,
    ! the indexing here is: (r,type1,type2)

    real(real64b), allocatable :: rcutpot(:,:)
    real(real64b), allocatable :: dr(:,:)
    real(real64b), allocatable :: dri(:,:)
    real(real64b), allocatable :: dP(:,:)
    integer, allocatable ::  nr(:,:)
    integer, allocatable ::  nP(:,:)

    integer :: eamrtablesz
    integer :: eamptablesz

    ! EAM pair params - pair potential parameters
    real(real64b), allocatable :: Vp_r(:,:,:)
    real(real64b), allocatable :: dVpdr_r(:,:,:)
    real(real64b), allocatable :: Vpsc(:,:,:)

    ! EAM P params - electron density parameters (read P as rho!)
    real(real64b), allocatable :: P_r(:,:,:,:)
    real(real64b), allocatable :: dPdr_r(:,:,:,:)
    real(real64b), allocatable :: Psc(:,:,:,:)

    ! EAM Fp params - embedding energy parameters
    real(real64b), allocatable :: F_p(:,:,:)
    real(real64b), allocatable :: dFdp_p(:,:,:)
    real(real64b), allocatable :: Fsc(:,:,:)

end module EAMAL_params_mod


module EAM_comms
    use datatypes, only: real64b
    use mdparsubs_mod, only: PassPacker, PassBackPacker

    implicit none
    public
    private :: real64b, PassPacker, PassBackPacker

    type, extends(PassBackPacker) :: RhoPassBackPacker
        real(real64b), pointer, contiguous :: rho(:,:)
        integer :: nbands
        real(real64b) :: rho_sum_passed
        real(real64b) :: rho_sum_received
    contains
        procedure :: do_pack => pass_back_rho_pack
        procedure :: do_unpack => pass_back_rho_unpack
    end type

    type, extends(PassPacker) :: dFpdpPassPacker
        real(real64b), pointer, contiguous :: dFpdp(:,:)
        integer :: nbands
    contains
        procedure :: do_pack => pass_dFpdp_pack
        procedure :: do_unpack => pass_dFpdp_unpack
    end type

contains

    subroutine pass_back_rho_pack(this, first_atom, num_atoms, buffer, filled_size)
        class(RhoPassBackPacker), intent(inout) :: this
        integer, intent(in) :: first_atom
        integer, intent(in) :: num_atoms
        real(real64b), contiguous, intent(out) :: buffer(:)
        integer, intent(out) :: filled_size

        integer :: i, iband

        filled_size = 0
        do iband = 1, this%nbands
            do i = first_atom, first_atom + num_atoms - 1
                filled_size = filled_size + 1
                buffer(filled_size) = this%rho(i, iband)
                this%rho_sum_passed = this%rho_sum_passed + this%rho(i, iband)
            end do
        end do
    end subroutine pass_back_rho_pack


    subroutine pass_back_rho_unpack(this, recvlist, buffer)
        class(RhoPassBackPacker), intent(inout) :: this
        integer, intent(in) :: recvlist(:)
        real(real64b), contiguous, intent(in) :: buffer(:)

        integer :: irecv, i, iband, bufind

        bufind = 0
        do iband = 1, this%nbands
            do irecv = 1, size(recvlist)
                i = recvlist(irecv)
                bufind = bufind + 1
                this%rho(i, iband) = this%rho(i, iband) + buffer(bufind)
                this%rho_sum_received = this%rho_sum_received + buffer(bufind)
            end do
        end do
    end subroutine pass_back_rho_unpack


    subroutine pass_dFpdp_pack(this, sendlist, buffer, filled_size)
        class(dFpdpPassPacker), intent(inout) :: this
        integer, intent(in) :: sendlist(:)
        real(real64b), contiguous, intent(out) :: buffer(:)
        integer, intent(out) :: filled_size

        integer :: i, isend, iband

        filled_size = 0
        do iband = 1, this%nbands
            do isend = 1, size(sendlist)
                i = sendlist(isend)
                filled_size = filled_size + 1
                buffer(filled_size) = this%dFpdp(i, iband)
            end do
        end do
    end subroutine pass_dFpdp_pack


    subroutine pass_dFpdp_unpack(this, first_atom, num_atoms, buffer)
        class(dFpdpPassPacker), intent(inout) :: this
        integer, intent(in) :: first_atom
        integer, intent(in) :: num_atoms
        real(real64b), contiguous, intent(in) :: buffer(:)

        integer :: i, iband, bufind

        bufind = 0
        do iband = 1, this%nbands
            do i = first_atom, first_atom + num_atoms - 1
                bufind = bufind + 1
                this%dFpdp(i, iband) = buffer(bufind)
            end do
        end do
    end subroutine pass_dFpdp_unpack

end module EAM_comms


module eamforces_eamal_mod

    use output_logger, only: log_buf, logger

    use defs

    use typeparam
    use datatypes, only: real64b
    use my_mpi

    use timers, only: &
        tmr, &
        TMR_EAM_CHARGE_COMMS, &
        TMR_EAM_FORCE_COMMS

    use EAMAL_params_mod

    use splinereppot_mod, only: reppot_only

    use para_common, only: &
        myatoms, np0pairtable, &
        debug, iprint

    use mdparsubs_mod, only: &
        pass_border_atoms, &
        pass_back_border_atoms, &
        potential_pass_back_border_atoms

    use EAM_comms


    implicit none

    private
    public :: Calc_P_eamal
    public :: Calc_Fp_eamal
    public :: Calc_Force_eamal


contains

    !***********************************************************************
    ! EAM density calculation - for SANDIA, NRL, and etp versions
    !***********************************************************************
    !
    ! KN  See file README.forcesubs for some notes on what these do
    !

    subroutine Calc_P_eamal(P, x0, atype, box, pbc, &
            nborlist, rcutmax, eamnbands)

        !
        !     From input x0 (from here and other nodes), return density P
        !     for all atom pairs which interact by EAM.
        !

        ! Variables passed in and out
        real(real64b), intent(out), contiguous, target :: P(:,:)
        real(real64b), intent(in), contiguous :: x0(:)
        real(real64b), intent(in) :: box(3), pbc(3)
        real(real64b), intent(in) :: rcutmax
        integer, intent(in), contiguous :: atype(:)
        integer, intent(in), contiguous :: nborlist(:)
        integer, intent(in) :: eamnbands

        ! Local variables and constants
        integer :: j, mij, nbr, nnbors
        real(real64b) :: rs, r, xp(3), deni, denj, boxs(3)

        integer :: i3, j3, i
        real(real64b) :: t1

        integer :: klo, khi
        real(real64b) :: a, b, ab, yplo, yphi, ylo, yhi

        real(real64b) :: x

        integer :: iband

        ! Atom types for each pair
        integer :: itype, jtype
        ! Atom types taking into account two possible iac modes for P_r
        integer :: itype_P, jtype_P

        if (debug) print *, 'EAM 1'

        boxs(:) = box(:)**2

        P(:np0pairtable, :eamnbands) = 0.0


        !  The nbands model is handled simply by looping over all bands.

        do iband = 1, eamnbands

            !
            !  Get P for atom pairs indexed in my node.
            !

            if (debug) print *, 'EAM 2'

            mij = 0

            do i = 1, myatoms

                i3 = 3*i - 3
                mij = mij + 1
                nnbors = nborlist(mij)
                do nbr = 1, nnbors
                    mij = mij + 1
                    j = nborlist(mij)

                    ! Handle only EAM here.
                    itype = abs(atype(i))
                    jtype = abs(atype(j))

                    select case(iac(itype, jtype))
                    case (1, 3, 4, 6)
                        continue ! EAM
                    case default
                        cycle ! other
                    end select

                    j3 = 3*j - 3

                    xp(:) = x0(i3+1:i3+3) - x0(j3+1:j3+3)

                    if (xp(1) >=  0.5d0) xp(1) = xp(1) - pbc(1)
                    if (xp(1) <  -0.5d0) xp(1) = xp(1) + pbc(1)
                    if (xp(2) >=  0.5d0) xp(2) = xp(2) - pbc(2)
                    if (xp(2) <  -0.5d0) xp(2) = xp(2) + pbc(2)
                    if (xp(3) >=  0.5d0) xp(3) = xp(3) - pbc(3)
                    if (xp(3) <  -0.5d0) xp(3) = xp(3) + pbc(3)

                    rs = sum(xp(:)**2 * boxs(:))

                    if (rs < rcutmax**2) then
                        r = sqrt(rs)

                        !             CALL Calc_Den(den, r)

                        itype_P = jtype  ! only depend on jtype density by default
                        jtype_P = jtype
                        if (iac(itype,jtype) == 4 .or. (iband > 1 .and. iac(itype,jtype) == 6)) then
                            itype_P = itype
                            jtype_P = jtype
                        end if

                        x = r * dri(itype_P,jtype_P)
                        klo = int(x) + 1
                        khi = klo + 1
                        if (khi <= nr(itype_P,jtype_P)) then
                            a = real(khi-1, real64b) - x
                            b = 1 - a
                            ab = a * b

                            ylo = P_r(klo,itype_P,jtype_P,iband) * a
                            yhi = P_r(khi,itype_P,jtype_P,iband) * b

                            yplo = dPdr_r(klo,itype_P,jtype_P,iband) * a
                            yphi = dPdr_r(khi,itype_P,jtype_P,iband) * b

                            denj = a * ylo + b * yhi + ab * (2d0 * (ylo + yhi) + &
                                dr(itype_P,jtype_P) * (yplo - yphi))
                        else
                            denj = 0.0
                        end if

                        itype_P = itype  ! only depend on itype density by default
                        jtype_P = itype
                        if (iac(itype,jtype) == 4 .or. (iband > 1 .and. iac(itype,jtype) == 6)) then
                            itype_P = jtype
                            jtype_P = itype
                        endif

                        x = r * dri(itype_P,jtype_P)
                        klo = int(x) + 1
                        khi = klo + 1
                        if (khi <= nr(itype_P,jtype_P)) then
                            a = real(khi-1, real64b) - x
                            b = 1 - a
                            ab = a * b

                            ylo = P_r(klo,itype_P,jtype_P,iband) * a
                            yhi = P_r(khi,itype_P,jtype_P,iband) * b

                            yplo = dPdr_r(klo,itype_P,jtype_P,iband) * a
                            yphi = dPdr_r(khi,itype_P,jtype_P,iband) * b

                            deni = a * ylo + b * yhi + ab * (2d0 * (ylo + yhi) + &
                                dr(itype_P,jtype_P) * (yplo - yphi))
                        else
                            deni = 0.0
                        end if

                        !              END CALL Calc_Den

                        ! Note: atom i is affected by j:s density, and vice versa
                        ! except if iac==4 or iac==6 in which case everything is mixed.

                        P(i,iband) = P(i,iband) + denj
                        P(j,iband) = P(j,iband) + deni

                    end if
                end do
            end do
        end do ! End of loop over bands

        if (debug) print *, 'EAM 3'

        ! Send back contributions to P of other nodes' atoms.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            block
                type(RhoPassBackPacker) :: rho_packer

                rho_packer%nbands = eamnbands
                rho_packer%rho => P
                rho_packer%rho_sum_passed = 0d0
                rho_packer%rho_sum_received = 0d0

                call pass_back_border_atoms(rho_packer)

                if (debug) then
                    write(log_buf,'(A,I3,A,3(A,G13.6))') &
                        "For proc ", myproc, " rho sum ", &
                        " local ", sum(P(:myatoms, :eamnbands)), &
                        " passed ", rho_packer%rho_sum_passed, &
                        " received ", rho_packer%rho_sum_received
                    call logger(log_buf)
                end if
            end block
            tmr(TMR_EAM_CHARGE_COMMS) = tmr(TMR_EAM_CHARGE_COMMS) + (mpi_wtime() - t1)
        end if

        if (debug) print  *, 'EAM 4'

    end subroutine Calc_P_eamal


    !***********************************************************************
    ! EAM F[p(r)] calculation
    !***********************************************************************
    subroutine Calc_Fp_eamal(atype, Fp, dFpdp, P, Fpextrap, eamnbands)

        !
        !     For input rho array P, return F[p] and d(F[p])/dp
        !

        ! Variables passed in and out
        real(real64b), intent(out), contiguous :: Fp(:,:)
        real(real64b), intent(out), contiguous :: dFpdp(:,:)
        real(real64b), intent(in), contiguous :: P(:,:)
        integer, intent(in), contiguous :: atype(:)
        integer, intent(in) :: Fpextrap
        integer, intent(in) :: eamnbands

        integer :: i, i1, iband
        integer :: khi, klo
        real(real64b) :: a, b, h, ab, ylo, yhi, yplo, yphi, x, xx
        logical :: overflow, underflow


        do iband = 1, eamnbands

            do i = 1, myatoms
                i1 = abs(atype(i))

                if (P(i,iband) == 0.0) then
                    ! Must be impurity or sputtered atom
                    Fp(i,iband) = 0.0
                    dFpdp(i,iband) = 0.0
                    cycle
                end if

                x = P(i,iband)
                xx = P(i,iband) / dP(i1,iband)
                klo = int(xx) + 1
                khi = klo + 1

                underflow = .false.
                if (x < 0.0) then
                    print *, 'EAM negative electron density??', i, x
                    if (Fpextrap /= 1) then
                        print *, '... proceeding with rho=zero.'
                        x = 0.0
                        xx = 0.0
                        klo = 1
                        khi = 2
                    else
                        print *, '... doing downwards extrapolation.'
                        klo = 1
                        khi = 2
                        underflow = .true.
                    end if
                end if

                overflow = .false.
                if (khi > nP(i1,iband)) then
                    print *, 'EAM Fp overflow', i, i1, P(i,iband), dP(i1,iband), nP(i1,iband), khi
                    if (Fpextrap == 1) then
                        ! This will effect a linear extrapolation
                        khi = nP(i1,iband)
                        klo = khi - 1
                        overflow = .true.
                    else
                        call my_mpi_abort('EAM Fp overflow', khi)
                    end if
                end if

                h = dP(i1,iband)
                a = ((khi - 1) * dP(i1,iband) - x) / dP(i1,iband)
                b = 1 - a

                ylo = F_p(klo,i1,iband) * a
                yhi = F_p(khi,i1,iband) * b

                if (.not. overflow .and. .not. underflow) then
                    ab = a * b
                    yplo = dFdp_p(klo,i1,iband) * a
                    yphi = dFdp_p(khi,i1,iband) * b
                    Fp(i,iband) = a * ylo + b * yhi + ab * (2d0 * (ylo + yhi) + h * (yplo - yphi))
                    dFpdp(i,iband) = ab * Fsc(klo,i1,iband) + yplo + yphi
                else
                    Fp(i,iband) = ylo + yhi
                    dFpdp(i,iband) = (F_p(khi,i1,iband) - F_p(klo,i1,iband)) / dP(i1,iband)
                    print *,'linear extrap', Fp(i,iband), dFpdp(i,iband)
                end if
            end do

        end do ! End of loop over bands

    end subroutine Calc_Fp_eamal


    !***********************************************************************
    ! Calculate the force on each atom
    !***********************************************************************
    subroutine Calc_Force_eamal(x0, xnp, Epair, atype, &
            wxxi, wyyi, wzzi, wxyi, wxzi, wyzi, &
            dFpdp, box, pbc, nborlist, &
            rcutmax, eamnbands, calc_vir)

        !
        ! Force calculation: The force xnp(i) obtained is scaled by 1/box,
        ! i.e. if F is the real force in units of eV/A,
        !
        !  xnp(i) = F(i)/box(idim)
        !
        real(real64b), intent(out), contiguous :: xnp(:)
        real(real64b), intent(out), contiguous :: Epair(:)
        real(real64b), intent(out), contiguous :: &
            wxxi(:), wyyi(:), wzzi(:), wxyi(:), wxzi(:), wyzi(:)

        real(real64b), intent(in), contiguous :: x0(:)
        real(real64b), intent(inout), contiguous, target :: dFpdp(:,:)
        real(real64b), intent(in) :: rcutmax
        real(real64b), intent(in) :: box(3), pbc(3)
        integer, intent(in), contiguous :: atype(:)

        integer, intent(in) :: eamnbands
        logical, intent(in) :: calc_vir

        integer, intent(in), contiguous :: nborlist(:)


        ! Local variables and constants
        integer :: nbr, nnbors, j, mij, iband
        real(real64b) :: V_ij, Vp_ij, vij2, r, rs, xp(3), xptemp(3), boxs(3)

        integer :: j3, i3, i, itype, jtype
        real(real64b) :: tmp, dendi, dendj, t1
        real(real64b) :: rcutmaxs

        ! Parameters for inlined calc_dend and calc_pair
        real(real64b) :: x, a, b, ab
        real(real64b) :: ylo, yhi, yplo, yphi

        integer :: klo, khi

        ! Atom types taking into account two possible iac modes for P_r
        integer :: itype_P, jtype_P


        rcutmaxs = rcutmax**2
        boxs(:) = box(:)**2

        ! Initialize forces

        xnp(:3*np0pairtable) = 0.0
        Epair(:np0pairtable) = 0.0

        wxxi(:np0pairtable) = 0.0
        wyyi(:np0pairtable) = 0.0
        wzzi(:np0pairtable) = 0.0
        if (calc_vir) then
            wxyi(:np0pairtable) = 0.0
            wxzi(:np0pairtable) = 0.0
            wyzi(:np0pairtable) = 0.0
        endif


        ! Send dFpdp values of border atoms to neighbor nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()
            block
                type(dFpdpPassPacker) :: dFpdp_packer

                dFpdp_packer%nbands = eamnbands
                dFpdp_packer%dFpdp => dFpdp

                call pass_border_atoms(dFpdp_packer)
            end block
            tmr(TMR_EAM_FORCE_COMMS) = tmr(TMR_EAM_FORCE_COMMS) + (mpi_wtime() - t1)
        end if

        !
        !  Get Epair and xnp for all atom pairs
        !

        mij = 0
        do i = 1, myatoms
            i3 = 3*i - 3
            mij = mij + 1
            nnbors = nborlist(mij)

            do nbr = 1, nnbors
                mij = mij + 1
                j = nborlist(mij)

                itype = abs(atype(i))
                jtype = abs(atype(j))

                select case (iac(itype,jtype))
                case (0)
                    cycle
                case (-1)
                    call logger("ERROR: IMPOSSIBLE INTERACTION")
                    call logger("myproc:", myproc, 4)
                    call logger("i:     ", i, 4)
                    call logger("j:     ", j, 4)
                    call logger("itype: ", itype, 4)
                    call logger("jtype: ", jtype, 4)
                    call my_mpi_abort('INTERACTION -1', myproc)
                end select

                j3 = 3*j - 3

                xp(:) = x0(i3+1:i3+3) - x0(j3+1:j3+3)

                if (xp(1) >=  0.5d0) xp(1) = xp(1) - pbc(1)
                if (xp(1) <  -0.5d0) xp(1) = xp(1) + pbc(1)
                if (xp(2) >=  0.5d0) xp(2) = xp(2) - pbc(2)
                if (xp(2) <  -0.5d0) xp(2) = xp(2) + pbc(2)
                if (xp(3) >=  0.5d0) xp(3) = xp(3) - pbc(3)
                if (xp(3) <  -0.5d0) xp(3) = xp(3) + pbc(3)

                rs = sum(xp(:)**2 * boxs(:))

                if (rs < rcutmaxs) then
                    r = sqrt(rs)

                    ! Force calc
                    if (iac(itype,jtype) == 2) then
                        V_ij = 0d0
                        tmp = 0d0
                        call reppot_only(r, V_ij, tmp, itype, jtype)
                        tmp = tmp / r

                    else

                        ! Inlined subroutines to increase efficiency
                        ! CALL Calc_Dend(dend, r)
                        ! For alloys, you need to calculate both
                        ! drho_i/dr_i and drho_j/dr_i !?

                        ! drho_j/dr_i

                        ! Be very careful with cutoff's here - alloys can have
                        ! different cutoff values !!

                        ! Need to calculate densities in all bands here
                        tmp = 0d0
                        do iband = 1, eamnbands
                            itype_P = jtype
                            jtype_P = jtype
                            if (iac(itype,jtype) == 4  .or. (iband > 1 .and. iac(itype,jtype) == 6)) then
                                itype_P = itype
                                jtype_P = jtype
                            end if

                            x = r * dri(itype_P,jtype_P)
                            klo = int(x) + 1
                            khi = klo + 1
                            if (khi <= nr(itype_P,jtype_P)) then
                                a = real(khi-1, real64b) - x
                                b = 1 - a
                                ab = a * b
                                yplo = dPdr_r(klo,itype_P,jtype_P,iband) * a
                                yphi = dPdr_r(khi,itype_P,jtype_P,iband) * b
                                dendj = ab * Psc(klo,itype_P,jtype_P,iband) + yplo + yphi
                            else
                                dendj = 0.0
                            end if

                            itype_P = itype
                            jtype_P = itype
                            if (iac(itype,jtype) == 4 .or. (iband > 1 .and. iac(itype,jtype) == 6)) then
                                itype_P = jtype
                                jtype_P = itype
                            end if

                            ! drho_i/dr_i
                            x = r * dri(itype_P,jtype_P)
                            klo = int(x) + 1
                            khi = klo + 1
                            if (khi <= nr(itype_P,jtype_P)) then
                                a = real(khi-1, real64b) - x
                                b = 1 - a
                                ab = a * b
                                yplo = dPdr_r(klo,itype_P,jtype_P,iband) * a
                                yphi = dPdr_r(khi,itype_P,jtype_P,iband) * b
                                dendi = ab * Psc(klo,itype_P,jtype_P,iband) + yplo + yphi
                            else
                                dendi = 0.0
                            endif

                            tmp = tmp + (dendj * dFpdp(i,iband) + dendi * dFpdp(j,iband)) / r
                        enddo ! End of loop over bands

                        ! CALL Calc_Pair(V_ij,Vp_ij, r)
                        ! dV2/dr
                        x = r * dri(itype,jtype)
                        klo = int(x) + 1
                        khi = klo + 1
                        if (khi <= nr(itype,jtype)) then
                            a = real(khi-1, real64b) - x
                            b = 1 - a
                            ab = a * b
                            ylo = Vp_r(klo,itype,jtype) * a
                            yhi = Vp_r(khi,itype,jtype) * b
                            yplo = dVpdr_r(klo,itype,jtype) * a
                            yphi = dVpdr_r(khi,itype,jtype) * b
                            V_ij = a * ylo + b * yhi + ab * (2d0 * (ylo + yhi) + &
                                dr(itype,jtype) * (yplo - yphi))
                            Vp_ij = ab * Vpsc(klo,itype,jtype) + yplo + yphi
                        else
                            V_ij = 0.0
                            Vp_ij = 0.0
                        end if

                        ! Force from pair potential
                        tmp = tmp + Vp_ij / r

                    endif
                    vij2 = V_ij * 0.5d0
                    Epair(i) = Epair(i) + vij2
                    Epair(j) = Epair(j) + vij2

                    !KN
                    ! This calculates the force in the right direction,
                    ! and scales with box size.
                    !
                    xptemp(:) = xp(:) * tmp
                    xnp(i3+1:i3+3) = xnp(i3+1:i3+3) - xptemp(:)
                    xnp(j3+1:j3+3) = xnp(j3+1:j3+3) + xptemp(:)

                    xp(:) = xp(:) * 0.5d0

                    wxxi(i) = wxxi(i) - xptemp(1) * xp(1)
                    wxxi(j) = wxxi(j) - xptemp(1) * xp(1)
                    wyyi(i) = wyyi(i) - xptemp(2) * xp(2)
                    wyyi(j) = wyyi(j) - xptemp(2) * xp(2)
                    wzzi(i) = wzzi(i) - xptemp(3) * xp(3)
                    wzzi(j) = wzzi(j) - xptemp(3) * xp(3)

                    if (calc_vir) then
                        wxyi(i) = wxyi(i) - xptemp(1)*xp(2)
                        wxyi(j) = wxyi(j) - xptemp(1)*xp(2)
                        wxzi(i) = wxzi(i) - xptemp(1)*xp(3)
                        wxzi(j) = wxzi(j) - xptemp(1)*xp(3)
                        wyzi(i) = wyzi(i) - xptemp(2)*xp(3)
                        wyzi(j) = wyzi(j) - xptemp(2)*xp(3)
                    end if
                end if
            end do
        end do


        ! Pass back contributions to forces, energies and virials
        ! to neighbor nodes.
        if (nprocs > 1) then
            t1 = mpi_wtime()

            if (calc_vir) then
                call potential_pass_back_border_atoms( &
                    xnp, Epair, wxxi, wyyi, wzzi, wxyi, wxzi, wyzi)
            else
                call potential_pass_back_border_atoms( &
                    xnp, Epair, wxxi, wyyi, wzzi)
            end if

            tmr(TMR_EAM_FORCE_COMMS) = tmr(TMR_EAM_FORCE_COMMS) + (mpi_wtime() - t1)
        end if

    end subroutine Calc_Force_eamal


end module eamforces_eamal_mod
