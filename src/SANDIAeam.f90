!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module SANDIAeam_mod

    use datatypes, only: real64b
    use output_logger, only: log_buf, logger

    use typeparam, only: itypelow, itypehigh, element, mass, iac, rcut
    use defs, only: EAMTABLESZ
    use para_common, only: iprint, debug, myproc
    use my_mpi, only: my_mpi_abort, my_mpi_bcast

    use EAMAL_params_mod


    implicit none

    private
    public :: Init_EAM_Pot


contains

    !***********************************************************************
    ! EAM density calculation from Sandia spline tables
    !
    ! This file is common to EAMforces.f90 and EAMforces_eamal.f90.
    ! It can deal with both elemental and alloy EAM variants.
    !
    ! For speed, most subroutines have been inlined in the force
    ! calculations routines.
    !
    !***********************************************************************


    !
    ! Initialize all splines, cutoffs, ... for EAM and EAMAL potentials.
    !
    subroutine Init_EAM_Pot(potmode, spline, eamnbands, EAMAL)

        integer, intent(in) :: potmode
        integer, intent(in) :: spline
        integer, intent(in) :: eamnbands
        logical, intent(in) :: EAMAL

        integer :: i, j

        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                select case (iac(i,j))
                case (1, 4, 6)
                    call Init_Pot_eam(rcut(i,j), i, j, potmode, spline, eamnbands, EAMAL)
                case default
                    rcut(i,j) = 0d0   ! Ignored anyway
                end select
            end do
        end do
        do i = itypelow, itypehigh
            do j = itypelow, itypehigh
                if (iac(i,j) == 3) then
                    if (.not. EAMAL) call my_mpi_abort("Cross pot without EAMAL", i)
                    call Make_Cross_Pot_eam(rcut(i,j), i, j)
                end if
            end do
        end do

    end subroutine Init_EAM_Pot


    !***********************************************************************
    !
    ! Read in one EAM potential
    !
    ! The file form should be the 'universal 3' EAM format
    !
    ! Containing for atype1=atype2:
    !
    ! LINE NUMBER      DATA
    ! 1                Comment, ignored
    ! 2                ielement, amass, blat, latname; format (i5,2g15.5,a8)
    ! 3                nP, dP, nr, dr, rcutpot; format (:)
    ! 4 - 4+nP-1       F_p, embedding function F(rho)
    ! previous + nr    Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
    ! previous + nr    P_r, electron density rho(r)
    !
    ! Containing for atype1/=atype2 with iac=1:
    !
    ! LINE NUMBER      DATA
    ! 1                Comment, ignored
    ! 2                nr, dr, rcutpot; format (:)
    ! 3 - 3+nr-1       Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
    !
    !
    ! Containing for atype1/=atype2 with iac=4:
    !
    ! LINE NUMBER      DATA
    ! 1                Comment, ignored
    ! 2                nr, dr, rcutpot; format (:)
    ! 3 - 3+nr-1       Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
    ! previous + nr    P_r, electron density rho(r)
    !
    ! File name conventions:
    !
    ! For itype1=itype2: If Xx = elementname as character*2
    !     Either eam.Xx.Xx.in
    !     or     xxu3.fcn       (note: name lowercased !)
    !
    ! For itype1 /= itype2: If elements are Xx and Yy
    !            eam.Xx.Yy.in
    !
    !* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
    !
    ! Many-band models (introduced in parcas V3.74beta3):
    !
    ! - New parameter eamnbands tells how many bands there are. Band index is n
    ! - Bands are in the form of additional embedding energy functions
    !   for each element i: F_ii^n
    ! - For each new embedding energy function there is a new electron
    !   density rho_ii^n
    ! - The pair potential part is unaffected!!
    !
    ! - Mixed electron density may be problematic, since in at least Wallenius
    !   PRB (2004) paper d-band is handled as ordinary EAM, s-band as mixed
    !   This should be handled with different iac options
    ! - For Wallenius PRB (2004) use iac=6 which makes bands>1 have their
    !   own mixed electron density
    ! - In this case the elemental s-band electron density is ignored,
    !   but it still needs to be in the file for consistency in the read-in routines
    !
    ! - The function read-in format for a 2-band model, iac=6 is:
    !
    !
    ! LINE NUMBER      DATA
    ! 1                Comment, ignored
    ! 2                ielement, amass, blat, latname; format (i5,2g15.5,a8)
    ! 3                nP, dP, nr, dr, rcutpot, nP2, dP2; format (:)
    ! 4 - 4+nP-1       F_p^1, embedding function F^1(rho)
    ! previous + nr    Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
    ! previous + nr    P_r^1, electron density rho^1(r)
    ! previous + nP    F_p^2, embedding function F^2(rho)
    ! previous + nr    P_r^2, electron density rho^2(r)
    !
    ! Containing for atype1/=atype2 with iac=6:
    !
    ! LINE NUMBER      DATA
    ! 1                Comment, ignored
    ! 2                nr, dr, rcutpot; format (:)
    ! 3 - 3+nr-1       Z_r, Z(r)(potmode=1) or pair potential V(r) (potmode=2)
    ! previous + nr    P_r^2, electron density rho^2(r)
    !
    !
    !************************************************************************
    subroutine Init_Pot_eam(rcutread, itype1, itype2, potmode, spline, eamnbands, EAMAL)

        real(real64b), intent(out) :: rcutread
        integer, intent(in) :: itype1, itype2
        integer, intent(in) :: potmode, spline, eamnbands
        logical, intent(in) :: EAMAL


        real(real64b) :: amass, blat
        integer :: ielement,nPmax_local
        character(len=8) :: latname
        character(len=8) :: el1,el2

        integer :: i,j,i1,i2,iband,nnr,nnp

        character(len=30) :: filename

        logical, save :: firsttime = .true.

        real(real64b), parameter :: phi0 = 14.39975d0

        integer :: ios ! IO status


        if (firsttime .and. iprint) then
            if (EAMAL) then
                call logger()
                call logger("PARCAS ALLOY EAM VERSION")
                call logger()
                if (eamnbands > 1) then
                    call logger("RAN FOR NBAND MODEL WITH N=", eamnbands, 4)
                    call logger()
                end if
            else
                call logger()
                call logger("PARCAS ELEMENTAL EAM VERSION")
                call logger()
                if (spline == 1) then
                    call logger("SPLINE INTERPOLATION USED")
                    call logger()
                else
                    call logger("LINEAR INTERPOLATION USED")
                    call logger("I HOPE YOU KNOW WHAT YOU'RE DOING")
                    call logger()
                end if
            end if
        end if

        if (EAMAL) then
            if (spline /= 1) then
                if (iprint) then
                    call logger('Alloy EAM does not yet support linear interpolation', 0, spline)
                end if
                call my_mpi_abort('no linear interpolation in eamal', spline)
            end if

        else ! .not. EAMAL
            if (.not. firsttime) then
                call my_mpi_abort("BUG: EAMAL detection on firsttime failed", itype1)
            end if

            if (iac(itype1,itype2) /= 1) then
                call my_mpi_abort("BUG: EAMAL detection on iac failed", iac(itype1,itype2))
            end if

            if (eamnbands > 1) then
                call my_mpi_abort("BUG: EAMAL detection on eamnbands failed", eamnbands)
            end if

            if (itype1 /= itype2) then
                call my_mpi_abort("BUG: EAMAL detection on types failed", itype1)
            end if
        end if


        if (firsttime) then
            if (iprint .and. debug) call logger("EAMAL allocating 1")

            ! Allocate basic variables
            allocate(nP(itypelow:itypehigh,eamnbands))
            allocate(dP(itypelow:itypehigh,eamnbands))
            allocate(nr(itypelow:itypehigh,itypelow:itypehigh))
            allocate(dr(itypelow:itypehigh,itypelow:itypehigh))
            allocate(dri(itypelow:itypehigh,itypelow:itypehigh))
            allocate(rcutpot(itypelow:itypehigh,itypelow:itypehigh))
        end if


        i1 = itype1
        i2 = itype2

        if (itype1 /= itype2 .and. iprint) then
            call logger("Reading in cross EAM pair potential for atom types", itype1, itype2, 0)
            if (iac(itype1,itype2) == 4) then
                call logger("Also reading in electron density cross term")
            end if
        end if


        ! Open the input file
        if (iprint) then
            el1 = element(itype1)
            el2 = element(itype2)

            filename = 'in/eam.' // trim(el1) // '.' // trim(el2) // '.in'
            open(5, file=filename, status='old', iostat=ios)

            if (ios /= 0 .and. itype1 == itype2) then
                filename = 'in/' // trim(el1) // 'u3.fcn'

                ! Lowercase first char of name if in upper case
                i = iachar(el1(1:1))
                if (i >= ichar('A') .and. i <= iachar('Z')) then
                    i = i - iachar('A') + iachar('a')
                end if
                filename(4:4) = achar(i)

                open(5, file=filename, status='old', iostat=ios)
            end if

            if (ios /= 0) then
                call logger("Failed to open EAM file for types:", itype1, itype2, 0)
                call logger("Last tried file:", filename, 0)
                call my_mpi_abort('EAMpotfile open', itype1)
            end if

            call logger("Now reading EAM file: " // trim(filename) // &
                " for types", itype1, itype2, 0)
        end if

        ! Skip the comment
        if (iprint) read(5,*)

        if (itype1 == itype2) then
            if (iprint) then
                ! ielement, blat, latname ignored
                ! amass used only for checking against md.in values
                ! TODO maybe allow free-format input
                read(5,"(I5,2G15.5,A8)") ielement, amass, blat, latname
                if (debug) then
                    write(log_buf,"(I5,2G15.5,A8)") ielement, amass, blat, latname
                    call logger(log_buf)
                end if

                if (mass(itype1) /= amass) then
                    call logger("EAM:")
                    call logger("type1: ", itype1, 4)
                    call logger("mass:  ", mass(itype1), 4)
                    call logger("amass: ", amass, 4)
                    call my_mpi_abort('mass discrepancy in md.in/pot', itype1)
                end if

                ! TODO deal with all eamnband values generically
                select case (eamnbands)
                case (1)
                    read(5,*) nP(i1,1), dP(i1,1), nr(i1,i2), dr(i1,i2), rcutpot(i1,i2)
                case (2)
                    read(5,*) nP(i1,1), dP(i1,1), nr(i1,i2), dr(i1,i2), rcutpot(i1,i2), &
                        nP(i1,2), dP(i1,2)
                case (3)
                    read(5,*) nP(i1,1), dP(i1,1), nr(i1,i2), dr(i1,i2), rcutpot(i1,i2), &
                        nP(i1,2), dP(i1,2), nP(i1,3), dP(i1,3)
                case default
                    call logger('Cannot read in nP line for more than 3 bands, please')
                    call logger('modify SANDIAeam.f90 to fix')
                end select
            end if
            do iband = 1, eamnbands
                call my_mpi_bcast(nP(i1,iband), 1, 0)
                call my_mpi_bcast(dP(i1,iband), 1, 0)
            end do
        else
            if (iprint) read(5,*) nr(i1,i2), dr(i1,i2), rcutpot(i1,i2)
        end if
        call my_mpi_bcast(nr(i1,i2), 1, 0)
        call my_mpi_bcast(dr(i1,i2), 1, 0)
        call my_mpi_bcast(rcutpot(i1,i2), 1, 0)
        dri(i1,i2) = 1d0 / dr(i1,i2)
        rcutread = rcutpot(i1,i2)

        if (debug) print *, nP(i1,1), dP(i1,1), nr(i1,i2), dr(i1,i2), rcutpot(i1,i2)

        nPmax_local = maxval(nP(i1, :eamnbands))
        if (firsttime) then
            if (iprint .and. debug) call logger("EAMAL allocating 2")
            ! Allocate all permanent EAM tables
            ! Reserve some extra space for possible larger tables of later
            ! atom types
            nnr = nr(i1,i2) + EAMTABLESZ
            nnp = nPmax_local + EAMTABLESZ
            eamrtablesz = nnr
            eamptablesz = nnp

            ! Pair potential (r)
            allocate(Vp_r   (nnr, itypelow:itypehigh, itypelow:itypehigh))
            allocate(dVpdr_r(nnr, itypelow:itypehigh, itypelow:itypehigh))
            allocate(Vpsc   (nnr, itypelow:itypehigh, itypelow:itypehigh))

            ! Electron density (r)
            allocate(P_r   (nnr, itypelow:itypehigh, itypelow:itypehigh, eamnbands))
            allocate(dPdr_r(nnr, itypelow:itypehigh, itypelow:itypehigh, eamnbands))
            allocate(Psc   (nnr, itypelow:itypehigh, itypelow:itypehigh, eamnbands))

            ! Embedding energy tables (rho)
            allocate(F_p   (nnp, itypelow:itypehigh, eamnbands))
            allocate(dFdp_p(nnp, itypelow:itypehigh, eamnbands))
            allocate(Fsc   (nnp, itypelow:itypehigh, eamnbands))
        end if

        if (nr(i1,i2) > eamrtablesz) then
            call logger('Init_Pot ERROR: later EAM r potential has larger')
            call logger('table size than earlier + EAMTABLE SIZE')
            write(log_buf,*) myproc,i1,i2,nr(i1,i2),eamrtablesz
            call logger(log_buf)
            call my_mpi_abort('EAMTABLE', nr(i1,i2))
        end if

        if (i1 == i2 .and. nPmax_local > eamptablesz) then
            call logger('Init_Pot ERROR: later EAM P potential has larger')
            call logger('table size than earlier + EAMTABLE SIZE')
            write(log_buf,*) myproc,i1,i2,nPmax_local,eamptablesz
            call logger(log_buf)
            call my_mpi_abort('EAMTABLE',nPmax_local)
        end if


        ! Read embedding function
        if (i1 == i2) then
            call read_spline_array(5, nP(i1,1), dP(i1,1), &
                F_p(:,i1,1), dFdp_p(:,i1,1), Fsc(:,i1,1))

            if (debug) print *, 'F(p) read in', nP(i1,1), i1
        end if


        ! Read pair potential
        if (iprint) then
            do j = 1, nr(i1,i2)
                read(5,*) Vp_r(j,i1,i2)
            end do
            if (potmode == 2) then
                call logger("Potential mode 2 selected, Vp(r) read in directly")
            else
                ! Convert  Z(r)  ==>  Vp(r) = Phi0 * Z(r)**2 / r
                do j = 2, nr(i1,i2)
                    Vp_r(j,i1,i2) = phi0 * Vp_r(j,i1,i2)**2 / (dr(i1,i2) * (j-1))
                end do
                ! Set first element to second to avoid infinity for r=0
                Vp_r(1,i1,i2) = Vp_r(2,i1,i2)
            end if
        end if
        call my_mpi_bcast(Vp_r(:,i1,i2), nr(i1,i2), 0)
        call CubicSplineSetup(nr(i1,i2), dr(i1,i2), Vp_r(:,i1,i2), dVpdr_r(:,i1,i2), &
            Vpsc(:,i1,i2), y11=0d0, yn1=0d0)
        if (debug) print *, 'Z(r) or Vp(r) read in', nr(i1,i2), i1, i2


        ! Read charge density
        if (i1 == i2 .or. iac(i1,i2) == 4) then
            call read_spline_array(5, nr(i1,i2), dr(i1,i2), &
                P_r(:,i1,i2,1), dPdr_r(:,i1,i2,1), Psc(:,i1,i2,1))

            if (debug) print *, 'P(r) read in', nr(i1,i2), i1, i2
        end if


        ! Handle many-body model read-in
        do iband = 2, eamnbands
            if (i1 == i2) then
                call read_spline_array(5, nP(i1,iband), dP(i1,iband), &
                    F_p(:,i1,iband), dFdp_p(:,i1,iband), Fsc(:,i1,iband))

                if (debug) print *, 'F(p) read in', nP(i1,1), i1, ' band', iband
            end if

            if (i1 == i2 .or. iac(i1,i2) == 6) then
                call read_spline_array(5, nr(i1,i2), dr(i1,i2), &
                    P_r(:,i1,i2,iband), dPdr_r(:,i1,i2,iband), Psc(:,i1,i2,iband))

                if (debug) print *, 'P(r) read in', nr(i1,i2), i1, i2, ' band', iband
            end if
        end do

        if (iprint) close(5)

        if (debug) print *, 'Pot. read in', rcutread, myproc, i1, i2

        firsttime = .false.


        if (.not. EAMAL) then
            ! Copy values from EAMAL arrays to EAM parameters.
            ! This is makes EAMforces.f90 faster, since it does not
            ! have to deal with indexes.
            block
                use EAM_params_mod, only: &
                    rcutpot_eam => rcutpot, &
                    nr_eam => nr, dr_eam => dr, dri_eam => dri, &
                    nP_eam => np, dP_eam => dP, &
                    Vp_r_eam => Vp_r, dVpdr_r_eam => dVpdr_r, Vpsc_eam => Vpsc, &
                    P_r_eam => P_r, dPdr_r_eam => dPdr_r, Psc_eam => Psc, &
                    F_p_eam => F_p, dFdp_p_eam => dFdp_p, Fsc_eam => Fsc

                rcutpot_eam = rcutpot(i1,i2)
                nr_eam = nr(i1,i2)
                dr_eam = dr(i1,i2)
                dri_eam = dri(i1,i2)
                nP_eam = nP(i1,1)
                dP_eam = dP(i1,1)

                Vp_r_eam   (:nr_eam) = Vp_r   (:nr_eam,i1,i2)
                dVpdr_r_eam(:nr_eam) = dVpdr_r(:nr_eam,i1,i2)
                Vpsc_eam   (:nr_eam) = Vpsc   (:nr_eam,i1,i2)

                P_r_eam   (:nr_eam) = P_r   (:nr_eam,i1,i2,1)
                dPdr_r_eam(:nr_eam) = dPdr_r(:nr_eam,i1,i2,1)
                Psc_eam   (:nr_eam) = Psc   (:nr_eam,i1,i2,1)

                F_p_eam   (:nP_eam) = F_p   (:nP_eam,i1,1)
                dFdp_p_eam(:nP_eam) = dFdp_p(:nP_eam,i1,1)
                Fsc_eam   (:nP_eam) = Fsc   (:nP_eam,i1,1)
            end block

            ! The EAMAL arrays are then not used anymore.
            deallocate(rcutpot, nr, dr, dri, nP, dP)
            deallocate(Vp_r, dVpdr_r, Vpsc)
            deallocate(P_r, dPdr_r, Psc)
            deallocate(F_p, dFdp_p, Fsc)
        end if

    end subroutine Init_Pot_eam


    subroutine read_spline_array(funit, n, dx, y, y1, sc)
        integer, intent(in) :: funit  ! Input file number
        integer, intent(in) :: n      ! Number of points
        real(real64b), intent(in) :: dx  ! Distance between points
        real(real64b), contiguous, intent(out) :: y(:)   ! Values as points
        real(real64b), contiguous, intent(out) :: y1(:)  ! Derivatives at points
        real(real64b), contiguous, intent(out) :: sc(:)  ! ??? spline thingy

        integer :: i

        if (iprint) then
            do i = 1, n
                read(funit, *) y(i)
            end do
        end if

        call my_mpi_bcast(y, n, 0)

        ! All callers currently use second boundary conditions.
        call CubicSplineSetup(n, dx, y, y1, sc, y12=0d0, yn2=0d0)
    end subroutine read_spline_array

    !***********************************************************************

    subroutine Make_Cross_Pot_eam(rcutread, itype1, itype2)

        real(real64b), intent(out) :: rcutread
        integer, intent(in) :: itype1, itype2

        integer :: i, ii1, ii2, i1, i2
        real(real64b) :: x, Vp2, dVp2


        if (iprint) then
            call logger("Constructing EAM cross potential for atom types:", itype1, itype2, 0)
        end if

        i1 = itype1
        i2 = itype2

        ! The cross potential in EAM is simply the geometric average of
        ! V1 and V2
        !
        ! In constructing the cross potential, use the thinner grid
        ! to interpolate data into the denser one.
        ! The end result has the dr and nr of the denser grid.
        ! This procedure should also be always symmetric!
        if (dr(i1,i1) <= dr(i2,i2)) then
            ii1 = i1
            ii2 = i2
        else
            ii1 = i2
            ii2 = i1
        end if
        nr(i1,i2) = nr(ii1,ii1)
        dr(i1,i2) = dr(ii1,ii1)
        dri(i1,i2) = dri(ii1,ii1)
        rcutpot(i1,i2) = rcutpot(ii1,ii1)
        rcutread = rcutpot(i1,i2)

        if (iprint) then
            write(log_buf,'(A,2I2,A,I6,2F10.5)') 'Cross potential', &
                i1,i2,' has n dr rcut',nr(i1,i2),dr(i1,i2),rcutpot(i1,i2)
            call logger(log_buf)
        end if

        do i = 1, nr(i1,i2)
            ! Get Vp_r of V2 at r1 using spline interpolation
            x = (i-1) * dr(i1,i2)
            if (x < rcutpot(ii2,ii2)) then
                call CubicSpline(nr(ii2,ii2), dr(ii2,ii2), Vp_r(:,ii2,ii2), &
                    dVpdr_r(:,ii2,ii2), Vpsc(:,ii2,ii2), x, Vp2, dVp2)
            else
                Vp2 = 0d0
                dVp2 = 0d0
            end if

            ! Then get geometric average of the two
            if (Vp_r(i,ii1,ii1) < 0 .or. Vp2 < 0) then
                call logger('HORROR ERROR: Attempting to create automatic EAM')
                call logger('cross potential, but one of potentials is < 0')
                write(log_buf,*) 'ii1 ii2 r Vii1 Vp2',x,ii1,ii2,Vp_r(i,ii1,ii1),Vp2
                call logger(log_buf)
                call my_mpi_abort('iac 3 component negative', i)
            end if
            Vp_r(i,i1,i2) = sqrt(Vp_r(i,ii1,ii1) * Vp2)
        end do

        call CubicSplineSetup(nr(i1,i2), dr(i1,i2), Vp_r(:,i1,i2), &
            dVpdr_r(:,i1,i2), Vpsc(:,i1,i2), y11=0d0, yn1=0d0)

    end subroutine Make_Cross_Pot_eam

    !***********************************************************************
    ! Cubic Spline interpolation routine and setup routine
    !***********************************************************************
    !
    ! Actual interpolation done directly in EAMforces.f, no subroutines
    ! used for that for maximum efficiency! Only initialization routines
    ! should use CubicSpline.
    !
    !***********************************************************************

    subroutine CubicSpline(n, dx, ya, y1a, sc, x, y, y1)

        ! Compute y and 1st prime y1 at x using pre-computed cubic
        ! spline tables ya, y1a, sc of length n.
        ! See CubicSplineSetup below.

        ! dx      : x distance between n knots of (tabulated values)
        ! ya(:)   : function at n knots 0,dx,2dx,... (tabulated values)
        ! y1a(:)  : function 1st prime at n knots (tabulated values)
        ! x       : variable for interpolation
        ! y       : function at interpolation point x (out)
        ! y1      : 1st prime at interpolation point x (out)

        integer, intent(in) :: n
        real(real64b), intent(in) :: dx
        real(real64b), contiguous, intent(in) :: ya(:)
        real(real64b), contiguous, intent(in) :: y1a(:)
        real(real64b), contiguous, intent(in) :: sc(:)
        real(real64b), intent(in) :: x
        real(real64b), intent(out) :: y, y1

        integer :: k, khi, klo
        real(real64b) :: a, b, ab, ylo, yhi, yplo, yphi

        klo = 1
        khi = n
        do while (khi - klo > 1)
            k = (khi + klo) / 2
            if (dx * (k-1) > x) then
                khi = k
            else
                klo = k
            end if
        end do
        a = (dx * (khi-1) - x) / dx
        b = 1 - a
        ab = a * b
        yplo = y1a(klo) * a
        yphi = y1a(khi) * b
        ylo = ya(klo) * a
        yhi = ya(khi) * b
        y = a*ylo + b*yhi + ab*(2*(ylo+yhi) + dx*(yplo-yphi))
        y1 = ab*sc(klo) + yplo + yphi

    end subroutine CubicSpline

    !***********************************************************************

    subroutine CubicSplineSetup(n, dx, ya, y1a, ysca, y11, yn1, y12, yn2)

        ! Calculate y1a  : 1st prime at knots
        ! Calculate ysca : ???
        ! From Fortran compliment volume 1 / page 51

        ! Cubic spline for first or second boundary condition.
        ! Pick the boundary condition by passing either (y11,yn1) or
        ! (y12,yn2) by name.

        ! n           : number of knots (input)
        ! dx,ya( )    : knots (input) = (0,ya(1)), (dx,ya(2)), (2*dx,ya(3))...
        ! y1a( )      : 1st prime at knots (output)
        ! y11,yn1     : 1st prime at boundaries (input, optional)
        ! y12,yn2     : 2nd prime at boundaries (input, optional)

        integer, intent(in) :: n
        real(real64b), intent(in)  :: dx
        real(real64b), contiguous, intent(in)  :: ya(:)
        real(real64b), contiguous, intent(out) :: y1a(:)
        real(real64b), contiguous, intent(out) :: ysca(:)
        real(real64b), intent(in), optional :: y11, yn1
        real(real64b), intent(in), optional :: y12, yn2


        integer :: j
        real(real64b) :: y, y1, bt, af
        real(real64b), allocatable :: b(:)

        allocate(b(n))

        y = ya(2) - ya(1)

        if (present(y11)) then
            y1a(1) = 1d0
            b(1) = y11
        else
            y1a(1) = -0.5d0
            b(1) = 1.5d0*y/dx - 0.25d0*dx*y12
        end if

        do j = 2, n-1
            y1 = y
            y = ya(j+1) - ya(j)
            af = 0.5d0
            bt = 3 * ((1-af)*y1/dx + af*y/dx)
            y1a(j) = -af / (2 + (1-af)*y1a(j-1))
            b(j) = (bt - (1-af)*b(j-1)) / (2 + (1-af)*y1a(j-1))
        end do

        if (present(y11)) then
            y1a(n) = yn1
        else
            y1a(n) = (3*y/dx + dx*yn2/2 - b(n-1)) / (2 + y1a(n-1))
        end if

        do j = n-1, 1, -1
            y1a(j) = y1a(j)*y1a(j+1) + b(j)
        end do

        do j = 1, n-1
            y = ya(j+1) - ya(j)
            ysca(j) = 6*y/dx - 3 * (y1a(j+1) + y1a(j))
        end do
        ysca(n) = 0d0

        deallocate(b)

    end subroutine CubicSplineSetup

end module SANDIAeam_mod
