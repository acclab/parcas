!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module elstop_mod

    use datatypes, only: real64b
    use file_units, only: TEMP_FILE

    use typeparam
    use my_mpi, only: my_mpi_abort, my_mpi_sum
    use para_common, only: myatoms, debug, iprint, myproc
    use random, only: gasdev

    use output_logger, only: &
        logger, log_buf, logger_write, &
        logger_clear_buffer, logger_append_buffer


    implicit none
    save

    private
    public :: elstop_init
    public :: elstop_loop


    ! Elstop arrays for velocity and stopping power for each type.
    real(real64b), allocatable :: V(:,:), S(:,:)
    integer :: current_array_size

    ! Number of values in V and S for this type.
    integer, allocatable :: nmax(:)

    ! Whether the V and S arrays have been initialized for this type.
    logical, allocatable :: type_init_done(:)


    ! Straggling elstop effective Z2
    real(real64b) :: effectiveZ2

    ! Straggling elstop statistics
    real(real64b) :: stragglingsum = 0.0
    integer :: stragglingn = 0


contains


    subroutine elstop_init(melstopst, natoms)

        integer, intent(in) :: melstopst
        integer, intent(in) :: natoms

        integer :: i

        if (debug) call logger("elstop allocating", myproc, 0)

        allocate(type_init_done(itypelow:itypehigh))
        type_init_done = .false.

        allocate(nmax(itypelow:itypehigh))
        nmax(:) = 0  ! In case elstop_grow_arrays is called

        ! Hard-coded default size, will increase automatically if needed
        current_array_size = 256

        allocate(V(current_array_size, itypelow:itypehigh))
        allocate(S(current_array_size, itypelow:itypehigh))

        if (melstopst == 1) then
            if (iprint) then
                call logger("*** Including straggling of electronic stopping in elstop calcs.")
            end if

            effectiveZ2 = 0.0
            do i = itypelow, itypehigh
                if (noftype(i) > 0) then
                    if (atomZ(i) <= 0.0) then
                        call logger("ERROR: elstop straggling needs Z for all atom types")
                        call my_mpi_abort('elstop straggling needs Z2', i)
                    endif
                    effectiveZ2 = noftype(i) * atomZ(i)
                endif
            enddo
            effectiveZ2 = effectiveZ2 / natoms

            if (iprint) then
                call logger("Elstop straggling calculation using effective Z2:", effectiveZ2, 0)
            end if
        endif

        do i = itypelow, itypehigh
            call read_elstop_file(i)
        end do

    end subroutine elstop_init


    subroutine elstop_grow_arrays()
        real(real64b), allocatable :: tmp_V(:,:), tmp_S(:,:)

        integer :: i

        current_array_size = 2 * current_array_size

        allocate(tmp_V(current_array_size, itypelow:itypehigh))
        allocate(tmp_S(current_array_size, itypelow:itypehigh))

        do i = itypelow, itypehigh
            tmp_V(:nmax(i), i) = V(:nmax(i), i)
            tmp_S(:nmax(i), i) = S(:nmax(i), i)
        end do

        ! Might be redundant? Haven't found a good source telling otherwise.
        deallocate(V)
        deallocate(S)

        call move_alloc(from=tmp_V, to=V)
        call move_alloc(from=tmp_S, to=S)
    end subroutine elstop_grow_arrays

    !
    ! Read the electronic stopping power for the type itype from the
    ! elstop file, and fill the arrays V(:,itype) and S(:itype) for it.
    ! Also write the number of rows read to nmax(itype).
    !
    subroutine read_elstop_file(itype)

        integer, intent(in) :: itype

        integer :: i, io
        character(:), allocatable :: filename
        character(len=120) :: buf

        ! Open the right file based on the 'atom type' and the 'substrate'
        filename = "in/elstop." // trim(element(itype)) // "." // &
            trim(substrate) // ".in"
        open(TEMP_FILE, file=filename, status='old', iostat=io)

        ! If the file doesn't exist try with 'in/elstop.in'
        if (io /= 0 .and. itype == 1 .and. ntype == 1) then
            open(TEMP_FILE, file='in/elstop.in', status='old', iostat=io)
        end if

        if (io /= 0) then
            ! Do not abort, since it might be that the file is never needed.
            if (iprint) call logger("WARNING: missing elstop file " // filename)
            return
        end if

        i = 0
        do
            read(TEMP_FILE, fmt='(A)', iostat=io) buf

            if (io < 0) exit  ! End of file
            if (io > 0) then
                call logger("Error reading elstop file")
                call my_mpi_abort("Error reading elstop file", io)
            end if

            ! Ignore comment lines and empty lines.
            select case (buf(1:1))
            case ('%', '#', '!')
                cycle
            end select
            if (len_trim(buf) == 0) cycle

            i = i + 1
            nmax(itype) = i

            if (i > current_array_size) then
                call elstop_grow_arrays()
            end if

            read(buf, *) V(i,itype), S(i,itype)
        end do

        close(TEMP_FILE)

        if (debug .and. iprint) then
            call logger_clear_buffer()
            call logger_append_buffer("*** Elstop file opened")
            call logger_append_buffer("processor:", myproc, 4)
            call logger_append_buffer("atom type:", element(itype), 4)
            call logger_append_buffer("file:", filename, 4)
            call logger_append_buffer("data points read:", nmax(itype))
            call logger_write(trim(log_buf))
        end if

        type_init_done(itype) = .true.

    end subroutine read_elstop_file


    !
    ! Apply electronic stopping power on appropriate atoms.
    ! Modifies the velocities x1(:) and kinetic energies Ekin(:).
    !
    ! Returns the total energy lost for all atoms (FDe(1)) and sputtered
    ! atoms (FDe(2)), as well as the total (num_applied) and sputtered
    ! (num_applied_sputtered) number of atoms the power was applied on.
    !
    ! TODO: find a better name for this
    !
    subroutine elstop_loop(natoms, x0, x1, Ekin, atype, box, delta, &
            irec, irecproc, melstop, melstopst, elstopmin, elstopsputlim, FDe, &
            num_applied, num_applied_sputtered)

        real(real64b), intent(inout) :: x1(:)
        real(real64b), intent(inout) :: Ekin(:)
        real(real64b), intent(out) :: FDe(2)
        integer, intent(out) :: num_applied
        integer, intent(out) :: num_applied_sputtered

        integer, intent(in) :: natoms
        real(real64b), intent(in) :: x0(:)
        integer, intent(in) :: atype(:)
        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: delta(itypelow:itypehigh)
        integer, intent(in) :: irec
        integer, intent(in) :: irecproc

        integer, intent(in) :: melstop
        integer, intent(in) :: melstopst
        real(real64b), intent(in) :: elstopmin
        real(real64b), intent(in) :: elstopsputlim

        integer :: i, i3
        integer :: isum_array(2)
        real(real64b) :: boxs(3)
        real(real64b) :: Elost
        integer :: Fdeindex
        logical :: sputtered

        boxs(:) = box(:)**2

        FDe(:) = 0.0
        num_applied = 0
        num_applied_sputtered = 0

        if (melstop == 1) then
            ! Elstop only for the recoil atom.
            if (myproc == irecproc) then
                i = irec
                num_applied = 1
                call subelstop(i, x1, Ekin, atype, box, boxs, delta, FDe(1), &
                    natoms, melstopst)
            end if

        else if (melstop >= 2) then
            do i = 1, myatoms
                i3 = 3*i - 3
                sputtered = .false.

                select case (melstop)
                case (2)
                    ! Elstop for all atoms.
                    continue

                case (3)
                    ! Elstop for all fast atoms.
                    if (Ekin(i) < elstopmin) cycle

                case (4)
                    ! Elstop for all fast atoms, and for all sputtered atoms.
                    if (Ekin(i) < elstopmin) cycle

                    sputtered = any(abs(x0(i3+1:i3+3)) > elstopsputlim)

                    if (sputtered) then
                        num_applied_sputtered = num_applied_sputtered + 1

                    else if (any(abs(x0(i3+1:i3+3)) > 0.5d0)) then
                        cycle
                    end if

                case (5)
                    ! Elstop for all fast, non-sputtered atoms.
                    if (Ekin(i) < elstopmin) cycle

                    if (any(abs(x0(i3+1:i3+3)) > elstopsputlim)) cycle
                end select

                num_applied = num_applied + 1

                call subelstop(i, x1, Ekin, atype, box, boxs, delta, &
                    Elost, natoms, melstopst)

                FDeindex = merge(2, 1, sputtered)
                FDe(FDeindex) = FDe(FDeindex) + Elost
            end do
        end if

        call my_mpi_sum(FDe, 2)

        isum_array(1) = num_applied
        isum_array(2) = num_applied_sputtered
        call my_mpi_sum(isum_array, 2)
        num_applied = isum_array(1)
        num_applied_sputtered = isum_array(2)

    end subroutine elstop_loop


    !
    ! Subtract elstop for one atom 'iatom' in my node.
    ! Also returns Elost, the energy lost in electronic stopping.
    !
    ! NOTE: This routine is not necessarily called by all processors.
    !
    subroutine subelstop(iatom, x1, Ekin, atype, box, boxs, delta, Elost, natoms, melstopst)

        real(real64b), intent(inout) :: x1(:)
        real(real64b), intent(inout) :: Ekin(:)
        real(real64b), intent(out) :: Elost
        integer, intent(in) :: iatom
        integer, intent(in) :: atype(:)
        real(real64b), intent(in) :: box(3), boxs(3)
        real(real64b), intent(in) :: delta(itypelow:itypehigh)

        integer :: i3, atypei, natoms, melstopst
        real(real64b) :: v, vSI, deltav, dsq, deltar

        real(real64b) :: E0, E1

        i3 = 3*iatom - 3
        atypei = abs(atype(iatom))
        dsq = delta(atypei)**2
        E0 = 0.5d0 * sum(x1(i3+1:i3+3)**2 * boxs(:)) / dsq

        if (E0 == 0.0d0) then
            Elost = 0.0
            return
        end if

        v = sqrt(2.0d0 * E0)
        vSI = v * vunit(atypei) * 1.0d5

        ! Calculate deltar using x=vt for elstop straggling calculations.
        deltar = v * delta(atypei)

        deltav = delta(atypei) * elstop(vSI, atypei, natoms, box, melstopst, deltar)

        associate (xv => x1(i3+1:i3+3))
            xv = xv * (1.0d0 - deltav / v)
        end associate

        E1 = 0.5d0 * sum(x1(i3+1:i3+3)**2 * boxs(:)) / dsq
        Ekin(iatom) = E1

        Elost = E0 - E1

    end subroutine subelstop

    ! ------------------------------------------------------------------------


    real(real64b) function elstop(vel, itype, natoms, box, melstopst, deltar)
        !
        ! PARCAS version of elstop subraction. Code taken from old
        ! MOLDY range code, based on original implementation by Kai Nordlund.
        !
        ! NOTE: this routine is not necessarily called by all processors
        !
        ! Returns the electronic stopping in eV/A for an input velocity in m/s
        !
        ! When called for the first time, the subroutine reads the electronic
        ! stopping power from elstop.in into the internal arrays V and S
        ! V should contain velocities in m/s, S the stopping power in eV/A.
        !
        ! File name convention: elstop.element(type).in
        !                       if itype==1 and ntype==1, elstop.in possible
        !
        ! The argument vel should be in m/s !
        ! If vel is too large for elstop file warning is printed
        !
        ! The subroutine returns the stopping power in eV/A by means of a linear
        ! interpolation between the points in the arrays V and S.
        !


        real(real64b), intent(in) :: vel
        real(real64b), intent(in) :: box(3)
        real(real64b), intent(in) :: deltar
        integer, intent(in) :: itype
        integer, intent(in) :: natoms
        integer, intent(in) :: melstopst

        integer :: i

        integer, save :: ul = 1
        integer, save :: ll = 2

        real(real64b) :: straggling_elstop, partdens, om, gd


        if (.not. type_init_done(itype)) then
            call logger("Missing required elstop for type " // element(itype), itype, 0)
            call my_mpi_abort("missing elstop file for type " // element(itype), itype)
        end if

        if (vel > V(nmax(itype),itype)) then
            call logger("Velocity out of range for type", itype, 0)
            call logger("Velocity:    ", vel, 4)
            call logger("Max velocity:", V(nmax(itype),itype), 4)
            call logger("nmax:        ", nmax(itype), 4)
            call logger("proc:        ", myproc, 4)
            do i = 1, nmax(itype)
                write(log_buf, "(F13.3,2X,F13.3)") V(i,itype), S(i,itype)
                call logger(log_buf)
            end do

            elstop = S(nmax(itype),itype)
            return
        end if

        ! The potential need not be with even intervals in V, but it should be ordered
        ! by increasing V. Therefore, binary search is the fastest way to locate vel:
        ! But first check whether we are in the right range already.

        if (ll >= nmax(itype)) then
            ll = 1
            ul = 2
        end if

        if (.not. (ul == ll+1 .and. vel > V(ll,itype) .and. vel < V(ul,itype))) then
            ul = nmax(itype)
            ll = 1
            do while(ul - ll > 1)
                i = (ul - ll) / 2
                if (vel < V(ll+i, itype)) then
                    ul = ll + i
                else
                    ll = ll + i
                end if
            end do
        end if
        if (ul <= ll .or. ul > nmax(itype)) then
            write(log_buf, *) "elstop",ll, ul, itype, nmax(itype), vel
            call logger(log_buf)
            call my_mpi_abort('ELSTOP HORROR', ul)
        end if


        ! And now for the linear interpolation!
        elstop = (S(ul,itype) - S(ll,itype)) / &
             (V(ul,itype) - V(ll,itype)) * (vel - V(ll,itype)) + S(ll,itype)


        ! Elstop straggling stuff

        if (melstopst == 1) then
            partdens = natoms / product(box)
            if (atomZ(itype) <= 0.0) then
                call logger("ERROR: elstop straggling needs Z1 for all recoil atom types")
                call my_mpi_abort('elstop straggling needs Z', i)
            end if
            om = LSSOmega(atomZ(itype), effectiveZ2, vel, partdens, deltar)
            gd = gasdev()

            straggling_elstop = om / deltar * gd
            elstop = elstop + straggling_elstop
            stragglingsum = stragglingsum + abs(straggling_elstop)
            stragglingn = stragglingn
            if (iprint .and. mod(stragglingn, 1000) == 1) then
                write(log_buf, *) "Latest and |average| elstop straggling", &
                    straggling_elstop, om, stragglingsum / stragglingn
                call logger(log_buf)
            end if
        end if

    end function elstop

    !-----------------------------------------------------------------------------
    !
    !     STRAGGLING version (23.6.1993 Kai Nordlund), upgraded to parcas 7.12.2011
    !
    !     The straggling of the electronic stopping is taken in account using
    !     the model presented in A. Luukkainen and M. Hautala, Rad. Eff. 59 (1982).
    !
    !  This means a gaussian distribution of the stopping,
    !
    !  S = S_mean+ omega/dr*gasdev(0,1),
    !
    !  where dr is the movement of the atom since the calculation was last done,
    !  S_mean is the expectation value of the straggling taken from the elstop file,
    !  and omega is a complex function of esZ1,esZ2,vel, the atomic density and dr.
    !
    !  The length of dr should not affect the final result, due to purely mathematical
    !  reasons.
    !

    real(real64b) function LSSOmega(Z1, Z2, vel, partdens, deltar)

        real(real64b), intent(in) :: Z1, Z2
        real(real64b), intent(in) :: vel   ! in SI units!
        real(real64b), intent(in) :: partdens  ! particle density
        real(real64b), intent(in) :: deltar

        real(real64b) :: x, omegaB, L

        ! TODO: Use the values from the constants module
        real(real64b), parameter :: Bohrvel = 2187700.0   ! SI units
        real(real64b), parameter :: e = 1.602189e-19
        real(real64b), parameter :: eps0 = 8.854188e-12
        real(real64b), parameter :: PI = 3.14159265358979
        real(real64b), parameter :: unitl = 1d-10

        ! This version corresponds exactly to MDRANGE
        ! based on equation (4) in Hautala, Rad. Eff. 59 (1982) 113

        ! partdens * deltar is in units of 1/A^2, i.e. correct for this to get SI
        omegaB = Z1 * e / (4 * PI * eps0) * sqrt(4 * PI * Z2 * partdens * deltar / unitl**2)

        x = vel**2 / (Bohrvel**2 * Z2)   ! divide with Bohr velocity

        L = 1.0d0
        if (x < 2.3) L = 0.5 * (1.36 * sqrt(x) - 0.016 * sqrt(x**3))

        LSSOmega = omegaB * sqrt(L)

    end function LSSOmega

end module elstop_mod
