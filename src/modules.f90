!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************


!
! File numbers, names and contents (all .in are in dir in/, all .out
! in out/)
!
!  4    reppot...in    Reppot input file
!  5    eam...in       EAM input file
!  5    mdlat.in       Atom coord/restart file
!  5    md.in          Parameter input file
!  6    (stdout)       Main output file
!  7    md.out         Main physical output file
!  8    mdlat.out      Restart output file
!  9    atoms.out      Final atom coord and energy output file
!  9    elstop.in      Elstop input file
!  12   movie.*        Binary output movie file, see
!                         documentation/README.DOCUMENTATION
!  17   pressures.out  Pressure output file
!  21   liquidat.out   Liquid output file
!  22   recoilat.out   Recoil output file
!  23   tersoff...in   Tersoff parameter files
!  25   silica.in      Parameter file for silica input files
!  59   track.in       Input file for track heating data
!
!*******************************************************************************

module file_units
    implicit none

    public

    integer, parameter :: TEMP_FILE = 5
    integer, parameter :: TRACK_ARRAY_FILE = 15

    integer, parameter :: TIME_LOG_FILE = 9

    integer, parameter :: BLOCK_NATOMS_FILE = 18

    integer, parameter :: OUT_RECOIL_FILE = 22
    integer, parameter :: OUT_PRESSURES_FILE = 17
    integer, parameter :: OUT_LIQUIDAT_FILE = 21
end module file_units


module lat_flags
    implicit none

    public

    integer, parameter :: LATFLAG_CREATE_FCC             = 0
    integer, parameter :: LATFLAG_FILE_COORDINATES_ONLY  = 1
    integer, parameter :: LATFLAG_CREATE_DIA             = 2
    integer, parameter :: LATFLAG_FILE_RESTART_GUESS_ION = 3
    integer, parameter :: LATFLAG_FILE_RESTART_NEW_ION   = 4
    ! general parameter for creating the lattice based on parameters
    ! in the md.in file.
    integer, parameter :: LATFLAG_CREATE_LATTICE         = 5

end module lat_flags


!some parameters related to datatype sizes
module datatypes
    implicit none

    public

    integer, parameter ::  int32b = SELECTED_INT_KIND(9)
    integer, parameter :: real32b = SELECTED_REAL_KIND(6, 30)
    integer, parameter ::  int64b = SELECTED_INT_KIND(12)
    integer, parameter :: real64b = SELECTED_REAL_KIND(15, 307)

end module datatypes


module symbolic_constants
    implicit none

    public

    integer, parameter :: IREC_NO_RECOIL = 0
    integer, parameter :: IREC_ADD_ATOM  = -2
    integer, parameter :: IREC_NEAREST   = -1

end module symbolic_constants


module timers
    use datatypes, only: real64b

    implicit none
    save

    public
    private :: real64b

    real(real64b) :: tmr(99)

    integer, parameter :: TMR_TOTAL = 1
    integer, parameter :: TMR_MAIN_LOOP = 2

    ! Make sure to keep all timers that are subtracted from TMR_TOTAL
    ! to get TMR_MAIN_LOOP time at consecutive numbers.
    integer, parameter :: TMR_TEMP_CONTROL = 3
    integer, parameter :: TMR_PREDICT = 4
    integer, parameter :: TMR_NBOR = 5
    integer, parameter :: TMR_EAM_CHARGE = 6
    integer, parameter :: TMR_EAM_EMBED = 7
    integer, parameter :: TMR_EAM_FORCE = 8
    integer, parameter :: TMR_CORRECT = 9
    integer, parameter :: TMR_MAIN_OUTPUT = 10
    integer, parameter :: TMR_SHUFFLE = 11
    integer, parameter :: TMR_MISC_PROPERTIES = 12
    integer, parameter :: TMR_ELSTOP = 13
    integer, parameter :: TMR_SEMICON_FORCE = 14
    integer, parameter :: TMR_REBALANCE = 15

    integer, parameter :: TMR_MAIN_OTHER = 16

    ! Then keep all communication timers together, leaving space
    ! for adding more main loop timers.
    integer, parameter :: TMR_NBOR_COMMS = 31
    integer, parameter :: TMR_EAM_CHARGE_COMMS = 32
    integer, parameter :: TMR_EAM_FORCE_COMMS = 33
    integer, parameter :: TMR_SEMICON_COMMS = 34

    integer, parameter :: TMR_COMMS_TOTAL = 35

    ! Then other stuff.
    integer, parameter :: TMR_NBOR_LINK_CELL = 51
    integer, parameter :: TMR_NBOR_GET_PAIRS = 52
    integer, parameter :: TMR_SEMICON_REPPOT = 53
    integer, parameter :: TMR_SHUFFLE_PRE = 54
    integer, parameter :: TMR_SHUFFLE_MAIN = 55
    integer, parameter :: TMR_SHUFFLE_POST = 56
    integer, parameter :: TMR_SHUFFLE_ALL_TO_ALL = 57
    integer, parameter :: TMR_MOVIE = 58

end module timers


module typeparam
    use datatypes, only: real64b

    implicit none
    save

    public
    private :: real64b

    ! Atom type dependent constants
    ! Allocated after ntypes is determined in read_params
    ! Note: atom type dependent _variables_ (like delta) are
    !  not part of module block

    integer :: ntype,itypelow,itypehigh
    character(len=8), allocatable :: element(:)
    character(len=8) :: substrate

    real(kind=real64b), allocatable :: mass(:),timeunit(:),vunit(:),aunit(:)
    ! This is only needed for elstop straggling calculations
    real(kind=real64b), allocatable :: atomZ(:)
    real(kind=real64b), allocatable :: rcut(:,:),rcutin(:,:)
    integer, allocatable :: iac(:,:), noftype(:)

end module typeparam


module border_params_mod
    use datatypes, only: real64b
    implicit none

    private
    public :: BorderParams

    !
    ! A type containing parameters that define which atoms are interpreted
    ! as border atoms for the sake of temperature scaling.
    ! These alone are not sufficient, since there is interaction with
    ! periodic boundaries and so on.
    !
    type BorderParams
        ! Atoms in z-range [zmin, zmax] included
        real(real64b) :: zmin
        real(real64b) :: zmax

        ! Atoms outside x-range [-xout, +xout] included
        real(real64b) :: xout

        ! Atoms outside y-range [-yout, +yout] included
        real(real64b) :: yout

        ! Atoms outside range [-sputlim, +sputlim] in any direction *excluded*
        real(real64b) :: sputlim

        ! Atoms within thickness of any borders included
        real(real64b) :: thickness

        ! Atoms of type Tcontroltype included, overrides others
        integer :: Tcontroltype
    end type

end module border_params_mod


module basis
    use datatypes, only: real64b

    implicit none
    save

    public
    private :: real64b

    ! Number of atoms in the largest supported lattice
    integer, parameter :: LARGEST_SUPPORTED_LATTICE = 8

    ! Read in atom basis for creating lattice for latflag=5

    integer :: nreadbasis
    real(real64b) :: readoffset(3)
    real(real64b), allocatable :: readbasis(:,:)
    integer, allocatable :: readtype(:)

    real(real64b), allocatable :: changeprob(:)
    integer, allocatable :: changeto(:)
end module basis


! *** physical constants, conversion factors ***
! Module by Paul Ehrhart/Yinon Ashkenazy

module PhysConsts
    use datatypes, only: real64b

    implicit none

    public
    private :: real64b

    ! Boltzmann constant in eV/K and its inverse (source: NIST)
    real(kind=real64b), parameter :: kBeV = 8.61734215d-5
    real(kind=real64b), parameter :: invkBeV = 11604.506153d0
    ! Boltzmann constant in J/K
    real(kind=real64b), parameter :: kB = 1.380662d-23
    ! the electron charge (source: NIST)
    real(kind=real64b), parameter :: e = 1.602176462d-19
    ! the atomic mass unit (source: NIST)
    real(kind=real64b), parameter :: u = 1.66054873d-27
    ! conversion factors for calculation of pressures from virials
    ! 1 Pa = 1 N/m^2 = 1 J/m^3 = 10^-30/e eV/A^3
    ! or 1 eV/A^3 = e*10^30 Pa and finally 1 kbar = 10^8 Pa
    real(kind=real64b), parameter :: eV_to_kbar = e/1d-30/1d8
    real(kind=real64b), parameter :: kbar_to_eV = 1.0d0/eV_to_kbar

    real(kind=real64b), parameter :: pi = 3.141592653589793238462643383279502884197d0

end module PhysConsts


!************************************************************
! this module holds all values related to the temperature-
! time program, written by Paul Ehrhart for parcas V3.70
!************************************************************
module Temp_Time_Prog
    use datatypes, only: real64b

    implicit none
    save

    public
    private :: real64b

    ! Maximum number of steps allowed in temperature-time-program
    integer, parameter :: TT_maxnsteps = 6

    ! True if the temperature-time program is activated
    logical :: TT_activated

    ! True if tempnew has been set for quenches in Temp_Control
    logical :: TT_tempnewset = .false.

    ! The current TT program step (from 1 to TT_maxnsteps)
    integer :: TT_step

    ! Parameter values for each step
    integer       :: TT_mtemp(TT_maxnsteps)  ! Thermostat mode -> mtemp
    real(real64b) :: TT_time(TT_maxnsteps)   ! Starting time (first must be 0)
    real(real64b) :: TT_temp(TT_maxnsteps)   ! target temperature -> temp
    real(real64b) :: TT_trate(TT_maxnsteps)  ! Quench rate -> trate
    real(real64b) :: TT_btctau(TT_maxnsteps) ! Berendsen tau -> btctau

end module Temp_Time_Prog
