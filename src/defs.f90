!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************

module defs
    implicit none

    public

    ! Maximum number of atoms per process (including processor boundary region).
    integer, parameter :: NPMAX = 1200000

    ! Number of atoms that can be passed between processors (from the
    ! processor boundary region).
    integer, parameter :: NPPASSMAX = 100000

    ! Maximum number of neighbors per atom.
    integer, parameter :: NNMAX = 100

    ! Size of the EAM parameter array.
    integer, parameter :: EAMTABLESZ = 100005

    ! Movie frames are written in chunks of atoms. This is the maximum size
    ! of one chunk, in atoms. Reduce to save memory, increase to improve speed.
    ! Memory use is this times CHARS_PER_ATOM, see mdoutput.f90.
    integer, parameter :: MOVIE_CHUNK_SIZE = 100000

end module
