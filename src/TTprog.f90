!*******************************************************************************
! PARCAS MD  - PARallell CAScade Molecular Dynamics code
!
! Copyright (C) 2017 Prof. Kai Nordlund <kai.nordlund@helsinki.fi>,
! University of Helsinki
!
! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.
!*******************************************************************************


! Temperature-time programme by Paul Erhart 17.10 2002, parcas V3.70
module ttprog_mod

    use datatypes, only: real64b

    use output_logger, only: log_buf, logger
    use PhysConsts, only: kBeV, invkBeV
    use para_common, only: iprint
    use my_mpi, only: my_mpi_abort

    use Temp_Time_Prog


    implicit none

    private
    public :: Init_Temp_Time_Prog
    public :: Set_Temp_Control


contains

    !************************************************************
    ! This subroutine sets the mtemp, temp, and trate values
    ! for the current time step.
    !************************************************************
    subroutine Set_Temp_Control(time_fs, mtemp, temp, temp0, trate, &
            ntimeini, timeini, btctau)

        real(real64b), intent(in) :: time_fs
        integer, intent(inout) :: mtemp
        integer, intent(inout) :: ntimeini
        real(real64b), intent(inout) :: temp, temp0
        real(real64b), intent(inout) :: trate, timeini
        real(real64b), intent(inout) :: btctau

        integer :: step
        integer, save :: laststep = 0

        ! First figure out which set of parameters should be used.
        do step = 1, TT_maxnsteps
            if (time_fs < TT_time(step)) exit
        end do
        step = step - 1
        TT_step = step

        ! Check if the TPP step has changed since last time.
        if (step <= laststep) return

        ! Now set values.
        mtemp = TT_mtemp(step)
        temp  = TT_temp(step)
        trate = TT_trate(step)
        btctau = TT_btctau(step)

        ! For mode 6 and 9 the time before quenching has to
        ! be set to zero explicitly.
        if (mtemp == 6 .or. mtemp == 9) then
            ntimeini = 0
            timeini = 0.0
            temp0 = TT_temp(step - 1)
        end if

        ! Print out info.
        if (iprint) then
            write(log_buf, "(A,1X,I2,1X,F11.2,1X,I2,4(1X,F10.3))") &
                "ttp", step, time_fs, mtemp, temp*invkBeV, temp0*invkBeV, trate*invkBeV, btctau
            call logger(log_buf)
        end if

        laststep = step

    end subroutine Set_Temp_Control


    !************************************************************
    ! This subroutine checks the internal consistency of the
    ! temperature-time program, prints a summary, and finally
    ! converts the temperatures and temperature rates from
    ! Kelvin to eV.
    !************************************************************
    subroutine Init_Temp_Time_Prog(tscaleth)

        real(real64b), intent(in) :: tscaleth

        integer :: i

        ! Validate inputs and log the configuration.
        ! Call only on one process to avoid duplicate prints.
        if (iprint) then
            call validate_temp_time_prog(tscaleth)
        end if

        ! Unit conversion
        do i = 1, TT_maxnsteps
            TT_temp(i) = kBeV * TT_temp(i)
            TT_trate(i) = kBeV * TT_trate(i)
        end do

    end subroutine Init_Temp_Time_Prog


    subroutine validate_temp_time_prog(tscaleth)

        real(real64b), intent(in) :: tscaleth

        real(real64b) :: maxtemp
        integer :: i

        ! Header for summary.
        call logger()
        call logger("temperature-time program:")
        call logger("time (fs), mtemp, temp (K), trate (K/fs)")

        do i = 1, TT_maxnsteps
            ! Check times.
            if (i == 1) then
                if (TT_time(i) /= 0.0) then
                    print *, TT_Time(i)
                    call my_mpi_abort('TPP-ERROR: initial time for first step should be 0.0 always', 0)
                end if
            else
                if (TT_time(i) >= 1.0d30) cycle
                if (TT_time(i) <= TT_time(i-1) .or. TT_time(i) <= 0.0) then
                    print *, TT_Time(i)
                    call my_mpi_abort('TPP-ERROR: invalid value(s) for TTtime', 0)
                end if
            end if

            ! Check mode.
            select case (TT_mtemp(i))
            case (0,1,2,3,5,6,7,9,13)
                ! OK
            case default
                print *, TT_mtemp(i)
                call my_mpi_abort('TTP-ERROR: invalid value for TTmt', TT_mtemp(i))
            end select

            ! No ramping mode initially.
            if (i == 1 .and. (TT_mtemp(i) == 6 .or. TT_mtemp(i) == 9)) then
                print *, TT_mtemp(i)
                call my_mpi_abort('TTP-ERROR: no ramping mode in first step', TT_mtemp(i))
            end if

            ! Check for impossible temperatures.
            if (TT_temp(i) < 0 .or. TT_temp(i) > 200000.0) then
                print *, TT_temp(i)
                call my_mpi_abort('TTP-ERROR: invalid value for TTtem', int(TT_temp(i)))
            end if

            if (TT_mtemp(i) == 6 .or. TT_mtemp(i) == 9) then
                ! Check whether the ramping rate is non-zero for ramping modes.
                if (TT_trate(i) == 0.0) then
                    print *, TT_mtemp(i), TT_trate(i)
                    call my_mpi_abort('TPP-ERROR: ramping rate has to be non-zero for ramping modes', 0)
                end if
                ! Print warning if time between successive TPP steps
                ! is too short too reach target temperature.
                if (i < TT_maxnsteps) then
                    maxtemp = TT_temp(i-1) + TT_trate(i) * (TT_time(i+1) - TT_time(i))
                    if (maxtemp < TT_temp(i)) then
                        call logger("WARNING: target temperature cannot be reached in step", i)
                    end if
                end if
            end if

            ! Check whether a border is defined if a border temp control is chosen.
            if ((TT_mtemp(i) == 5 .or. TT_mtemp(i) == 7) .and. tscaleth <= 0.0d0) then
                print *, TT_mtemp(i), tscaleth
                call my_mpi_abort('TPP-ERROR: border temperature control selected but no border region specified', 0)
            end if

            ! Print summary.
            write(log_buf,"(F11.2,3X,I2,3X,F7.1,3X,F7.3)") &
                TT_time(i), TT_mtemp(i), TT_temp(i), TT_trate(i)
            call logger(log_buf)
        end do

        call logger()

    end subroutine validate_temp_time_prog

end module ttprog_mod
