#
# Go though all source files and parse module dependencies.
# Prints the dependencies in the Makefile format.
#
# For each file:
# - Keep modules declared in it in the array "file_modules".
# - Keep modules used in it in the array "file_uses".
#
# Arrays are used as collections such that the index is the
# value you want to save, and the value at that index is 1.
# This way you can check for existence of a value with e.g.
#     if ("value" in file_modules) {...}
#
# Might require GNU Awk.
#

# Run only once at the beginning.
BEGIN {
    # Keep a list of external modules, i.e. all mods that are not
    # found in this tree. They should be skipped for dependencies.

    # Modules from the Fortran standard.
    external_modules["iso_fortran_env"] = 1;
    external_modules["iso_c_binding"] = 1;
    external_modules["ieee_arithmetic"] = 1;
    external_modules["ieee_exceptions"] = 1;
    external_modules["ieee_features"] = 1;

    # Modules from the MPI standard.
    external_modules["mpi"] = 1;
    external_modules["mpi_f08"] = 1;
}


# Start of a new file.
FNR == 1 {
    object_file = gensub(/src\/(.+)\.f90$/, "$(OBJ)/\\1.o", "g", FILENAME);

    for (i in file_modules)
        delete file_modules[i];
    for (i in file_uses)
        delete file_uses[i];
}

# New module declaration.
# Must match exactly to avoid stuff like "module parameter :: foo".
/^[ \t]*module[ \t]+[a-zA-Z0-9_]+[ \t]*(!.*)?$/ {
    module = gensub(/^[ \t]*module[ \t]+([a-zA-Z0-9_]+)([ \t!].*)?$/, "\\1", "g", $0);
    module = tolower(module);

    file_modules[module] = 1;

    printf "$(OBJ)/%s.mod: %s\n", module, object_file;
}

# New module use.
/^[ \t]*use[ \t]+([a-zA-Z0-9_]+)([ \t!,].*)?$/ {
    module = gensub(/^[ \t]*use[ \t]+([a-zA-Z0-9_]+)([ \t!,].*)?$/, "\\1", "g", $0);
    module = tolower(module)

    if (module in file_modules) {
        # Circular dependency, skip it.
        next;
    }
    if (module in file_uses) {
        # Module used twice, skip it.
        next;
    }
    if (module in external_modules) {
        next;
    }

    file_uses[module] = 1;

    printf "%s: $(OBJ)/%s.mod\n", object_file, module;
}
