#############################################################################
#
#  Makefile parameters for PARCAS compilation with Cray Compiler
#
#  These are used by the Makefile to compile PARCAS.
#
#  When changing systems, you should at least:
#  1) select the right compilation commands
#  2) Select the right options in defs.f90
#
#  - Sometimes it may also be necessary to modify Makefile
# 
#############################################################################


#export OMPI_FC = ifort
F90 = ftn

# Allows for C sources to be compiled alongside the Fortran sources.
#export OMPI_CC = icc
CC = cc

# The $(OBJ) variable is defined in the real Makefile
FFLAGS.release := -O3
FFLAGS.debug   := -eD -h ipa0
FFLAGS_GENERAL := -em -ef -J $(OBJ) -en

# The $(build) variable is defined in the real Makefile
FFLAGS := $(FFLAGS_GENERAL) $(FFLAGS.$(build))

LDFLAGS =
LINK = $(F90)
