#############################################################################
#
#  Makefile parameters for PARCAS compilation with Intel Compiler
#
#  These are used by the Makefile to compile PARCAS.
#
#  When changing systems, you should at least:
#  1) select the right compilation commands
#  2) Select the right options in defs.f90
#
#  - Sometimes it may also be necessary to modify Makefile
# 
#############################################################################


F90 = mpiifort

# Allows for C sources to be compiled alongside the Fortran sources.
CC = mpiicc

# The $(OBJ) variable is defined in the real Makefile
FFLAGS.release := -Ofast -xHost
FFLAGS.debug   := -O0 -check all
FFLAGS_GENERAL := -g -module $(OBJ) -stand f08

# The $(build) variable is defined in the real Makefile
FFLAGS := $(FFLAGS_GENERAL) $(FFLAGS.$(build))

LDFLAGS =
LINK = $(F90)
