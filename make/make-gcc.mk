#############################################################################
#
#  Makefile parameters for PARCAS compilation with GCC
#
#  These are used by the Makefile to compile PARCAS.
#
#  When changing systems, you should at least:
#  1) select the right compilation commands
#  2) Select the right options in defs.f90
#
#  - Sometimes it may also be necessary to modify Makefile
# 
#############################################################################


export OMPI_FC = gfortran
F90 = mpif90

# Allows for C sources to be compiled alongside the Fortran sources.
export OMPI_CC = gcc
CC = mpicc

# The $(OBJ) variable is defined in the real Makefile
FFLAGS.release := -Ofast
FFLAGS.debug   := -O0 -fcheck=all -ffpe-trap=invalid -Warray-temporaries
FFLAGS_GENERAL := -g --std=f2008 -fall-intrinsics -I$(OBJ) -J$(OBJ) \
                  -Wall -Wextra -Wno-compare-reals -Wpedantic

# The $(build) variable is defined in the real Makefile
FFLAGS := $(FFLAGS_GENERAL) $(FFLAGS.$(build))

LDFLAGS =
LINK = $(F90)
